<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h4 class="customer-profile-group-heading"><?php echo _l('client_add_edit_profile'); ?></h4>
<div class="row">
    <?php echo form_open($this->uri->uri_string(),array('class'=>'client-form','autocomplete'=>'off')); ?>
    <div class="additional"></div>
    <div class="col-md-12">
        <div class="horizontal-scrollable-tabs">
            <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
            <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>
            <div class="horizontal-tabs">
                <ul class="nav nav-tabs profile-tabs row customer-profile-tabs nav-tabs-horizontal" role="tablist">
                    <li role="presentation" class="<?php if(!$this->input->get('tab')){echo 'active';}; ?>">
                        <a href="#contact_info" aria-controls="contact_info" role="tab" data-toggle="tab">
                            <?php echo _l( 'customer_profile_details'); ?>
                        </a>
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class="tab-content mtop15">
          
            <div role="tabpanel" class="tab-pane<?php if(!$this->input->get('tab')){echo ' active';}; ?>" id="contact_info">
                <div class="row">
                    
                    <div class="col-md-6">
                        <?php $value=( isset($client) ? $client->company : ''); ?>
                        <?php $attrs = (isset($client) ? array() : array('autofocus'=>true)); ?>
                        <?php echo render_input( 'company', 'client_company',$value,'text',$attrs); ?>
                        <?php $value=( isset($client) ? $client->latitude : ''); ?>
                        <?php echo render_input( 'latitude', 'latitude',$value,'text'); ?>
                        <div id="municipio">
                            <?php $selected =( isset($client) ? $client->municipalities : ''); 
                             echo render_select('municipalities',$municipalities,array('id_municipio','municipio'),'municipalities',$selected);?>
                       </div>
                        
                        
                    </div>
                    <div class="col-md-6">
                        <!-- <?php $value=( isset($client) ? $client->city : ''); ?>
                        <?php echo render_input( 'company', 'code_company',$value,'text',$attrs); ?> -->
                        <?php $value=( isset($client) ? $client->longitude : ''); ?>
                        <?php echo render_input( 'longitude', 'longitude',$value,'text',$attrs); ?>
                        <?php
                        $selected =( isset($client) ? $client->state : '');
                        echo render_select('state', $state,array('id_estado','estado'),'state',$selected);?> 
                        <div id="parroquia">
                            <?php  $selected =( isset($client) ? $client->parishes : '');
                            echo render_select('parishes',$parishes,array('id_parroquia','parroquia'),'parishes',$selected);?>
                        </div>  
                           
                    </div>
                </div>
            </div>


        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<!-- <?php $this->load->view('admin/clients/client_group'); ?> -->

