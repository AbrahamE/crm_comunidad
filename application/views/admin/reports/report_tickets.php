<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="bold"><?php echo _l('filter_by'); ?></p>
                            </div>
                            <div class="col-md-4 leads-filter-column">
                                <?php echo render_select('company', $community, ['company', 'company'], '', '', array('data-width' => '100%', 'data-none-selected-text' => _l('Comunidad')), array(), 'no-mbot'); ?>
                            </div>

                            <div class="col-md-4 leads-filter-column">
                                <?php echo render_select('departments', $departments, ['name', 'name'], '', '', array('data-width' => '100%', 'data-none-selected-text' => _l('Departamento')), array(), 'no-mbot'); ?>
                            </div>

                            <div class="col-md-4 leads-filter-column">
                                <?php echo render_select('category', $categories, ['categoryId', 'categories'], '', '', array('data-width' => '100%', 'data-none-selected-text' => _l('Categoria')), array(), 'no-mbot'); ?>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <div id="date-range" class="mbot15">
                            <div class="row">


                                <div class="clearfix"></div>
                                <hr class="hr-panel-heading" />
                            </div>
                        </div>
                        <?php
                        $table_data = array(
                           _l('Tickets ') . ' #',
                           _l('Categoria'),
                           _l('Departamento'),
                           _l('Servicio'),
                           _l('Cominidad'),
                           _l('Estado'),
                           _l('Prioridad'),
                           _l('Última modificación'),
                           _l('Fecha'),
                        );

                           render_datatable(
                              $table_data,
                              'requests',
                              ['customizable-table'],
                              [
                                 'id' => 'table-expenses',
                                 'data-last-order-identifier' => 'expenses_table',
                                 'data-default-order'         => get_table_last_order('expenses'),
                              ]
                           );
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>

<script>
(function() {

    var report_from = $('input[name="report-from"]');
    var report_to = $('input[name="report-to"]');
    var filter_selector_helper =
        '.expenses-filter-year,.expenses-filter-month-wrapper,.expenses-filter-month,.months-divider,.years-divider';

    let Ingenious_ServerParams = {
        "company": "[name='company']",
        "category": "[name='category']",
        "departments": "[name='departments']",
    };

    let table_requestes = $('#table-expenses');

    if (table_requestes.length) {

        var tableLeadsConsentHeading = table_requestes.find('#th-consent');
        var leadsTableNotSortable = [0];
        var leadsTableNotSearchable = [0, table_requestes.find('#th-assigned').index()];

        if (tableLeadsConsentHeading.length > 0) {
            leadsTableNotSortable.push(tableLeadsConsentHeading.index());
            leadsTableNotSearchable.push(tableLeadsConsentHeading.index());
        }

        _table_api = initDataTable(
            table_requestes,
            window.location.href,
            'undefined',
            'undefined',
            Ingenious_ServerParams,
            [6, 'desc']
        );
        init_requests();

        if (_table_api && tableLeadsConsentHeading.length > 0) {
            _table_api.on('draw', function() {
                var tableData = table_requestes.find('tbody tr');
                $.each(tableData, function() {
                    $(this).find('td:eq(3)').addClass('bg-light-gray');
                });
            });
        }

        $.each(Ingenious_ServerParams, function(i, obj) {
            $('select' + obj).on('change', function(event) {
                console.log(event.target.value);
                table_requestes.DataTable().ajax.reload().columns.adjust().responsive.recalc();
            });
        });
    }





})()
</script>
</body>

</html>