<html>

<head>
<style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 3cm;
                text-align: center;
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;
                text-align: center;

            }
        </style>
</head>

<body>
    <header>
        <h1><?php echo _l('Laboratory_incident_sheet') ?></h1>
        <h1><?php echo _l('Laboratory') ?></h1>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br> <br><br><br><br><br><br><br><br><br><br><br><br> <br><br><br><br><br><br><br><br><br><br><br><br> <br><br><br><br><br><br><br><br><br><br><br><br> <br><br><br><br><br><br><br><br><br><br><br><br>   
    </header>
    <!-- vista para el PDF -->
    <main>
        <table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="1">
            <tr>
                <td height="30" style="color:#fff; text-align:center" bgcolor="#3A4656" colspan="3"><?php echo _l('Laboratory_incident_sheet') ?></td>
                <td><?php echo _l('code') ?> : <strong>PT-FO-PGI-001</strong> </td>
            </tr>
            
            <tr bgcolor="#D4E6F1" style="text-align:center">
                <td colspan="2"><?php echo _l('date_of_admission')?></td>
                <td colspan="2"><?php echo _l('ticket_number') ?></td>
            </tr>
            <tr>
                <td colspan="2"><?php echo $terminals[0]['datecreated'] ?></td>
                <td colspan="2"><?php echo $terminals[0]['ticketid'] ?></td>
            </tr>

            <?php foreach ($resumetickets as $key => $aRows) { ?>

                <tr bgcolor="#D4E6F1" style="text-align:center">
                    <td colspan="2"><?php echo _l('sim_card_serial') ?></td>
                    <td colspan="2"><?php echo _l('affiliate_code') ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo $aRows->serialsimcard ?></td>
                    <td colspan="2"><?php echo $aRows->codaffiliate ?></td>
                </tr>
                <tr bgcolor="#D4E6F1" style="text-align:center">
                    <td><?php echo _l('computer_serial') ?></td>
                    <td><?php echo  htmlentities(_l('equipment_model')) ?></td>
                    <td><?php echo _l('operator') ?></td>
                    <td><?php echo _l('allied_bank') ?></td>
                </tr>
                <tr>
                    <td><?php echo $aRows->serial ?></td>
                    <td><?php echo $aRows->model ?></td>
                    <td><?php echo $aRows->operadora?></td>
                    <td><?php echo htmlentities($aRows->bank) ?></td>
                </tr>
                <tr bgcolor="#D4E6F1" style="text-align:center">
                    <td colspan="2"><?php echo _l('IMEI_MAC_of_the_device') ?></td>
                    <td><?php echo _l('recidivism')?></td>
                    <td><?php echo _l('equipment_under_warranty') ?></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo $aRows->imei ?> / <?php echo $aRows->mac ?></td>
                    <td></td>
                    <td></td>
                </tr>
            <?php } ?>

            <tr bgcolor="#D4E6F1" style="text-align:center">
                <td colspan="4"><?php echo _l('incidence') ?></td>
            </tr>
            <tr>
                <td colspan="4" style="text-align:center"><?php echo $incidence_1[0]['incidents'] ?></td>
            </tr>
            <tr bgcolor="#D4E6F1" style="text-align:center">
                <td colspan="4"><?php echo _l('observations') ?></td>
            </tr>
        </table>
        <br>
        <br><br><br><br><br><br>
        
        <table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="0">
            <tr style="text-align:justify">
                <td colspan="4"><?php echo _l('note_1')?></td>
            </tr>
            <tr style="text-align:justify">
                <td colspan="4"><?php echo _l('note_2') ?></td>
            </tr>
            <!-- <tr style="text-align:justify">
                <td colspan="4"><?php echo _l('note_3') ?></td>
            </tr> -->
            <tr> <br><br><br>
                <td colspan="4"><strong><?php echo _l('for _the_client') ?></strong>: _________________________</td>
            </tr><br><br><br>
            <tr>
                <td colspan="2"><strong><?php echo _l('firm') ?></strong>: ________________________________</td>
                <td colspan="2" style="text-align:center">______________________________________________________________________________</td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td colspan="2"  style="text-align:center"><strong><?php echo _l('signature_and_company_stamp') ?></strong></td>
            </tr>
            
        </table>
    </main>
    <footer>
        <h4><?php echo _l('footer') ?></h4>
    </footer>
</body>

</html>



