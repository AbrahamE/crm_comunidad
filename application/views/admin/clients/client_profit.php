<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper" >
   <div class="content">
         <div class="col-md">
            <div class="panel_s">
               <div class="panel-body">
                  <div>
                     <div class="tab-content">
                     <h4 class="customer-profile-group-heading"><?php echo _l('import_profit_customers'); ?></h4>
                        <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php echo render_select('type_rif', $rif_type, array('type_rif','type_rif'), 'type_rif'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <!-- <?php echo render_input( 'rif_number', 'rif_number','','number','','','','','rif_number'); ?> -->
                                        <?php echo render_input( 'rif_number', 'rif_number','','number'); ?>
                                    </div>
                                </div>
                             
                                <div class="modal-footer">
                                    <!--button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button-->
                                    <button href="#" onclick="setclient(); return false;" style="margin-top: 1em;" class="btn btn-info"><?php echo _l('import'); ?></button>
                                </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<?php $this->load->view('admin/clients/client_profit_js'); ?>
<!--?php require 'assets/js/client_profit.js'; ?-->
</div>


