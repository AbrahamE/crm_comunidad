INSERT INTO `tbltickets_status` (`ticketstatusid`, `name`, `isdefault`, `statuscolor`, `statusorder`, `day`) VALUES
(1, 'Creado', 1, '#ff2d42', 1, 0),
(2, 'Informado y cerrado', 1, '#84c529', 2, 0),
(3, 'Atendiendo', 1, '#0000ff', 3, 0),
(4, 'Esperando para atender', 1, '#c0c0c0', 4, 0),
(5, 'Coordinado con otros organismos', 1, '#03a9f4', 5, 0),
(6, 'Cerrado', 1, '#84c529', 6, 0);
