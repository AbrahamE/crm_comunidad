<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="ticket-terminal-modal_edit" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('tickets/terminal_add'),array('id'=>'ticket-terminal-form')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="add-title"><?php echo _l('new_terminal'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style='display: none'>
                        <div id="additional"></div>
                        <?php echo render_input('userid','codigó de afiliado', $userid ,'text',array('required'=>true)); ?>
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $value=( isset($terminal) ? $terminal['codaffiliate'] : ''); ?>
                        <?php echo render_input('codaffiliate_user','Codigó de afiliado',$value,'text',array('required'=>true)); ?>
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('serial','Serial','','text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('mac','MAC','','text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('imei','Imei','','text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"> Banco</div>
                        <?php echo render_select_terminal('banksid', 'banksid', $banks,array('id','banks'),'Banco',(count($banks) == 1) ? $banks[0]['id'] : '',array('required'=>'true')); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"> Modelo</div>
                        <?php echo render_select_terminal('modelid', 'modelid', $model,array('modelid','model'),'Banco',(count($model) == 1) ? $model[0]['modelid'] : '',array('required'=>'true')); ?>	
                    </div>
                    <div class="col-md-12">
                        <label for="serialsimcard"><?php echo _l('Serial simcard'); ?></label>
                        <select name="simcard" required="true" id="simcard" class="ajax-search simcard" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div><br>
                        <label for="operadoraid"><?php echo _l('Operadora'); ?></label>
                        <?php echo render_select_terminal('operadoraid','operadoraid',$operadora,array('operadoraid','operadora'),'ticket_settings_departments',(count($operadora) == 1) ? $operadora[0]['operadoraid'] : '',array('required'=>'true')); ?>
                    </div>

                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    // cuando cierra el modal
    window.addEventListener('load',function(){
        appValidateForm($('#ticket-terminal-form'),{name:'required'},manage_ticket_terminals);
        $('#ticket-terminal-modal_edit').on('hidden.bs.modal', function(event) {
            $('#additional').html('');
            $('#ticket-terminal-modal_edit input[id="codaffiliate_terminal"]').val("");
            $('#ticket-terminal-modal_edit input[name="serial"]').val('');
            $('#ticket-terminal-modal_edit input[name="model"]').val('');
            $('#ticket-terminal-modal_edit input[name="imei"]').val('');
            $('#ticket-terminal-modal_edit input[name="mac"]').val('');
            $('#ticket-terminal-modal_edit input[name="operadora"]').val('');

            $('#ticket-terminal-modal_edit select[name="simcardid"]').val('');
            $('#ticket-terminal-modal_edit button[data-id="simcardid"] .filter-option .filter-option-inner .filter-option-inner-inner').html('Nada seleccionado')

            $('#ticket-terminal-modal_edit select[id="banksid"]').val('');
            $('#ticket-terminal-modal_edit bunew_terminaltton[data-id="banksid"] .filter-option .filter-option-inner .filter-option-inner-inner').html('Nada seleccionado');
            
            $('#ticket-terminal-modal_edit select[id="modelid"]').val('');
            $('#ticket-terminal-modal_edit button[data-id="modelid"] .filter-option .filter-option-inner .filter-option-inner-inner').html('Nada seleccionado');

        });

        

    });

    function manage_ticket_terminals(form) {

        var Data = $('#ticket-terminal-form').serializeArray();
        var data = $(form).serialize();
        var url = form.action;
        var ticketArea = $('body').hasClass('ticket');
        if(ticketArea) {
            data += '&ticket_area=true';
        }
        
        $.post(url, data).done(function(e) {
            if(ticketArea) {
                var response = JSON.parse(e);
                if(response.status == 'success'){
                    alert_float('success', response.message);
                    //SerializeArrayTerminalExists(response.contact_data);
                }else{
                    alert_float('danger', response.message);
                }
                $('#ticket-terminal-modal_edit').modal('hide');
            } else {
                window.location.reload();
            }
        });
        return false;
    }

    function new_terminal(){
            $('#ticket-terminal-modal_edit').modal('show');
            $('.edit-title').addClass('hide');
            init_ajax_search_simcard('simcard','#simcard.ajax-search',{
                tickets_contacts: true,
                contact_userid: function(){
                        // when ticket is directly linked to project only search project client id contacts
                        var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
                        if(uid){
                            return uid;
                        } else {
                            return '';
                        }
                    }
                },
            );
            $(".simcard .dropdown-menu").click(function() {
                var t = $('#simcard').val();
                $.post(admin_url + "tickets/ticket_change_data_simcard", { contact_id: t }).done(
                    function (e) {
                        res = JSON.parse(e).contact_data;
                        if (res) {
                            console.log(res.operadora);
                            $('select[id="operadoraid"]').selectpicker('val', res.operadora);
                        }
                    }
                )
            })
        
    }

   

</script>

