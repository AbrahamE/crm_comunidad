<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal fade " id="simcard-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('terminals/simcard_add'),array('id'=>'add-new-terminal-form')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('ticket_terminal_edit'); ?></span>
                    <span class="add-title"><?php echo _l('new_simcard'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 hidden id" >
                        <div id="additional"></div>
                        <?php $id = ( isset($simcard) ? $simcard['id'] : null); ?>
                        <?php echo render_input('id','id', $id ,'text',); ?>    
                    </div> 

                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $serialsimcard = ( isset($simcard) ? $simcard['serialsimcard'] : ''); ?>
                        <?php echo render_input('serialsimcard', _l('sim_card_serial'),$serialsimcard,'text',array('required'=>true)); ?>
                    </div>

                    <div class="col-md-12">
                        <div id="additional"></div>
                        <label for="operadoraid"><?php echo _l('operator'); ?></label>
                        <?php echo render_select_terminal('operadoraid','operadoraid',$operadora,array('operadoraid','operadora'),'ticket_settings_departments',(isset($simcard) ? $simcard['operadora'] : ''),array('required'=>'true')); ?>
                    </div>
            
                    <div class="col-md-12  hidden status" >
                        <div id="additional"></div>
                        <label for="operadoraid"><?php echo _l('Estatus'); ?></label>
                        <select name="status" required="true" id="status" class="selectpicker" data-width="100%" data-live-search="true" data-abs-cache="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" >
                            <option value="1">Activa</option>
                            <option value="2">Inactiva</option>
                        </select>
                    </div> 
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?> </button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    // cuando cierra el modal
    (function(){
        appValidateForm('#add-new-terminal-form',{name:'required'}, manage_simcard,'enviado');
        $('#simcard-modal').on('hidden.bs.modal', function(event) {
            $('#additional').html('');
            $('#simcard-modal input[id="id"]').val("");
            $('#simcard-modal input[id="serialsimcard"]').val("");
            $('#simcard-modal select[name="operadoraid"]').selectpicker('val', '')
            //$('#simcard-modal select[name="status"]').selectpicker('val', '')
        });

    })();

    function manage_simcard(form) {   
       
        var Data = $('#add-new-terminal-form').serializeArray();
        var data = $(form).serialize();
        var url = form.action;
        var ticketArea = $('body').hasClass('ticket');
        if(ticketArea) {
            data += '&ticket_area=true';
        }
        
        $.post(url, data).done(function(e) {

            var response = JSON.parse(e);
            if(response.status == 'success'){
                if($.fn.DataTable.isDataTable('.table-simcard')){
                    $('.table-simcard').DataTable().ajax.reload();
                }
                alert_float('success', response.message);
            }else{
                alert_float('danger', response.message);
            }
            $('#simcard-modal').modal('hide');
        });
        return false;
    }

    function news_simcard(data){
        $('#simcard-modal').modal('show');
        $('.edit-title').addClass('hide');
        var id =$('#simcard-modal input[id="id"]').val();
        if(id != ''){
            $('.status').removeClass("hidden");
            let status = <?= $simcard["status"] ?? 0 ?>;
            $('#simcard-modal select[name="status"]').selectpicker('val', status)
        } else{
            $('.status').addClass("hidden");
        }

    }

</script>