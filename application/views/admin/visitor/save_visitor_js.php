<script>

function save_visitor() {

        var formData = new FormData();

        var name_and_surname = $("#name_and_surname").val();
        var identification_card = $("#identification_card").val();
        var admission_date = $("#admission_date").val();
        var business = $("#business").val();
        var rif = $("#rif").val();
        var email = $("#email").val();
        var contact_number = $("#contact_number").val();
        var place = $("#place").val();
        var check_in = $("#check_in").val();
        var sale_agent = $("#sale_agent").val();
        var attention_time = $("#attention_time").val();
        var management_time = $("#management_time").val();
        var bank = $("#bank").val();
        var fountain = $("#fountain").val();
        var description = $("#description").val();
        


        //console.log(tasa);
        if (typeof(csrfData) !== 'undefined') {
            formData.append(csrfData['token_name'], csrfData['hash']);
        }
        
        formData.append("name_and_surname", name_and_surname);
        formData.append("identification_card", identification_card);
        formData.append("admission_date", admission_date);
        formData.append("business", business);
        formData.append("rif", rif);
        formData.append("email", email);
        formData.append("email", email);
        formData.append("contact_number", contact_number);
        formData.append("place", place);
        formData.append("check_in", check_in);
        formData.append("sale_agent", sale_agent);
        formData.append("attention_time", attention_time);
        formData.append("management_time", management_time);
        formData.append("bank", bank);
        formData.append("fountain", fountain);
        formData.append("description", description);



        //console.log(formData);
        //console.log(admin_url + 'invoices/save_tasa');
        $.ajax({
            method: 'post',
            data: formData,
            contentType: false,
            processData: false,
            url: admin_url + 'visitor/create_new_visitor',
            dataType: 'json'
            
        }).done(function(response){
            //console.log(response);    
                if (response.status) {
                    //console.log(response.status);
                    window.location.href = admin_url + 'visitor';
                    alert_float('success', response.message);                    
                }
        }).fail(function(error){
            alert_float('danger', JSON.parse(error.responseText));
            //console.log("error");
        });
   
}

</script>