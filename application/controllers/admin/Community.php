<?php

use Ddeboer\Imap\Message;

use function GuzzleHttp\json_decode;

defined('BASEPATH') or exit('No direct script access allowed');

class Community extends AdminController
{
    /* List all clients */
    public function __construct()
    {
        parent::__construct();

        require_once module_dir_path('warehouse') . '/third_party/excel/PHPExcel.php';
    }
  
    public function index()
    {
        if (!has_permission('customers', '', 'view')) {
            if (!have_assigned_customers() && !has_permission('customers', '', 'create')) {
                access_denied('customers');
            }
        }

        $this->load->model('contracts_model');
        $data['contract_types'] = $this->contracts_model->get_contract_types();
        $data['groups']         = $this->clients_model->get_groups();
        $data['title']          = _l('community');

        $this->load->model('proposals_model');
        $data['proposal_statuses'] = $this->proposals_model->get_statuses();

        $this->load->model('invoices_model');
        $data['invoice_statuses'] = $this->invoices_model->get_statuses();

        $this->load->model('estimates_model');
        $data['estimate_statuses'] = $this->estimates_model->get_statuses();

        $this->load->model('projects_model');
        $data['project_statuses'] = $this->projects_model->get_project_statuses();

        $data['customer_admins'] = $this->clients_model->get_customers_admin_unique_ids();

        $whereContactsLoggedIn = '';
        if (!has_permission('customers', '', 'view')) {
            $whereContactsLoggedIn = ' AND userid IN (SELECT customer_id FROM ' . db_prefix() . 'customer_admins WHERE staff_id=' . get_staff_user_id() . ')';
        }

        $data['contacts_logged_in_today'] = $this->clients_model->get_contacts('', 'last_login LIKE "' . date('Y-m-d') . '%"' . $whereContactsLoggedIn);

        $data['countries'] = $this->clients_model->get_clients_distinct_countries();

        $this->load->view('admin/clients/manage', $data);
    }

    public function table()
    {
        if (!has_permission('customers', '', 'view')) {
            if (!have_assigned_customers() && !has_permission('customers', '', 'create')) {
                ajax_access_denied();
            }
        }

        $this->app->get_table_data('community');
    }

    public function table_terminals($clientid)
    {
        
        $this->app->get_table_data('terminal', [
            'clientid' => $clientid,
        ]);
    }

    public function table_accounts($clientid)
    {
        
        $this->app->get_table_data('accounts', [
            'clientid' => $clientid,
        ]);
    }
    

    public function all_contacts()
    {
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('all_contacts');
        }

        if (is_gdpr() && get_option('gdpr_enable_consent_for_contacts') == '1') {
            $this->load->model('gdpr_model');
            $data['consent_purposes'] = $this->gdpr_model->get_consent_purposes();
        }

        $data['title'] = _l('customer_contacts');
        $this->load->view('admin/clients/all_contacts', $data);
    }
    /*Importar cliente profif*/
    public function import_profit_customers()
    {
        if ($this->input->post()) {
            $data = $this->input->post();
        }

        $data['title'] = _l('import_profit_customers');
        $data['rif_type'] = $this->clients_model->get_type_rif();
        //var_dump($data['rif_type']);
        $this->load->view('admin/clients/client_profit', $data);
    }

     /*Peticion a servicio de guardar cliente profit*/
     public function setclient()
     {
  
         $fields = array("rif" =>  $this->input->post('number_rif'),true);
         $url= "localhost:8081/venepos/api/setClient";
         $curl = curl_init();
         
            curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($fields),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $data = json_decode($response);
            
             try {
                 if($data->estatus!='null' || $data->estatus == 'error' ){
                    echo json_encode(['success'=>'error', 'message'=>$data->message]);
                 }else{
                    echo json_encode(['success'=>$data->estatus, 'message'=>$data->message]);
                 }
                
             } catch (Exception $e) {
                 echo json_encode(['success'=> false, 'message'=>'Ha ocurrido un error en el proceso de su peticion']);
             }
                

    }

    /* Edit client or add new client*/
    public function communit($id = '')
    {  
        if (!has_permission('customers', '', 'view')) {
            if ($id != '' && !is_customer_admin($id)) {
                access_denied('customers');
            }
        }

        
        
        if ($this->input->post() && !$this->input->is_ajax_request()) {

            $data = $this->input->post();
            //$campoRif=$data['typerif_rif'].$data['rif_number'];    
            $rif_array = $this->clients_model->get_rif_number($data['rif_number']);

        
            if ($id == '') {
                //se valida que el rif ingresado no exista en la tabla clientes antes de de hacer el INSERT
                if (count($rif_array)>0) {
                    unset_alert('danger');
                    set_alert('danger', _l('Rif_Number_entered_already_exist'));
                }else{
                    if (!has_permission('customers', '', 'create')) {
                        access_denied('customers');
                    }

                    //$data = $this->input->post();
                    //var_dump($data); die;
                    $save_and_add_contact = false;
                    if (isset($data['save_and_add_contact'])) {
                        unset($data['save_and_add_contact']);
                        $save_and_add_contact = true;
                    }
                    
                    $id = $this->clients_model->add($data);
                
                    if (!has_permission('customers', '', 'view')) {
                        $assign['customer_admins']   = [];
                        $assign['customer_admins'][] = get_staff_user_id();
                        $this->clients_model->assign_admins($assign, $id);
                    }
                    if ($id) {
                        set_alert('success', _l('added_successfully', _l('client')));
                        if ($save_and_add_contact == false) {
                            redirect(admin_url('community/communit/' . $id));
                        } else {
                            redirect(admin_url('community/communit/' . $id . '?group=contacts&new_contact=true'));
                        }
                    }
                }    
            } else {

                //se valida que el rif no exista para otro cliente antes de hacer update
                        //var_dump($result); die;
                $repetido=false;
                if (count($rif_array)>0)  { 
                    foreach ($rif_array as $row) {                        
                        if ($row['userid'] != $id) {
                            $repetido=true;
                            $otroCliente=$row['company'];        
                            break;                            
                        } 
                    } 
                }
                //$result = $this->clients_model->get_field_by_id('rif_number',$id);
                if ($repetido==true) {                         
                        set_alert('danger', _l('Rif_Number_already_associated').$otroCliente);
                }else{
                    if (!has_permission('customers', '', 'edit')) {
                        if (!is_customer_admin($id)) {
                            access_denied('customers');
                        }
                    }
                    $success = $this->clients_model->update($this->input->post(), $id);
                    if ($success == true) {
                        set_alert('success', _l('updated_successfully', _l('client')));
                    }
                    redirect(admin_url('community/communit/' . $id));
                }                
            }
            
        } 

        $group         = !$this->input->get('group') ? 'profile' : $this->input->get('group');
        //echo '<pre>'; print_r($group); die;
        $data['group'] = $group;

        if ($group != 'contacts' && $contact_id = $this->input->get('contactid')) {
            redirect(admin_url('community/communit/' . $id . '?group=contacts&contactid=' . $contact_id));
        }

        // Customer groups
        $data['groups'] = $this->clients_model->get_groups();

        if ($id == '') {
            $title = _l('add_new', _l('client_lowercase'));
        } else {
            $client                = $this->clients_model->get($id);
            $data['customer_tabs'] = get_customer_profile_tabs();
            
            if (!$client) {
                show_404();
            }

            $data['contacts'] = $this->clients_model->get_contacts($id);
            $data['tab']      = isset($data['customer_tabs'][$group]) ? $data['customer_tabs'][$group] : null;
            //var_dump($data['tab']); die;
            if (!$data['tab']) {
                show_404();
            }

            // Fetch data based on groups
            if ($group == 'profile') {
                $data['customer_groups'] = $this->clients_model->get_customer_groups($id);
                $data['customer_admins'] = $this->clients_model->get_admins($id);
            } elseif ($group == 'attachments') {
                $data['attachments'] = get_all_customer_attachments($id);
            } elseif ($group == 'vault') {
                $data['vault_entries'] = hooks()->apply_filters('check_vault_entries_visibility', $this->clients_model->get_vault_entries($id));

                if ($data['vault_entries'] === -1) {
                    $data['vault_entries'] = [];
                }
            } elseif ($group == 'estimates') {
                $this->load->model('estimates_model');
                $data['estimate_statuses'] = $this->estimates_model->get_statuses();
            } elseif ($group == 'invoices') {
                $this->load->model('invoices_model');
                $data['invoice_statuses'] = $this->invoices_model->get_statuses();
            } elseif ($group == 'credit_notes') {
                $this->load->model('credit_notes_model');
                $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
                $data['credits_available']     = $this->credit_notes_model->total_remaining_credits_by_customer($id);
            } elseif ($group == 'payments') {
                $this->load->model('payment_modes_model');
                $data['payment_modes'] = $this->payment_modes_model->get();
            } elseif ($group == 'notes') {
                $data['user_notes'] = $this->misc_model->get_notes($id, 'customer');
            } elseif ($group == 'projects') {
                $this->load->model('projects_model');
                $data['project_statuses'] = $this->projects_model->get_project_statuses();
            } elseif ($group == 'statement') {
                if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
                    set_alert('danger', _l('access_denied'));
                    redirect(admin_url('clients/client/' . $id));
                }

                $data = array_merge($data, prepare_mail_preview_data('customer_statement', $id));
            } elseif ($group == 'map') {
                if (get_option('google_api_key') != '' && !empty($client->latitude) && !empty($client->longitude)) {
                    $this->app_scripts->add('map-js', base_url($this->app_scripts->core_file('assets/js', 'map.js')) . '?v=' . $this->app_css->core_version());

                    $this->app_scripts->add('google-maps-api-js', [
                        'path'       => 'https://maps.googleapis.com/maps/api/js?key=' . get_option('google_api_key') . '&callback=initMap',
                        'attributes' => [
                            'async',
                            'defer',
                            'latitude'       => "$client->latitude",
                            'longitude'      => "$client->longitude",
                            'mapMarkerTitle' => "$client->company",
                        ],
                        ]);
                }
            }

            $data['staff'] = $this->staff_model->get('', ['active' => 1]);

            $data['client'] = $client;
            $title          = $client->company;

            // Get all active staff members (used to add reminder)
            $data['members'] = $data['staff'];

            if (!empty($data['client']->company)) {
                // Check if is realy empty client company so we can set this field to empty
                // The query where fetch the client auto populate firstname and lastname if company is empty
                if (is_empty_customer_company($data['client']->userid)) {
                    $data['client']->company = '';
                }
            }
        }

        $this->load->model('currencies_model');
        $data['currencies'] = $this->currencies_model->get();

        if ($id != '') {
            $customer_currency = $data['client']->default_currency;

            foreach ($data['currencies'] as $currency) {
                if ($customer_currency != 0) {
                    if ($currency['id'] == $customer_currency) {
                        $customer_currency = $currency;

                        break;
                    }
                } else {
                    if ($currency['isdefault'] == 1) {
                        $customer_currency = $currency;

                        break;
                    }
                }
            }

            if (is_array($customer_currency)) {
                $customer_currency = (object) $customer_currency;
            }

            $data['customer_currency'] = $customer_currency;

            $slug_zip_folder = (
                $client->company != ''
                ? $client->company
                : get_contact_full_name(get_primary_contact_user_id($client->userid))
            );

            $data['zip_in_folder'] = slug_it($slug_zip_folder);
        }

        $data['bodyclass'] = 'customer-profile dynamic-create-groups';
        $data['title']     = $title;
         /*
         * consulta a la Funcion para los campos de tipo solicitud
         */
        $this->load->model('departments_model');
        $this->load->model('tickets_model');
        $data['incidents']    = $this->departments_model->incidents();
        $data['type_rif']  = $this->clients_model->get_type_rif();
        $data['state']  = $this->clients_model->get_state();
        $data['municipalities']  = $this->clients_model->get_municipalities();
        $data['parishes']  = $this->clients_model->get_parishes();
        $this->load->view('admin/clients/client', $data);
    }

    public function export($contact_id)
    {
        if (is_admin()) {
            $this->load->library('gdpr/gdpr_contact');
            $this->gdpr_contact->export($contact_id);
        }
    }

    // Used to give a tip to the user if the company exists when new company is created
    public function check_duplicate_customer_name()
    {
        if (has_permission('customers', '', 'create')) {
            $companyName = trim($this->input->post('company'));
            $response    = [
                'exists'  => (bool) total_rows(db_prefix() . 'clients', ['company' => $companyName]) > 0,
                'message' => _l('company_exists_info', '<b>' . $companyName . '</b>'),
            ];
            echo json_encode($response);
        }
    }

    public function save_longitude_and_latitude($client_id)
    {
        if (!has_permission('customers', '', 'edit')) {
            if (!is_customer_admin($client_id)) {
                ajax_access_denied();
            }
        }

        $this->db->where('userid', $client_id);
        $this->db->update(db_prefix() . 'clients', [
            'longitude' => $this->input->post('longitude'),
            'latitude'  => $this->input->post('latitude'),
        ]);
        if ($this->db->affected_rows() > 0) {
            echo 'success';
        } else {
            echo 'false';
        }
    }

    public function form_contact($customer_id, $contact_id = '')
    {
        if (!has_permission('customers', '', 'view')) {
            if (!is_customer_admin($customer_id)) {
                echo _l('access_denied');
                die;
            }
        }
        $data['customer_id'] = $customer_id;
        $data['contactid']   = $contact_id;
        if ($this->input->post()) {
            $data             = $this->input->post();
            $data['password'] = $this->input->post('password', false);

            unset($data['contactid']);
            if ($contact_id == '') {
                if (!has_permission('customers', '', 'create')) {
                    if (!is_customer_admin($customer_id)) {
                        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad error');
                        echo json_encode([
                            'success' => false,
                            'message' => _l('access_denied'),
                        ]);
                        die;
                    }
                }
                $id      = $this->clients_model->add_contact($data, $customer_id);
                $message = '';
                $success = false;
                if ($id) {
                    handle_contact_profile_image_upload($id);
                    $success = true;
                    $message = _l('added_successfully', _l('contact'));
                }
                echo json_encode([
                    'success'             => $success,
                    'message'             => $message,
                    'has_primary_contact' => (total_rows(db_prefix() . 'contacts', ['userid' => $customer_id, 'is_primary' => 1]) > 0 ? true : false),
                    'is_individual'       => is_empty_customer_company($customer_id) && total_rows(db_prefix() . 'contacts', ['userid' => $customer_id]) == 1,
                ]);
                die;
            }
            if (!has_permission('customers', '', 'edit')) {
                if (!is_customer_admin($customer_id)) {
                    header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad error');
                    echo json_encode([
                            'success' => false,
                            'message' => _l('access_denied'),
                        ]);
                    die;
                }
            }
            $original_contact = $this->clients_model->get_contact($contact_id);
            $success          = $this->clients_model->update_contact($data, $contact_id);
            $message          = '';
            $proposal_warning = false;
            $original_email   = '';
            $updated          = false;
            if (is_array($success)) {
                if (isset($success['set_password_email_sent'])) {
                    $message = _l('set_password_email_sent_to_client');
                } elseif (isset($success['set_password_email_sent_and_profile_updated'])) {
                    $updated = true;
                    $message = _l('set_password_email_sent_to_client_and_profile_updated');
                }
            } else {
                if ($success == true) {
                    $updated = true;
                    $message = _l('updated_successfully', _l('contact'));
                }
            }
            if (handle_contact_profile_image_upload($contact_id) && !$updated) {
                $message = _l('updated_successfully', _l('contact'));
                $success = true;
            }
            if ($updated == true) {
                $contact = $this->clients_model->get_contact($contact_id);
                if (total_rows(db_prefix() . 'proposals', [
                        'rel_type' => 'customer',
                        'rel_id' => $contact->userid,
                        'email' => $original_contact->email,
                    ]) > 0 && ($original_contact->email != $contact->email)) {
                    $proposal_warning = true;
                    $original_email   = $original_contact->email;
                }
            }
            echo json_encode([
                    'success'             => $success,
                    'proposal_warning'    => $proposal_warning,
                    'message'             => $message,
                    'original_email'      => $original_email,
                    'has_primary_contact' => (total_rows(db_prefix() . 'contacts', ['userid' => $customer_id, 'is_primary' => 1]) > 0 ? true : false),
                ]);
            die;
        }
        if ($contact_id == '') {
            $title = _l('add_new', _l('contact_lowercase'));
        } else {
            $data['contact'] = $this->clients_model->get_contact($contact_id);

            if (!$data['contact']) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad error');
                echo json_encode([
                    'success' => false,
                    'message' => 'Contact Not Found',
                ]);
                die;
            }
            $title = $data['contact']->firstname . ' ' . $data['contact']->lastname;
        }

        $data['customer_permissions'] = get_contact_permissions();
        $data['title']                = $title;
        $this->load->view('admin/clients/modals/contact', $data);
    }

    public function edit_terminals($client_id = '', $id_terminal= '') {
      
        $this->load->model('tickets_model');
        if ($id_terminal != ''){ 
            $data['terminal'] = $this->tickets_model->get_terminal($id_terminal);
        }
        $data['banks']     = $this->tickets_model->get_banks();
        $data['model']     = $this->tickets_model->get_model();
        $data['simcard']   = $this->tickets_model->get_simcard();
        $data['operadora'] = $this->tickets_model->get_operadora();
        $data['userid']    = $client_id;
        $this->load->view('admin/clients/modals/modal_terminal', $data);
       
    }

    public function terminal_add($client_id = '', $id_terminal = ''){
       
        $this->load->model('tickets_model');
        $this->load->model('terminal_model');
        $this->load->model('simcard_model');

        if (!is_admin() && get_option('staff_members_save_tickets_predefined_replies') == '0') {
            access_denied('Ticket Terminal');
        }

        $id_simCard='';
        if ($id_terminal != ''){ 
            //$data['terminal'] = $this->tickets_model->get_terminal($id_terminal); 
            //se crea funcion igual en  terminals_model, se devuelve un valor adiconal que muestra el serial del SIMCARD concatenado con la operadora
            $data['terminal'] = $this->terminal_model->get_terminal($id_terminal);
            //var_dump($data['terminal']); die; 
            $id_simCard=$data['terminal']['simcard'];
        }
        $data['banks']     = $this->tickets_model->get_banks();
        //$data['model']   = $this->tickets_model->get_model();
        $data['model']     = $this->terminal_model->get_terminal_model();
        //$data['simcard'] = $this->tickets_model->get_simcard();
        //se listan las simcards que no esta asociadas a una terminal con estatus 1(activa)
        $data['simcard']   = $this->simcard_model->get_avalables_simcards($id_simCard);
        $data['operadora'] = $this->tickets_model->get_operadora();
        $data['status']    = $this->tickets_model->get_terminal_status();
        
        $data['userid']    = $client_id;
        $data['customer_id'] = $client_id;
        $data['contactid']   = $id_terminal;
        

        if ($this->input->post()) {
            $post_data = $this->input->post();
            if ($post_data['requiere_simcard'] =='si') $sim_exists=$this->simcard_model->validate_simcard_available($post_data['simcard'], $id_terminal);
            //var_dump($sim_exists); die;
            if(isset($sim_exists)){
                echo json_encode(['status' => 'danger', 'message' => _l('la SIM Card ya se encuentra asociada a otra terminal')]); die;
            }
                
            unset($data['contactid']);
            if ($id_terminal == '') {
                    //$post_data = $this->input->post();
                    if (!$this->input->post('id')) {
                        $requestFromTicketArea = isset($post_data['ticket_area']);
                        if (isset($post_data['ticket_area'])) {
                            unset($post_data['ticket_area']);
                        }
                        $data = $this->clients_model->add_terminal($post_data, $id_terminal);
                    
                        if ($data) {
                            echo json_encode(['status' => 'success', 'message' => _l('new_terminal_was_added'), 'contact_data' => $data]); die;
                        } else {
                            echo json_encode(['status' => 'danger', 'message' => _l('error_adding_a_new_terminal')]);die;
                        }
                    } else {
                        echo json_encode(['status' => 'danger', 'message' => _l('error_interno')]); die;
                    }
                    die;
            }
            
            //$original_terminal = $this->clients_model->get_terminal($id_terminal);            
            //$post_data = $this->input->post();
            // si se seleecciono una sim cardse busca la operadora de la sim card
            if (isset ($post_data['simcard']) && $post_data['simcard']!= ''){
                $array_sim= $this->simcard_model->get_operator($post_data['simcard']);
                $post_data['operadoraid']=$array_sim[0]['operadora'];
            } else{
                $post_data['operadoraid']=null;
            }   
            
            $update    = $this->clients_model->update_terminal($post_data, $id_terminal);
            
            if ($update) {
                echo json_encode(['status' => 'success', 'message' => _l('terminal_has_been_updated'), 'contact_data' => $data]); die;
            } else {
                echo json_encode(['status' => 'danger', 'message' => _l('error_adding_a_new_terminal')]);die;
            }
        }
    
    $this->load->view('admin/clients/modals/modal_terminal', $data);

    }



    public function accounts_add($client_id = '', $account_id = ''){
        
        $this->load->model('tickets_model');

        if ($account_id != ''){ 
            $data['accounts'] = $this->clients_model->get_accounts($account_id);
            //var_dump($data['accounts']); die;
        }

        $data['banks']     = $this->tickets_model->get_banks();
        $data['status']     = $this->clients_model->get_status_accounts();
        $data['userid']    = $client_id;
        $data['customer_id'] = $client_id;
        $data['account_id']   = $account_id;
       
        if ($this->input->post()) {
         
            if ($account_id == '') {
                $post_data = $this->input->post();
                if ($post_data) {
                    $data = $this->clients_model->add_accounts($post_data);
                    if ($data) {
                        if($data == 'existe'){
                            echo json_encode(['status' => 'danger', 'message' => _l('El codigó de afiliado ya existe')]); die;
                        }elseif($data == 'inserto'){
                            echo json_encode(['status' => 'success', 'message' => _l('Se añadio una nueva cuenta'), 'contact_data' => $data]); die;
                        }
                    } else {
                        echo json_encode(['status' => 'danger', 'message' => _l('Error al añadir una nueva cuenta')]);die;
                    }
    
                } else {
                    echo json_encode(['status' => 'danger', 'message' => _l('error_interno')]); die;
                }
                die;
            }

            $post_data = $this->input->post();
            $update = $this->clients_model->update_accounts($post_data, $account_id);
            
            if ($update) {
                echo json_encode(['status' => 'success', 'message' => _l('Se ha actualizado la cuenta'), 'contact_data' => $data]); die;
            } else {
                echo json_encode(['status' => 'danger', 'message' => _l('Error al momento de actualizar la cuenta')]);die;
            }
    }

    $this->load->view('admin/clients/modals/modal_accounts', $data);

    }

    public function associate_terminal($client_id = '', $id_terminal = ''){
        
        $this->load->model('tickets_model');
        $this->load->model('terminal_model');
        
        if (!is_admin() && get_option('staff_members_save_tickets_predefined_replies') == '0') {
            access_denied('Ticket Terminal');
        }

        if ($id_terminal != ''){ 
            $data['terminal'] = $this->tickets_model->get_terminal($id_terminal);
        }
        
        $data['userid']    = $client_id;
        $data['customer_id'] = $client_id;
        $data['contactid']   = $id_terminal;        
        //se buscan las terminales disponibles
        $data['lisTerminals'] = $this->terminal_model->get_availables_terminals();
        $data['acounts']= $this->clients_model->get_client_acounts($client_id);       

        if (count($data['lisTerminals'])==0){
            //echo json_encode(['status' => 'success', 'message' => _l('No_terminals_avaliables'), 'contact_data' => $data]); die; 
            $data['message']   = 'No hay terminales disponibles para associar'; 
        }
            
        if ($this->input->post()) {                
            $post_data = $this->input->post();
                        
            $dataAcount=$this->clients_model->get_acount($post_data['codeafiliado']);
            //var_dump($dataAcount); die;
            $post_data['bankid']=$dataAcount[0]['bankid'];
            $post_data['codeafiliado']=$dataAcount[0]['codaffiliate'];
            //var_dump($post_data); die;
            $update = $this->clients_model->associate_terminal($post_data);
            //$original_terminal = $this->clients_model->get_terminal($id_terminal);            

            //var_dump($update);
            if ($update) {
                echo json_encode(['status' => 'success', 'message' => _l('terminal_has_been_updated'), 'contact_data' => $data]); die;
            } else {
                echo json_encode(['status' => 'danger', 'message' => _l('error_adding_a_new_terminal')]);die;
            }
        }            
       // var_dump($data);
        $this->load->view('admin/clients/modals/modal_terminal_association', $data);
    }    


    public function confirm_registration($client_id)
    {
        if (!is_admin()) {
            access_denied('Customer Confirm Registration, ID: ' . $client_id);
        }
        $this->clients_model->confirm_registration($client_id);
        set_alert('success', _l('customer_registration_successfully_confirmed'));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function update_file_share_visibility()
    {
        if ($this->input->post()) {
            $file_id           = $this->input->post('file_id');
            $share_contacts_id = [];

            if ($this->input->post('share_contacts_id')) {
                $share_contacts_id = $this->input->post('share_contacts_id');
            }

            $this->db->where('file_id', $file_id);
            $this->db->delete(db_prefix() . 'shared_customer_files');

            foreach ($share_contacts_id as $share_contact_id) {
                $this->db->insert(db_prefix() . 'shared_customer_files', [
                    'file_id'    => $file_id,
                    'contact_id' => $share_contact_id,
                ]);
            }
        }
    }

    public function delete_contact_profile_image($contact_id)
    {
        $this->clients_model->delete_contact_profile_image($contact_id);
    }

    public function mark_as_active($id)
    {
        $this->db->where('userid', $id);
        $this->db->update(db_prefix() . 'clients', [
            'active' => 1,
        ]);
        redirect(admin_url('clients/client/' . $id));
    }

    public function consents($id)
    {
        if (!has_permission('customers', '', 'view')) {
            if (!is_customer_admin(get_user_id_by_contact_id($id))) {
                echo _l('access_denied');
                die;
            }
        }

        $this->load->model('gdpr_model');
        $data['purposes']   = $this->gdpr_model->get_consent_purposes($id, 'contact');
        $data['consents']   = $this->gdpr_model->get_consents(['contact_id' => $id]);
        $data['contact_id'] = $id;
        $this->load->view('admin/gdpr/contact_consent', $data);
    }

    public function update_all_proposal_emails_linked_to_customer($contact_id)
    {
        $success = false;
        $email   = '';
        if ($this->input->post('update')) {
            $this->load->model('proposals_model');

            $this->db->select('email,userid');
            $this->db->where('id', $contact_id);
            $contact = $this->db->get(db_prefix() . 'contacts')->row();

            $proposals = $this->proposals_model->get('', [
                'rel_type' => 'customer',
                'rel_id'   => $contact->userid,
                'email'    => $this->input->post('original_email'),
            ]);
            $affected_rows = 0;

            foreach ($proposals as $proposal) {
                $this->db->where('id', $proposal['id']);
                $this->db->update(db_prefix() . 'proposals', [
                    'email' => $contact->email,
                ]);
                if ($this->db->affected_rows() > 0) {
                    $affected_rows++;
                }
            }

            if ($affected_rows > 0) {
                $success = true;
            }
        }
        echo json_encode([
            'success' => $success,
            'message' => _l('proposals_emails_updated', [
                _l('contact_lowercase'),
                $contact->email,
            ]),
        ]);
    }

    public function assign_admins($id)
    {
        if (!has_permission('customers', '', 'create') && !has_permission('customers', '', 'edit')) {
            access_denied('customers');
        }
        $success = $this->clients_model->assign_admins($this->input->post(), $id);
        if ($success == true) {
            set_alert('success', _l('updated_successfully', _l('client')));
        }

        redirect(admin_url('clients/client/' . $id . '?tab=customer_admins'));
    }

    public function delete_customer_admin($customer_id, $staff_id)
    {
        if (!has_permission('customers', '', 'create') && !has_permission('customers', '', 'edit')) {
            access_denied('customers');
        }

        $this->db->where('customer_id', $customer_id);
        $this->db->where('staff_id', $staff_id);
        $this->db->delete(db_prefix() . 'customer_admins');
        redirect(admin_url('clients/client/' . $customer_id) . '?tab=customer_admins');
    }

    public function delete_contact($customer_id, $id)
    {
        if (!has_permission('customers', '', 'delete')) {
            if (!is_customer_admin($customer_id)) {
                access_denied('customers');
            }
        }
        $contact      = $this->clients_model->get_contact($id);
        $hasProposals = false;
        if ($contact && is_gdpr()) {
            if (total_rows(db_prefix() . 'proposals', ['email' => $contact->email]) > 0) {
                $hasProposals = true;
            }
        }

        $this->clients_model->delete_contact($id);
        if ($hasProposals) {
            $this->session->set_flashdata('gdpr_delete_warning', true);
        }
        redirect(admin_url('clients/client/' . $customer_id . '?group=contacts'));
    }

    public function contacts($client_id)
    {
        $this->app->get_table_data('contacts', [
            'client_id' => $client_id,
        ]);
    }

    public function upload_attachment($id)
    {
        handle_client_attachments_upload($id);
    }

    public function add_external_attachment()
    {
        if ($this->input->post()) {
            $this->misc_model->add_attachment_to_database($this->input->post('clientid'), 'customer', $this->input->post('files'), $this->input->post('external'));
        }
    }

    public function delete_attachment($customer_id, $id)
    {
        if (has_permission('customers', '', 'delete') || is_customer_admin($customer_id)) {
            $this->clients_model->delete_attachment($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    /* Delete client */
    public function delete($id)
    {
        if (!has_permission('customers', '', 'delete')) {
            access_denied('customers');
        }
        if (!$id) {
            redirect(admin_url('clients'));
        }
        $response = $this->clients_model->delete($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('customer_delete_transactions_warning', _l('invoices') . ', ' . _l('estimates') . ', ' . _l('credit_notes')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('client')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('client_lowercase')));
        }
        redirect(admin_url('clients'));
    }

    /* Staff can login as client */
    public function login_as_client($id)
    {
        if (is_admin()) {
            login_as_client($id);
        }
        hooks()->do_action('after_contact_login');
        redirect(site_url());
    }

    public function get_customer_billing_and_shipping_details($id)
    {
        echo json_encode($this->clients_model->get_customer_billing_and_shipping_details($id));
    }

    /* Change client status / active / inactive */
    public function change_contact_status($id, $status)
    {
        if (has_permission('customers', '', 'edit') || is_customer_admin(get_user_id_by_contact_id($id))) {
            if ($this->input->is_ajax_request()) {
                $this->clients_model->change_contact_status($id, $status);
            }
        }
    }

    /* Change client status / active / inactive */
    public function change_client_status($id, $status)
    {
        if ($this->input->is_ajax_request()) {
            $this->clients_model->change_client_status($id, $status);
        }
    }

    /* Zip function for credit notes */
    public function zip_credit_notes($id)
    {
        $has_permission_view = has_permission('credit_notes', '', 'view');

        if (!$has_permission_view && !has_permission('credit_notes', '', 'view_own')) {
            access_denied('Zip Customer Credit Notes');
        }

        if ($this->input->post()) {
            $this->load->library('app_bulk_pdf_export', [
                'export_type'       => 'credit_notes',
                'status'            => $this->input->post('credit_note_zip_status'),
                'date_from'         => $this->input->post('zip-from'),
                'date_to'           => $this->input->post('zip-to'),
                'redirect_on_error' => admin_url('clients/client/' . $id . '?group=credit_notes'),
            ]);

            $this->app_bulk_pdf_export->set_client_id($id);
            $this->app_bulk_pdf_export->in_folder($this->input->post('file_name'));
            $this->app_bulk_pdf_export->export();
        }
    }

    public function zip_invoices($id)
    {
        $has_permission_view = has_permission('invoices', '', 'view');
        if (!$has_permission_view && !has_permission('invoices', '', 'view_own')
            && get_option('allow_staff_view_invoices_assigned') == '0') {
            access_denied('Zip Customer Invoices');
        }

        if ($this->input->post()) {
            $this->load->library('app_bulk_pdf_export', [
                'export_type'       => 'invoices',
                'status'            => $this->input->post('invoice_zip_status'),
                'date_from'         => $this->input->post('zip-from'),
                'date_to'           => $this->input->post('zip-to'),
                'redirect_on_error' => admin_url('clients/client/' . $id . '?group=invoices'),
            ]);

            $this->app_bulk_pdf_export->set_client_id($id);
            $this->app_bulk_pdf_export->in_folder($this->input->post('file_name'));
            $this->app_bulk_pdf_export->export();
        }
    }

    /* Since version 1.0.2 zip client estimates */
    public function zip_estimates($id)
    {
        $has_permission_view = has_permission('estimates', '', 'view');
        if (!$has_permission_view && !has_permission('estimates', '', 'view_own')
            && get_option('allow_staff_view_estimates_assigned') == '0') {
            access_denied('Zip Customer Estimates');
        }

        if ($this->input->post()) {
            $this->load->library('app_bulk_pdf_export', [
                'export_type'       => 'estimates',
                'status'            => $this->input->post('estimate_zip_status'),
                'date_from'         => $this->input->post('zip-from'),
                'date_to'           => $this->input->post('zip-to'),
                'redirect_on_error' => admin_url('clients/client/' . $id . '?group=estimates'),
            ]);

            $this->app_bulk_pdf_export->set_client_id($id);
            $this->app_bulk_pdf_export->in_folder($this->input->post('file_name'));
            $this->app_bulk_pdf_export->export();
        }
    }

    public function zip_payments($id)
    {
        $has_permission_view = has_permission('payments', '', 'view');

        if (!$has_permission_view && !has_permission('invoices', '', 'view_own')
            && get_option('allow_staff_view_invoices_assigned') == '0') {
            access_denied('Zip Customer Payments');
        }

        $this->load->library('app_bulk_pdf_export', [
                'export_type'       => 'payments',
                'payment_mode'      => $this->input->post('paymentmode'),
                'date_from'         => $this->input->post('zip-from'),
                'date_to'           => $this->input->post('zip-to'),
                'redirect_on_error' => admin_url('clients/client/' . $id . '?group=payments'),
            ]);

        $this->app_bulk_pdf_export->set_client_id($id);
        $this->app_bulk_pdf_export->set_client_id_column(db_prefix() . 'clients.userid');
        $this->app_bulk_pdf_export->in_folder($this->input->post('file_name'));
        $this->app_bulk_pdf_export->export();
    }

    public function import()
    {
        if (!has_permission('customers', '', 'create')) {
            access_denied('customers');
        }

       
		$total_row_false = 0;
		$total_rows = 0;
		$dataerror = 0;
		$total_row_success = 0;

		if ($this->input->post()) {

			if (isset($_FILES['file_csv']['name']) && $_FILES['file_csv']['name'] != '') {
				//do_action('before_import_leads');

				// Get the temp file path
				$tmpFilePath = $_FILES['file_csv']['tmp_name'];
				// Make sure we have a filepath
				if (!empty($tmpFilePath) && $tmpFilePath != '') {
					$tmpDir = TEMP_FOLDER . '/' . time() . uniqid() . '/';

					if (!file_exists(TEMP_FOLDER)) {
						mkdir(TEMP_FOLDER, 0755);
					}

					if (!file_exists($tmpDir)) {
						mkdir($tmpDir, 0755);
					}

					// Setup our new file path
					$newFilePath = $tmpDir . $_FILES['file_csv']['name'];

					if (move_uploaded_file($tmpFilePath, $newFilePath)) {
						$import_result = true;
						$rows = [];

						$objReader = new PHPExcel_Reader_Excel2007();
						$objReader->setReadDataOnly(true);
						$objPHPExcel = $objReader->load($newFilePath);
						$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
						$sheet = $objPHPExcel->getActiveSheet();

						//innit  file exel error start

						$dataError = new PHPExcel();
						$dataError->setActiveSheetIndex(0);
						//create header file

						// add style to the header
						$styleArray = array(
							'font' => array(
								'bold' => true,

							),

							'borders' => array(
								'top' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN,
								),
							),
							'fill' => array(

								'rotation' => 90,
								'startcolor' => array(
									'argb' => 'FFA0A0A0',
								),
								'endcolor' => array(
									'argb' => 'FFFFFFFF',
								),
							),
						);

						// set the names of header cells
						$dataError->setActiveSheetIndex(0)
							->setCellValue("A1", "(*)" . _l('Nombre'))
							->setCellValue("B1", _l('Apellido'))
							->setCellValue("C1", _l('Email'))
							->setCellValue("D1", _l('Company'));
						/*set style for header*/
						$dataError->getActiveSheet()->getStyle('A1:D1')->applyFromArray($styleArray);

						// auto fit column to content

						foreach (range('A', 'D') as $columnID) {
							$dataError->getActiveSheet()->getColumnDimension($columnID)
								->setAutoSize(true);

						}

						$dataError->getActiveSheet()->getStyle('A1:D1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$dataError->getActiveSheet()->getStyle('A1:D1')->getFill()->getStartColor()->setARGB('29bb04');
						// Add some data
						$dataError->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
						$dataError->getActiveSheet()->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

						/*set header middle alignment*/
						$dataError->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

						$dataError->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						/*set row1 height*/
						$dataError->getActiveSheet()->getRowDimension('1')->setRowHeight(40);

						//init file error end

						// start row write 2
						$numRow = 2;
						$total_rows = 0;

						$total_rows_actualy = 0;
						//get data for compare

						foreach ($rowIterator as $row) {
							$rowIndex = $row->getRowIndex();
							if ($rowIndex > 1) {

								$nombre = $sheet->getCell('A' . $rowIndex)->getValue();
								$apellido = $sheet->getCell('B' . $rowIndex)->getValue();

								$email = $sheet->getCell('C' . $rowIndex)->getValue();
                                $company = $sheet->getCell('D' . $rowIndex)->getValue();
                                
                               
                                $id = $this->clients_model->add([
                                    'firstname' => $nombre, 
                                    'lastname'=> $apellido, 
                                    'email' => $email,
                                    'company' =>  $company,
                                    'is_primary' => 1,
                                    'password' => $this->input->post('default_pass_all')
                                ], true);
								
                            }
                        }
                    }
                }
            }
        
        }
                    
    
        $data['groups']    = $this->clients_model->get_groups();
        $data['title']     = _l('import');
        $data['bodyclass'] = 'dynamic-create-groups';
        $this->load->view('admin/clients/import', $data);
    }

    public function groups()
    {
        if (!is_admin()) {
            access_denied('Customer Groups');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('customers_groups');
        }
        $data['title'] = _l('customer_groups');
        $this->load->view('admin/clients/groups_manage', $data);
    }

    public function group()
    {
        if (!is_admin() && get_option('staff_members_create_inline_customer_groups') == '0') {
            access_denied('Customer Groups');
        }

        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $id      = $this->clients_model->add_group($data);
                $message = $id ? _l('added_successfully', _l('customer_group')) : '';
                echo json_encode([
                    'success' => $id ? true : false,
                    'message' => $message,
                    'id'      => $id,
                    'name'    => $data['name'],
                ]);
            } else {
                $success = $this->clients_model->edit_group($data);
                $message = '';
                if ($success == true) {
                    $message = _l('updated_successfully', _l('customer_group'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                ]);
            }
        }
    }

    public function delete_group($id)
    {
        if (!is_admin()) {
            access_denied('Delete Customer Group');
        }
        if (!$id) {
            redirect(admin_url('clients/groups'));
        }
        $response = $this->clients_model->delete_group($id);
        if ($response == true) {
            set_alert('success', _l('deleted', _l('customer_group')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('customer_group_lowercase')));
        }
        redirect(admin_url('clients/groups'));
    }

    public function bulk_action()
    {
        hooks()->do_action('before_do_bulk_action_for_customers');
        $total_deleted = 0;
        if ($this->input->post()) {
            $ids    = $this->input->post('ids');
            $groups = $this->input->post('groups');

            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if ($this->input->post('mass_delete')) {
                        if ($this->clients_model->delete($id)) {
                            $total_deleted++;
                        }
                    } else {
                        if (!is_array($groups)) {
                            $groups = false;
                        }
                        $this->client_groups_model->sync_customer_groups($id, $groups);
                    }
                }
            }
        }

        if ($this->input->post('mass_delete')) {
            set_alert('success', _l('total_clients_deleted', $total_deleted));
        }
    }

    public function vault_entry_create($customer_id)
    {
        $data = $this->input->post();

        if (isset($data['fakeusernameremembered'])) {
            unset($data['fakeusernameremembered']);
        }

        if (isset($data['fakepasswordremembered'])) {
            unset($data['fakepasswordremembered']);
        }

        unset($data['id']);
        $data['creator']      = get_staff_user_id();
        $data['creator_name'] = get_staff_full_name($data['creator']);
        $data['description']  = nl2br($data['description']);
        $data['password']     = $this->encryption->encrypt($this->input->post('password', false));

        if (empty($data['port'])) {
            unset($data['port']);
        }

        $this->clients_model->vault_entry_create($data, $customer_id);
        set_alert('success', _l('added_successfully', _l('vault_entry')));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function vault_entry_update($entry_id)
    {
        $entry = $this->clients_model->get_vault_entry($entry_id);

        if ($entry->creator == get_staff_user_id() || is_admin()) {
            $data = $this->input->post();

            if (isset($data['fakeusernameremembered'])) {
                unset($data['fakeusernameremembered']);
            }
            if (isset($data['fakepasswordremembered'])) {
                unset($data['fakepasswordremembered']);
            }

            $data['last_updated_from'] = get_staff_full_name(get_staff_user_id());
            $data['description']       = nl2br($data['description']);

            if (!empty($data['password'])) {
                $data['password'] = $this->encryption->encrypt($this->input->post('password', false));
            } else {
                unset($data['password']);
            }

            if (empty($data['port'])) {
                unset($data['port']);
            }

            $this->clients_model->vault_entry_update($entry_id, $data);
            set_alert('success', _l('updated_successfully', _l('vault_entry')));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function vault_entry_delete($id)
    {
        $entry = $this->clients_model->get_vault_entry($id);
        if ($entry->creator == get_staff_user_id() || is_admin()) {
            $this->clients_model->vault_entry_delete($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function vault_encrypt_password()
    {
        $id            = $this->input->post('id');
        $user_password = $this->input->post('user_password', false);
        $user          = $this->staff_model->get(get_staff_user_id());

        if (!app_hasher()->CheckPassword($user_password, $user->password)) {
            header('HTTP/1.1 401 Unauthorized');
            echo json_encode(['error_msg' => _l('vault_password_user_not_correct')]);
            die;
        }

        $vault    = $this->clients_model->get_vault_entry($id);
        $password = $this->encryption->decrypt($vault->password);

        $password = html_escape($password);

        // Failed to decrypt
        if (!$password) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad error');
            echo json_encode(['error_msg' => _l('failed_to_decrypt_password')]);
            die;
        }

        echo json_encode(['password' => $password]);
    }

    public function get_vault_entry($id)
    {
        $entry = $this->clients_model->get_vault_entry($id);
        unset($entry->password);
        $entry->description = clear_textarea_breaks($entry->description);
        echo json_encode($entry);
    }

    public function statement_pdf()
    {
        $customer_id = $this->input->get('customer_id');

        if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
            set_alert('danger', _l('access_denied'));
            redirect(admin_url('clients/client/' . $customer_id));
        }

        $from = $this->input->get('from');
        $to   = $this->input->get('to');

        $data['statement'] = $this->clients_model->get_statement($customer_id, to_sql_date($from), to_sql_date($to));

        try {
            $pdf = statement_pdf($data['statement']);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== false) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type = 'D';
        if ($this->input->get('print')) {
            $type = 'I';
        }

        $pdf->Output(slug_it(_l('customer_statement') . '-' . $data['statement']['client']->company) . '.pdf', $type);
    }

    public function send_statement()
    {
        $customer_id = $this->input->get('customer_id');

        if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
            set_alert('danger', _l('access_denied'));
            redirect(admin_url('clients/client/' . $customer_id));
        }

        $from = $this->input->get('from');
        $to   = $this->input->get('to');

        $send_to = $this->input->post('send_to');
        $cc      = $this->input->post('cc');

        $success = $this->clients_model->send_statement_to_email($customer_id, $send_to, $from, $to, $cc);
        // In case client use another language
        load_admin_language();
        if ($success) {
            set_alert('success', _l('statement_sent_to_client_success'));
        } else {
            set_alert('danger', _l('statement_sent_to_client_fail'));
        }

        redirect(admin_url('clients/client/' . $customer_id . '?group=statement'));
    }

    public function statement()
    {
        if (!has_permission('invoices', '', 'view') && !has_permission('payments', '', 'view')) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad error');
            echo _l('access_denied');
            die;
        }

        $customer_id = $this->input->get('customer_id');
        $from        = $this->input->get('from');
        $to          = $this->input->get('to');

        $data['statement'] = $this->clients_model->get_statement($customer_id, to_sql_date($from), to_sql_date($to));

        $data['from'] = $from;
        $data['to']   = $to;

        $viewData['html'] = $this->load->view('admin/clients/groups/_statement', $data, true);

        echo json_encode($viewData);
    }

   
}
