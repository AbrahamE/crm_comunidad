<?php defined('BASEPATH') or exit('No direct script access allowed');

render_datatable([
    _l('the_number_sign'),
    _l('message_name'),
    _l('date_message'),
    _l('related_to'),
    _l('rel_type'),
    // _l('tag'),
], (isset($class) ? $class : 'message'), [], [
    'data-last-order-identifier' => 'message',
    'data-default-order'         => get_table_last_order('message'),
]);