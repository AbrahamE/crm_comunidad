<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="panel_s">
			<div class="panel-body">
              <div class="col-md-8">
                  <a href="#" onclick="new_message(<?php if($this->input->get('project_id')){ echo "'".admin_url('message/message?rel_id='.$this->input->get('project_id').'&rel_type=project')."'";} ?>); return false;" class="btn btn-info pull-left new"><?php echo _l('new_message'); ?></a>
              </div>
				<div class="clearfix"></div>
				<hr class="hr-panel-heading" />
				<div class="col-md-12">
					<?php $this->load->view('admin/message/table_html'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php init_tail(); ?>
<script>
	$(function(){
		initDataTable('.table-message', admin_url+'message/table', undefined, undefined,'undefined',<?php echo hooks()->apply_filters('payments_table_default_order', json_encode(array(0,'desc'))); ?>);
	});
</script>
</body>
</html>
