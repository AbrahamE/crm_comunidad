<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="accounts_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('clients/accounts_add/'.$customer_id.'/'.$account_id),array('id'=>'accounts-modal-form')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="add-title"><?php echo _l('new_accounts'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style='display: none'>
                        <div id="additional"></div>
                        <?php echo render_input('userid','codigó de afiliado', $userid ,'text',array('required'=>true)); ?>
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $codaffiliate=( isset($accounts) ? $accounts[0]['codaffiliate'] : ''); ?>
                        <?php echo render_input('codaffiliate_user','Código de Afiliado',$codaffiliate,'number',array('required'=>true,'min'=>1)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $naccount=( isset($accounts) ? $accounts[0]['naccount'] : ''); ?>
                        <?php echo render_input('number_account','Número de Cuenta',$naccount,'number',array('required'=>true,'min'=>1)); ?>
                    </div>
                     <div class="col-md-12">
                        <div id="additional"></div>
                           
                            <label for="banks"><?php echo _l('Banco'); ?></label>
                            <?php echo render_select_terminal('banks','banks',$banks,array('id','banks'),'Banco',(isset($accounts) ? $accounts[0]['bankid'] : ''),array('required'=>'true')); ?> 
                        </div>
                        <div class="col-md-12">
                        <div id="additional"></div>
                            <label for="status"><?php echo _l('Estatus'); ?></label>
                            <?php echo render_select_terminal('status','status',$status,array('id','name'),'Estatus',(isset($accounts) ? $accounts[0]['status'] : ''),array('required'=>'true')); ?>  
                            
                        </div>
           
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit"  class="btn btn-info"><?php echo _l('submit'); ?></button>
                
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>       
   
    (function(){
        appValidateForm($('#accounts-modal-form'),
        {
            name:'required',
            codaffiliate_user: {
                required:true,
                maxlength:8
            },
            number_account: {
                required:true,
                maxlength:20
            }
        },manage_accounts);
            $('#accounts_modal').on('hidden.bs.modal', function(event) {
                $('#additional').html('');
                $('#accounts_modal input[id="codaffiliate_user"]').val("");
                $('#accounts_modal select[neme="banks"]').selectpicker('val', '')
                $('#accounts_modal select[name="status"]').selectpicker('val', '') 
            } );
    })();
 
    function manage_accounts(form) {
        //debugger;
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                if($.fn.DataTable.isDataTable('.table-accounts-single-client')){
                    $('.table-accounts-single-client').DataTable().ajax.reload();
                }
                alert_float('success', response.message);
                $('#accounts_modal').modal('hide');
            } else {
                alert_float('danger', response.message);
            }
        });
        return false;
    }

    function select_banks(){

        var banks = <?= $accounts[0]['bankid'] ?? ''; ?>
        //$('').modal('show');
        //$('.edit-title').addClass('hide');
        $('#banks').selectpicker('val', banks); 
    }

    function select_status(){
        var status = <?= $accounts[0]['status'] ?? '1' ;  ?>
        // $('').modal('show');
        //$('.edit-title').addClass('hide');
        $('#status').selectpicker('val', status); 
    }

    

   

</script>

