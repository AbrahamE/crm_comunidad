<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionDelete = has_permission('payments', '', 'delete');

$aColumns = [
    'name_and_surname',
    'identification_card',
    'business',
    'rif',
    'email',
    'fountain',
    ];


$sIndexColumn = 'id';
$sTable       = db_prefix() . 'visitor';

$result = data_tables_init($aColumns, $sIndexColumn, $sTable);

$output  = $result['output'];
$rResult = $result['rResult'];



foreach ($rResult as $aRow) {
    $row = [];

    $row[] = $aRow['name_and_surname'];
    $row[] = $aRow['identification_card'];
    $row[] = $aRow['business'];
    $row[] = $aRow['rif'];
    $row[] = $aRow['email'];
    $row[] = $aRow['fountain'];

    $output['aaData'][] = $row;
}