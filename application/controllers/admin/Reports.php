<?php

use function GuzzleHttp\json_decode;

defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends AdminController
{
    /**
     * Codeigniter Instance
     * Expenses detailed report filters use $ci
     * @var object
     */
    private $ci;

    public function __construct()
    {
        parent::__construct();
        if (!has_permission('reports', '', 'view')) {
            access_denied('reports');
        }
        $this->ci = &get_instance();
        $this->load->model('reports_model');
    }

    /* No access on this url */
    public function index()
    {
        redirect(admin_url());
    }

    /* See knowledge base article reports*/
    public function knowledge_base_articles()
    {
        $this->load->model('knowledge_base_model');
        $data['groups'] = $this->knowledge_base_model->get_kbg();
        $data['title']  = _l('kb_reports');
        $this->load->view('admin/reports/knowledge_base_articles', $data);
    }

    /*
        public function tax_summary(){
           $this->load->model('taxes_model');
           $this->load->model('payments_model');
           $this->load->model('invoices_model');
           $data['taxes'] = $this->db->query("SELECT DISTINCT taxname,taxrate FROM ".db_prefix()."item_tax WHERE rel_type='invoice'")->result_array();
            $this->load->view('admin/reports/tax_summary',$data);
        }*/
    /* Repoert leads conversions */
    public function leads()
    {
        $type = 'leads';
        if ($this->input->get('type')) {
            $type                       = $type . '_' . $this->input->get('type');
            $data['leads_staff_report'] = json_encode($this->reports_model->leads_staff_report());
        }
        $this->load->model('leads_model');
        $data['statuses']               = $this->leads_model->get_status();
        $data['leads_this_week_report'] = json_encode($this->reports_model->leads_this_week_report());
        $data['leads_sources_report']   = json_encode($this->reports_model->leads_sources_report());
        $this->load->view('admin/reports/' . $type, $data);
    }

    /* Sales reportts */
    public function sales()
    {
        $data['mysqlVersion'] = $this->db->query('SELECT VERSION() as version')->row();
        $data['sqlMode']      = $this->db->query('SELECT @@sql_mode as mode')->row();

        if (is_using_multiple_currencies() || is_using_multiple_currencies(db_prefix() . 'creditnotes') || is_using_multiple_currencies(db_prefix() . 'estimates') || is_using_multiple_currencies(db_prefix() . 'proposals')) {
            $this->load->model('currencies_model');
            $data['currencies'] = $this->currencies_model->get();
        }
        $this->load->model('invoices_model');
        $this->load->model('estimates_model');
        $this->load->model('proposals_model');
        $this->load->model('credit_notes_model');

        $data['credit_notes_statuses'] = $this->credit_notes_model->get_statuses();
        $data['invoice_statuses']      = $this->invoices_model->get_statuses();
        $data['estimate_statuses']     = $this->estimates_model->get_statuses();
        $data['payments_years']        = $this->reports_model->get_distinct_payments_years();
        $data['estimates_sale_agents'] = $this->estimates_model->get_sale_agents();

        $data['invoices_sale_agents'] = $this->invoices_model->get_sale_agents();

        $data['proposals_sale_agents'] = $this->proposals_model->get_sale_agents();
        $data['proposals_statuses']    = $this->proposals_model->get_statuses();

        $data['invoice_taxes']     = $this->distinct_taxes('invoice');
        $data['estimate_taxes']    = $this->distinct_taxes('estimate');
        $data['proposal_taxes']    = $this->distinct_taxes('proposal');
        $data['credit_note_taxes'] = $this->distinct_taxes('credit_note');


        $data['title'] = _l('sales_reports');
        $this->load->view('admin/reports/sales', $data);
    }

    /* Customer report */
    public function customers_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $select = [
                get_sql_select_client_company(),
                '(SELECT COUNT(clientid) FROM ' . db_prefix() . 'invoices WHERE ' . db_prefix() . 'invoices.clientid = ' . db_prefix() . 'clients.userid AND status != 5)',
                '(SELECT SUM(subtotal) - SUM(discount_total) FROM ' . db_prefix() . 'invoices WHERE ' . db_prefix() . 'invoices.clientid = ' . db_prefix() . 'clients.userid AND status != 5)',
                '(SELECT SUM(total) FROM ' . db_prefix() . 'invoices WHERE ' . db_prefix() . 'invoices.clientid = ' . db_prefix() . 'clients.userid AND status != 5)',
            ];

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' ' . $custom_date_select . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
            }
            $by_currency = $this->input->post('report_currency');
            $currency    = $this->currencies_model->get_base_currency();
            if ($by_currency) {
                $i = 0;
                foreach ($select as $_select) {
                    if ($i !== 0) {
                        $_temp = substr($_select, 0, -1);
                        $_temp .= ' AND currency =' . $this->db->escape_str($by_currency) . ')';
                        $select[$i] = $_temp;
                    }
                    $i++;
                }
                $currency = $this->currencies_model->get($by_currency);
            }
            $aColumns     = $select;
            $sIndexColumn = 'userid';
            $sTable       = db_prefix() . 'clients';
            $where        = [];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, [], $where, [
                'userid',
            ]);
            $output  = $result['output'];
            $rResult = $result['rResult'];
            $x       = 0;
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($i == 0) {
                        $_data = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                    } elseif ($aColumns[$i] == $select[2] || $aColumns[$i] == $select[3]) {
                        if ($_data == null) {
                            $_data = 0;
                        }
                        $_data = app_format_money($_data, $currency->name);
                    }
                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
                $x++;
            }
            echo json_encode($output);
            die();
        }
    }

    public function payments_received()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('payment_modes_model');
            $payment_gateways = $this->payment_modes_model->get_payment_gateways(true);
            $select           = [
                db_prefix() . 'invoicepaymentrecords.id',
                db_prefix() . 'invoicepaymentrecords.date',
                'invoiceid',
                get_sql_select_client_company(),
                'paymentmode',
                'transactionid',
                'note',
                'amount',
                //db_prefix() . 'invoices.status as status',
                //'status_payment',
                db_prefix() . 'banks.banks as bank',
                db_prefix() . 'invoicepaymentrecords.account',
            ];
            $where = [
                //'AND invoices.status != 5',
            ];

            $custom_date_select = $this->get_where_report_period(db_prefix() . 'invoicepaymentrecords.date');
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'invoicepaymentrecords';
            $join         = [
                'JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id = ' . db_prefix() . 'invoicepaymentrecords.invoiceid',
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'invoices.clientid',
                'LEFT JOIN ' . db_prefix() . 'payment_modes ON ' . db_prefix() . 'payment_modes.id = ' . db_prefix() . 'invoicepaymentrecords.paymentmode',
                //'LEFT JOIN ' . db_prefix() . 'status_payment ON ' . db_prefix() . 'status_payment.id_payment = ' . db_prefix() . 'invoicepaymentrecords.status_payment',
                'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.codbank = ' . db_prefix() . 'invoicepaymentrecords.bank',
                
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'number',
                'clientid',
                db_prefix() . 'payment_modes.name',
                db_prefix() . 'payment_modes.id as paymentmodeid',
                'paymentmethod',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];
            //var_dump($rResult); die;
            $footer_data['total_amount'] = 0;
            foreach ($rResult as $aRow) {
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                        $_data = $aRow[strafter($aColumns[$i], 'as ')];
                    } else {
                        $_data = $aRow[$aColumns[$i]];
                    }
                    if ($aColumns[$i] == 'paymentmode') {
                        $_data = $aRow['name'];
                        if (is_null($aRow['paymentmodeid'])) {
                            foreach ($payment_gateways as $gateway) {
                                if ($aRow['paymentmode'] == $gateway['id']) {
                                    $_data = $gateway['name'];
                                }
                            }
                        }
                        if (!empty($aRow['paymentmethod'])) {
                            $_data .= ' - ' . $aRow['paymentmethod'];
                        }
                    } elseif ($aColumns[$i] == db_prefix() . 'invoicepaymentrecords.id') {
                        $_data = '<a href="' . admin_url('payments/payment/' . $_data) . '" target="_blank">' . $_data . '</a>';
                    } elseif ($aColumns[$i] == db_prefix() . 'invoicepaymentrecords.date') {
                        $_data = _d($_data);
                    } elseif ($aColumns[$i] == 'invoiceid') {
                        $_data = '<a href="' . admin_url('invoices/list_invoices/' . $aRow[$aColumns[$i]]) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                    }elseif ($i == 3) {
                        if (empty($aRow['deleted_customer_name'])) {
                            $_data = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                        } else {
                            $row[] = $aRow['deleted_customer_name'];
                        }
                    } elseif ($aColumns[$i] == 'amount') {
                        $footer_data['total_amount'] += $_data;
                        $_data = app_format_money($_data, $currency->name);
                        
                    }

                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
            }

            $footer_data['total_amount'] = app_format_money($footer_data['total_amount'], $currency->name);
            $output['sums']              = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function proposals_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('proposals_model');

            $proposalsTaxes    = $this->distinct_taxes('proposal');
            $totalTaxesColumns = count($proposalsTaxes);

            $select = [
                'id',
                'subject',
                'proposal_to',
                'date',
                'open_till',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                'status',
            ];

            $proposalsTaxesSelect = array_reverse($proposalsTaxes);

            foreach ($proposalsTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="proposal" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'proposals.id) as total_tax_single_' . $key);
            }

            $where              = [];
            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('proposal_status')) {
                $statuses  = $this->input->post('proposal_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            if ($this->input->post('proposals_sale_agents')) {
                $agents  = $this->input->post('proposals_sale_agents');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND assigned IN (' . implode(', ', $_agents) . ')');
                }
            }


            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'proposals';
            $join         = [];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'rel_id',
                'rel_type',
                'discount_percent',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'          => 0,
                'subtotal'       => 0,
                'total_tax'      => 0,
                'discount_total' => 0,
                'adjustment'     => 0,
            ];

            foreach ($proposalsTaxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('proposals/list_proposals/' . $aRow['id']) . '" target="_blank">' . format_proposal_number($aRow['id']) . '</a>';

                $row[] = '<a href="' . admin_url('proposals/list_proposals/' . $aRow['id']) . '" target="_blank">' . $aRow['subject'] . '</a>';

                if ($aRow['rel_type'] == 'lead') {
                    $row[] = '<a href="#" onclick="init_lead(' . $aRow['rel_id'] . ');return false;" target="_blank" data-toggle="tooltip" data-title="' . _l('lead') . '">' . $aRow['proposal_to'] . '</a>' . '<span class="hide">' . _l('lead') . '</span>';
                } elseif ($aRow['rel_type'] == 'customer') {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['rel_id']) . '" target="_blank" data-toggle="tooltip" data-title="' . _l('client') . '">' . $aRow['proposal_to'] . '</a>' . '<span class="hide">' . _l('client') . '</span>';
                } else {
                    $row[] = '';
                }

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['open_till']);

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($proposalsTaxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[]              = format_proposal_status($aRow['status']);
                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function estimates_report()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $this->load->model('estimates_model');

            $estimateTaxes     = $this->distinct_taxes('estimate');
            $totalTaxesColumns = count($estimateTaxes);

            $select = [
                'number',
                get_sql_select_client_company(),
                'invoiceid',
                'YEAR(date) as year',
                'date',
                'expirydate',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                'reference_no',
                'status',
            ];

            $estimatesTaxesSelect = array_reverse($estimateTaxes);

            foreach ($estimatesTaxesSelect as $key => $tax) {
                array_splice($select, 9, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="estimate" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'estimates.id) as total_tax_single_' . $key);
            }

            $where              = [];
            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('estimate_status')) {
                $statuses  = $this->input->post('estimate_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            if ($this->input->post('sale_agent_estimates')) {
                $agents  = $this->input->post('sale_agent_estimates');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'estimates';
            $join         = [
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'estimates.clientid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                db_prefix() . 'estimates.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'          => 0,
                'subtotal'       => 0,
                'total_tax'      => 0,
                'discount_total' => 0,
                'adjustment'     => 0,
            ];

            foreach ($estimateTaxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('estimates/list_estimates/' . $aRow['id']) . '" target="_blank">' . format_estimate_number($aRow['id']) . '</a>';

                if (empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                if ($aRow['invoiceid'] === null) {
                    $row[] = '';
                } else {
                    $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['invoiceid']) . '" target="_blank">' . format_invoice_number($aRow['invoiceid']) . '</a>';
                }

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['expirydate']);

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($estimateTaxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];


                $row[] = $aRow['reference_no'];

                $row[] = format_estimate_status($aRow['status']);

                $output['aaData'][] = $row;
            }
            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }
            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    private function get_where_report_period($field = 'date')
    {
        $months_report      = $this->input->post('report_months');
        $custom_date_select = '';
        if ($months_report != '') {
            if (is_numeric($months_report)) {
                // Last month
                if ($months_report == '1') {
                    $beginMonth = date('Y-m-01', strtotime('first day of last month'));
                    $endMonth   = date('Y-m-t', strtotime('last day of last month'));
                } else {
                    $months_report = (int) $months_report;
                    $months_report--;
                    $beginMonth = date('Y-m-01', strtotime("-$months_report MONTH"));
                    $endMonth   = date('Y-m-t');
                }

                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $beginMonth . '" AND "' . $endMonth . '")';
            } elseif ($months_report == 'this_month') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' . date('Y-m-01') . '" AND "' . date('Y-m-t') . '")';
            } elseif ($months_report == 'this_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d', strtotime(date('Y-01-01'))) .
                '" AND "' .
                date('Y-m-d', strtotime(date('Y-12-31'))) . '")';
            } elseif ($months_report == 'last_year') {
                $custom_date_select = 'AND (' . $field . ' BETWEEN "' .
                date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-01-01'))) .
                '" AND "' .
                date('Y-m-d', strtotime(date(date('Y', strtotime('last year')) . '-12-31'))) . '")';
            } elseif ($months_report == 'custom') {
                $from_date = to_sql_date($this->input->post('report_from'));
                $to_date   = to_sql_date($this->input->post('report_to'));
                if ($from_date == $to_date) {
                    $custom_date_select = 'AND ' . $field . ' = "' . $this->db->escape_str($from_date) . '"';
                } else {
                    $custom_date_select = 'AND (' . $field . ' BETWEEN "' . $this->db->escape_str($from_date) . '" AND "' . $this->db->escape_str($to_date) . '")';
                }
            }
        }

        return $custom_date_select;
    }

    public function items()
    {
        if ($this->input->is_ajax_request()) {
            $this->load->model('currencies_model');
            $v = $this->db->query('SELECT VERSION() as version')->row();
            // 5.6 mysql version don't have the ANY_VALUE function implemented.

            if ($v && strpos($v->version, '5.7') !== false) {
                $aColumns = [
                        'ANY_VALUE(description) as description',
                        'ANY_VALUE((SUM(' . db_prefix() . 'itemable.qty))) as quantity_sold',
                        'ANY_VALUE(SUM(rate*qty)) as rate',
                        'ANY_VALUE(AVG(rate*qty)) as avg_price',
                    ];
            } else {
                $aColumns = [
                        'description as description',
                        '(SUM(' . db_prefix() . 'itemable.qty)) as quantity_sold',
                        'SUM(rate*qty) as rate',
                        'AVG(rate*qty) as avg_price',
                    ];
            }

            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'itemable';
            $join         = ['JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id = ' . db_prefix() . 'itemable.rel_id'];

            $where = ['AND rel_type="invoice"', 'AND status != 5', 'AND status=2'];

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }
            $by_currency = $this->input->post('report_currency');
            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            if ($this->input->post('sale_agent_items')) {
                $agents  = $this->input->post('sale_agent_items');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [], 'GROUP by description');

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total_amount' => 0,
                'total_qty'    => 0,
            ];

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = $aRow['description'];
                $row[] = $aRow['quantity_sold'];
                $row[] = app_format_money($aRow['rate'], $currency->name);
                $row[] = app_format_money($aRow['avg_price'], $currency->name);
                $footer_data['total_amount'] += $aRow['rate'];
                $footer_data['total_qty'] += $aRow['quantity_sold'];
                $output['aaData'][] = $row;
            }

            $footer_data['total_amount'] = app_format_money($footer_data['total_amount'], $currency->name);

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function credit_notes()
    {
        if ($this->input->is_ajax_request()) {
            $credit_note_taxes = $this->distinct_taxes('credit_note');
            $totalTaxesColumns = count($credit_note_taxes);

            $this->load->model('currencies_model');

            $select = [
                'number',
                'date',
                get_sql_select_client_company(),
                'reference_no',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                '(SELECT ' . db_prefix() . 'creditnotes.total - (
                  (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'credits WHERE ' . db_prefix() . 'credits.credit_id=' . db_prefix() . 'creditnotes.id)
                  +
                  (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'creditnote_refunds WHERE ' . db_prefix() . 'creditnote_refunds.credit_note_id=' . db_prefix() . 'creditnotes.id)
                  )
                ) as remaining_amount',
                'status',
            ];

            $where = [];

            $credit_note_taxes_select = array_reverse($credit_note_taxes);

            foreach ($credit_note_taxes_select as $key => $tax) {
                array_splice($select, 5, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="credit_note" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'creditnotes.id) as total_tax_single_' . $key);
            }

            $custom_date_select = $this->get_where_report_period();

            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            $by_currency = $this->input->post('report_currency');

            if ($by_currency) {
                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency = $this->currencies_model->get_base_currency();
            }

            if ($this->input->post('credit_note_status')) {
                $statuses  = $this->input->post('credit_note_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'creditnotes';
            $join         = [
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'creditnotes.clientid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                db_prefix() . 'creditnotes.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'            => 0,
                'subtotal'         => 0,
                'total_tax'        => 0,
                'discount_total'   => 0,
                'adjustment'       => 0,
                'remaining_amount' => 0,
            ];

            foreach ($credit_note_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }
            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('credit_notes/list_credit_notes/' . $aRow['id']) . '" target="_blank">' . format_credit_note_number($aRow['id']) . '</a>';

                $row[] = _d($aRow['date']);

                if (empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                $row[] = $aRow['reference_no'];

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($credit_note_taxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[] = app_format_money($aRow['remaining_amount'], $currency->name);
                $footer_data['remaining_amount'] += $aRow['remaining_amount'];

                $row[] = format_credit_note_status($aRow['status']);

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function invoices_report()
    {
        if ($this->input->is_ajax_request()) {
            $invoice_taxes     = $this->distinct_taxes('invoice');
            $totalTaxesColumns = count($invoice_taxes);

            $this->load->model('currencies_model');
            $this->load->model('invoices_model');

            $select = [
                'number',
                get_sql_select_client_company(),
                'YEAR(date) as year',
                'date',
                'duedate',
                'subtotal',
                'total',
                'total_tax',
                'discount_total',
                'adjustment',
                '(SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'credits WHERE ' . db_prefix() . 'credits.invoice_id=' . db_prefix() . 'invoices.id) as credits_applied',
                '(SELECT total - (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'invoicepaymentrecords WHERE invoiceid = ' . db_prefix() . 'invoices.id) - (SELECT COALESCE(SUM(amount),0) FROM ' . db_prefix() . 'credits WHERE ' . db_prefix() . 'credits.invoice_id=' . db_prefix() . 'invoices.id))',
                'status',
            ];

            $where = [
                'AND status != 5',
            ];

            $invoiceTaxesSelect = array_reverse($invoice_taxes);

            foreach ($invoiceTaxesSelect as $key => $tax) {
                array_splice($select, 8, 0, '(
                    SELECT CASE
                    WHEN discount_percent != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * discount_percent/100)),' . get_decimal_places() . ')
                    WHEN discount_total != 0 AND discount_type = "before_tax" THEN ROUND(SUM((qty*rate/100*' . db_prefix() . 'item_tax.taxrate) - (qty*rate/100*' . db_prefix() . 'item_tax.taxrate * (discount_total/subtotal*100) / 100)),' . get_decimal_places() . ')
                    ELSE ROUND(SUM(qty*rate/100*' . db_prefix() . 'item_tax.taxrate),' . get_decimal_places() . ')
                    END
                    FROM ' . db_prefix() . 'itemable
                    INNER JOIN ' . db_prefix() . 'item_tax ON ' . db_prefix() . 'item_tax.itemid=' . db_prefix() . 'itemable.id
                    WHERE ' . db_prefix() . 'itemable.rel_type="invoice" AND taxname="' . $tax['taxname'] . '" AND taxrate="' . $tax['taxrate'] . '" AND ' . db_prefix() . 'itemable.rel_id=' . db_prefix() . 'invoices.id) as total_tax_single_' . $key);
            }

            $custom_date_select = $this->get_where_report_period();
            if ($custom_date_select != '') {
                array_push($where, $custom_date_select);
            }

            if ($this->input->post('sale_agent_invoices')) {
                $agents  = $this->input->post('sale_agent_invoices');
                $_agents = [];
                if (is_array($agents)) {
                    foreach ($agents as $agent) {
                        if ($agent != '') {
                            array_push($_agents, $this->db->escape_str($agent));
                        }
                    }
                }
                if (count($_agents) > 0) {
                    array_push($where, 'AND sale_agent IN (' . implode(', ', $_agents) . ')');
                }
            }

            $by_currency              = $this->input->post('report_currency');
            $totalPaymentsColumnIndex = (12 + $totalTaxesColumns - 1);

            if ($by_currency) {
                $_temp = substr($select[$totalPaymentsColumnIndex], 0, -2);
                $_temp .= ' AND currency =' . $by_currency . ')) as amount_open';
                $select[$totalPaymentsColumnIndex] = $_temp;

                $currency = $this->currencies_model->get($by_currency);
                array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
            } else {
                $currency                          = $this->currencies_model->get_base_currency();
                $select[$totalPaymentsColumnIndex] = $select[$totalPaymentsColumnIndex] .= ' as amount_open';
            }

            if ($this->input->post('invoice_status')) {
                $statuses  = $this->input->post('invoice_status');
                $_statuses = [];
                if (is_array($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status != '') {
                            array_push($_statuses, $this->db->escape_str($status));
                        }
                    }
                }
                if (count($_statuses) > 0) {
                    array_push($where, 'AND status IN (' . implode(', ', $_statuses) . ')');
                }
            }

            $aColumns     = $select;
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'invoices';
            $join         = [
                'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'invoices.clientid',
            ];

            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                'userid',
                'clientid',
                db_prefix() . 'invoices.id',
                'discount_percent',
                'deleted_customer_name',
            ]);

            $output  = $result['output'];
            $rResult = $result['rResult'];

            $footer_data = [
                'total'           => 0,
                'subtotal'        => 0,
                'total_tax'       => 0,
                'discount_total'  => 0,
                'adjustment'      => 0,
                'applied_credits' => 0,
                'amount_open'     => 0,
            ];

            foreach ($invoice_taxes as $key => $tax) {
                $footer_data['total_tax_single_' . $key] = 0;
            }

            foreach ($rResult as $aRow) {
                $row = [];

                $row[] = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['id']) . '" target="_blank">' . format_invoice_number($aRow['id']) . '</a>';

                if (empty($aRow['deleted_customer_name'])) {
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank">' . $aRow['company'] . '</a>';
                } else {
                    $row[] = $aRow['deleted_customer_name'];
                }

                $row[] = $aRow['year'];

                $row[] = _d($aRow['date']);

                $row[] = _d($aRow['duedate']);

                $row[] = app_format_money($aRow['subtotal'], $currency->name);
                $footer_data['subtotal'] += $aRow['subtotal'];

                $row[] = app_format_money($aRow['total'], $currency->name);
                $footer_data['total'] += $aRow['total'];

                $row[] = app_format_money($aRow['total_tax'], $currency->name);
                $footer_data['total_tax'] += $aRow['total_tax'];

                $t = $totalTaxesColumns - 1;
                $i = 0;
                foreach ($invoice_taxes as $tax) {
                    $row[] = app_format_money(($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]), $currency->name);
                    $footer_data['total_tax_single_' . $i] += ($aRow['total_tax_single_' . $t] == null ? 0 : $aRow['total_tax_single_' . $t]);
                    $t--;
                    $i++;
                }

                $row[] = app_format_money($aRow['discount_total'], $currency->name);
                $footer_data['discount_total'] += $aRow['discount_total'];

                $row[] = app_format_money($aRow['adjustment'], $currency->name);
                $footer_data['adjustment'] += $aRow['adjustment'];

                $row[] = app_format_money($aRow['credits_applied'], $currency->name);
                $footer_data['applied_credits'] += $aRow['credits_applied'];

                $amountOpen = $aRow['amount_open'];
                $row[]      = app_format_money($amountOpen, $currency->name);
                $footer_data['amount_open'] += $amountOpen;

                $row[] = format_invoice_status($aRow['status']);

                $output['aaData'][] = $row;
            }

            foreach ($footer_data as $key => $total) {
                $footer_data[$key] = app_format_money($total, $currency->name);
            }

            $output['sums'] = $footer_data;
            echo json_encode($output);
            die();
        }
    }

    public function expenses($type = 'simple_report')
    {
        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['currencies']    = $this->currencies_model->get();

        $data['title'] = _l('expenses_report');
        if ($type != 'simple_report') {
            $this->load->model('expenses_model');
            $data['categories'] = $this->expenses_model->get_category();
            $data['years']      = $this->expenses_model->get_expenses_years();

            if ($this->input->is_ajax_request()) {
                $aColumns = [
                    db_prefix().'expenses.category',
                    'amount',
                    'expense_name',
                    'tax',
                    'tax2',
                    '(SELECT taxrate FROM ' . db_prefix() . 'taxes WHERE id=' . db_prefix() . 'expenses.tax)',
                    'amount as amount_with_tax',
                    'billable',
                    'date',
                    get_sql_select_client_company(),
                    'invoiceid',
                    'reference_no',
                    'paymentmode',
                ];
                $join = [
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'expenses.clientid',
                    'LEFT JOIN ' . db_prefix() . 'expenses_categories ON ' . db_prefix() . 'expenses_categories.id = ' . db_prefix() . 'expenses.category',
                ];
                $where  = [];
                $filter = [];
                include_once(APPPATH . 'views/admin/tables/includes/expenses_filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $by_currency = $this->input->post('currency');
                if ($by_currency) {
                    $currency = $this->currencies_model->get($by_currency);
                    array_push($where, 'AND currency=' . $this->db->escape_str($by_currency));
                } else {
                    $currency = $this->currencies_model->get_base_currency();
                }

                $sIndexColumn = 'id';
                $sTable       = db_prefix() . 'expenses';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
                    db_prefix() . 'expenses_categories.name as category_name',
                    db_prefix() . 'expenses.id',
                    db_prefix() . 'expenses.clientid',
                    'currency',
                ]);
                $output  = $result['output'];
                $rResult = $result['rResult'];
                $this->load->model('currencies_model');
                $this->load->model('payment_modes_model');

                $footer_data = [
                    'tax_1'           => 0,
                    'tax_2'           => 0,
                    'amount'          => 0,
                    'total_tax'       => 0,
                    'amount_with_tax' => 0,
                ];

                foreach ($rResult as $aRow) {
                    $row = [];
                    for ($i = 0; $i < count($aColumns); $i++) {
                        if (strpos($aColumns[$i], 'as') !== false && !isset($aRow[$aColumns[$i]])) {
                            $_data = $aRow[strafter($aColumns[$i], 'as ')];
                        } else {
                            $_data = $aRow[$aColumns[$i]];
                        }
                        if ($aRow['tax'] != 0) {
                            $_tax = get_tax_by_id($aRow['tax']);
                        }
                        if ($aRow['tax2'] != 0) {
                            $_tax2 = get_tax_by_id($aRow['tax2']);
                        }
                        if ($aColumns[$i] == 'category') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['category_name'] . '</a>';
                        } elseif ($aColumns[$i] == 'expense_name') {
                            $_data = '<a href="' . admin_url('expenses/list_expenses/' . $aRow['id']) . '" target="_blank">' . $aRow['expense_name'] . '</a>';
                        } elseif ($aColumns[$i] == 'amount' || $i == 6) {
                            $total = $_data;
                            if ($i != 6) {
                                $footer_data['amount'] += $total;
                            } else {
                                if ($aRow['tax'] != 0 && $i == 6) {
                                    $total += ($total / 100 * $_tax->taxrate);
                                }
                                if ($aRow['tax2'] != 0 && $i == 6) {
                                    $total += ($aRow['amount'] / 100 * $_tax2->taxrate);
                                }
                                $footer_data['amount_with_tax'] += $total;
                            }

                            $_data = app_format_money($total, $currency->name);
                        } elseif ($i == 9) {
                            $_data = '<a href="' . admin_url('clients/client/' . $aRow['clientid']) . '">' . $aRow['company'] . '</a>';
                        } elseif ($aColumns[$i] == 'paymentmode') {
                            $_data = '';
                            if ($aRow['paymentmode'] != '0' && !empty($aRow['paymentmode'])) {
                                $payment_mode = $this->payment_modes_model->get($aRow['paymentmode'], [], false, true);
                                if ($payment_mode) {
                                    $_data = $payment_mode->name;
                                }
                            }
                        } elseif ($aColumns[$i] == 'date') {
                            $_data = _d($_data);
                        } elseif ($aColumns[$i] == 'tax') {
                            if ($aRow['tax'] != 0) {
                                $_data = $_tax->name . ' - ' . app_format_number($_tax->taxrate) . '%';
                            } else {
                                $_data = '';
                            }
                        } elseif ($aColumns[$i] == 'tax2') {
                            if ($aRow['tax2'] != 0) {
                                $_data = $_tax2->name . ' - ' . app_format_number($_tax2->taxrate) . '%';
                            } else {
                                $_data = '';
                            }
                        } elseif ($i == 5) {
                            if ($aRow['tax'] != 0 || $aRow['tax2'] != 0) {
                                if ($aRow['tax'] != 0) {
                                    $total = ($total / 100 * $_tax->taxrate);
                                    $footer_data['tax_1'] += $total;
                                }
                                if ($aRow['tax2'] != 0) {
                                    $total += ($aRow['amount'] / 100 * $_tax2->taxrate);
                                    $footer_data['tax_2'] += $total;
                                }
                                $_data = app_format_money($total, $currency->name);
                                $footer_data['total_tax'] += $total;
                            } else {
                                $_data = app_format_number(0);
                            }
                        } elseif ($aColumns[$i] == 'billable') {
                            if ($aRow['billable'] == 1) {
                                $_data = _l('expenses_list_billable');
                            } else {
                                $_data = _l('expense_not_billable');
                            }
                        } elseif ($aColumns[$i] == 'invoiceid') {
                            if ($_data) {
                                $_data = '<a href="' . admin_url('invoices/list_invoices/' . $_data) . '">' . format_invoice_number($_data) . '</a>';
                            } else {
                                $_data = '';
                            }
                        }
                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }

                foreach ($footer_data as $key => $total) {
                    $footer_data[$key] = app_format_money($total, $currency->name);
                }

                $output['sums'] = $footer_data;
                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/expenses_detailed', $data);
        } else {
            if (!$this->input->get('year')) {
                $data['current_year'] = date('Y');
            } else {
                $data['current_year'] = $this->input->get('year');
            }


            $data['export_not_supported'] = ($this->agent->browser() == 'Internet Explorer' || $this->agent->browser() == 'Spartan');

            $this->load->model('expenses_model');

            $data['chart_not_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('not_billable_expenses_by_categories'), [
                'billable' => 0,
            ], [
                'backgroundColor' => 'rgba(252,45,66,0.4)',
                'borderColor'     => '#fc2d42',
            ], $data['current_year']));

            $data['chart_billable'] = json_encode($this->reports_model->get_stats_chart_data(_l('billable_expenses_by_categories'), [
                'billable' => 1,
            ], [
                'backgroundColor' => 'rgba(37,155,35,0.2)',
                'borderColor'     => '#84c529',
            ], $data['current_year']));

            $data['expense_years'] = $this->expenses_model->get_expenses_years();

            if (count($data['expense_years']) > 0) {
                // Perhaps no expenses in new year?
                if (!in_array_multidimensional($data['expense_years'], 'year', date('Y'))) {
                    array_unshift($data['expense_years'], ['year' => date('Y')]);
                }
            }

            $data['categories'] = $this->expenses_model->get_category();

            $this->load->view('admin/reports/expenses', $data);
        }
    }

    public function expenses_vs_income($year = '')
    {
        $_expenses_years = [];
        $_years          = [];
        $this->load->model('expenses_model');
        $expenses_years = $this->expenses_model->get_expenses_years();
        $payments_years = $this->reports_model->get_distinct_payments_years();

        foreach ($expenses_years as $y) {
            array_push($_years, $y['year']);
        }
        foreach ($payments_years as $y) {
            array_push($_years, $y['year']);
        }

        $_years = array_map('unserialize', array_unique(array_map('serialize', $_years)));

        if (!in_array(date('Y'), $_years)) {
            $_years[] = date('Y');
        }

        rsort($_years, SORT_NUMERIC);
        $data['report_year'] = $year == '' ? date('Y') : $year;

        $data['years']                           = $_years;
        $data['chart_expenses_vs_income_values'] = json_encode($this->reports_model->get_expenses_vs_income_report($year));
        $data['base_currency']                   = get_base_currency();
        $data['title']                           = _l('als_expenses_vs_income');
        $this->load->view('admin/reports/expenses_vs_income', $data);
    }

    /* Total income report / ajax chart*/
    public function total_income_report()
    {
        echo json_encode($this->reports_model->total_income_report());
    }

    public function report_by_payment_modes()
    {
        echo json_encode($this->reports_model->report_by_payment_modes());
    }

    public function report_by_customer_groups()
    {
        echo json_encode($this->reports_model->report_by_customer_groups());
    }

    /* Leads conversion monthly report / ajax chart*/
    public function leads_monthly_report($month)
    {
        echo json_encode($this->reports_model->leads_monthly_report($month));
    }

    private function distinct_taxes($rel_type)
    {
        return $this->db->query('SELECT DISTINCT taxname,taxrate FROM ' . db_prefix() . "item_tax WHERE rel_type='" . $rel_type . "' ORDER BY taxname ASC")->result_array();
    }


    public function ingenious_report()
    {
        $data['title'] = _l('ingenious_report');
        $data['field_ingenico'] = json_encode($this->reports_model->field_ingenico());
        //var_dump($this->input->is_ajax_request()); die;
            if ($this->input->is_ajax_request()) {
                $aColumns = [
                    db_prefix() . 'ingenico.id as id',
                    'country',
                    'repair_center',
                    'customer_name',
                    'in_serial_number',
                    'out_serial_number',
                    'terminal_part_number.part_number as terminal_part_number',
                    'terminal_models.model as model',
                    'terminal_imei_address',
                    'terminal_mac_address',
                    'made_in',
                    'pci',
                    'sales_date',
                    'end_sales_warranty',
                    'date_of_customization',
                    'in_warranty',
                    'out_warranty',
                    'in_warranty_accesories',
                    'out_warranty_accesories',
                    'in_date',
                    'date_of_entry_to_repair',
                    'date_start_stop_terminal',
                    'date_end_stop_terminal',
                    'data_of_repair',
                    'out_date',
                    'deadline',
                    'report_month',
                    'backlog',
                    'time_of_repairclient_with_discount_supply',
                    'time_of_repairclient_with_discount_customer',
                    'sla_with_discount_cr',
                    'sla_with_discount_customer',
                    'mrr',
                    'bounce',
                    'date_of_the_last_repair',
                    'drr',
                    'doa_repair',
                    'eco_1',
                    'custom_doa',
                    'first_pass_yield_fpytest2',
                    'first_pass_yield_fpyqa',
                    '1_customer_symptom',
                    '2_customer_symptom',
                    '3_customer_symptom',
                    '4_customer_symptom',
                    '5_customer_symptom',
                    'tabla6.description as 1_technician_diagnosis',
                    'tabla7.description as 2_technician_diagnosis',
                    'tabla8.description as 3_technician_diagnosis',
                    'tabla9.description as 4_technician_diagnosis',
                    'tabla10.description as 5_technician_diagnosis',
                    '6_technician_diagnosis',
                    '7_technician_diagnosis',
                    '8_technician_diagnosis',
                    '9_technician_diagnosis',
                    '10_technician_diagnosis',
                    'tabla1.description as 1_technician_resolution',
                    'tabla2.description as 2_technician_resolution',
                    'tabla3.description as 3_technician_resolution',
                    'tabla4.description as 4_technician_resolution',
                    'tabla5.description as 5_technician_resolution',
                    '6_technician_resolution',
                    '7_technician_resolution',
                    '8_technician_resolution',
                    '9_technician_resolution',
                    '10_technician_resolution',
                    '11_technician_resolution',
                    '12_technician_resolution',
                    '13_technician_resolution',
                    '14_technician_resolution',
                    '15_technician_resolution',
                    '1_part_status',
                    '1_part_number',
                    '1_part_description',
                    '1_part_cost',
                    '2_part_status',
                    '2_part_number',
                    '2_part_description',
                    '2_part_cost',
                    '3_part_status',
                    '3_part_number',
                    '3_part_description',
                    '3_part_cost',
                    '4_part_status',
                    '4_part_number',
                    '4_part_description',
                    '4_part_cost',
                    '5_part_status',
                    '5_part_number',
                    '5_part_description',
                    '5_part_cost',
                    /*'6_part_status',
                    '6_part_number',
                    '6_part_descript/ion',
                    '6_part_cost',
                    '7_part_status',

                    '7_part_number',
                    '7_part_description',
                    '7_part_cost',
                    '8_part_status',
                    '8_part_number',
                    '8_part_description',
                    '8_part_cost',
                    '9_part_status',
                    '9_part_number',db_prefix() . 'technician_resolution.description as 1_technician_resolution'
                    '9_part_descript/ion',
                    '9_part_cost',
                    '10_part_status',
                    '10_part_number',
                    '10_part_description',
                    '10_part_cost',
                    '11_part_status',
                    '11_part_number',
                    '11_part_description',
                    '11_part_cost',
                    '12_part_status',
                    '12_part_number',
                    '12_part_description',
                    '12_part_cost',
                    '13_part_status',
                    '13_part_number',
                    '13_part_description',
                    '13_part_cost',
                    '14_part_status','tabla10.description as 5_technician_diagnosis'
                    '14_part_number',
                    '14_part_description',
                    '14_part_cost',
                    '15_part_status',
                    '15_part_number',
                    '15_part_description',
                    '15_part_cost',
                    '16_part_status',
                    '16_part_number',
                    '16_part_description',
                    '16_part_cost',*/
                    'in_number',
                    'out_number',
                    'lote_placa_box_out_number',
                    db_prefix() . 'repair_status.repair as repair_status',
                    db_prefix() . 'repair_level.repair as repair_level',
                    'detail_repair_status',
                    'terminal_status',
                    'accesories_status',
                    'unit_cogs_USD',
                    //'unit_cogs',
                    'logistics',
                    'other_cost',
                    'total_cogs_unit_cogs_Logistica_otros',
                    'workforce_cost',
                    'app',
                    'workforce_cost_total_work_app',
                    'administration_cost',
                    'external_services_cost_USD',
                    'comments1',
                    'comments2',
                    'comments3',
                    'comments4',

                                        

                ];
                $join = [
                    'LEFT JOIN ' . db_prefix() . 'repair_status ON ' . db_prefix() . 'repair_status.id = ' . db_prefix() . 'ingenico.repair_status',
                    'LEFT JOIN ' . db_prefix() . 'repair_level ON ' . db_prefix() . 'repair_level.id = ' . db_prefix() . 'ingenico.repair_level',
                    'LEFT JOIN ' . db_prefix() . 'technician_resolution as tabla1 ON tabla1.id = ' . db_prefix() . 'ingenico.1_technician_resolution ',
                    'LEFT JOIN ' . db_prefix() . 'technician_resolution as tabla2 ON tabla2.id = ' . db_prefix() . 'ingenico.2_technician_resolution',
                    'LEFT JOIN ' . db_prefix() . 'technician_resolution as tabla3 ON tabla3.id = ' . db_prefix() . 'ingenico.3_technician_resolution',
                    'LEFT JOIN ' . db_prefix() . 'technician_resolution as tabla4 ON tabla4.id = ' . db_prefix() . 'ingenico.4_technician_resolution',
                    'LEFT JOIN ' . db_prefix() . 'technician_resolution as tabla5 ON tabla5.id = ' . db_prefix() . 'ingenico.5_technician_resolution',
                    'LEFT JOIN ' . db_prefix() . 'technician_diagnosis as tabla6 ON tabla6.id = ' . db_prefix() . 'ingenico.1_technician_diagnosis',
                    'LEFT JOIN ' . db_prefix() . 'technician_diagnosis as tabla7 ON tabla7.id = ' . db_prefix() . 'ingenico.2_technician_diagnosis',
                    'LEFT JOIN ' . db_prefix() . 'technician_diagnosis as tabla8 ON tabla8.id = ' . db_prefix() . 'ingenico.3_technician_diagnosis',
                    'LEFT JOIN ' . db_prefix() . 'technician_diagnosis as tabla9 ON tabla9.id = ' . db_prefix() . 'ingenico.4_technician_diagnosis',
                    'LEFT JOIN ' . db_prefix() . 'technician_diagnosis as tabla10 ON tabla10.id = ' . db_prefix() . 'ingenico.5_technician_diagnosis',
                    'LEFT JOIN ' . db_prefix() . 'terminal_part_number as terminal_part_number ON terminal_part_number.id = ' . db_prefix() . 'ingenico.terminal_part_number',
                    'LEFT JOIN ' . db_prefix() . 'terminal_models as terminal_models ON terminal_models.modelid = ' . db_prefix() . 'ingenico.model',
                ];
                $where  = [];
                //$GroupBy  = ' ORDER BY id ';
                $filter = [];
                include_once(APPPATH . 'views/admin/tables/includes/ingenious_filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $sIndexColumn = 'id';
                $sTable       = db_prefix() . 'ingenico';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where);
                $output  = $result['output'];
                $rResult = $result['rResult'];
                //var_dump($rResult); die;

                
                foreach ($rResult as $aRow) {
                    $row = [];
                    for ($i = 0; $i < count($aColumns); $i++) {
                        //var_dump($aColumns); die;
                        if ($aColumns[$i] == db_prefix() . 'ingenico.id as id') {
                            $_data = $aRow['id'];
                        }elseif ($aColumns[$i] == 'country') {
                            $_data = $aRow['country'];
                        }elseif ($aColumns[$i] == 'repair_center') {
                            $_data = $aRow['repair_center'];
                        }elseif ($aColumns[$i] == 'customer_name') {
                            $_data = $aRow['customer_name'];
                        }elseif ($aColumns[$i] == 'in_serial_number') {
                            $_data = $aRow['in_serial_number'];
                        }elseif ($aColumns[$i] == 'out_serial_number') {
                            $_data = $aRow['out_serial_number'];
                        }elseif ($aColumns[$i] == 'terminal_part_number.part_number as terminal_part_number') {
                            $_data = $aRow['terminal_part_number'];
                        }elseif ($aColumns[$i] == 'terminal_models.model as model') {
                            $_data = $aRow['model'];
                        }elseif ($aColumns[$i] == 'terminal_imei_address') {
                            $_data = $aRow['terminal_imei_address'];
                        }elseif ($aColumns[$i] == 'terminal_mac_address') {
                            $_data = $aRow['terminal_mac_address'];
                        }elseif ($aColumns[$i] == 'made_in') {
                            $_data = $aRow['made_in'];
                        }elseif ($aColumns[$i] == 'pci') {
                            $_data = $aRow['pci'];
                        }elseif ($aColumns[$i] == 'sales_date') {
                            $_data = $aRow['sales_date'];
                        }elseif ($aColumns[$i] == 'end_sales_warranty') {
                            $_data = $aRow['end_sales_warranty'];
                        }elseif ($aColumns[$i] == 'date_of_customization') {
                            $_data = $aRow['date_of_customization'];
                        }elseif ($aColumns[$i] == 'in_warranty') {
                            $_data = $aRow['in_warranty'];
                        }elseif ($aColumns[$i] == 'out_warranty') {
                            $_data = $aRow['out_warranty'];
                        }elseif ($aColumns[$i] == 'in_warranty_accesories') {
                            $_data = $aRow['in_warranty_accesories'];
                        }elseif ($aColumns[$i] == 'out_warranty_accesories') {
                            $_data = $aRow['out_warranty_accesories'];
                        }elseif ($aColumns[$i] == 'in_date') {
                            $_data = $aRow['in_date'];
                        }elseif ($aColumns[$i] == 'date_of_entry_to_repair') {
                            $_data = $aRow['date_of_entry_to_repair'];
                        }elseif ($aColumns[$i] == 'date_start_stop_terminal') {
                            $_data = $aRow['date_start_stop_terminal'];
                        }elseif ($aColumns[$i] == 'date_end_stop_terminal') {
                            $_data = $aRow['date_end_stop_terminal'];
                        }elseif ($aColumns[$i] == 'data_of_repair') {
                            $_data = $aRow['data_of_repair'];
                        }elseif ($aColumns[$i] == 'out_date') {
                            $_data = $aRow['out_date'];
                        }elseif ($aColumns[$i] == 'deadline') {
                            $_data = $aRow['deadline'];
                        }elseif ($aColumns[$i] == 'report_month') {
                            $_data = $aRow['report_month'];
                        }elseif ($aColumns[$i] == 'backlog') {
                            $_data = $aRow['backlog'];
                        }elseif ($aColumns[$i] == 'time_of_repairclient_with_discount_supply') {
                            $_data = $aRow['time_of_repairclient_with_discount_supply'];
                        }elseif ($aColumns[$i] == 'time_of_repairclient_with_discount_customer') {
                            $_data = $aRow['time_of_repairclient_with_discount_customer'];
                        }elseif ($aColumns[$i] == 'sla_with_discount_cr') {
                            $_data = $aRow['sla_with_discount_cr'];
                        }elseif ($aColumns[$i] == 'sla_%_with_discount_customer') {
                            $_data = $aRow['sla_%_with_discount_customer'];
                        }elseif ($aColumns[$i] == 'mrr') {
                            $_data = $aRow['mrr'];
                        }elseif ($aColumns[$i] == 'bounce') {
                            $_data = $aRow['bounce'];
                        }elseif ($aColumns[$i] == 'date_of_the_last_repair') {
                            $_data = $aRow['date_of_the_last_repair'];
                        }
                        elseif ($aColumns[$i] == 'drr') {
                            $_data = $aRow['drr'];
                        }elseif ($aColumns[$i] == 'doa_repair') {
                            $_data = $aRow['doa_repair'];
                        }elseif ($aColumns[$i] == 'eco_1') {
                            $_data = $aRow['eco_1'];
                        }elseif ($aColumns[$i] == 'custom_doa') {
                            $_data = $aRow['custom_doa'];
                        }elseif ($aColumns[$i] == 'first_pass_yield_fpytest2') {
                            $_data = $aRow['first_pass_yield_fpytest2'];
                        }elseif ($aColumns[$i] == 'first_pass_yield_fpyqa') {
                            $_data = $aRow['first_pass_yield_fpyqa'];
                        }elseif ($aColumns[$i] == '1_customer_symptom') {
                            $_data = $aRow['1_customer_symptom'];
                        }elseif ($aColumns[$i] == '2_customer_symptom') {
                            $_data = $aRow['2_customer_symptom'];
                        }elseif ($aColumns[$i] == '3_customer_symptom') {
                            $_data = $aRow['3_customer_symptom'];
                        }elseif ($aColumns[$i] == '4_customer_symptom') {
                            $_data = $aRow['4_customer_symptom'];
                        }elseif ($aColumns[$i] == '5_customer_symptom') {
                            $_data = $aRow['5_customer_symptom'];
                        }
                        
                        elseif ($aColumns[$i] == 'tabla6.description as 1_technician_diagnosis') {
                            $_data = $aRow['1_technician_diagnosis'];
                        }elseif ($aColumns[$i] == 'tabla7.description as 2_technician_diagnosis') {
                            $_data = $aRow['2_technician_diagnosis'];
                        }elseif ($aColumns[$i] == 'tabla8.description as 3_technician_diagnosis') {
                            $_data = $aRow['3_technician_diagnosis'];
                        }elseif ($aColumns[$i] == 'tabla9.description as 4_technician_diagnosis') {
                            $_data = $aRow['4_technician_diagnosis'];
                        }elseif ($aColumns[$i] == 'tabla10.description as 5_technician_diagnosis') {
                            $_data = $aRow['5_technician_diagnosis'];
                        }elseif ($aColumns[$i] == '6_technician_diagnosis') {
                            $_data = $aRow['6_technician_diagnosis'];
                        }elseif ($aColumns[$i] == '7_technician_diagnosis') {
                            $_data = $aRow['7_technician_diagnosis'];
                        }elseif ($aColumns[$i] == '8_technician_diagnosis') {
                            $_data = $aRow['8_technician_diagnosis'];
                        }elseif ($aColumns[$i] == '9_technician_diagnosis') {
                            $_data = $aRow['9_technician_diagnosis'];
                        }elseif ($aColumns[$i] == '10_technician_diagnosis') {
                            $_data = $aRow['10_technician_diagnosis'];
                        }
                        elseif ($aColumns[$i] == 'tabla1.description as 1_technician_resolution') {
                            $_data = $aRow['1_technician_resolution'];
                        }elseif ($aColumns[$i] == 'tabla2.description as 2_technician_resolution') {
                            $_data = $aRow['2_technician_resolution'];
                        }elseif ($aColumns[$i] == 'tabla3.description as 3_technician_resolution') {
                            $_data = $aRow['3_technician_resolution'];
                        }elseif ($aColumns[$i] == 'tabla4.description as 4_technician_resolution') {
                            $_data = $aRow['4_technician_resolution'];
                        }elseif ($aColumns[$i] == 'tabla5.description as 5_technician_resolution') {
                            $_data = $aRow['5_technician_resolution'];
                        }elseif ($aColumns[$i] == '6_technician_resolution') {
                            $_data = $aRow['6_technician_resolution'];
                        }elseif ($aColumns[$i] == '7_technician_resolution') {
                            $_data = $aRow['7_technician_resolution'];
                        }elseif ($aColumns[$i] == '8_technician_resolution') {
                            $_data = $aRow['8_technician_resolution'];
                        }elseif ($aColumns[$i] == '9_technician_resolution') {
                            $_data = $aRow['9_technician_resolution'];
                        }elseif ($aColumns[$i] == '10_technician_resolution') {
                            $_data = $aRow['10_technician_resolution'];
                        }elseif ($aColumns[$i] == '11_technician_resolution') {
                            $_data = $aRow['11_technician_resolution'];
                        }elseif ($aColumns[$i] == '12_technician_resolution') {
                            $_data = $aRow['12_technician_resolution'];
                        }elseif ($aColumns[$i] == '13_technician_resolution') {
                            $_data = $aRow['13_technician_resolution'];
                        }elseif ($aColumns[$i] == '14_technician_resolution') {
                            $_data = $aRow['14_technician_resolution'];
                        }elseif ($aColumns[$i] == '15_technician_resolution') {
                            $_data = $aRow['15_technician_resolution'];
                        }
                        elseif ($aColumns[$i] == '1_part_status') {

                            if($aRow['1_part_status'] == ''){
                                $_data = 'NI';
                            }else{
                                $_data = $aRow['1_part_status'];
                            }
                        }
                        elseif ($aColumns[$i] == '1_part_number') {
                           
                            if($aRow['1_part_number'] == ''){
                                $_data = 'NI';
                            }else{
                                $_data = $aRow['1_part_number'];
                            }

                        }elseif ($aColumns[$i] == '1_part_description') {
                           
                            if($aRow['1_part_description'] == ''){
                                $_data = 'NI';
                            }else{
                                $_data = $aRow['1_part_description'];
                            }

                        }elseif ($aColumns[$i] == '1_part_cost') {

                            if($aRow['1_part_cost'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['1_part_cost'];
                            }

                        }elseif ($aColumns[$i] == '2_part_status') {

                            if($aRow['2_part_status'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['2_part_status'];
                            }

                        }

                        elseif ($aColumns[$i] == '2_part_number') {

                            if($aRow['2_part_number'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['2_part_number'];
                            }
                           
                        }elseif ($aColumns[$i] == '2_part_description') {

                            if($aRow['2_part_description'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['2_part_description'];
                            }
                            
                        }elseif ($aColumns[$i] == '2_part_cost') {

                            if($aRow['2_part_cost'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['2_part_cost'];
                            }
                            
                        }elseif ($aColumns[$i] == '3_part_status') {

                            if($aRow['3_part_status'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['3_part_status'];
                            }
                           
                        }elseif ($aColumns[$i] == '3_part_number') {

                            if($aRow['3_part_number'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['3_part_number'];
                            }
                            
                        }elseif ($aColumns[$i] == '3_part_description') {

                            if($aRow['3_part_description'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['3_part_description'];
                            }
                            
                        }elseif ($aColumns[$i] == '3_part_cost') {

                            if($aRow['3_part_cost'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['3_part_cost'];
                            }
                            
                        }elseif ($aColumns[$i] == '4_part_status') {

                            if($aRow['4_part_status'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['4_part_status'];
                            }
                            
                        }elseif ($aColumns[$i] == '4_part_number') {

                            if($aRow['4_part_number'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['4_part_number'];
                            }
                           
                        }elseif ($aColumns[$i] == '4_part_description') {

                            if($aRow['4_part_description'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['4_part_description'];
                            }
                            
                        }elseif ($aColumns[$i] == '4_part_cost') {

                            if($aRow['4_part_cost'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['4_part_cost'];
                            }
                            
                        }elseif ($aColumns[$i] == '5_part_status') {

                            if($aRow['5_part_status'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['5_part_status'];
                            }
                            
                        }elseif ($aColumns[$i] == '5_part_number') {

                            if($aRow['5_part_number'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['5_part_number'];
                            }
                           
                        }elseif ($aColumns[$i] == '5_part_description') {
                        
                            if($aRow['5_part_description'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['5_part_description'];
                            }
                            
                        }
                        elseif ($aColumns[$i] == '5_part_cost') {

                            if($aRow['5_part_cost'] == ''){
                                $_data = 'NI';
                            }else{
                             $_data = $aRow['5_part_cost'];
                            }
                           
                        }
                        /*elseif ($aColumns[$i] == '6_part_status') {
                            $_data = $aRow['6_part_status'];
                        }elseif ($aColumns[$i] == '6_part_number') {
                            $_data = $aRow['6_part_number'];
                        }elseif ($aColumns[$i] == '6_part_description') {
                            $_data = $aRow['6_part_description'];
                        }elseif ($aColumns[$i] == '6_part_cost') {
                            $_data = $aRow['6_part_cost'];
                        }elseif ($aColumns[$i] == '7_part_status') {
                            $_data = $aRow['7_part_status'];
                        }

                        elseif ($aColumns[$i] == '7_part_number') {
                            $_data = $aRow['7_part_number'];
                        }elseif ($aColumns[$i] == '7_part_description') {
                            $_data = $aRow['7_part_description'];
                        }elseif ($aColumns[$i] == '7_part_cost') {
                            $_data = $aRow['7_part_cost'];
                        }elseif ($aColumns[$i] == '8_part_status') {
                            $_data = $aRow['8_part_status'];
                        }elseif ($aColumns[$i] == '8_part_number') {
                            $_data = $aRow['8_part_number'];
                        }elseif ($aColumns[$i] == '8_part_description') {
                            $_data = $aRow['8_part_description'];
                        }elseif ($aColumns[$i] == '8_part_cost') {
                            $_data = $aRow['8_part_cost'];
                        }elseif ($aColumns[$i] == '9_part_status') {
                            $_data = $aRow['9_part_status'];
                        }elseif ($aColumns[$i] == '9_part_status') {
                            $_data = $aRow['9_part_status'];
                        }elseif ($aColumns[$i] == '9_part_number') {
                            $_data = $aRow['9_part_number'];
                        }elseif ($aColumns[$i] == '9_part_description') {
                            $_data = $aRow['9_part_description'];
                        }elseif ($aColumns[$i] == '9_part_cost') {
                            $_data = $aRow['9_part_cost'];
                        }elseif ($aColumns[$i] == '10_part_status') {
                            $_data = $aRow['10_part_status'];
                        }elseif ($aColumns[$i] == '10_part_number') {
                            $_data = $aRow['10_part_number'];
                        }elseif ($aColumns[$i] == '10_part_description') {
                            $_data = $aRow['10_part_description'];
                        }elseif ($aColumns[$i] == '10_part_cost') {
                            $_data = $aRow['10_part_cost'];
                        }elseif ($aColumns[$i] == '11_part_status') {
                            $_data = $aRow['11_part_status'];
                        }elseif ($aColumns[$i] == '11_part_number') {
                            $_data = $aRow['11_part_number'];
                        }elseif ($aColumns[$i] == '11_part_description') {
                            $_data = $aRow['11_part_description'];
                        }elseif ($aColumns[$i] == '11_part_cost') {
                            $_data = $aRow['11_part_cost'];
                        }elseif ($aColumns[$i] == '12_part_status') {
                            $_data = $aRow['12_part_status'];
                        }

                        elseif ($aColumns[$i] == '12_part_number') {
                            $_data = $aRow['12_part_number'];
                        }elseif ($aColumns[$i] == '12_part_description') {
                            $_data = $aRow['12_part_description'];
                        }elseif ($aColumns[$i] == '12_part_cost') {
                            $_data = $aRow['12_part_cost'];
                        }elseif ($aColumns[$i] == '13_part_status') {
                            $_data = $aRow['13_part_status'];
                        }elseif ($aColumns[$i] == '13_part_number') {
                            $_data = $aRow['13_part_number'];
                        }elseif ($aColumns[$i] == '13_part_description') {
                            $_data = $aRow['13_part_description'];
                        }elseif ($aColumns[$i] == '13_part_cost') {
                            $_data = $aRow['13_part_cost'];
                        }elseif ($aColumns[$i] == '14_part_status') {
                            $_data = $aRow['14_part_status'];
                        }elseif ($aColumns[$i] == '14_part_number') {
                            $_data = $aRow['14_part_number'];
                        }elseif ($aColumns[$i] == '14_part_description') {
                            $_data = $aRow['14_part_description'];
                        }elseif ($aColumns[$i] == '14_part_cost') {
                            $_data = $aRow['14_part_cost'];
                        }elseif ($aColumns[$i] == '15_part_status') {
                            $_data = $aRow['15_part_status'];
                        }elseif ($aColumns[$i] == '15_part_number') {
                            $_data = $aRow['15_part_number'];
                        }elseif ($aColumns[$i] == '15_part_description') {
                            $_data = $aRow['15_part_description'];
                        }elseif ($aColumns[$i] == '15_part_cost') {
                            $_data = $aRow['15_part_cost'];
                        }elseif ($aColumns[$i] == '16_part_status') {
                            $_data = $aRow['16_part_status'];
                        }elseif ($aColumns[$i] == '16_part_number') {
                            $_data = $aRow['16_part_number'];
                        }elseif ($aColumns[$i] == '16_part_description') {
                            $_data = $aRow['16_part_description'];
                        }elseif ($aColumns[$i] == '16_part_cost') {
                            $_data = $aRow['16_part_cost'];
                        }*/
                        elseif ($aColumns[$i] == 'in_number') {
                            $_data = $aRow['in_number'];
                        }

                        elseif ($aColumns[$i] == 'out_number') {
                            $_data = $aRow['out_number'];
                        }elseif ($aColumns[$i] == 'lote_placa_box_out_number') {
                            $_data = $aRow['lote_placa_box_out_number'];
                        }
                        elseif ($aColumns[$i] == db_prefix() . 'repair_status.repair as repair_status') {
                            $_data = $aRow['repair_status'];
                        }elseif ($aColumns[$i] == db_prefix() . 'repair_level.repair as repair_level') {
                            $_data = $aRow['repair_level'];
                        }elseif ($aColumns[$i] == 'detail_repair_status') {
                            $_data = $aRow['detail_repair_status'];
                        }elseif ($aColumns[$i] == 'terminal_status') {
                            $_data = $aRow['terminal_status'];
                        }elseif ($aColumns[$i] == 'accesories_status') {
                            $_data = $aRow['accesories_status'];
                        }elseif ($aColumns[$i] == 'unit_cogs_USD') {
                            $_data = $aRow['unit_cogs_USD'];
                        }
                        /*
                        elseif ($aColumns[$i] == 'unit_cogs') {
                            $_data = $aRow['unit_cogs'];
                        }*/
                        elseif ($aColumns[$i] == 'logistics') {
                            $_data = $aRow['logistics'];
                        }elseif ($aColumns[$i] == 'other_cost') {
                            $_data = $aRow['other_cost'];
                        }


                        elseif ($aColumns[$i] == 'total_cogs_unit_cogs_Logistica_otros') {
                            $_data = $aRow['total_cogs_unit_cogs_Logistica_otros'];
                        }elseif ($aColumns[$i] == 'workforce_cost') {
                            $_data = $aRow['workforce_cost'];
                        }elseif ($aColumns[$i] == 'app') {
                            $_data = $aRow['app'];
                        }elseif ($aColumns[$i] == 'workforce_cost_total_work_app') {
                            $_data = $aRow['workforce_cost_total_work_app'];
                        }elseif ($aColumns[$i] == 'administration_cost') {
                            $_data = $aRow['administration_cost'];
                        }elseif ($aColumns[$i] == 'external_services_cost_USD') {
                            $_data = $aRow['external_services_cost_USD'];
                        }elseif ($aColumns[$i] == 'comments1') {
                            $_data = $aRow['comments1'];
                        }elseif ($aColumns[$i] == 'comments2') {
                            $_data = $aRow['comments2'];
                        }elseif ($aColumns[$i] == 'comments3') {
                            $_data = $aRow['comments3'];
                        }elseif ($aColumns[$i] == 'comments4') {
                            $_data = $aRow['comments4'];
                        }
                        
                        


                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }

                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/ingenious_detailed', $data);
    
    }


    public function report_terminals()
    {
        $data['title'] = _l('report_terminal');

            if ($this->input->is_ajax_request()) {
                $aColumns = [
                    db_prefix() . 'clients.company as company',
                    db_prefix() . 'clients.rif_number as rif_number',
                    'codaffiliate',
                    'serial',
                    'mac',
                    'imei',
                    db_prefix() . 'simcard.serialsimcard as simcard',
                    db_prefix() . 'terminal_models.model as modelid',
                    db_prefix() . 'operadora.operadora as operadoraid',
                    db_prefix() . 'terminals.status as status',
                    db_prefix() . 'terminals.datecreated as datecreated',
                    db_prefix() . 'banks.banks as banks',
                    ];
                
                    $join = [
                        //'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.id = ' . db_prefix() . 'terminals.banks',
                        //'LEFT JOIN ' . db_prefix() . 'operadora ON ' . db_prefix() . 'operadora.operadoraid = ' . db_prefix() . 'terminals.operadoraid',
                        'LEFT JOIN ' . db_prefix() . 'simcard ON ' . db_prefix() . 'simcard.id = ' . db_prefix() . 'terminals.simcard',
                        'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'terminals.userid',
                        'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.id = ' . db_prefix() . 'terminals.banks',
                        'LEFT JOIN ' . db_prefix() . 'operadora ON ' . db_prefix() . 'operadora.operadoraid = ' . db_prefix() . 'terminals.operadoraid',
                        'LEFT JOIN ' . db_prefix() . 'terminal_models ON ' . db_prefix() . 'terminal_models.modelid = ' . db_prefix() . 'terminals.modelid',
                    ];
                    
                
                $where = [];
                $filter = [];
                include_once(APPPATH . 'views/admin/tables/includes/filter_terminals.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                // if ($clientid != '') {
                //     array_push($where, 'AND ' . db_prefix() . 'terminals.userid=' . $this->ci->db->escape_str($clientid));
                // }
                
                
                
                $sIndexColumn = 'id';
                $sTable       = db_prefix() . 'terminals';
                
                $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where,[db_prefix() . 'terminals.id as id',db_prefix() . 'terminals.userid as userid']);
                
                $output  = $result['output'];
                
                $rResult = $result['rResult'];
                //var_dump($rResult); die;
                
                foreach ($rResult as $aRow) {
                   //var_dump($aRow); die;
                    $row = [];
                
                    //$row[] = '<a href="#" >' . $aRow['company'] . '</a>';
                    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '" target="_blank" data-toggle="tooltip" data-title="' . _l('client') . '">' . $aRow['company'] . '</a>' . '<span class="hide">' . $aRow['company'] . '</span>';
                    // $rowName .= '<div class="row-options">';
                
                    // $rowName .= '<a href="#" onclick="terminals(' . $aRow['userid'] . ',' . $aRow['id'] . ');return false;">' . _l('edit') . '</a>';
                
                    // $rowName .= '</div>';
                
                    $row[] = $aRow['rif_number'];
                
                    $row[] = $aRow['codaffiliate'];
                
                    $row[] = $aRow['serial'];
                
                    $row[] = $aRow['mac'];
                
                    $row[] = $aRow['imei'];
                
                    $row[] = $aRow['simcard'];

                    $row[] = $aRow['modelid'];
                
                    $row[] = $aRow['operadoraid'];

                    if ($aRow['status'] == '1') {
                        $row[] = '<span class="label s-status" style="border: 1px solid #84c529;color:#84c529;">' ._l('Activo'). '</span>';
                    }elseif($aRow['status_payment'] != '1'){
                        $row[] = '<span class="label s-status" style="border: 1px solid #FB2507;color:#FB2507;">' ._l('Inactivo'). '</span>';
                    }

                    $row[] = $aRow['datecreated'];
                
                    $row[] = $aRow['banks'];
                
                    $row['DT_RowClass'] = 'has-row-options';
                    $output['aaData'][] = $row;
                }

                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/report_terminals', $data);
    
    }

    public function report_tickets($id = '')
    {
        $data['title'] = _l('report_tickets');
        $data['ticketsid'] = $this->reports_model->get_ticketsid();
        $data['get_terminals'] = $this->reports_model->get_terminals();
        $data['community'] = $this->reports_model->get_community($id);
        $data['departments'] = $this->reports_model->get_departments($id);
        $data['categories'] = $this->reports_model->get_categories();
        
            if ($this->input->is_ajax_request()) {
                
                $aColumns = [
                    'ticketid',
                    'categories',
                    db_prefix() . 'departments.name as department_name',
                    db_prefix() . 'services.name as service_name',
                    'CONCAT(' . db_prefix() . 'contacts.firstname, \' \', ' . db_prefix() . 'contacts.lastname) as contact_full_name',
                    db_prefix() . 'tickets_status.name as status',
                    db_prefix() . 'tickets_priorities.name as priority',
                    'lastreply',
                    db_prefix() . 'tickets.date as date',
                    
                    ];

                
                    $join = [
                        'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'contacts.id = ' . db_prefix() . 'tickets.contactid',
                        'LEFT JOIN ' . db_prefix() . 'services ON ' . db_prefix() . 'services.serviceid = ' . db_prefix() . 'tickets.service',
                        'LEFT JOIN ' . db_prefix() . 'departments ON ' . db_prefix() . 'departments.departmentid = ' . db_prefix() . 'tickets.department',
                        'LEFT JOIN ' . db_prefix() . 'tickets_status ON ' . db_prefix() . 'tickets_status.ticketstatusid = ' . db_prefix() . 'tickets.status',
                        'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'tickets.userid',
                        'LEFT JOIN ' . db_prefix() . 'tickets_priorities ON ' . db_prefix() . 'tickets_priorities.priorityid = ' . db_prefix() . 'tickets.priority',
                        'LEFT JOIN ' . db_prefix() . 'categories ON ' . db_prefix() . 'categories.categoryId = ' . db_prefix() . 'tickets.subject',
                    ];

                    $additionalSelect = [
                        db_prefix() . 'clients.company',
                        db_prefix() . 'tickets.userid',
                        ];
                    
                
                $where = [];
                $filter = [];
                // include_once(APPPATH . 'views/admin/reports/filter_report_tickets.php');
                // if (count($filter) > 0) {
                //     array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                // }

                if ($this->ci->input->post('company')) {
                    $filter = $this->ci->input->post('company');
                    array_push($where, 'AND `tblclients`.`company` = "'.$filter.'"');
                } if ($this->ci->input->post('departments')) {
                    $filter = $this->ci->input->post('departments');
                    array_push($where, 'AND `tbldepartments`.`name` = "'.$filter.'"');
                } if ($this->ci->input->post('category')) {
                    $filter = $this->ci->input->post('category');
                    array_push($where, 'AND `tblcategories`.`categoryId` = "'.$filter.'"');
                }
                
                $sIndexColumn = 'ticketid';
                $sTable       = db_prefix() . 'tickets';
                
                $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where,$additionalSelect);
                
                $output  = $result['output'];
                
                $rResult = $result['rResult'];
                
                foreach ($rResult as $aRow) {
                 
                    $row = [];
                
                    $url   = admin_url('tickets/ticket/' . $aRow['ticketid']);
                    $row[] = '<a href="' . $url . '">' .  $aRow['ticketid'] . '</a>';
                
                    $row[] = $aRow['categories'];
                
                    $row[] = $aRow['department_name'];
                
                    $row[] = $aRow['service_name'];

                    $row[] = '<a href="' . admin_url('community/communit/' . $aRow['userid'] . '?group=contacts') . '">' . $aRow['company'];
                    
                    $row[] = $aRow['status'];

                    $row[] = $aRow['priority'];

                    $row[] = $aRow['lastreply'];

                   
                
                    $row[] = $aRow['date'];
                
                    $row['DT_RowClass'] = 'has-row-options';
                    $output['aaData'][] = $row;
                }

                echo json_encode($output);
                die;
            }
            $this->load->view('admin/reports/report_tickets', $data);
    
    }

    public function report_requests()
    {
        $data['title'] = _l('requests_report');
        if ($this->input->is_ajax_request()) {
            $aColumns = [
                db_prefix() . 'operations.id as id',
                db_prefix() . 'banks.banks as banks',
                db_prefix() . 'terminal_models.model as model',
                db_prefix() . 'terminals.serial as serial',
                db_prefix() . 'clients.company as company',
                'affiliate_code',
                'terminal',
                db_prefix() . 'clients.rif_number as rif_number',
                db_prefix() . 'invoices.id as invoices_id',
                db_prefix() . 'contacts.phonenumber as phonenumber',
                db_prefix() . 'type_operations.name as type',
                db_prefix() . 'operations_dates.sale as sale',
                db_prefix() . 'operations_dates.payment_confirmed as payment_confirmed',
                db_prefix() . 'operations_dates.bank_management as bank_management',
                db_prefix() . 'operations_dates.account_opened as account_opened',
                db_prefix() . 'operations_dates.send_code_af as send_code_af',
                db_prefix() . 'operations_dates.bank_code_af as bank_code_af',
                db_prefix() . 'operations_dates.serial_assignment as serial_assignment',
                db_prefix() . 'operations_dates.send_par_loading as send_par_loading',
                db_prefix() . 'operations_dates.bank_par_loading as bank_par_loading',
                db_prefix() . 'operations_dates.ccrd_answer as ccrd_answer',
                db_prefix() . 'operations_dates.parameterization as parameterization',
                db_prefix() . 'operations_dates.cari_relocated as cari_relocated',
                db_prefix() . 'operations_dates.ready as ready',
                db_prefix() . 'operations_dates.retired as retired',
                ];
            
                $join = [
                    
                    'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.id = ' . db_prefix() . 'operations.bank_id',
                    'LEFT JOIN ' . db_prefix() . 'terminals ON ' . db_prefix() . 'terminals.id = ' . db_prefix() . 'operations.terminal',
                    'LEFT JOIN ' . db_prefix() . 'terminal_models ON ' . db_prefix() . 'terminal_models.modelid = ' . db_prefix() . 'terminals.modelid',
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'operations.client_id',
                    'LEFT JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id = ' . db_prefix() . 'operations.invoice_id',
                    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'contacts.userid',
                    'LEFT JOIN ' . db_prefix() . 'type_operations ON ' . db_prefix() . 'type_operations.id_type = ' . db_prefix() . 'operations.type',
                    'LEFT JOIN ' . db_prefix() . 'operations_dates ON ' . db_prefix() . 'operations_dates.operation_id = ' . db_prefix() . 'operations.id_operation',
                    

                ];
                
            
            $where = [];
            $filter = [];
            include_once(APPPATH . 'views/admin/tables/includes/filter_requests.php');
            if (count($filter) > 0) {
                array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
            }

            // if ($clientid != '') {
            //     array_push($where, 'AND ' . db_prefix() . 'terminals.userid=' . $this->ci->db->escape_str($clientid));
            // }
            
            
            
            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'operations';
            
            $result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where,[db_prefix() . 'operations.id as id',db_prefix() . 'operations.client_id as userid']);
            
            $output  = $result['output'];
            
            $rResult = $result['rResult'];
            //var_dump($rResult); die;
            
            foreach ($rResult as $aRow) {
               //var_dump($rResult); die;
                $row = [];
            
                $row[] = $aRow['id'] ;
                $row[] = $aRow['banks'] ;
                $row[] = $aRow['model'];
                $row[] = $aRow['serial'];
                //$row[] = $aRow['company'];
                $row[] = '<a href="' . admin_url('clients/client/' . $aRow['userid'] . '?group=contacts') . '">' . $aRow['company'];
                $row[] = $aRow['affiliate_code'];
                $row[] = $aRow['terminal'];
                $row[] = $aRow['rif_number'];
                $row[]  = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['invoices_id']) . '">' . format_invoice_number($aRow['invoices_id']) . '</a>';
                $row[] = $aRow['phonenumber'];
                $row[] = $aRow['type'];
                $row[] = $aRow['sale'];
                $row[] = $aRow['payment_confirmed'];
                $row[] = $aRow['bank_management'];
                $row[] = $aRow['account_opened'];
                $row[] = $aRow['send_code_af'];
                $row[] = $aRow['bank_code_af'];
                $row[] = $aRow['serial_assignment'];
                $row[] = $aRow['send_par_loading'];
                $row[] = $aRow['bank_par_loading'];
                $row[] = $aRow['ccrd_answer'];
                $row[] = $aRow['parameterization'];
                $row[] = $aRow['cari_relocated'];
                $row[] = $aRow['ready'];
                $row[] = $aRow['retired'];


            
            
                $row['DT_RowClass'] = 'has-row-options';
                $output['aaData'][] = $row;
            }

            echo json_encode($output);
            die;
        }
        $this->load->view('admin/reports/reports_requests', $data);
    
    }

    public function request_indicators()
    {
        $type = 'indicators';
        if ($this->input->get('type')) {
            $type                       = $type . '_' . $this->input->get('type');
            $data['leads_staff_report'] = json_encode($this->reports_model->leads_staff_report());
        }
        $this->load->model('leads_model');
        $data['statuses']               = $this->leads_model->get_status();
        $data['leads_this_week_report'] = json_encode($this->reports_model->leads_this_week_report());
        $data['requests_activation_report']   = json_encode($this->reports_model->requests_activation_report());
        $this->load->view('admin/reports/' . $type, $data);
    }

    

}
