<div class="col-md-12">
    <?php echo form_open_multipart(admin_url('warehouse/inventory_entry_pdf'), array('id' => 'inventory_entry_repor')); ?>

    <div class="col-md-12 pull-right">
    <div class="col-md-6">
            <input type="checkbox" name="expandir_rango" value="1" onclick="expandir_formulario('rango')">
            <?php echo _l('by_date_range'); ?>
            <div id='rangofecha' style="display: none;">
                <div class="col-md-4">
                    <?php echo render_date_input('from_date', 'from_date', date('Y-m-d', strtotime('-7 day', strtotime(date('Y-m-d'))))); ?>
                </div>

                <div class="col-md-4">
                    <?php echo render_date_input('to_date', 'to_date', date('Y-m-d')); ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <input type="checkbox" name="expandir_almacen" value="1" onclick="expandir_formulario('almacen')">
            <?php echo _l('by_warehouse'); ?>
            <div id="almacen" style="display: none;">
                <div class="col-md-4">
                    <?php echo render_select('warehouse_id', $warehouses, array('warehouse_id', array('warehouse_code', 'warehouse_name')),'_warehouse'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-2 button-pdf-margin-top">
            <div class="form-group">
                <div class="btn-group">
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-pdf-o"></i><?php if (is_mobile()) {
                                                                                                                                                                                echo ' PDF';
                                                                                                                                                                            } ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="hidden-xs"><a href="?output_type=I" target="_blank" onclick="stock_submit(this); return false;"><?php echo _l('download_pdf'); ?></a></li>

                    </ul>
                     <br>                                                                                                                                                       
                    <a href="#" onclick="get_data_inventory_release_report(); return false;" style="margin-top: 1em;" class="btn btn-info display-block button-pdf-margin-top"><?php echo _l('ok'); ?></a>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            
        </div>

    </div>

    <?php echo form_close(); ?>
</div>
<hr class="hr-panel-heading" />
<div class="col-md-12" id="report">
    <div class="panel panel-info col-md-12 panel-padding">

        <div class="panel-body" id="inventory_entry_report">
            <p>
                <h3 class="bold text-center"><?php echo mb_strtoupper(_l('inventory_release_report')); ?></h3>
            </p>
            </div-->
            
            <div id="inventaryRelease" class="table-responsive">
                <table class="table table-striped" id="tblIinventaryRelease">
                    <thead>
                        <tr>
                            
                            <th colspan="1"><?php echo _l('goods_delivery_code') ?></th>
                            <th  colspan="1"><?php echo _l('customer_code') ?></th>
                            <th  colspan="1"><?php echo _l('customer_name') ?></th>
                            <th  colspan="2"><?php echo _l('to') ?></th>
                            <th  colspan="2"><?php echo _l('address') ?></th>
                            <th  colspan="2"><?php echo _l('staff_id') ?></th>
                        </tr>
                    </thead>
                    
                    <tbody class="list">
                        <tr>
                            <td colspan="1" id="goods_delivery_code">.....</td>
                            <td colspan="1" id="customer_code">.....</td>
                            <td colspan="1" id="customer_name">.....</td>
                            <td colspan="2" id="to">.....</td>
                            <td colspan="2" id="address">.....</td>
                            <td colspan="2" id="staff_id">.....</td>
                        </tr>
                    </tbody>
                    
                </table>
                <ul class="pagination"></ul>

            </div>


        </div>
    </div>
    <?php require 'modules/warehouse/assets/js/inventory_release_report.php'; ?>

</div>