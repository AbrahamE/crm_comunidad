<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
   <div class="content">
      <div class="row">
         <div class="_filters _hidden_inputs">
            <?php
            // foreach($statuses as $_status){
            // $val = '';
            //  if($_status == $this->input->get('status')){
            //   $val = $_status;
            //  }
            //  echo form_hidden('proposals_'.$_status,$val);
            // }
            // foreach($years as $year){
            //    echo form_hidden('year_'.$year['year'],$year['year']);
            // }
            // foreach($proposals_sale_agents as $agent){
            //  echo form_hidden('sale_agent_'.$agent['sale_agent']);
            // }
            // echo form_hidden('leads_related');
            // echo form_hidden('customers_related');
            // echo form_hidden('expired');
            ?>
         </div>
         <div class="col-md-12">
            <div class="panel_s mbot10">
               <div class="panel-body _buttons">
                  <?php if (has_permission('proposals', '', 'create')) { ?>
                     <a href="<?php echo admin_url('requests/requests'); ?>" class="btn btn-info pull-left display-block">
                        <?php echo _l('new_requests'); ?>
                     </a>
                  <?php } ?>
                  <!-- <a href="<?php echo admin_url('proposals/pipeline/' . $switch_pipeline); ?>" class="btn btn-default mleft5 pull-left hidden-xs"><?php echo _l('switch_to_pipeline'); ?></a> -->
                  <!-- <div class="display-block text-right">
                     <div class="btntable table-requests dataTable no-footer dtr-inline collapsed-group pull-right mleft4 btn-with-tooltip-group _filter_data" data-toggle="tooltip" data-title="<?php echo _l('filter_by'); ?>">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        </button>
                        <ul class="dropdown-menu width300">
                           <li>
                              <a href="#" data-cview="all" onclick="dt_custom_view('','.table-requests',''); return false;">
                              <?php echo _l('proposals_list_all'); ?>
                              </a>
                           </li>
                           <li class="divider"></li>
                           <?php foreach ($statuses as $status) { ?>
                           <li class="<?php if ($this->input->get('status') == $status) {
                                          echo 'active';
                                       } ?>">
                              <a href="#" data-cview="proposals_<?php echo $status; ?>" onclick="dt_custom_view('proposals_<?php echo $status; ?>','.table-requests','proposals_<?php echo $status; ?>'); return false;">
                              <?php echo format_proposal_status($status, '', false); ?>
                              </a>
                           </li>
                           <?php } ?>
                           <?php if (count($years) > 0) { ?>
                           <li class="divider"></li>
                           <?php foreach ($years as $year) { ?>
                           <li class="active">
                              <a href="#" data-cview="year_<?php echo $year['year']; ?>" onclick="dt_custom_view(<?php echo $year['year']; ?>,'.table-requests','year_<?php echo $year['year']; ?>'); return false;"><?php echo $year['year']; ?>
                              </a>
                           </li>
                           <?php } ?>
                           <?php } ?>
                           <?php if (count($proposals_sale_agents) > 0) { ?>
                           <div class="clearfix"></div>
                           <li class="divider"></li>
                           <li classtable table-requests dataTable no-footer dtr-inline collapsed="dropdown-submenu pull-left">
                              <a href="#" tabindex="-1"><?php echo _l('sale_agent_string'); ?></a>
                              <ul class="dropdown-menu dropdown-menu-left">
                                 <?php foreach ($proposals_sale_agents as $agent) { ?>
                                 <li>
                                    <a href="#" data-cview="sale_agent_<?php echo $agent['sale_agent']; ?>" onclick="dt_custom_view('sale_agent_<?php echo $agent['sale_agent']; ?>','.table-requests','sale_agent_<?php echo $agent['sale_agent']; ?>'); return false;"><?php echo get_staff_full_name($agent['sale_agent']); ?>
                                    </a>
                                 </li>
                                 <?php } ?>
                              </ul>
                           </li>
                           <?php } ?>
                           <div class="clearfix"></div>
                           
                        </ul>
                     </div>
                     <a href="#" class="btn btn-default btn-with-tooltip toggle-small-view hidden-xs" onclick="toggle_small_view('.table-requests','#requests'); return false;" data-toggle="tooltip" title="<?php echo _l('invoices_toggle_table_tooltip'); ?>"><i class="fa fa-angle-double-left"></i></a>
                  </div> -->
               </div>
            </div>

            <!-- fillter -->

            <div class="row">
               <div class="col-md-12">
                  <div class="panel_s">
                     <div class="panel-body">
                        <div class="row">
                           <div class="col-md-12">
                              <p class="bold"><?php echo _l('filter_by'); ?></p>
                           </div>
                           <div class="col-md-4 leads-filter-column">
                              <?php echo render_select('company', $requests, ['company', 'company'], '', '', array('data-width' => '100%', 'data-none-selected-text' => _l('client')), array(), 'no-mbot'); ?>
                           </div>
                                   
                           <div class="col-md-4 leads-filter-column">
                              <?php echo render_select('type', $type, ['name','name'], '', '', array('data-width' => '100%', 'data-none-selected-text' => _l('Tipo de operación')), array(), 'no-mbot'); ?>
                           </div>

                           <div class="col-md-4 leads-filter-column">
                              <?php echo render_select('status_name', $status_reques, ['name', 'name'], '', '', array('data-width' => '100%', 'data-none-selected-text' => _l('Tipo de gestión')), array(), 'no-mbot'); ?>
                           </div>


                        </div>
                     </div>
                  </div>
               </div>
            </div>


            <!-- end fillter -->


            <div class="row">
               <div class="col-md-12" id="small-table">
                  <div class="panel_s">
                     <div class="panel-body">
                        <!-- if invoiceid found in url -->
                        <?php echo form_hidden('requests_id', $requests_id); ?>
                        <?php
                        $table_data = array(
                           _l('ID ') . ' #',
                           _l('client'),
                           _l('Tipo de operación'),
                           _l('Estado'),
                           _l('Fecha de Registro'),
                           _l('Alerta'),
                           _l('Ultima Actualización'),
                           _l('rif'),
                           _l('invoice'),
                           _l('phonenumber'),
                           _l('status_requests_sale'),
                           _l('status_requests_payment_confirmation'),
                           _l('status_requests_bank_management'),
                           _l('status_requests_account_opened'),
                           _l('status_requests_affiliate_code_submission'),
                           _l('status_requests_bank affiliate code'),
                           _l('status_requests_assign_serial'),
                           _l('status_requests_shipment_freight_parameter'),
                           _l('status_requests_send_bank'),
                           _l('status_requests_answer_credicard'),
                           _l('status_requests_parameterization'),
                           _l('status_requests_cary_transfer'),
                           _l('status_requests_ready'),
                           _l('status_requests_retired'),
                        );

                           render_datatable(
                              $table_data,
                              'requests',
                              ['customizable-table'],
                              [
                                 'id' => 'table-requests',
                                 'data-last-order-identifier' => 'requests_table',
                                 'data-default-order'         => get_table_last_order('requests'),
                              ]
                           );
                        ?>
                     </div>
                  </div>
               </div>
               <div class="col-md-7 small-table-right-col">
                  <div id="requests" class="hide">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('admin/includes/modals/sales_attach_file'); ?>
<script>
   var hidden_columns = [4, 5, 6, 7];
</script>
<?php init_tail(); ?>


<div id="requests_data"></div>
<div id="requests_convet_data"></div>

<script>
   var requests_id;

   (function() {
      let table_requestes = $('table.table-requests');
      let LeadsServerParams = {
         "company": "[name='company']",
         "status_name": "[name='status_name']",
         "type": "[name='type']",
      };

      if (table_requestes.length) {

         var tableLeadsConsentHeading = table_requestes.find('#th-consent');
         var leadsTableNotSortable = [0];
         var leadsTableNotSearchable = [0, table_requestes.find('#th-assigned').index()];

         if (tableLeadsConsentHeading.length > 0) {
            leadsTableNotSortable.push(tableLeadsConsentHeading.index());
            leadsTableNotSearchable.push(tableLeadsConsentHeading.index());
         }

         _table_api = initDataTable(
            table_requestes,
            admin_url + 'requests/table',
            leadsTableNotSearchable,
            leadsTableNotSortable,
            LeadsServerParams,
            [6, 'desc']
         );
         init_requests();

         if (_table_api && tableLeadsConsentHeading.length > 0) {
            _table_api.on('draw', function() {
               var tableData = table_requestes.find('tbody tr');
               $.each(tableData, function() {
                  $(this).find('td:eq(3)').addClass('bg-light-gray');
               });
            });
         }

         $.each(LeadsServerParams, function(i, obj) {
            $('select' + obj).on('change', function(event) {
               console.log(event.target.value);
               table_requestes.DataTable().ajax.reload().columns.adjust().responsive.recalc();
            });
         });
      }

   })();
</script>

<?php $this->load->view('admin/requests/requests_js'); ?>

</body>

</html>