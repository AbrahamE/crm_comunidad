<?php

defined('BASEPATH') or exit('No direct script access allowed');

include_once(__DIR__ . '/App_pdf.php');

class Incidence_pdf extends App_pdf
{
    protected $id;

    public function __construct($userid,$ticketid)
    {
       
        parent::__construct();

        $this->id       = $userid;
        $this->ticketid       = $ticketid;
        
    }

    public function prepare()
    {
        
        $data['sql_contact'] = $this->ci->tickets_model->sql_contact($this->id);
        $data['terminals'] = $this->ci->tickets_model->terminals($this->id,$this->ticketid );
        $data['incidence_1'] = $this->ci->tickets_model->incidence($this->id);
        $data['resumetickets'] = json_decode($data['terminals'][0]['resumetickets']);
        
        //var_dump($data['resumetickets']);die;
       
        $this->set_view_vars($data);

        return $this->build();
    }

    protected function type()
    {
        return 'incidence';
    }

    protected function file_path()
    {
        $customPath = APPPATH . 'views/themes/' . active_clients_theme() . '/views/my_invoicepdf.php';
        $actualPath = APPPATH . 'views/themes/' . active_clients_theme() . '/views/incidencepdf.php';

        if (file_exists($customPath)) {
            $actualPath = $customPath;
        }

        return $actualPath;
    }

    
}
