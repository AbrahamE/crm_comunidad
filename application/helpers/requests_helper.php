<?php

defined('BASEPATH') or exit('No direct script access allowed');


/**
 * funcion para estilo de estatus de activacion
 */
function format_requests_status($status, $classes = '', $label = true)
{
    $id = $status;
    if ($status == 1) {
        $status      = _l('status_requests_sale');
        $label_class = 'success';
    } elseif ($status == 2) {
        $status      = _l('status_requests_payment_confirmation');
        $label_class = 'success';
    } elseif ($status == 3) {
        $status      = _l('status_requests_bank_management');
        $label_class = 'success';
    } elseif ($status == 4) {
        $status      = _l('status_requests_account_opened');
        $label_class = 'success';
    } elseif ($status == 5) {
        $status      = _l('status_requests_affiliate_code_submission');
        $label_class = 'success';
    } elseif ($status == 6) {
        $status      = _l('status_requests_bank affiliate code');
        $label_class = 'success';
    } elseif ($status == 7) {
        $status      = _l('status_requests_assign_serial');
        $label_class = 'success';
    } elseif ($status == 8) {
        $status      = _l('status_requests_shipment_freight_parameter');
        $label_class = 'success';
    } elseif ($status == 9) {
        $status      = _l('status_requests_send_bank');
        $label_class = 'success';
    } elseif ($status == 10) {
        $status      = _l('status_requests_answer_credicard');
        $label_class = 'success';
    } elseif ($status == 11) {
        $status      = _l('status_requests_parameterization');
        $label_class = 'success';
    } elseif ($status == 12) {
        $status      = _l('status_requests_cary_transfer');
        $label_class = 'success';
    } elseif ($status == 13) {
        $status      = _l('status_requests_ready');
        $label_class = 'success';
    }  elseif ($status == 14) {
        $status      = _l('status_requests_retired');
        $label_class = 'success';
    } 
    

    if ($label == true) {
        return '<span class="label label-' . $label_class . ' ' . $classes . ' s-status proposal-status-' . $id . '">' . $status . '</span>';
    }

    return $status;
}

/**
 * funcion para estilo de tipo de solicitud
 */
function format_requests_type($status, $classes = '', $label = true)
{
    $id = $status;
    if ($status == 1) {
        $status      = _l('type_requests_inactive');
        $label_class = 'info';
    }elseif ($status == 2) {
        $status      = _l('type_requests_reactivation');
        $label_class = 'default';
    }elseif ($status == 3) {
        $status      = _l('type_requests_razon_social');
        $label_class = 'success';
    }elseif ($status == 4) {
        $status      = _l('type_requests_cambio_banco');
        $label_class = 'success';
    }elseif ($status == 5) {
        $status      = _l('type_requests_uninstallation');
        $label_class = 'danger';
    }elseif ($status == 6) {
        $status      = _l('type_requests_corrective');
        $label_class = 'info';
    }elseif ($status == 7) {
        $status      = _l('type_requests_change_simcard');
        $label_class = 'info';
    } 


    

    if ($label == true) {
        return '<span class="label label-' . $label_class . ' ' . $classes . ' s-status proposal-status-' . $id . '">' . $status . '</span>';
    }

    return $status;
}




/**
*funcion para la abrevicion de activacion
 */

function format_requests_number($id)
{
    return get_option('requests_number_prefix') . str_pad($id, get_option('number_padding_prefixes'), '0', STR_PAD_LEFT);
}



function status_interface_by_days($table = '' ,$where = []){
    $CI = &get_instance();
        $CI->db->select('status');
        $CI->db->from(db_prefix() . $table);
        $CI->db->where($where);
    return (array) $CI->db->get()->row();
}

function status_interface_selection_by_days($table = '' , $select = '*' , $where = []){
    $CI = &get_instance();
        $CI->db->select($select);
        $CI->db->from(db_prefix() . $table);
        $CI->db->where($where);
    return (array) $CI->db->get()->row();    
}

function type_of_table_depending_on_the_case($options = []){ // Funcion que te retorna los dias de la tabla segun el caso

    $CI = &get_instance();
    $CI->load->model($options['model']);

    $status = status_interface_by_days($options['table'], $options['where']);
    $campo = [
        'requests_model' => $options['model'] == 'requests_model'
            ? $CI->requests_model->fields_of_operation_days($status["status"]) 
                : 'modification_date', 
        'tickets_model' => 'modification_date', 
        'departments_model' => $options['model'] == 'departments_model'
            ? $CI->departments_model->fields_of_operation_days($status["status"]) 
                : 'modification_date'
    ];
           
    $day = status_interface_selection_by_days(
        $options['where_two_table'], 
        $campo[
            $options['model']
        ],
        $options['where_two'],
    );

    $fecha_one = new DateTime($day[
        $campo[$options['model']]
    ]);

    $fecha_two = new DateTime($options['today']);
    $diff = $fecha_one->diff($fecha_two, TRUE);
    $diff_one = $diff->days .' Dias';

    if($status['status'] == '14'){

        $day = status_interface_selection_by_days(
            $options['where_two_table'], 
            '*' ,
            $options['where_three']
        );

        $fechaIni = new DateTime($day['sale']);
        $fechaFin = new DateTime($day['retired']);
        $diff = $fechaIni->diff($fechaFin);
        $diff_two = $diff->days .' Dias';

        return [
            'diff' => $diff,
            'diff_one' => $diff_one,
            'diff_two' => $diff_two,
            'status' => $status['status'],
        ];
    }

    return [
        'diff' => $diff,
        'diff_one' => $diff_one,
        'diff_two' => false,
        'status' => $status['status'],
    ];
}

function requests_day($id_operation, $dias, $classes = '', $table = ''){
    $today = date('Y-m-d');
    
    if (!empty($table)){

        $_Closure = [
            'operations' => function() use($id_operation, $today, $table){
                return type_of_table_depending_on_the_case([
                    'table' => $table,
                    'today' => $today,
                    'model' => 'requests_model',
                    'where' => ['id_operation' => $id_operation],
                    'where_two_table' => 'operations_dates',
                    'where_two' => ['operation_id' => $id_operation],
                    'where_three' => ['operation_id' => $id_operation],
                ]);
            },
            'tickets' => function() use($id_operation, $today, $table){
                return type_of_table_depending_on_the_case([
                    'table' => $table,
                    'today' => $today,
                    'model' => 'tickets_model',
                    'where' => ['ticketid' => $id_operation],
                    'where_two_table' => 'tickets',
                    'where_two' => ['ticketid' => $id_operation],
                    'where_three' => ['ticketid' => $id_operation],
                ]);
            },
            'department' => function() use($id_operation, $today, $table){
                return type_of_table_depending_on_the_case([
                    'table' => $table,
                    'today' => $today,
                    'model' => 'departments_model',
                    'where' => ['ticketid' => $id_operation],
                    'where_two_table' => 'departments_status',
                    'where_two' => ['id_ticket' => $id_operation],
                    'where_three' => ['id_ticket' => $id_operation],
                ]);
            },
        ];

        $alert = $_Closure[$table]();
    
        if(@$alert['status'] == '14'){
            $label_class = 'info';
            return '<span class="label label-' . $label_class . ' ' . $classes . '">' . @$alert['diff_two'] . '</span>';
        }else{
            if (@$alert['diff']->days > $dias){
                $label_class = 'danger';
                return '<span class="pulse label label-' . $label_class . ' ' . $classes . '">' . @$alert['diff_one'] . '</span>';
                
            }else{
                $label_class = 'info';
                return '<span class="label label-' . $label_class . ' ' . $classes . '">' . @$alert['diff_one'] . '</span>';
            }
        }    
            

    } else{
        echo json_encode(['mesage' => 'nesesitar indicar la tabla en la cual se va a trabajar']);
    }
   
}







 




