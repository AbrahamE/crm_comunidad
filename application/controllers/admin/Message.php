<?php

use app\services\utilities\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Message extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('message_model');
    }

    /* Open also all taks if user access this /tasks url */
    public function index($id = '')
    {
        $this->list_message($id);
    }

    /* List all tasks */
    public function list_message($id = '')
    {
        close_setup_menu();
        // If passed from url
        $data['custom_view'] = $this->input->get('custom_view') ? $this->input->get('custom_view') : '';
        $data['taskid']      = $id;

        if ($this->input->get('kanban')) {
            $this->switch_kanban(0, true);
        }

        $data['switch_kanban'] = false;
        $data['bodyclass']     = 'tasks-page';

        if ($this->session->userdata('tasks_kanban_view') == 'true') {
            $data['switch_kanban'] = true;
            $data['bodyclass']     = 'tasks-page kan-ban-body';
        }

        $data['title'] = _l('message');
        $this->load->view('admin/message/manage', $data);
    }

    public function table()
    {
        
        $this->app->get_table_data('message');
    }

   



    /* Add new task or update existing */
    public function message($id = '')
    {
        
        $data = [];

        if ($this->input->get('start_date')) {
            $data['start_date'] = $this->input->get('start_date');
        }
        
        if ($this->input->post()) {
            $data                = $this->input->post();
            $data['description'] = html_purify($this->input->post('description', false));
            if ($id == '') {
                $id      = $this->message_model->add($data);
                
               // echo "<pre>"; print_r ($id); die;
                
                $_id     = false;
                $success = false;
                $message = '';
                if ($id) {
                    $success       = true;
                    $_id           = $id;
                    $message       = _l('added_successfully', _l('message'));
                }
                echo json_encode([
                    'success' => $success,
                    'id'      => $_id,
                    'message' => $message,
                ]);
            } else {
                
                $success = $this->message_model->update($data, $id);
                $message = '';
                if ($success) {
                    $message = _l('updated_successfully', _l('message'));
                }
                echo json_encode([
                    'success' => $success,
                    'message' => $message,
                    'id'      => $id,
                ]);
            }
            die;
        }


        $data['id']    = $id;
        $this->load->view('admin/message/message', $data);
    }


   

    /**
     * Task ajax request modal
     * @param  mixed $taskid
     * @return mixed
     */
    public function get_message_data($messageid, $return = false)
    {
        $message_where = [];

        $message = $this->message_model->get($messageid, $message_where);
         //echo "<pre>"; print_r($message); die;

        $data['checklistTemplates'] = $this->tasks_model->get_checklist_templates();
        $data['task']               = $message;
        $data['id']                 = $message->id;
        
        if ($return == false) {
            $this->load->view('admin/message/view_message_template', $data);
        } else {
            return $this->load->view('admin/message/view_message_template', $data, true);
        }
    }



    /* Add new task comment / ajax */
    public function add_message_comment()
    {
        $data            = $this->input->post();
        //echo "<pre>"; print_r($data); die;
        $data['content'] = html_purify($this->input->post('content', false));
        if ($this->input->post('no_editor')) {
            $data['content'] = nl2br($this->input->post('content'));
        }
        $comment_id = false;
        if (
            $data['content'] != ''
            || (isset($_FILES['file']['name']) && is_array($_FILES['file']['name']) && count($_FILES['file']['name']) > 0)
        ) {
            $comment_id = $this->message_model->add_message_comment($data);
            if ($comment_id) {
                $commentAttachments = handle_task_attachments_array($data['taskid'], 'file');
                if ($commentAttachments && is_array($commentAttachments)) {
                    foreach ($commentAttachments as $file) {
                        $file['task_comment_id'] = $comment_id;
                        $this->misc_model->add_attachment_to_database($data['taskid'], 'task', [$file]);
                    }

                    if (count($commentAttachments) > 0) {
                        $this->db->query('UPDATE ' . db_prefix() . "task_comments SET content = CONCAT(content, '[task_attachment]')
                            WHERE id = " . $this->db->escape_str($comment_id));
                    }
                }
            }
        }
        echo json_encode([
            'success'  => $comment_id ? true : false,
            'taskHtml' => $this->get_message_data($data['taskid'], true),
        ]);
    }


    public function get_message_by_id($id)
    {
        if ($this->input->is_ajax_request()) {
            $tasks_where = [];
            // if (!staff_can('view', 'tasks')) {
            //     $tasks_where = get_tasks_where_string(false);
            // }
            $message = $this->message_model->get($id, $tasks_where);
            if (!$message) {
                header('HTTP/1.0 404 Not Found');
                echo 'Message not found';
                die();
            }
            echo json_encode($message);
        }
    }


}
