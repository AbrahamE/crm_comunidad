<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="ticket-terminal-modal_" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('clients/terminal_add/'.$customer_id.'/'.$contactid),array('id'=>'ticket-terminal-formd')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="add-title"><?php echo _l('terminal_edit'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                <input type="hidden" name="requiere_simcard" id="requiere_simcard"  value=""/>  

                    <div class="col-md-12" style='display: none'>
                        <div id="additional"></div>
                        <?php echo render_input('userid','codigó de afiliado', $userid ,'text',array('required'=>true)); ?>
                    </div> 
                    <div class="col-md-12 hidden">
                        <div id="additional"></div>
                        <?php $codaffiliate=( isset($terminal) ? $terminal['codaffiliate'] : ''); ?>
                        <?php echo render_input('codaffiliate_user','Codigó de afiliado',$codaffiliate,'text',array('required'=>true)); ?>
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $serial=( isset($terminal) ? $terminal['serial'] : ''); ?>
                        <?php echo render_input('serial','Serial',$serial,'text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $mac=( isset($terminal) ? $terminal['mac'] : ''); ?>
                        <?php echo render_input('mac','MAC',$mac,'text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $imei=( isset($terminal) ? $terminal['imei'] : ''); ?>
                        <?php echo render_input('imei','IMEI',$imei,'text',array('required'=>true)); ?>
                    </div>
                     <!-- <div class="col-md-12">
                        <div id="additional"> Banco</div>
                        <?php ##echo render_select_terminal('banksid', 'banksid',$banks,array('id','banks'),'Banco',(isset($terminal) ? $terminal['banks'] : ''),array('required'=>'true')); ?>
                    </div>  -->
                    <div class="col-md-12">
                        <div id="additional"> Modelo</div>
                        <?php echo render_select_terminal('modelid', 'modelid',$model,array('modelid','model'),'Banco',(isset($terminal) ? $terminal['modelid'] : ''),array('required'=>'true')); ?>	
                    </div>
                    <div id="div_simcar" class="col-md-12">
                        <div id="additional"></div><br>
                        <label for="serialsimcard"><?php echo _l('sim_card_serial'); ?></label>
                        <!-- <select name="simcard" required="true" id="simcard" class="ajax-search simcard" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <option value=""></option>
                        </select> --> 
                        <?php $simcardvalue=( isset($terminal) ? $terminal['simcard'] : ''); ?>

                        <?php echo render_select_terminal('simcard', 'simcard',$simcard, array('id','serialyoperadora'),$simcardvalue,(isset($terminal) ? $terminal['simcard'] : '')); //array('required'=>'true') ?>	
                    </div>

                   

                    <?php if($userid == 0) :?> 

                        <div class="col-md-12">
                            <div id="additional"></div>
                            <label for="operadoraid"><?php echo _l('Status'); ?></label>
                            <?php echo render_select_terminal('status','status',$status,array('id','name_status'),'ticket_settings_departments',(isset($terminal) ? $terminal["status"] : ''),array('required'=>'true')); ?>
                        </div>
                    
                    <?php endif?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit"  class="btn btn-info"><?php echo _l('submit'); ?></button>
                <!-- <button href="#" onclick="manage_ticket_terminals(); return false;"  class="btn btn-info"><?php echo _l('submit'); ?></button> -->
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>       
    // cuando cierra el modal
    // window.addEventListener('load',
    (function(){
        appValidateForm($('#ticket-terminal-formd'),{name:'required'},manage_ticket_terminals);
        
        $('#ticket-terminal-modal_').on('hidden.bs.modal', function(event) {
            $('#additional').html('');
            $('#ticket-terminal-modal_ input[id="codaffiliate_terminal"]').val("");
            $('#ticket-terminal-modal_ input[name="serial"]').val('');
            $('#ticket-terminal-modal_ input[name="model"]').val('');
            $('#ticket-terminal-modal_ input[name="imei"]').val('');
            $('#ticket-terminal-modal_ input[name="mac"]').val('');
            $('#ticket-terminal-modal_ input[name="operadora"]').val('');

            $('#ticket-terminal-modal_ select[name="simcardid"]').val('');
            $('#ticket-terminal-modal_ button[data-id="simcardid"] .filter-option .filter-option-inner .filter-option-inner-inner').html('Nada seleccionado')

            $('#ticket-terminal-modal_ select[id="banksid"]').val('');
            $('#ticket-terminal-modal_ bunew_terminaltton[data-id="banksid"] .filter-option .filter-option-inner .filter-option-inner-inner').html('Nada seleccionado');
            
            $('#ticket-terminal-modal_ select[id="modelid"]').val('');
            $('#ticket-terminal-modal_ button[data-id="modelid"] .filter-option .filter-option-inner .filter-option-inner-inner').html('Nada seleccionado');

        } );

        validate_requiere_simcard();    

    })();

    function validate_requiere_simcard(){
       // console.log('cambio el modelo de terminal');
        let mArray = <?= $model ? json_encode($model) : []; ?>;
        console.log(mArray);

        mArray.find( m =>  m.modelid == $('#modelid').val() );
        a = mArray.find( m =>  m.modelid == $('#modelid').val() );
        a.conexiontype;
        //console.log('CNECCION TYPE:'+a.conexiontype);  
         
            if (a.conexiontype=='Dial up/Lan'){
               $('#simcard').removeAttr('required');    
               $('#terminal-modal select[id="simcard"]').selectpicker('val','');
               $('#simcard').val('');
               $('#operadoraid').val('');
               $('#terminal-modal select[id="operadoraid"]').selectpicker('val', '');   
               $('.selectpicker').selectpicker('refresh');             
               $("#requiere_simcard").val('NO');
               $("#div_simcar").hide();
               
            }else{
               $("#requiere_simcard").val('SI');
               $('#simcard').prop('required',true);
               $("#div_simcar").show();
            }
            //console.log( 'valor de requiere_simcard:'+ $("#requiere_simcard").val());
        //conexiontype Dial up/Lan
    };


    $("#modelid").change(function() {
            //console.log ('se llam a changeimput por el evento on change de modelo de terminal.');
            validate_requiere_simcard();
    });  


    // );
    function manage_ticket_terminals(form) {
        
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                if($.fn.DataTable.isDataTable('.table-expenses')){
                    $('.table-expenses').DataTable().ajax.reload();
                }
                alert_float('success', response.message);
                $('#ticket-terminal-modal_').modal('hide');
            } else {
                alert_float('danger', response.message);
            }
        });
        return false;
    }

    function new_terminal(){
            //var simcard = <?= $simcardvalue ?>


            $('').modal('show');
            $('.edit-title').addClass('hide');
            //$('#simcard').selectpicker('val', simcard);





            // init_ajax_search_simcard('simcard_available','#simcard.ajax-search',{
            //     tickets_contacts: true,
            //     contact_userid: function(){
            //             // when ticket is directly linked to project only search project client id contacts
            //             var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
            //             if(uid){
            //                 return uid;
            //             } else {
            //                 return '';
            //             }
            //         }
            //     },
            // );
            // $(".simcard .dropdown-menu").click(function() {
            //     var t = $('#simcard').val();
            //     $.post(admin_url + "tickets/ticket_change_data_simcard", { contact_id: t }).done(
            //         function (e) {
            //             res = JSON.parse(e).contact_data;
            //             if (res) {
            //                 console.log(res.operadora);
            //                 $('select[id="operadoraid"]').selectpicker('val', res.operadora);
            //             }
            //         }
            //     )
            // })
        
    }

   

</script>

