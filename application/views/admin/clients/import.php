<?php defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php
$file_header = array();
$file_header[] = _l('Nombre');
$file_header[] = _l('Apellido');
$file_header[] = _l('Email');
$file_header[] = _l('Compañia');

?>

<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div id="dowload_file_sample">


                        </div>

                        <?php if (!isset($simulate)) { ?>
                            <ul>
                                <li class="text-danger">1. La columna con el símbolo "*" es necesaria para ingresar los
                                    registros</li>
                            </ul>
                            <div class="table-responsive no-dt">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <?php
                                            $total_fields = 0;

                                            for ($i = 0; $i < count($file_header); $i++) {
                                                if ($i == 0 || $i == 1 || $i == 4) {
                                            ?>
                                                    <th class="bold"><span class="text-danger">*</span>
                                                        <?php echo html_entity_decode($file_header[$i]) ?> </th>
                                                <?php
                                                } else {
                                                ?>
                                                    <th class="bold"><span class="text-danger">*</span> <?php echo html_entity_decode($file_header[$i]) ?> </th>

                                            <?php

                                                }
                                                $total_fields++;
                                            }

                                            ?>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < 1; $i++) {
                                            echo '<tr>';
                                            for ($x = 0; $x < count($file_header); $x++) {
                                                echo '<td>- </td>';
                                            }
                                            echo '</tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <hr>

                        <?php } ?>

                        <div class="row">
                            <div class="col-md-4">
                                <?php echo form_open_multipart(admin_url('hrm/import_job_p_excel'), array('id' => 'import_form')); ?>
                                <?php echo form_hidden('leads_import', 'true'); ?>
                                <?php echo render_input('file_csv', 'choose_excel_file', '', 'file'); ?>
                                <?php echo render_input('default_pass_all', 'default_pass_clients_import', $this->input->post('default_pass_all')); ?>
                                <div class="form-group">
                                    <button id="uploadfile" type="button" class="btn btn-info import" onclick="return uploadfilecsv();"><?php echo _l('import'); ?></button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group" id="file_upload_response">

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>

<script>
    (function() {
        $("#dowload_file_sample").append('<a href="' + site_url + 'uploads/comunidad/comunidad.xlsx" class="btn btn-primary" ><?php echo _l('Excel de comunidad') ?></a><hr>');
    })()
    function uploadfilecsv() {
        "use strict";

        if (($("#file_csv").val() != '') && ($("#file_csv").val().split('.').pop() == 'xlsx')) {
            var formData = new FormData();
            formData.append("file_csv", $('#file_csv')[0].files[0]);
            formData.append("csrf_token_name", $('input[name="csrf_token_name"]').val());
            formData.append("leads_import", $('input[name="leads_import"]').val());
            formData.append("default_pass_all", $('input[name="default_pass_all"]').val());


            $.ajax({
                url: admin_url + 'community/import',
                method: 'post',
                data: formData,
                contentType: false,
                processData: false

            }).done(function(response) {
                response = JSON.parse(response);
                $("#file_csv").val(null);
                $("#file_csv").change();
                $(".panel-body").find("#file_upload_response").html();

                if ($(".panel-body").find("#file_upload_response").html() != '') {
                    $(".panel-body").find("#file_upload_response").empty();
                };

                if (response.total_rows) {
                    $("#file_upload_response").append("<h4><?php echo _l("_Result ") ?></h4><h5><?php echo _l('import_line_number') ?> :" + response.total_rows + " </h5>");
                }

                if (response.total_row_success) {
                    $("#file_upload_response").append("<h5><?php echo _l('import_line_number_success') ?> :" + response.total_row_success + " </h5>");
                }
                if (response.total_row_false) {
                    $("#file_upload_response").append("<h5><?php echo _l('import_line_number_failed') ?> :" + response.total_row_false + " </h5>");
                }
                if (response.total_row_false > 0) {
                    $("#file_upload_response").append('<a href="' + response.site_url + 'FILE_ERROR_COMMODITY' + response.staff_id + '.xlsx" class="btn btn-warning"  ><?php echo _l('download_file_error ') ?></a>');
                }
                if (response.total_rows < 1) {
                    alert_float('warning', response.message);
                }
            });
            return false;
        } else if ($("#file_csv").val() != '') {
            alert_float('warning', "<?php echo _l('_please_select_a_file') ?>");
        }

    }
</script>