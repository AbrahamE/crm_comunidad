<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <div id="date-range" class="mbot15">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="#" onclick="news_terminal();return false;" class="btn btn-info pull-left display-block mright5"><?php echo _l('new_terminal'); ?> <i class="fa fa-plus"></i></a>
                                   
                                </div>
                            </div>
                        </div>
                        <table class="table dt-table-loading table-expenses">
                            <thead>
                                <tr>
                                    <th><?php echo _l('id'); ?></th>
                                    <th><?php echo _l('id_client'); ?></th>
                                    <th><?php echo _l('Cliente'); ?></th>
                                    <th><?php echo _l('model'); ?></th>
                                    <th><?php echo _l('affiliate_code'); ?></th>
                                    <th><?php echo _l('serial'); ?></th>
                                    <th><?php echo _l('mac'); ?></th>
                                    <th><?php echo _l('imei'); ?></th>
                                    <th><?php echo _l('simcard'); ?></th>
                                    <th><?php echo _l('operator'); ?></th>
                                    <th><?php echo _l('bank'); ?></th>
                                    <th><?php echo _l('status'); ?></th>

                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php init_tail(); ?>
<div id="terminals_data"></div>
<?php $this->load->view('admin/terminals/terminal-modal', array('banks' => $banks, 'model' => $model, 'sincard' => $simcard, 'operadora' => $operadora )); ?>
<?php  $this->load->view('admin/clients/client_js');?>

<script>
    $(function() {

        var report_from = $('input[name="report-from"]');
        var report_to = $('input[name="report-to"]');
        var filter_selector_helper = '.expenses-filter-year,.expenses-filter-month-wrapper,.expenses-filter-month,.months-divider,.years-divider';

        var Ingenious_ServerParams = {};
        $.each($('._hidden_inputs._filters input'), function() {
            Ingenious_ServerParams[$(this).attr('name')] = '[name="' + $(this).attr('name') + '"]';
        });


        Ingenious_ServerParams['report_from'] = '[name="report-from"]';
        Ingenious_ServerParams['report_to'] = '[name="report-to"]';

        initDataTable('.table-expenses', window.location.href, 'undefined', 'undefined', Ingenious_ServerParams, [0, 'desc']);

        report_from.on('change', function() {
            var val = $(this).val();
            var report_to_val = report_to.val();
            if (val != '') {
                report_to.attr('disabled', false);
                $(filter_selector_helper).removeClass('active').addClass('hide');
                $('input[name^="year_"]').val('');
                $('input[name^="expenses_by_month_"]').val('');
            } else {
                report_to.attr('disabled', true);
            }

            if ((report_to_val != '' && val != '') || (val == '' && report_to_val == '') || (val == '' && report_to_val != '')) {
                $('.table-expenses').DataTable().ajax.reload();
            }

            if (val == '' && report_to_val == '' || report_to.is(':disabled') && val == '') {
                $(filter_selector_helper).removeClass('hide');
            }
        });

        report_to.on('change', function() {
            var val = $(this).val();
            if (val != '') {
                $('.table-expenses').DataTable().ajax.reload();
            }
        });


        $('select[name="currencies"]').on('change', function() {
            $('.table-expenses').DataTable().ajax.reload();
        });
    })

</script>
</body>

</html>