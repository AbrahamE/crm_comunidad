<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Requests_model extends App_Model
{
    private $statuses;

    private $copy = false;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('date_management');
        $this->statuses = hooks()->apply_filters(
            'before_set_proposal_statuses', 
            [ 6, 4, 1, 5, 2, 3, 7,]
        );
    }

    
    public function add($data)
    {
        // datos para actualizacion de terminales         
        $userid        = $data['userid'];                
        $bank_id       = isset($data['requests_banks'])? $data['requests_banks']:0;
        $codaffiliate  = isset($data['requests_affiliate_code'])? $data['requests_affiliate_code']:0;
        $numterminal   = isset($data['num_terminal'])? $data['num_terminal']:NULL;    
        $terminal      = !empty($data['terminal'])? $data['terminal']: NULL;
        $banks_reason_change   = isset($data['requests_banks_reason_change'])? $data['requests_banks_reason_change']:'';
       
        $type = $data['type_operation'];
        //pasar $data['date'], de tipo dato string a fecha. 
        $datecreated = date('Y-m-d');
        $id_operation = $this->db->query('SELECT id_operation, CASE WHEN id_operation > 0 THEN MAX(id_operation) + 1 ELSE 1 END AS id FROM  '. db_prefix() . 'operations')->row();

        switch ($type) {
            case 1: // activacion
                $this->db->insert(db_prefix() . 'operations', [
                    'id_operation' => $id_operation->id,
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'registration_date'  =>  $datecreated, // 
                    'affiliate_code'  =>  $data['requests_affiliate_code'],
                    'bank_id'  =>  $data['requests_banks'],
                    'terminal'  =>  $data['requests_serial_install'],
                    'num_terminal'  =>  $data['num_terminal'],
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],

                    
                ]);
                $serial = $data['requests_serial_install'];
                $userid = $data['userid'];
                $bank_id = $data['requests_banks'];
              
                $this->add_terminals($userid,$serial,$bank_id,$codaffiliate,$numterminal);
                break;
            case 2: // reactivacion
                $this->db->insert(db_prefix() . 'operations', [
                    'id_operation' => $id_operation->id,
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'registration_date'  =>  $datecreated,
                    'affiliate_code'  =>  $data['requests_affiliate_code'],
                    'bank_id'  =>  $data['requests_banks'],
                    'terminal'  =>  $data['terminal'],
                    'num_terminal'  =>  $data['num_terminal'],
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],
                     
                ]);
                // asociar 
                if (!empty($data['terminal'])){
                    $serial = $data['terminal'];
                    $this->update_terminal($serial,$userid,$bank_id,$codaffiliate,$numterminal,1);
                } 
                break;
                //agrgar update en la tabla terminal
            case 3: // cambio de razon social
                $this->db->insert(db_prefix() . 'operations', [
                    'id_operation' => $id_operation->id,
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'registration_date'  => $datecreated,
                    'affiliate_code'  =>  $data['requests_affiliate_code'],
                    'bank_id'  =>  $data['requests_banks'],
                    'terminal'  =>  $data['terminal'],
                    'num_terminal'  =>  $data['num_terminal'],
                    'rif_reason_change'       => $data['type_rif']. $data['rif_reason_change'],
                    'business_name_reason_change'       =>  $data['business_name_reason_change'],
                    'phone_reason_change'  =>  $data['phone_reason_change'],
                    'affiliate_code_reason_change'  =>  $data['affiliate_code_reason_change'],
                    'requests_banks_reason_change'  =>  $data['requests_banks_reason_change'],
                    'terminal_reason_change'  =>  $data['terminal_reason_change'] ?? $data['num_terminal_reason_change'],
                    'num_terminal_reason_change'  =>  $data['num_terminal_reason_change'] ?? $data['terminal_reason_change'],
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],
                ]);
                $this->add_clients($data);
                break;
            case 4: // cambio de banco
                $this->db->insert(db_prefix() . 'operations', [
                    'id_operation' => $id_operation->id,
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'registration_date'  =>  $datecreated,
                    'affiliate_code'  =>  $data['requests_affiliate_code'],
                    'bank_id'  =>  $data['requests_banks'],
                    'terminal'  =>  $data['terminal'],
                    'num_terminal'  =>  $data['num_terminal'],
                    'rif_bank_change'       =>  $data['rif_bank_change'],
                    'business_name_bank_change'       =>  $data['business_name_bank_change'],
                    'phone_bank_change'  =>  $data['phone_bank_change'],
                    'affiliate_code_bank_change'  =>  $data['affiliate_code_bank_change'],
                    'requests_banks_change'  =>  $data['requests_banks_change'],
                    'terminal_banks_change'  =>  $data['terminal'],
                    'num_terminal_banks_change'  =>  $data['num_terminal_banks_change'],
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],
                ]); 
                $this->change_bank($data);
                break;
            case 5: // desintalacion
                $this->db->insert(db_prefix() . 'operations', [
                    'id_operation' => $id_operation->id,
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'registration_date'  =>  $datecreated,
                    'affiliate_code'  =>  $data['requests_affiliate_code'],
                    'bank_id'  =>  $data['requests_banks'],
                    'terminal'  =>  $data['terminal'],
                    'num_terminal'  =>  $data['num_terminal'],
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],
                     
                ]);
                $this->uninstall_terminal($data);
                break;
            case 6: //correctivo
                $this->db->insert(db_prefix() . 'operations', [
                    'id_operation' => $id_operation->id,
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'registration_date'  =>  $datecreated,
                    'affiliate_code'  =>  $data['requests_affiliate_code'],
                    'bank_id'  =>  $data['requests_banks'],
                    'num_terminal'  =>  $data['num_terminal'],
                    'terminal_saliente'  =>  $data['terminal_saliente'],
                    'terminal_entrante'  =>  $data['terminal_entrante'],
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],
                     
                ]);
                $this->associate_terminal_saliente($data);
                $this->associate_terminal_entrante($data);
               break;
            case 7: // cambio de simcard
               $this->db->insert(db_prefix() . 'operations', [
                   'id_operation' => $id_operation->id,
                   'client_id'       =>  $data['userid'],
                   'type'       =>  $data['type_operation'],
                   'status'  =>  $data['requests_status'],
                   'registration_date'  =>  $datecreated,
                   'affiliate_code'  =>  $data['requests_affiliate_code'],
                   'bank_id'  =>  $data['requests_banks'],
                   'terminal'  =>  $data['terminal_simcard'],
                   'num_terminal'  =>  $data['num_terminal'],
                   'observation'  =>  $data['observation'],
                   'chanel'  =>  $data['chanel'],
                    
               ]);
               $this->change_simcard($data);
             break;             

        }
        //se invierte el formato de la fecha proviniente del formulario para que sea compatible con el formato de la bd   
        $date= $this->format_date ($data['date']);
        // $campo= $this->get_date_name_2($data['requests_status']);
        $campo = $this->fields_of_operation_days($data['requests_status']);  
          
        $this->add_operation_date_2($id_operation->id,$campo,$date);

        return true;
    }

    public function update($data)
    {

        
        $type = $data['type_operation'];
        $datecreated = date('Y-m-d');  
      
       // datos para actualizacion de terminales         
        $userid        = $data['userid'];                
        $bank_id       = isset($data['requests_banks'])? $data['requests_banks']:0;
        $codaffiliate  = isset($data['requests_affiliate_code'])? $data['requests_affiliate_code']:0;
        $numterminal   = isset($data['num_terminal'])? $data['num_terminal']:NULL;    
        $terminal      = !empty($data['terminal'])? $data['terminal']: NULL;
        $banks_reason_change   = isset($data['requests_banks_reason_change'])? $data['requests_banks_reason_change']:'';
        
        $old_data = (array) $this->get($data["request_id"]);

        //datos anterriores
        $old_userid   = $old_data['userid'];                    
        $old_bank_id  = isset($old_data['bank_id'])? $old_data['bank_id']:0;
        $old_codaffiliate  = isset($old_data['affiliate_code'])? $old_data['affiliate_code']:0;
        $old_numterminal   = !empty($old_data['num_terminal'])? $old_data['num_terminal']:NULL;    
        $old_serial   =isset($old_data['terminal'])? $old_data['terminal']: '';
        $old_banks_reason_change   = isset($dold_data['requests_banks_reason_change'])? $old_data['requests_banks_reason_change']:'';
        

        switch ($type) {
            case 1: // activacion            
                $Dataentrante = array(
                    'client_id'    =>  $data['userid'],
                    'type'         =>  $data['type_operation'],
                    'status'       =>  $data['requests_status'],// $data['Estatus'],
                    'affiliate_code'  =>  $codaffiliate,
                    'bank_id'       => $bank_id,
                    'terminal'      => $terminal,
                    'num_terminal'  => $numterminal,
                    'modified_date' => $datecreated,
                    'observation'   =>  $data['observation'],
                    'chanel'        =>  $data['chanel'],      
                );
                $this->db->where('id', $data['request_id']);
                $this->db->update(db_prefix() .'operations', $Dataentrante);

                //en caso de que la terminal cambie, se elimina la asociacion de la terminal original y se asosia la terminal seleccionada
                //desasociar          
                if (!empty($old_data['terminal'])){
                    $serial_old=$old_data['terminal'];
                    $this->update_terminal($serial_old,0,0,0,NULL,3);
                }  
                // asociar 
                if (!empty($data['terminal'])){
                    $serial = $data['terminal'];
                    $this->update_terminal($serial,$userid,$bank_id,$codaffiliate,$numterminal,1);
                }                           
                break;
            case 2: // reactivacion
                $Dataentrante = array(
                    'client_id'    =>  $data['userid'],
                    'type'         =>  $data['type_operation'],
                    'status'       =>  $data['requests_status'],// $data['Estatus'],
                    'affiliate_code'  =>  $codaffiliate,
                    'bank_id'       => $bank_id,
                    'terminal'      => $terminal,
                    'num_terminal'  => $numterminal,
                    'modified_date' => $datecreated,
                    'observation'   =>  $data['observation'],
                    'chanel'        =>  $data['chanel'],       
                );
                $this->db->where('id', $data['request_id']);
                $this->db->update(db_prefix() .'operations', $Dataentrante);

                //en caso de que la terminal cambie, se elimina la asociacion de la terminal original  y se deja como desisnstalada. y se asosia la terminal seleccionada
                if (!empty($old_data['terminal'])){                    
                    $this->update_terminal($old_serial,'',$old_bank_id,'','',2);
                }  
                // asociar 
                if (!empty($data['terminal'])){
                    $serial = $data['terminal'];
                    $this->update_terminal($serial,$userid,$bank_id,$codaffiliate,$numterminal,1);
                }   
 
            case 3: // cambio de razon social
                
                $Dataentrante = array(
                    'client_id'    =>  $data['userid'],
                    'type'         =>  $data['type_operation'],
                    'status'       =>  $data['requests_status'],// $data['Estatus'],
                    'affiliate_code'  =>  $codaffiliate,
                    'bank_id'       => $bank_id,
                    'terminal'      => $terminal,
                    'num_terminal'  => $numterminal,
                    'rif_reason_change'       => $data['type_rif']. $data['rif_reason_change'],
                    'business_name_reason_change'       =>  $data['business_name_reason_change'],
                    'phone_reason_change'  =>  $data['phone_reason_change'],
                    'affiliate_code_reason_change'  =>  $data['affiliate_code_reason_change'],
                    'requests_banks_reason_change'  =>  $banks_reason_change,
                    'terminal_reason_change'  =>  $terminal,
                    'num_terminal_reason_change'  =>  $data['num_terminal_reason_change'],
                    'modified_date' => $datecreated, 
                    'observation'  =>  $data['observation'],  
                    'chanel'       =>  $data['chanel'],
                );         
                $this->db->where('id', $data['request_id']);
                $this->db->update(db_prefix() .'operations', $Dataentrante);

                //en caso de que la terminal cambie, se elimina la asociacion de la terminal original  
                //y se deja como INSTALADA y se asosia la terminal seleccionada  al NUEVO RAZON SOCIAL
                if (!empty($old_data['terminal'])){
                    $serial_old=$old_data['terminal'];
                    $this->update_terminal($serial_old,$old_userid,$old_bank_id,$old_codaffiliate,$old_numterminal,1);
                }  

                $this->update_clients_and_terminal($data);              
                break;
               
            case 4: // cambio de banco
                $Dataentrante = array(
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'registration_date'  =>  $data['date'],
                    'affiliate_code'  =>  $codaffiliate,
                    'bank_id'  =>  $bank_id,
                    'terminal'  =>  $terminal,
                    'num_terminal'  =>  $numterminal,
                    'rif_bank_change'       =>  $data['rif_bank_change'],
                    'business_name_bank_change'       =>  $data['business_name'],
                    'phone_bank_change'  =>  $data['phone_bank_change'],
                    'affiliate_code_bank_change'  =>  $data['affiliate_code_bank_change'],
                    'requests_banks_change'  =>  $data['requests_banks_change'],
                    'terminal_banks_change'  =>  $data['terminal'],
                    'num_terminal_banks_change'  =>  $data['num_terminal_banks_change'],
                    'modified_date' => $datecreated, 
                    'observation'  =>  $data['observation'],
                    'chanel'       =>  $data['chanel'],
                ); 
                $this->db->where('id', $data['request_id']);
                $this->db->update(db_prefix() .'operations', $Dataentrante);

                //se reversan los datos de la terminal  original de la operacion en caso de que cambie
                if (!empty($old_data['terminal'])){
                    $this->reverse_change_bank($old_data);
                }
                if (!empty($data['terminal'])){
                    $this->change_bank($data);
                }
                break;

            case 5: // desintalacion
                $Dataentrante = array(
                    'client_id'       =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'affiliate_code'  =>  $codaffiliate,
                    'bank_id'  =>  $bank_id,
                    'terminal'  =>  $terminal,
                    'num_terminal'  =>  $numterminal,
                    'modified_date' => $datecreated,
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],
                     
                );
                $this->db->where('id', $data['request_id']);
                $this->db->update(db_prefix() .'operations', $Dataentrante);

                // reversar la desisnstalacion de la terminal original anterior
                if (!empty($old_data['terminal'])){
                    $serial_old=$old_data['terminal'];
                    $this->update_terminal($serial_old,$old_userid,$old_bank_id,$old_codaffiliate,$old_numterminal,1);
                } 
                // desinstalar la terminal actual 
                if (!empty($data['terminal'])){
                    $this->uninstall_terminal($data);
                }
                
                break;
            
            case 6: //correctivo
                 $Dataentrante = array(
                    'client_id'  =>  $data['userid'],
                    'type'       =>  $data['type_operation'],
                    'status'  =>  $data['requests_status'],
                    'affiliate_code'  =>  $codaffiliate,
                    'bank_id'  =>  $bank_id,
                    'num_terminal'  =>  $numterminal,
                    'terminal_saliente'  =>  $data['terminal_saliente'],
                    'terminal_entrante'  =>  $data['terminal_entrante'],
                    'modified_date' => $datecreated,
                    'observation'  =>  $data['observation'],
                    'chanel'  =>  $data['chanel'],
                     
                );

                $this->db->where('id', $data['request_id']);
                $this->db->update(db_prefix() .'operations', $Dataentrante);

                //REVERSO DEL CORRECTIVO ORIGIAL----------------------------
                // reversar los cambios previos en la terminal entrante original.
                if (!empty($old_data['terminal_entrante'])){
                    $this->reverse_associate_terminal_entrante($old_data);
                } 
                //reversar la terminal saliente original
                if (!empty($old_data['terminal_saliente'])){
                    $this->reverse_associate_terminal_saliente($old_data);
                }

                //GESTION NUEVOS DATOS DE TERMINALES-------------------------
                // se desinstala la terminal actual saliente
                if (!empty($data['terminal_saliente'])){
                    $this->associate_terminal_saliente($data);                
                }
                if (!empty($data['terminal_entrante'])){                   
                    $this->associate_terminal_entrante($data);
                }               
                break;

            case 7: // cambio de simcard
                $Dataentrante = array(
                   'client_id'       =>  $data['userid'],
                   'type'       =>  $data['type_operation'],
                   'status'  =>  $data['requests_status'],
                   'registration_date'  =>  $data['date'],
                   'affiliate_code'  =>  $data['requests_affiliate_code'],
                   'bank_id'  =>  $data['requests_banks'],
                   'terminal'  =>  $data['terminal_simcard'],
                   'num_terminal'  =>  $data['num_terminal'],
                   'modified_date' => $datecreated,  
                   'observation'  =>  $data['observation'],   
                   'chanel'  =>  $data['chanel'],               
                );
                $this->db->where('id', $data['request_id']);
                $this->db->update(db_prefix() .'operations', $Dataentrante);

                if ($data['terminal_simcard']!=''){
                    $this->change_simcard($data);
                }        
               
            break;
        
        }
        //if ($data['requests_status'] != $old_data['status']) {


            
            $data = array(
                'requests_id' =>$data['request_id'],
                'status'      =>  $data['requests_status'], 
                'option'      =>'date_update',
                'date'      =>  $data['date'],
            );
             $this->status_update($data);
        //} 

        return true;
    }


        
        
    /*
     * Update proposal
     * @param  mixed $data $_POST data
     * @param  mixed $id   proposal id
     * @return boolean
     */

                
    public function update_terminal($id,$userid=0,$bank_id=0,$codaffiliate=0,$numterminal=NULL,$status=1)
    {
        
        $Dataentrante = array(
            'userid'       =>  $userid, 
            'codaffiliate' =>  $codaffiliate, 
            'status'       =>  $status,
            'banks'       =>   $bank_id,
            'numterminal'  =>  $numterminal,
        );

        $this->db->where('id', $id);
        $this->db->update(db_prefix() .'terminals', $Dataentrante);
        
    }    

    public function add_operation_date($data)
    {
        $datecreated = date('Y-m-d');
        $this->db->insert(db_prefix() . 'operations_dates', [
                'operation_id'   =>  $data->id,
                'sale'      => $datecreated,
        ]);          
        
    }

    public function add_operation_date_2($id_operation,$campo,$fecha)
    {
        $this->db->insert(db_prefix() . 'operations_dates', [
                'operation_id'   =>  $id_operation,
                    $campo       => $fecha,
        ]);          
        
    }

    public function associate_terminal_entrante($data)
    {
        
        $Dataentrante = array(
            'userid'       =>  $data["userid"], 
            'codaffiliate' =>  $data['requests_affiliate_code'], 
            'status'       =>  '1',
            'numterminal' => $data['num_terminal'],
            'banks'       =>$data['requests_banks'],
        );

        $this->db->where('id', $data['terminal_entrante']);
        $this->db->update(db_prefix() .'terminals', $Dataentrante);
        
    }
    public function reverse_associate_terminal_entrante($data)
    {
        
        $Dataentrante = array(
            'userid'       =>  '0', 
            'codaffiliate' =>  '0',
            'status'       =>  '3',
            'numterminal'  =>  '0',
            'banks'        =>  NULL,
            
        );

        $this->db->where('id', $data['terminal_entrante']);
        $this->db->update(db_prefix() .'terminals', $Dataentrante);
        
    }

    public function associate_terminal_saliente($data)
    {        

        $Datasaliente = array(
            'userid'       =>  '0',  
            'status'       =>  '2',
            //'codaffiliate' =>  '0', 
        );

        $this->db->where('id', $data['terminal_saliente']);
        $this->db->update(db_prefix() .'terminals', $Datasaliente);
        
    }

    public function reverse_associate_terminal_saliente($data)
    {        
        $Datasaliente = array(
            'userid'       =>  $data["userid"], 
            //'codaffiliate' =>  $data['affiliate_code'], 
            'status'       =>  '1',
        );        

        $this->db->where('id', $data['terminal_saliente']);
        $this->db->update(db_prefix() .'terminals', $Datasaliente);
        
    }    

    public function uninstall_terminal($data)
    {        

        $Data = array(
            'userid'       =>  '0', 
            'status'       =>  '2',
            'codaffiliate' =>  '0', 
        );

        $this->db->where('id', $data['terminal']);
        $this->db->update(db_prefix() .'terminals', $Data);
        
    }


    public function add_clients($data)
    {
        $datecreated = date('Y-m-d');
        $rif = $data['type_rif']. $data['rif_reason_change'];
        $serial = $data['terminal_reason_change'] ?? $data['num_terminal_reason_change'];
        $bank_id = $data['requests_banks_reason_change'];
        $codaffiliate =$data['affiliate_code_reason_change'];
        $numterminal   = isset($data['num_terminal'])? $data['num_terminal']:NULL;    


        $existe = $this->db->select('rif_number,userid')->from('clients') ->where(db_prefix(). 'clients.rif_number', $rif)->get()->row();
        //->get_compiled_select();
        if(is_null($existe)){
            $this->db->insert(db_prefix() . 'clients', [
                'rif_number'   =>  $rif,
                'company'      =>  $data['business_name_reason_change'],
                'phonenumber'  =>  $data['phone_reason_change'],
                'datecreated'  =>  $datecreated,
                'dateReplica'  =>  $datecreated, 
            ]);
            $userid = $this->db->insert_id();            
        }else{
            $userid =$existe->userid;           
        }
         // asociar
        if (!empty($data['terminal'])){
            $serial = $data['terminal'];
            $this->add_terminals($userid,$serial,$bank_id,$codaffiliate,$numterminal);
        }  

    }

    public function update_clients_and_terminal($data)
    {
        $datecreated = date('Y-m-d');
        $rif           = trim($data['type_rif']. $data['rif_reason_change']);
        $serial        =!empty($data['terminal'])? $data['terminal']: NULL;
        $bank_id       = isset($data['requests_banks'])? $data['requests_banks']:0;
        $codaffiliate_cangue  = isset($data['affiliate_code_reason_change'])? $data['affiliate_code_reason_change']:0;
        $numterminal   = isset($data['num_terminal_reason_change'])? $data['num_terminal_reason_change']:NULL;        
        $banks_reason_change   = isset($data['requests_banks_reason_change'])? $data['requests_banks_reason_change']:'';
        $company       = isset($data['business_name_reason_change'])? $data['business_name_reason_change']:'';
        $phonenumber   =isset($data['phone_reason_change'])? $data['phone_reason_change']: '';
        

        $existe = $this->db->select('rif_number,userid')->from('clients') ->where(db_prefix(). 'clients.rif_number', $rif)->get()->row();
        //->get_compiled_select();        
        
        if(is_null($existe)){
            $this->db->insert(db_prefix() . 'clients', [
                'rif_number'   =>  $rif,
                'company'      =>  $company,
                'phonenumber'  =>  $phonenumber,
                'datecreated'  =>  $datecreated,
                'dateReplica'  =>  $datecreated, 
            ]);
            $userid = $this->db->insert_id();  

        }else{
            
            $userid = $existe->userid; 
            $Dataentrante = array(
                'company'      =>  $company,
                'phonenumber'  =>  $phonenumber,
                'datecreated'  =>  $datecreated,
                'dateReplica'  =>  $datecreated, 
            );
            $this->db->where('userid', $userid);
            $this->db->update(db_prefix() .'clients', $Dataentrante);     
          
        }
        
        if (!empty($data['terminal'])){
            $this->update_terminal($serial,$userid,$banks_reason_change,$codaffiliate_cangue,$numterminal,1);
        }
    }

   

    public function add_terminals($userid,$serial,$bank_id,$codaffiliate,$numterminal)
    {
        $Dataentrante = array(
            'userid'       =>  $userid,             
            'banks'        =>  $bank_id,
            'status'       =>  1,  
            'codaffiliate' => $codaffiliate,
            'numterminal'  => $numterminal,
        );
        // --pendiente agregar num terminal a la lista de campos->data entrante

        $this->db->where('id', $serial);
        $this->db->update(db_prefix() .'terminals', $Dataentrante);
    }

    public function change_simcard($data)
    {
        $Data = array(
            'simcard' =>  $data['simcard'],
        );

        $this->db->where('id', $data['terminal_simcard']);
        $this->db->update(db_prefix() .'terminals', $Data);
    }

    public function change_bank($data)
    {
        $Data = array(
            'banks' =>  $data['requests_banks_change'],  
            'codaffiliate' => $data['affiliate_code_bank_change'],
            'numterminal' => $data['num_terminal_banks_change'],
        );

        $this->db->where('id', $data['terminal']);
        $this->db->update(db_prefix() .'terminals', $Data);
    }

    public function reverse_change_bank($data)
    {
        $Data = array(
            'banks' =>  $data['bank_id'],  
            'codaffiliate' => $data['affiliate_code'],
            'numterminal' => $data['num_terminal'],
        );

        $this->db->where('id', $data['terminal']);
        $this->db->update(db_prefix() .'terminals', $Data);
    }


    public function get_rif_clients($rif)
    {
        $this->db->select('company, userid,phonenumber');
        $this->db->where('rif_number', $rif);
        $data = $this->db->get(db_prefix() . 'clients')->row(); 
        return $data ;

    }

    public function get_type_operation()
    {
        $this->db->select('*');
        $this->db->from(db_prefix() . 'type_operations');
        return $this->db->get()->result_array();
    }

    public function get_banks($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            
            return $this->db->get(db_prefix() . 'banks')->row();
        }

        $this->db->order_by('id', 'asc');
        $this->db->where('status', 1);
        return $this->db->get(db_prefix() . 'banks')->result_array();
    }

    public function get_status($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            
            return $this->db->get(db_prefix() . 'status_operations')->row();
        }
        
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'status_operations')->result_array();
    }

    public function get_status_add($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            
            return $this->db->get(db_prefix() . 'status_operations')->row();
        }
        $ctx= '1';
        $this->db->where('status', $ctx);
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'status_operations')->result_array();
    }

    public function get_contact($id)
    {
        $query = $this->db->select(implode(',', prefixed_table_fields_array(db_prefix() . 'contacts')).',company'.',tblclients.rif_number'.',tblaccounts.codaffiliate')
            ->from('contacts')
            ->join(db_prefix() . 'clients', '' . db_prefix() . 'clients.userid=' . db_prefix() . 'contacts.userid', 'left')
            ->join(db_prefix() . 'accounts', '' . db_prefix() . 'accounts.userid=' . db_prefix() . 'contacts.userid', 'left')
            ->where(db_prefix(). 'contacts.id', $id)
            //->get_compiled_select();
            ->get()->row();

        return $query;
    }

    public function get_contact_by_client($id)
    {
        $query = $this->db->select(implode(',', prefixed_table_fields_array(db_prefix() . 'contacts')).',company'.',tblclients.rif_number'.',tblaccounts.codaffiliate')
            ->from('contacts')
            ->join(db_prefix() . 'clients', '' . db_prefix() . 'clients.userid=' . db_prefix() . 'contacts.userid', 'left')
            ->join(db_prefix() . 'accounts', '' . db_prefix() . 'accounts.userid=' . db_prefix() . 'contacts.userid', 'left')
            ->where(db_prefix(). 'clients.userid', $id)
            //->get_compiled_select();
            ->get()->row();

        return $query;
    }    

    public function get_availables_terminals()
    {  //$this->db->where('userid', $userid);
        //$this->db->where('status', '1');
        $this->db->select('id, serial');
        $this->db->where('userid = 0 and status = 3');
        $this->db->order_by('id', 'asc');
        return $this->db->get(db_prefix() . 'terminals')->result_array(); //get_copiled_select();   
    }

    public function get_type_rif()
    {
        return $this->db->query('SELECT id, type_rif FROM ' . db_prefix() . 'type_rif ')->result_array();
               
    }

    public function get($id)
    {
        
        $this->db->select('*,'. db_prefix() . 'type_operations.name as type_name, '.  db_prefix() . 'clients.company as company , clients.address as address, clients.rif_number as rif_number');
        $this->db->from(db_prefix() . 'operations'); 
        $this->db->join(db_prefix() . 'type_operations', db_prefix() . 'type_operations.id_type = ' . db_prefix() . 'operations.type', 'left');
        $this->db->join(db_prefix() . 'clients', db_prefix() . 'clients.userid = ' . db_prefix() . 'operations.client_id', 'left');

        if (is_numeric($id)) {
            $this->db->where(db_prefix() . 'operations.id', $id);
            $requests = $this->db->get()->row();
            return $requests;
           
        }

        return $this->db->get()->result_array();
    }    



    public function fields_of_operation_days($status = 1){

        $country = [
            '1'  =>  "sale",
            '2'  =>  "payment_confirmed",
            '3'  =>  "bank_management",
            '4'  =>  "account_opened",
            '5'  =>  "send_code_af",
            '6'  =>  "bank_code_af",
            '7'  =>  "serial_assignment",
            '8'  =>  "send_par_loading",
            '9'  =>  "bank_par_loading",
            '10' => "ccrd_answer",
            '11' => "parameterization",
            '12' => "cari_relocated",
            '13' => "ready",
            '14' => "retired"
        ];

        return (String)$country[$status];
    }


    public function status_update($data){

        $datecreated = date('Y-m-d');

        $current_status =  @$this->fields_of_operation_days($data['status_change']);  // El estatus actual del operacion en String
        $Current_status =  @$data['status_change']; // El estatus actual del operacion en Int

        $change_status =  $this->fields_of_operation_days($data['status']); // El estatus a cambiar la operacion en String
        $Change_status =  $data['status'];   // El estatus a cambiar la operacion en Int

        $opt = $Change_status > $Current_status ? 'Update' : 'Reverse';

        $status = '';
        $new_status = [];

        $this->db->where('id_operation', $data['requests_id']);
        $this->db->update(db_prefix() . 'operations', [
            'status' => $data['status'],
            'modified_date' => $datecreated,
        ]);

        if(($this->db->affected_rows() > 0 ) or (isset( $data['option']) and $data['option']=='date_update')){        
            
            // si la funcion recive el parametro Opcion= update se modifica la fecha de la tabla oprations_dates en base a la fecha seleccionada por el usuario
            if (isset($data['option']) and $data['option' ] == 'date_update'){
                
                //format_date retorna la fecha  ingresada por el usuario ó la actual, con el fomato 'Y-m-d'
                $newDate = $this->format_date($data['date']);
                $query2="UPDATE ". db_prefix()."operations_dates
                SET ".$change_status." = '".$newDate."'
                WHERE " . db_prefix()."operations_dates.operation_id = ".$data['requests_id'];
                return ($this->db->query($query2) ? true : false);

            } else {

                $this->db->select('*');
                $this->db->where('operation_id' ,$data['requests_id']);
                $campos = current($this->db->get(db_prefix() . 'operations_dates')->result_array()); 
              
                $new_status = date_management([
                    'arr' => [],
                    'opt' => $opt,
                    'status' => $status,
                    'campos' => $campos,
                    'case' => 'operations',
                    'table' => 'status_operations',
                    'Current_status' => $Current_status, 
                    'Change_status' => $change_status,
                ]);

                $this->db->where('operation_id', $data['requests_id']);
                $this->db->update(db_prefix()."operations_dates", $new_status);

                return $this->db->affected_rows() ? true : false;
            }

        }
        return (false);
        
    }  

   
    public function get_numbers_fom_string($string)
    {
        $newString =  preg_replace('/\D/', '', $string);
        return  $newString;
    }

    public function get_type_from_rif($rif)
    {
        $first =  substr($rif,0,1);
        $type= (!is_numeric($first)) ? $first :'';

        return  $type;
    }
    
    public function format_date ($date){
             
        if ($date != "0000-00-00") {
            $dateArray=explode('-',$date);
            $time =strtotime($dateArray[2].'-'.$dateArray[1].'-'.$dateArray[0]);       
            //dateNewformat
            $newDate = date('Y-m-d',$time);
        }else{
            $newDate = date('Y-m-d');                       
        }
         return $newDate;
    }

    // public function reverse_format_date ($date){
             
    //     if ($date != "0000-00-00") {
    //         $dateArray=explode('-',$date);
    //         $time =strtotime($dateArray[2].'-'.$dateArray[1].'-'.$dateArray[0]);       
    //         //dateNewformat
    //         $newDate = date('Y-m-d',$time);
    //     }else{
    //         $newDate = date('Y-m-d');  
    //     }
    //      return $newDate;
    // }
    
    public function get_status_date($id,$status)
    {
                
        $campo = $this->fields_of_operation_days($status);     
        
        $this->db->select($campo.' as fecha');
        $this->db->from(db_prefix() . 'operations_dates');
        $this->db->where(db_prefix() . 'operations_dates.operation_id', $id);
        //$requests = $this->db->get_compiled_select();

        $requests = $this->db->get()->row();

        //se setea e la feca en formato inverso al de la bd, porque el claendario de la vista lo muestra asi.
        $statusDate= date('d-m-Y',strtotime($requests->fecha));
        return $statusDate;      

    }    
    
    function get_date_name_2($status){
        
        
        switch ($status) {
            case 1:
                $campo= "sale";
            break;
            case 2:
                $campo= "payment_confirmed";
            break;
            case 3:
                $campo= "bank_management";
            break;
            case 4:
                $campo= "account_opened";
            break;
            case 5:
                $campo= "send_code_af";
            break;
            case 6:
                $campo= "bank_code_af";
            break;
            case 7:
                $campo= "serial_assignment";
            break;
            case 8:
                $campo= "send_par_loading";
            break;
            case 9:
                $campo= "bank_par_loading";
            break;
            case 10:
                $campo= "ccrd_answer";
            break;
            case 11:
                $campo= "parameterization";
            break;
            case 12:
                $campo= "cari_relocated";
            break;
            case 13:
                $campo= "ready";
            break;
            case 14:
                $campo= "retired";
            break;
        }
    return $campo;
    }

    public function get_source_requests($id = false)
    {
        $this->db->where('requests_source',1);
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get(db_prefix() . 'requests_sources')->row();
        }

        $this->db->order_by('name', 'asc');

        return $this->db->get(db_prefix() . 'requests_sources')->result_array();
    }

   
}
