<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
/**
 * Included in application/views/admin/clients/client.php
 */
?>
<script>
    var respuesta='';
    function validate_rate() {
        

        requestGet('invoices/get_rate_by_date').done(function(response) {
            var response = JSON.parse(response);

            if (response.status == 'danger') {
                alert_float('danger', response.message);
                respuesta='alerta';
            } else {
                respuesta='valida';
            }

        }).fail(function(error) {
            var response = JSON.parse(error.responseText);
            alert_float('danger', response.message);
        });

        return respuesta;
    }

    function requests_modal_convert_document_sale(id = '') {
        console.log('aqui siii essss:');
        console.log(id);
        if (typeof(id) == 'undefined') {
            id = '';
        }

        //llamado a funcion de validacion de tasa del dia antes de continuar.
        let validate = validate_rate();
        console.log('este es el valor de validate: ');
        console.log(validate);

        if (validate=='valida'){
            
            requestGet('requests/get_requests_data_ajax/' + id).done(function(response) {
                $('#requests_data').html(response);
                $('#modal_convert_document_sale').modal({
                    show: true,
                    backdrop: 'static'
                });
                $('body').off('shown.bs.modal', '#modal_convert_document_sale');
                $('body').on('shown.bs.modal', 'modal_convert_document_sale', function() {
                    if (id == '') {
                        $('#modal_convert_document_sale').find('input[name="firstname"]').focus();
                    }
                });
    
            }).fail(function(error) {
                var response = JSON.parse(error.responseText);
                alert_float('danger', response.message);
            });
            console.log(' volvio aqui despues de llamar  a funcion- valor de validate:');
            console.log(validate);
            console.log(id);
        }
        return false;
    }

    function requests_convert_template(e) {

        console.log(e, 'iuiujuuu');
        var t,
            a = $(e).data("template");
        console.log(a, 'iuiujuuu22');
        if ("requests" == a) t = "requests";
        else {
            if ("invoice" != a) return !1;
            t = "invoice";
        }
        console.log(t);
        $('#modal_convert_document_sale').hide()
        requestGet("requests/get_" + t + "_convert_data/" + requests_id).done(function(e) {
            console.log(e);
            //$(".proposal-pipeline-modal").is(":visible") && $(".proposal-pipeline-modal").modal("hide"), $("#convert_helper").html(e), $("#convert_to_" + t).modal({ show: !0, backdrop: "static" }), reorder_items();
            $('#requests_convet_data').html(e);
            $('#convert_to_invoice').modal({
                show: true,
                // backdrop: 'static'
            });
            $('body').off('shown.bs.modal', '#convert_to_invoice');
            $('body').on('shown.bs.modal', 'convert_to_invoice', function() {
                if (id == '') {
                    $('#convert_to_invoice').find('input[name="firstname"]').focus();
                }
            });
        });
    }
</script>