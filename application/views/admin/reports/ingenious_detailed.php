<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <div id="date-range" class="mbot15">
                            <div class="row">
                             <div class="col-md-6">
                              <label for="report-from" class="control-label"><?php echo _l('report_sales_from_date'); ?></label>
                              <div class="input-group date">
                               <input type="text" class="form-control datepicker" id="report-from" name="report-from">
                               <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <label for="report-to" class="control-label"><?php echo _l('report_sales_to_date'); ?></label>
                      <div class="input-group date">
                       <input type="text" class="form-control datepicker" disabled="disabled" id="report-to" name="report-to">
                       <div class="input-group-addon">
                        <i class="fa fa-calendar calendar-icon"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table dt-table-loading table-expenses">
       <thead>
           <tr>
           <th>id</th>
            <th>country</th>
            <th>repair center</th>
            <th>customer name</th>
            <th>in serial number</th>
            <th>out serial number</th>
            <th>terminal part number</th>
            <th>model</th>
            <th>terminal imei address</th>
            <th>terminal mac address</th>
            <th>made in</th>
            <th>pci</th>
            <th>sales date</th>
            <th>end sales warranty</th>
            <th>date of customization</th>
            <th>in warranty</th>
            <th>out warranty</th>
            <th>in warranty accesories</th>
            <th>out warranty accesories</th>
            <th>in date</th>
            <th>date of entry to repair</th>
            <th>date start stop terminal</th>
            <th>date end stop terminal</th>
            <th>data of repair</th>
            <th>out date</th>
            <th>deadline</th>
            <th>report month</th>
            <th>backlog</th>
            <th>time of repairclient with discount supply</th>
            <th>time of repair with discount customer</th>
            <th>sla % with discount cr</th>
            <th>sla % with discount customer</th>
            <th>mrr</th>
            <th>bounce</th>
            <th>date of the last repair</th>
            <th>drr</th>
            <th>doa repair</th>
            <th>eco 1</th>
            <th>custom doa</th>
            <th>first pass yield fpytest2</th>
            <th>first pass yield fpyqa</th>
            <th>1 customer symptom</th>
            <th>2 customer symptom</th>
            <th>3 customer symptom</th>
            <th>4 customer symptom</th>
            <th>5 customer symptom</th>
            <th>1 technician diagnosis</th>
            <th>2 technician diagnosis</th>
            <th>3 technician diagnosis</th>
            <th>4 technician diagnosis</th>
            <th>5 technician diagnosis</th>
            <th>6 technician diagnosis</th>
            <th>7 technician diagnosis</th>
            <th>8 technician diagnosis</th>
            <th>9 technician diagnosis</th>
            <th>10 technician diagnosis</th>
            <th>1 technician resoliution</th>
            <th>2 technician resoliution</th>
            <th>3 technician resoliution</th>
            <th>4 technician resoliution</th>
            <th>5 technician resoliution</th>
            <th>6 technician resoliution</th>
            <th>7 technician resoliution</th>
            <th>8 technician resoliution</th>
            <th>9 technician resoliution</th>
            <th>10 technician resolution</th>
            <th>11 technician resolution</th>
            <th>12 technician resolution</th>
            <th>13 technician resolution</th>
            <th>14 technician resolution</th>
            <th>15 technician resolution</th>
            <th>1 part status</th>
            <th>1 part number</th>
            <th>1 part description</th>
            <th>1 part cost</th>
            <th>2 part status</th>
            <th>2 part number</th>
            <th>2 part description</th>
            <th>2 part cost</th>
            <th>3 part status</th>
            <th>3 part number</th>
            <th>3 part description</th>
            <th>3 part cost</th>
            <th>4 part status</th>
            <th>4 part number</th>
            <th>4 part description</th>
            <th>4 part cost</th>
            <th>5 part status</th>
            <th>5 part number</th>
            <th>5 part description</th>
            <th>5 part cost</th>
            <!--th>6 part status</th>
            <th>6 part number</th>
            <th>6 part description</th>
            <th>6 part cost</th>
            <th>7 part status</th>

            <th>7 part number</th>
            <th>7 part description</th>
            <th>7 part cost</th>
            <th>8 part status</th>
            <th>8 part number</th>
            <th>8 part description</th>
            <th>8 part cost</th>
            <th>9 part status</th>
            <th>9 part number</th>
            <th>9 part description</th>
            <th>9 part cost</th>
            <th>10 part status</th>
            <th>10 part number</th>
            <th>10 part description</th>
            <th>10 part cost</th>
            <th>11 part status</th>
            <th>11 part number</th>
            <th>11 part description</th>
            <th>11 part cost</th>
            <th>12 part status</th>

            <th>12 part number</th>
            <th>12 part description</th>
            <th>12 part cost</th>
            <th>13 part status</th>
            <th>13 part number</th>
            <th>13 part description</th>
            <th>13 part cost</th>
            <th>14 part status</th>
            <th>14 part number</th>
            <th>14 part description</th>
            <th>14 part cost</th>
            <th>15 part status</th>
            <th>15 part number</th>
            <th>15 part description</th>
            <th>15 part cost</th>
            <th>16 part status</th>
            <th>16 part number</th>
            <th>16 part description</th>
            <th>16 part cost</th-->
            <th>in number</th>
            <th>out number</th>
            <th>lote placa out mumber</th>
            <th>repair status</th>
            <th>repair level</th>
            <th>detail repair status</th>
            <th>terminal status</th>
            <th>accesories status</th>
            <th>unit cogs</th>
            <th>logistics</th>
            <th>other cost</th>

            <th>total cogs unit cogs+Logistica+otros</th>
            <th>workforce cost</th>
            <th>app</th>
            <th>workforce cost total work app</th>
            <th>administration cost</th>
            <th>external services cost</th>
            <th>comments1</th>
            <th>comments2</th>
            <th>comments3</th>
            <th>comments4</th>


            
            
           </tr>
       </thead>
       <tbody></tbody>
       <tfoot>
           <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>

           </tr>
       </tfoot>
   </table>
</div>
</div>
</div>
</div>
</div>
</div>
<?php init_tail(); ?>

<script>
    $(function(){

       var report_from = $('input[name="report-from"]');
       var report_to = $('input[name="report-to"]');
       var filter_selector_helper = '.expenses-filter-year,.expenses-filter-month-wrapper,.expenses-filter-month,.months-divider,.years-divider';

        var Ingenious_ServerParams = {};
           $.each($('._hidden_inputs._filters input'),function(){
            Ingenious_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
        });

       
       Ingenious_ServerParams['report_from'] = '[name="report-from"]';
       Ingenious_ServerParams['report_to'] = '[name="report-to"]';

       initDataTable('.table-expenses', window.location.href, 'undefined', 'undefined', Ingenious_ServerParams, [8, 'desc']);

       report_from.on('change', function() {
         var val = $(this).val();
         var report_to_val = report_to.val();
         if (val != '') {
           report_to.attr('disabled', false);
           $(filter_selector_helper).removeClass('active').addClass('hide');
           $('input[name^="year_"]').val('');
           $('input[name^="expenses_by_month_"]').val('');
         } else {
            report_to.attr('disabled', true);
         }

         if ((report_to_val != '' && val != '') || (val == '' && report_to_val == '') || (val == '' && report_to_val != '')) {
             $('.table-expenses').DataTable().ajax.reload();
         }

         if(val == '' && report_to_val == '' || report_to.is(':disabled') && val == ''){
                $(filter_selector_helper).removeClass('hide');
         }
        });

        report_to.on('change', function() {
             var val = $(this).val();
             if (val != '') {
               $('.table-expenses').DataTable().ajax.reload();
           }
       });

       
       $('select[name="currencies"]').on('change',function(){
        $('.table-expenses').DataTable().ajax.reload();
    });
   })

</script>
</body>
</html>
