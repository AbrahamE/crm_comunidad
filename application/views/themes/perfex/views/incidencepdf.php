<?php defined('BASEPATH') or exit('No direct script access allowed');

$dimensions = $pdf->getPageDimensions();

// Get Y position for the separation
$y = $pdf->getY();



$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Venepos');
$pdf->SetTitle('Planilla Incidencia');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

$pdf->SetFontSize(15);


$receit_heading = '<div style="text-align:center">' . mb_strtoupper(_l('incidence_management_worksheet'), 'UTF-8') . '</div></br>';
$pdf->Ln(20);
$pdf->writeHTMLCell(0, '', '', '', $receit_heading, 0, 1, false, true, 'L', true);
$pdf->SetFontSize(6);

// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="1">';
$tblhtml .= '<tr>';
$tblhtml .= '<td height="30" style="color:#fff; text-align:center" bgcolor="#3A4656" colspan="3">' . mb_strtoupper(_l('incidence_management_worksheet'),'UTF-8') . '</td>';
$tblhtml .= '<td>'._l('code').' : <strong>PT-FO-PGI-001</strong> </td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
$tblhtml .= '<td colspan="2">'._l('receiver').'</td>';
$tblhtml .= '<td>'._l('invoice_number').'</td>';
$tblhtml .= '<td>'._l('purchase_date').'</td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr>';
$tblhtml .= '<td colspan="2">'.$terminals[0]['name'].'</td>';
$tblhtml .= '<td></td>';
$tblhtml .= '<td></td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
$tblhtml .= '<td colspan="3">'._l('email').'</td>';
$tblhtml .= '<td>'._l('contact_number').'</td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr>';
$tblhtml .= '<td colspan="3">'.$sql_contact[0]['email'].'</td>';
$tblhtml .= '<td>'.$sql_contact[0]['phonenumber'].'</td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
$tblhtml .= '<td colspan="3">'._l('direction').'</td>';
$tblhtml .= '<td>Persona de contacto</td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr>';
$tblhtml .= '<td colspan="3">'.$sql_contact[0]['direction'].'</td>';
$tblhtml .= '<td>'.$sql_contact[0]['contactPerson'].'</td>';
$tblhtml .= '</tr>';

$tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
    $tblhtml .= '<td>'._l('trade_name').'</td>';
    $tblhtml .= '<td>'._l('rif').'</td>';
    $tblhtml .= '<td>'._l('date_of_admission').'</td>';
    $tblhtml .= '<td>'._l('ticket_number').'</td>';
    $tblhtml .= '</tr>';
    $tblhtml .= '<tr>';
    $tblhtml .= '<td>'.$terminals[0]['company'].'</td>';
    $tblhtml .= '<td>'.$terminals[0]['rif_number'].'</td>';
    $tblhtml .= '<td>'.$terminals[0]['datecreated'].'</td>';
    $tblhtml .= '<td>'.$terminals[0]['ticketid'].'</td>';
    $tblhtml .= '</tr>';

foreach ($resumetickets as $key => $aRows) {
    
    $tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
    $tblhtml .= '<td colspan="2">'._l('sim_card_serial').'</td>';
    $tblhtml .= '<td colspan="2">'._l('affiliate_code').'</td>';
    $tblhtml .= '</tr>';
    $tblhtml .= '<tr>';
    $tblhtml .= '<td colspan="2">'.$aRows->serialsimcard.'</td>';
    $tblhtml .= '<td colspan="2">'.$aRows->codaffiliate.'</td>';
    $tblhtml .= '</tr>';
    $tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
    $tblhtml .= '<td>'._l('computer_serial').'</td>';
    $tblhtml .= '<td>'.htmlentities(_l('equipment_model')).'</td>';
    $tblhtml .= '<td>'._l('operator').'</td>';
    $tblhtml .= '<td>'._l('allied_bank').'</td>';
    $tblhtml .= '</tr>';
    $tblhtml .= '<tr>';
    $tblhtml .= '<td>'.$aRows->serial.'</td>';
    $tblhtml .= '<td>'.$aRows->model.'</td>';
    $tblhtml .= '<td>'.$aRows->operadora.'</td>';
    $tblhtml .= '<td>'.htmlentities($aRows->banks).'</td>';
    $tblhtml .= '</tr>';
    $tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
    $tblhtml .= '<td colspan="2">'._l('IMEI_MAC_of_the_device').'</td>';
    $tblhtml .= '<td>'._l('recidivism').'</td>';
    $tblhtml .= '<td>'._l('equipment_under_warranty').' </td>';
    $tblhtml .= '</tr>';
    $tblhtml .= '<tr>';
    $tblhtml .= '<td colspan="2"></td>';
    $tblhtml .= '<td></td>';
    $tblhtml .= '<td></td>';
    $tblhtml .= '</tr>';
}

$tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
$tblhtml .= '<td colspan="4">'._l('incidence').'</td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr>';
$tblhtml .= '<td colspan="4" style="text-align:center">' .$incidence_1[0]['incidents']. '</td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr bgcolor="#D4E6F1" style="text-align:center">';
$tblhtml .= '<td colspan="4">'._l('observations').'</td>';
$tblhtml .= '</tr>';
$tblhtml .= '<tr>';
$tblhtml .= '<td></td>';
$tblhtml .= '</tr>';



$tblhtml .= '</table>';
$tblhtml1 = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="5" border="0">';
$tblhtml1 .= '<tr style="text-align:justify">';
$tblhtml1 .= '<td colspan="4">'._l('note_1').'</td>';
$tblhtml1 .= '</tr>';
$tblhtml1 .= '<tr style="text-align:justify">';
$tblhtml1 .= '<td colspan="4">'._l('note_2').'</td>';
$tblhtml1 .= '</tr>';
$tblhtml1 .= '<tr style="text-align:justify">';
$tblhtml1 .= '<td colspan="4">'._l('note_3').'</td>';
$tblhtml1 .= '</tr>';
$tblhtml1 .= '<tr> <br><br><br><br><br><br><br><br><br><br>';
$tblhtml1 .= '<td colspan="4"><strong>'._l('for _the_client').'</strong>: _________________________</td>';
$tblhtml1 .= '</tr><br><br><br>';
$tblhtml1 .= '<tr>';
$tblhtml1 .= '<td colspan="2"><strong>'._l('firm').'</strong>: ________________________________</td>';
$tblhtml1 .= '<td colspan="2" style="text-align:center">______________________________________________________________________________</td>';
$tblhtml1 .= '</tr>';
$tblhtml1 .= '<tr>';
$tblhtml1 .= '<td colspan="2"></td>';
$tblhtml1 .= '<td colspan="2" style="text-align:center"><strong>'._l('signature_and_company_stamp').'</strong></td>';
$tblhtml1 .= '</tr><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>';
$tblhtml1 .= '<tr style="text-align:center">';
$tblhtml1 .= '<td colspan="4">'._l('footer').'</td>';
$tblhtml1 .= '</tr>';
$tblhtml1 .= '</table>';


$pdf->writeHTML($tblhtml, true, false, false, false, '');
$pdf->writeHTML($tblhtml1, true, false, false, false, '');