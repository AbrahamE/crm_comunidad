<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open_multipart('clients/open_ticket', array('id' => 'open-new-ticket-form')); ?>
<div class="row">
   <div class="col-md-12">

      <?php hooks()->do_action('before_client_open_ticket_form_start'); ?>

      <div class="panel_s">
         <div class="panel-heading text-uppercase open-ticket-subject">
            <?php echo _l('clients_ticket_open_subject'); ?>
         </div>
         <div class="panel-body">
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group open-ticket-subject-group">
                     <!-- <label for="subject"><?php echo _l('customer_ticket_subject'); ?></label>
                     <input type="text" class="form-control mbot10" name="subject" id="subject" value="<?php echo set_value('subject'); ?>">
                   
                     <?php #echo form_error('subject'); ?> -->
                     <label for="subject">Tipo de Tickets</label>
                     <select name="typeTickes" required="true" id="typeTickes" class="selectpicker  mbot15" data-width="100%" data-abs-cache="false" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                        <option value="1"> Servicio público</option>
                        <option value="2"> Otro</option>
                     </select>
                  </div>

                  <?php if (total_rows(db_prefix() . 'projects', array('clientid' => get_client_user_id())) > 0 && has_contact_permission('projects')) { ?>
                     <div class="form-group open-ticket-project-group">
                        <label for="project_id"><?php echo _l('project'); ?></label>
                        <select data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" name="project_id" id="project_id" class="form-control selectpicker">
                           <option value=""></option>
                           <?php foreach ($projects as $project) { ?>
                              <option value="<?php echo $project['id']; ?>" <?php echo set_select('project_id', $project['id']); ?><?php if ($this->input->get('project_id') == $project['id']) {
                                                                                                                                       echo ' selected';
                                                                                                                                    } ?>><?php echo $project['name']; ?></option>
                           <?php } ?>
                        </select>
                     </div>

                  <?php } ?>
                  <div class="row">
                     <div class="col-md-6">
                        <?php echo render_select('categories', $categories, ['categoryId', 'categories'], 'categories', (count($categories) == 1) ? $categories[0]['categoryId'] : '', array('required' => 'true')); ?>
                     </div>

                     <div class="col-md-6" id="subCategories">
                        <?php echo render_select('sub_categories', $sub_categories, ['productId', 'subCategories'], 'Sub categories', (count($sub_categories) == 1) ? $sub_categories[0]['productId'] : '', array('required' => 'true')); ?>
                     </div>
                    
                     <div class="col-md-6">
                        <?php echo render_select('department', $departments, array('departmentid', 'name'), 'ticket_settings_departments', (count($departments) == 1) ? $departments[0]['departmentid'] : '', array('required' => 'true'), array(), '', '', false); ?>
                     </div>

                     <div class="col-md-6">
                        <?php echo render_input('lat', 'Latitud', '0', 'text', array('readonly' => true)); ?>
                     </div>
                     <div class="col-md-6">
                        <?php echo render_input('log', 'Longitud', '0', 'text', array('readonly' => true)); ?>
                     </div>
                  </div>

                 


                  <?php
                  if (get_option('services') == 1 && count($services) > 0) { ?>
                     <div class="form-group open-ticket-service-group">
                        <label for="service"><?php echo _l('clients_ticket_open_service'); ?></label>
                        <select data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" name="service" id="service" class="form-control selectpicker">
                           <option value=""></option>
                           <?php foreach ($services as $service) { ?>
                              <option value="<?php echo $service['serviceid']; ?>" <?php echo set_select('service', $service['serviceid'], (count($services) == 1 ? true : false)); ?>><?php echo $service['name']; ?></option>
                           <?php } ?>
                        </select>
                     </div>
                  <?php } ?>




                  <div class="custom-fields">
                     <?php echo render_custom_fields('tickets', '', array('show_on_client_portal' => 1)); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12">
      <div class="panel_s">
         <div class="panel-body">
            <div class="form-group open-ticket-message-group">
               <label for=""><?php echo _l('clients_ticket_open_body'); ?></label>
               <textarea name="message" id="message" class="form-control" rows="15"><?php echo set_value('message'); ?></textarea>
            </div>
         </div>
         <div class="panel-footer attachments_area open-ticket-attachments-area">
            <div class="row attachments">
               <div class="attachment">
                  <div class="col-md-6 col-md-offset-3">
                     <div class="form-group">
                        <label for="attachment" class="control-label"><?php echo _l('clients_ticket_attachments'); ?></label>
                        <div class="input-group">
                           <input type="file" extension="<?php echo str_replace(['.', ' '], '', get_option('ticket_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_ticket_form_accepted_mimes(); ?>">
                           <span class="input-group-btn">
                              <button class="btn btn-success add_more_attachments p8-half" data-max="<?php echo get_option('maximum_allowed_ticket_attachments'); ?>" type="button"><i class="fa fa-plus"></i></button>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-12 text-center mtop20">
      <button type="submit" class="btn btn-info" data-form="#open-new-ticket-form" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>"><?php echo _l('submit'); ?></button>
   </div>
</div>
<?php echo form_close(); ?>


<script>
   $('select[name="categories"]').on("change", function() {
      var categories = $('select[name="categories"]').val();
      var filter = <?php echo json_encode($sub_categories); ?>;
      filter = filter.filter(el => el.categoryId == categories)

      let option = filter.map(el => {
         return `<option value="${el.productId}">${el.subCategories}</option>`
      });

      var template = '<div class="form-group select-placeholder">'
      template += '<label for="terminal">Sub Categories</label>'
      template += '<select name="sub_categories" required="true" id="sub_categories" data-width="100%" data-abs-cache="false" data-live-search="true">'
      for (var i in option) template += option[i];
      template += '</select>'
      $("#subCategories").html(template);
      $('select[name="sub_categories"]').selectpicker();
      $('select[name="department"]').selectpicker('val', categories);
   })
</script>