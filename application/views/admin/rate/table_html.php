<?php defined('BASEPATH') or exit('No direct script access allowed');

render_datatable([
    _l('id'),
    _l('tasa'),
    _l('date'),
], (isset($class) ? $class : 'rate'), [], [
    'data-last-order-identifier' => 'rate',
    'data-default-order'         => get_table_last_order('rate'),
]);
