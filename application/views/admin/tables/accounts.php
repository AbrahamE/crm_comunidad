<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionDelete = has_permission('terminal', '', 'delete');

$aColumns = [
    'codaffiliate',
     db_prefix() . 'banks.banks as banks',
     db_prefix() . 'status_accounts.name as status',
     'naccount',
     'datecreated',
    ];

    $join = [
        'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.id = ' . db_prefix() . 'accounts.bankid',
        'LEFT JOIN ' . db_prefix() . 'status_accounts ON ' . db_prefix() . 'status_accounts.id = ' . db_prefix() . 'accounts.status',
    
    ];
    

$where = [];
if ($clientid != '') {
    array_push($where, 'AND ' . db_prefix() . 'accounts.userid=' . $this->ci->db->escape_str($clientid));
}



$sIndexColumn = 'id';
$sTable       = db_prefix() . 'accounts';

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where,[db_prefix() . 'accounts.id as id', 'userid']);

$output  = $result['output'];

$rResult = $result['rResult'];
//var_dump($rResult); die;

foreach ($rResult as $aRow) {
    $row = [];

    $rowName = '<a href="#" >' . $aRow['codaffiliate'] . '</a>';

    $rowName .= '<div class="row-options">';

    $rowName .= '<a href="#" onclick="accounts(' . $aRow['userid'] . ',' . $aRow['id'] . ');return false;">' . _l('edit') . '</a>';

    $rowName .= '</div>';


    $row[] = $rowName;

    $row[] = $aRow['banks'];

    if($aRow['status'] =='Activo'){
        $row[] = '<span class="label s-status" style="border: 1px solid #84c529;color:#84c529;">' .$aRow['status']. '</span>';
    }elseif($aRow['status'] =='Inactivo'){
        $row[] = '<span class="label s-status" style="border: 1px solid #FB2507;color:#FB2507;">' .$aRow['status']. '</span>';
    }
    
    if($aRow['naccount']){
        $row[] = $aRow['naccount'];
    }else{
        $row[] = 'NO APLICA';
    }

    $row[] = $aRow['datecreated'];

    

    $row['DT_RowClass'] = 'has-row-options';
    $output['aaData'][] = $row;
}
