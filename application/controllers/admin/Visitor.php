<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Visitor extends AdminController
{

    
    /**
     * Codeigniter Instance
     * Expenses detailed report filters use $ci
     * @var object
     */
    private $ci;

    public function __construct()
    {
        parent::__construct();

        $this->ci = &get_instance();
        $this->load->model('visitor_model');
        $this->load->model('staff_model');
        $this->load->model('tickets_model');
        $this->load->model('leads_model');
        
    }

  
    public function index()
    {
        $this->list_visitor();
    }

    public function list_visitor__()
    {

        $data['title'] = _l('visitor');
        $data['sources']  = $this->leads_model->get_source();
        $this->load->view('admin/visitor/manage', $data);
    }

    public function table($clientid = '')
    {
        if (!has_permission('invoices', '', 'view')
            && !has_permission('invoices', '', 'view_own')
            && get_option('allow_staff_view_invoices_assigned') == '0') {
            ajax_access_denied();
        }

        $this->app->get_table_data('visitor', [
            'clientid' => $clientid,
        ]);
    }

    public function create_new_visitor()
    {
         if ($this->input->post()) {
            $post_data =  $this->input->post();
            //var_dump($post_data['userid']); die;
                if($post_data['userid'] != ''){
                     $data = $this->visitor_model->get_contacts($post_data['userid']);
                     //var_dump($data); die;
                     if($data == ''){
                        $data = $this->visitor_model->add_contacts($post_data);
                     }
                }
                $data = $this->visitor_model->add($post_data);
                //var_dump($data); die;
                if ($data == true) {
                    echo json_encode(['status' => 'success', 'message' => _l('added_successfully')]);die;
                }else{
                    echo json_encode(['status' => 'success', 'message' => _l('an_error_has_occurred')]);die;
                } 
                die;
         }

         
         $data['title'] = _l('visitor');
         $data['staff']     = $this->staff_model->get('', ['active' => 1]);
         $data['banks']           = $this->tickets_model->get_banks();
         $data['sources']  = $this->leads_model->get_source();
         $data['typing']  = $this->visitor_model->get_typing();
         $data['type_rif']  = $this->visitor_model->get_type_rif();
         //var_dump($data['sources']); die;
         $this->load->view('admin/visitor/new_visitor', $data);
    }


    public function list_visitor()
    {
        $data['title'] = _l('visitor');

            if ($this->input->is_ajax_request()) {
                $aColumns = [
                    'id_visitor',
                    'CONCAT(name_and_surname, " ", surname) as name_and_surname',
                    'identification_card',
                    'admission_date',
                    'business',
                    'rif',
                    'correo',
                    'contact_number',
                    'place',
                    'check_in',
                    'attention_time',
                    'management_time',
                    'CONCAT(' . db_prefix() . 'staff.firstname," ",' . db_prefix() . 'staff.lastname) as sale_agente',
                    db_prefix().'banks.banks as banks',
                    db_prefix().'leads_sources.name as fountain',
                    'description',
                    db_prefix().'typing_visitor.typing as typing',
                ];
                $join = [
                    'LEFT JOIN ' . db_prefix() . 'staff ON ' . db_prefix() . 'staff.staffid = ' . db_prefix() . 'visitor.sale_agent',
                    'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.id = ' . db_prefix() . 'visitor.bank',
                    'LEFT JOIN ' . db_prefix() . 'leads_sources ON ' . db_prefix() . 'leads_sources.id = ' . db_prefix() . 'visitor.fountain',
                    'LEFT JOIN ' . db_prefix() . 'typing_visitor ON ' . db_prefix() . 'typing_visitor.id = ' . db_prefix() . 'visitor.reason_fo_visit',
                ];
                $where  = [];
                $filter = [];
                include_once(APPPATH . 'views/admin/visitor/filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $sIndexColumn = 'id_visitor';
                $sTable       = db_prefix() . 'visitor';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where);
                $output  = $result['output'];
                $rResult = $result['rResult'];
    
                //var_dump($rResult); die;

                foreach ($rResult as $aRow) {
                    //var_dump($aRow); die;
                    $row = [];
                    for ($i = 0; $i < count($aColumns); $i++) {
                        
                        if ($aColumns[$i] == 'id_visitor') {
                            $_data = $aRow['id_visitor'];
                        }elseif ($aColumns[$i] == 'CONCAT(name_and_surname, " ", surname) as name_and_surname') {
                            $_data = $aRow['name_and_surname'];
                        }elseif ($aColumns[$i] == 'identification_card') {
                            $_data = $aRow['identification_card'];
                        }elseif ($aColumns[$i] == 'admission_date') {
                            $_data = $aRow['admission_date'];
                        }elseif ($aColumns[$i] == 'business') {
                            $_data = $aRow['business'];
                        }elseif ($aColumns[$i] == 'rif') {
                            $_data = $aRow['rif'];
                        }elseif ($aColumns[$i] == 'correo') {
                            $_data = $aRow['correo'];
                        }elseif ($aColumns[$i] == 'contact_number') {
                            $_data = $aRow['contact_number'];
                        }elseif ($aColumns[$i] == 'place') {
                            $_data = $aRow['place'];
                        }elseif ($aColumns[$i] == 'check_in') {
                            $_data = $aRow['check_in'];
                        }elseif ($aColumns[$i] == 'attention_time') {
                            $_data = $aRow['attention_time'];
                        }elseif ($aColumns[$i] == 'management_time') {
                            $_data = $aRow['management_time'];
                        }elseif ($aColumns[$i] == 'CONCAT(tblstaff.firstname," ",tblstaff.lastname) as sale_agente') {
                            $_data = $aRow['sale_agente'];
                        }elseif ($aColumns[$i] == 'tblbanks.banks as banks') {
                            $_data = $aRow['banks'];
                        }elseif ($aColumns[$i] == 'tblleads_sources.name as fountain') {
                            $_data = $aRow['fountain'];
                        }elseif ($aColumns[$i] == 'description') {
                            $_data = $aRow['description'];
                        }elseif ($aColumns[$i] == 'tbltyping_visitor.typing as typing') {
                            $_data = $aRow['typing'];
                        }


                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }

                echo json_encode($output);
                die;
            }
            $this->load->view('admin/visitor/manage', $data);
    
    }



    public function rif_exists(){

        if ($this->input->post()) {
            $rif = $this->input->post('rif');
            $data = $this->visitor_model->get_rif_clients($rif);
            //var_dump($data); die;
            echo json_encode($data);
            
        }
    }
    
    

   
}
