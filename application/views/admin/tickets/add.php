<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">

	<div class="content">

		<?php echo form_open_multipart($this->uri->uri_string(), array('id' => 'new_ticket_form')); ?>

		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body">

						<div class="col-md-12">

							<div class="row">
								<div class="col-md-12">
									<label for="typeTickes"><?php echo _l('tipo de tickes'); ?></label>

									<select name="typeTickes" required="true" id="typeTickes" class="selectpicker  mbot15" data-width="100%" data-abs-cache="false" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
										<option value="1"> Servicio público</option>
										<option value="2"> Otro</option>
									</select>
								</div>
							</div>

							<div class="row">

								<div class="col-md-6">
									<?php echo render_select('categories', $categories, ['categoryId', 'categories'], 'categories', (count($categories) == 1) ? $categories[0]['categoryId'] : '', array('required' => 'true')); ?>
								</div>

								<div class="col-md-6" id="subCategories">
									<?php echo render_select('sub_categories', $sub_categories, ['productId', 'subCategories'], 'Sub categories', (count($sub_categories) == 1) ? $sub_categories[0]['productId'] : '', array('required' => 'true')); ?>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group select-placeholder" id="ticket_contact_w">

										<label for="contactid"><?php echo _l('Responsable'); ?></label>

										<select name="contactid" required="true" id="contactid" class="ajax-search search-contactid" data-width="100%" data-abs-cache="false" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
											<?php if (isset($contact)) { ?>
												<option value="<?php echo $contact['id']; ?>" selected><?php echo $contact['firstname'] . ' ' . $contact['lastname']; ?></option>
											<?php } ?>
											<option value=""></option>
										</select>

										<?php echo form_hidden('userid'); ?>
									</div>
								</div>

								<div class="col-md-6">
									<?php echo render_select('department', $departments, array('departmentid', 'name'), 'ticket_settings_departments', (count($departments) == 1) ? $departments[0]['departmentid'] : '', array('required' => 'true'), array(), '', '', false); ?>
								</div>

							</div>

							<div class="row">
								<div class="col-md-6">
									<?php echo render_input('name_user', 'ticket_settings_to', '', 'text', array('readonly' => true)); ?>
								</div>
								<div class="col-md-6">
									<?php echo render_input('email_user', 'ticket_settings_email', '', 'email', array('readonly' => true)); ?>
								</div>
								<div class="col-md-6">
									<?php echo render_input('lat', 'Latitud', 0, 'text', array('readonly' => true)); ?>
								</div>
								<div class="col-md-6">
									<?php echo render_input('log', 'Longitud', 0, 'text', array('readonly' => true)); ?>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="description">Example textarea</label>
										<textarea name='description' class="form-control" id="description" rows="3"></textarea>
									</div>
								</div>
							</div>

						</div>

						<!-- <div class="col-md-6"><br><br>

							<div class="row">

								<div class="col-md-12">
									<div class="form-group">
										<label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
										<input type="text" class="tagsinput" id="tags" name="tags" data-role="tagsinput">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group form-check mtop25">
										<label class="form-check-label" for="citado">Citado</label>
										<input type="checkbox" class="form-check-input" id="citado">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group select-placeholder">
										<label for="assigned" class="control-label">
											<?php echo _l('ticket_settings_assign_to'); ?>
										</label>
										<select name="assigned" id="assigned" class="form-control selectpicker" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-width="100%">
											<option value=""><?php echo _l('ticket_settings_none_assigned'); ?></option>
											<?php foreach ($staff as $member) { ?>
												<option value="<?php echo $member['staffid']; ?>" <?php if ($member['staffid'] == get_staff_user_id()) {
																										echo 'selected';
																									} ?>>
													<?php echo $member['firstname'] . ' ' . $member['lastname']; ?>
												</option>
											<?php } ?>
										</select>
									</div>
								</div>

								<div class="col-md-6">
									<?php $priorities['callback_translate'] = 'ticket_priority_translate';
									echo render_select('priority', $priorities, array('priorityid', 'name'), 'ticket_settings_priority', hooks()->apply_filters('new_ticket_priority_selected', 2), array('required' => 'true')); ?>
								</div>

								<div class="col-md-6">
									<?php if (get_option('services') == 1) : ?>
										<?php if (is_admin() || get_option('staff_members_create_inline_ticket_services') == '1') {
											echo render_select_with_input_group('service', $services, array('serviceid', 'name'), 'type_request', '', '<a href="#" onclick="new_service();return false;"><i class="fa fa-plus"></i></a>');
										} else {
											echo render_select('service', $services, array('serviceid', 'name'), 'ticket_settings_service');
										}
										?>
									<?php endif ?>
								</div>

								<div class="col-md-6">

									<div class="terminales form-group form-group-select-input-service input-group-select">
										<label for="terminal">Terminal</label>
										<div class='input-group input-group-select select-terminal'>
											<select name="terminal" id="terminal" class="ajax-search-terminal selectpicker jqTransformSelectWrapper" data-width="100%" data-live-search="true" data-abs-cache="false" data-abs-bind-event="click" data-abs-preserve-selected="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" disabled>
												<option value=""></option>
											</select>
											<div id='addterminal' class='input-group-addon' style="opacity: 1;">
												<a href="#" onclick="new_terminal();return false;"><i class="fa fa-plus"></i></a>
											</div>
										</div>
									</div>

								</div>

								<div class="col-md-6">
									
										<?php echo render_input('file_csv', 'choose_csv_file', '', 'file'); ?>
										<a href='#'id='excel' onclick="readExcel()" data-form="#readExcel" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info">
											<?php echo _l('Exportar desde excel'); ?>
										</a>
										
									</div>

							</div>

							<div class="row">
								<div class="col-md-12">
									<?php echo render_custom_fields('tickets'); ?>
								</div>
							</div>

							<div class="row">
								<div class="form-group projects-wrapper hide">
									<label for="project_id"><?php echo _l('project'); ?></label>
									<div id="project_ajax_search_wrapper">
										<select name="project_id" id="project_id" class="projects ajax-search" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" <?php if (isset($project_id)) { ?> data-auto-project="true" data-project-userid="<?php echo $userid; ?>" <?php } ?>>
											<?php if (isset($project_id)) { ?>
												<option value="<?php echo $project_id; ?>" selected><?php echo '#' . $project_id . ' - ' . get_project_name_by_id($project_id); ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>

						</div> -->

					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel_s">
					<div class="panel-body" style="height: 350px;">
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="btn-bottom-toolbar text-right" style="right: 0;">
				<button type="submit" id='submit1' data-form="#new_ticket_form" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info"><?php echo _l('open_ticket'); ?></button>
			</div>
		</div>

		<?php echo form_close(); ?>

	</div>

</div>


<?php init_tail(); ?>
<?php hooks()->do_action('new_ticket_admin_page_loaded'); ?>
<?php $this->load->view('admin/tickets/terminal/terminal', array('banks' => $banks, 'model' => $model, 'sincard' => $simcard, 'operadora' => $operadora)); ?>

<script>
	$(function() {
		var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
		init_ajax_search('contact', '#contactid.ajax-search', {
			tickets_contacts: true,
			contact_userid: function() {
				// when ticket is directly linked to project only search project client id contacts
				var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
				if (uid) {
					return uid;
				} else {
					return '';
				}
			}
		});

		validate_new_ticket_form();

		<?php if (isset($project_id) || isset($contact)) { ?>
			$('body.ticket select[name="contactid"]').change();
		<?php } ?>

		<?php if (isset($project_id)) { ?>
			$('body').on('selected.cleared.ajax.bootstrap.select', 'select[data-auto-project="true"]', function(e) {
				$('input[name="userid"]').val('');
				$(this).parents('.projects-wrapper').addClass('hide');
				$(this).prop('disabled', false);
				$(this).removeAttr('data-auto-project');
				$('body.ticket select[name="contactid"]').change();
			});
		<?php } ?>


		$(".ajax-search .dropdown-menu").click(function() {
			var e;
			var t = $('select[name="contactid"]').val();
			var a = $('select[name="project_id"]');
			var i = a.attr("data-auto-project");
			var n = $(".projects-wrapper");

			var stock_name = [];

			$("#tableticketsresumen tbody tr").each(function(index) {
				if (index >= 0) {
					$('#terminales').html('');
					$('#table').removeClass('shadow-z-1');
					i = 0;
				}
			});

			a.attr("disabled") || (i ? (e = a.clone()).prop("disabled", !0) : (e = a.html("").clone()),
				a.selectpicker("destroy").remove(),
				(a = e),
				$("#project_ajax_search_wrapper").append(e),
				init_ajax_search("project", a, {
					customer_id: function() {
						return $('input[name="userid"]').val();
					},
				})
			);
			if (t != '') {
				$.post(admin_url + "tickets/ticket_change_data/", {
					contact_id: t
				}).done(function(e) {
					var res = JSON.parse(e).contact_data;
					$('input[name="name_user"]').val(res.firstname + " " + res.lastname);
					$('input[name="email_user"]').val(res.email);
					$('input[name="userid_user"]').val(res.userid);
					$('input[name="rif_number_user"]').val(res.rif_number);
					$('input[name="codaffiliate_user"]').val(res.codaffiliate);
					$('input[name="userid"]').val(res.userid);
					$('input[name="lat"]').val(res.latitude);
					$('input[name="log"]').val(res.longitude);
					AjaxTerminal(res.userid);
					if (res.ticket_emails == "0") {
						show_ticket_no_contact_email_warning(res.userid, res.id);
					} else {
						clear_ticket_no_contact_email_warning()
					}
					(i) ? n.removeClass("hide"): res.customer_has_projects ? n.removeClass("hide") : n.addClass("hide");
				})
			}
		});

		function AjaxTerminal(id) {
			var template = "<label for='terminal'>Terminal</label>";
			template += "<div class='input-group input-group-select select-terminal'>";
			template += "<select name='terminal' id='terminal' class='ajax-search-terminal selectpicker jqTransformSelectWrapper' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php echo _l('dropdown_non_selected_tex'); ?>' >";
			template += "<option value=''></option>";
			template += "</select>";
			template += "<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
			template += "<a href='#' onclick='new_terminal();return false;'><i class='fa fa-plus'></i></a>";
			template += "</div>";
			template += "</div>";

			if (id == $('#userid').val()) {
				$(".terminales").html(template);
				init_ajax_search_terminal('terminal', '#terminal.ajax-search-terminal', {
					tickets_contacts: true,
					contact_userid: id
				});

				$(".jqTransformSelectWrapper").on("click", '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])', function(event) {
					var t = $('#terminal').val();
					$.post(admin_url + "tickets/ticket_change_data_terminal/", {
						contact_id: t
					}).done(
						function(e) {
							res = JSON.parse(e);
							if (res.contact_data) {
								console.log(res.contact_data);
								SerializeArrayTerminalExists(res.contact_data);
							}
							// else{
							// 	console.log(res.contact_data);
							// 	alert_float('danger', 'terminal no existe');
							// }
						}
					)
				});
			}

		}
		$('select[name="categories"]').on("change", function() {
			var categories = $('select[name="categories"]').val();
			var filter = <?php echo json_encode($sub_categories); ?>;
			filter = filter.filter(el => el.categoryId == categories)

			let option = filter.map(el => {
				return `<option value="${el.productId}">${el.subCategories}</option>`
			});

			if (categories != 7) {
				var template = '<div class="form-group select-placeholder">'
				template += '<label for="terminal">Sub Categories</label>'
				template += '<select name="sub_categories" required="true" id="sub_categories" data-width="100%" data-abs-cache="false" data-live-search="true">'
					for (var i in option) template += option[i];
				template += '</select>'
			
			} else {

				var template = '<?php echo render_input('sub_categories', 'Sub categories', '', 'text', array('required' => true)); ?>'
			}

			$("#subCategories").html(template);
			$('select[name="sub_categories"]').selectpicker();
			$('select[name="department"]').selectpicker('val', categories == 3 ? categoties - 1 : categories);
		})

	});



	function TerminalExists(data, id) {

		var namebanks = 'terminal[' + i + '][banks]';

		var namemodel = 'terminal[' + i + '][model]';

		var nameoperadora = 'terminal[' + i + '][operadora]';

		var namesimcard = 'terminal[' + i + '][simcardid]';

		var idbanks = 'banks_' + i + '';

		var idmodel = 'model_' + i + '';

		var idoperadora = 'operadora_' + i + '';

		var idsimcard = 'simcardid_' + i + '';

		var template = '';

		var rowCount = $('#terminales tr').length;

		var error = 0;

		var controlP1 = 0;

		var controlP2 = 0;

		var controlM1 = 0;

		var controlM2 = 0;

		var stock_name = [];

		var stock_name2 = [];


		if (rowCount === 0) {
			template = terminaltemplate(data);
			alert_float('success', 'terminal agregada');


		} else {

			$("#tableticketsresumen tbody tr").each(function(index) {

				$(this).children("td:eq(0)").each(function(index2) {
					if ($(this).find('input').val() != '') {
						stock_name[controlP1++] = $(this).find('input').val();
					}
				});

			});

			if (stock_name.indexOf(data[0].value) == -1) {
				template = terminaltemplate(data);
				alert_float('success', 'terminal agregada');
			} else {
				alert_float('danger', 'terminal duplicada');
				return false;
			}
		}

		$("#terminales").append(template);

		// inicia selectpicker
		$('select[id="simcardid"]').selectpicker('val', sincard);
		$('select[id="banks"]').selectpicker('val', banks);
		$('select[id="model"]').selectpicker('val', model);
		$('select[id="operadora"]').selectpicker('val', operadora);
		$('select[id="terminal"]').selectpicker('val', id);

		// cambia valores de los input 
		$('select[id="banks"]').attr("name", namebanks);
		$('select[id="banks"]').attr("id", idbanks);
		$('select[id="model"]').attr("name", namemodel);
		$('select[id="model"]').attr("id", idmodel);
		$('select[id="operadora"]').attr("name", nameoperadora);
		$('select[id="operadora"]').attr("id", idoperadora);
		$('select[id="simcardid"]').attr("name", namesimcard);
		$('select[id="simcardid"]').attr("id", idsimcard);
		$('#table').addClass('shadow-z-1');
		i++;

	}

	function terminaltemplate(data) {
		var template = '';

		var InArrayTerminal = ['id', "codaffiliate_user", "serial", "mac", "imei", "modelid", "banksid", "simcardid", "simcard", "operadoraid"];

		for (const K in data) {
			if (Object.hasOwnProperty.call(data, K)) {
				const element = data[K];
				if (element.name == 'id') {
					template = `<tr id='T_id_${element.value}'>`;
				}
				if (InArrayTerminal.indexOf(element.name) != -1) {
					switch (element.name) {
						case 'id':
							template += `<td data-title="${element.name}" style="display: none"> <div class="form-group ptop10"> <input id="${element.name}_${i}" type="text" name="terminal[${i}][${element.name}]" value="${element.value}"> </div> </td>`;
							break;
						case 'modelid':
							template += '<td data-title="' + element.name + '" > <div class="form-group ptop10"> <?php echo render_select_terminal('model', 'terminal[0][model]', $model, array('modelid', 'model'), 'Banco', (count($model) == 1) ? $model[0]['modelid'] : '', array('required' => 'true')); ?>	</div> </td>';
							model = element.value;
							break;
						case 'simcardid':
							template += `<td data-title="sincard" > <div class="form-group ptop10"> <?php echo render_select_terminal('simcardid', 'simcardid', $simcard, array('id', 'serialsimcard'), 'Banco', (count($simcard) == 1) ? $simcard[0]['id'] : '', array('required' => 'true')); ?> 	</td>`;
							sincard = element.value;
							break;
						case 'simcard':
							template += `<td data-title="${element.name}" > <div class="form-group ptop10"> <?php echo render_select_terminal('simcardid', 'simcardid', $simcard, array('id', 'serialsimcard'), 'Banco', (count($simcard) == 1) ? $simcard[0]['id'] : '', array('required' => 'true')); ?> 	</td>`;
							sincard = element.value;
							break;
						case 'banksid':
							template += '<td data-title="' + element.name + '" > <div class="form-group ptop10"> <?php echo render_select_terminal('banks', 'terminal[0][banks]', $banks, array('id', 'banks'), 'Banco', (count($banks) == 1) ? $banks[0]['id'] : '', array('required' => 'true')); ?> </div> </td>';
							banks = element.value;
							break;
						case "operadoraid":
							template += '<td data-title="' + element.name + '" > <div class="form-group ptop10"> <?php echo render_select_terminal('operadora', 'terminal[0][operadora]', $operadora, array('operadoraid', 'operadora'), 'ticket_settings_departments', (count($operadora) == 1) ? $operadora[0]['operadoraid'] : '', array('required' => 'true')); ?> </div> </td>';
							operadora = element.value;
							break;
						default:
							template += '<td data-title="' + element.name + '" ><div class="form-group ptop10"> <input type="text" id="' + element.name + '" name="terminal[' + i + '][' + element.name + ']" class="form-control input_class" ' + (element.value != null ? 'value="' + element.value + '"' : "") + ' required></div></td>';
							break;
					}
				}
			}
		}

		template += '<td data-title="delete"> <a href="#" onclick="DeleTeterminal(this)" class="btn btn-primary mtop10"><i class="fa fa-trash"></i></a> </td> ';
		template += '</tr>';
		return template;
	}

	function readExcel() {
		var t = $('#file_csv').val();
		$(this).hasClass("simulate") && $("#import_form").append(hidden_input("simulate", !0)), $("#import_form").submit();
		if (t != '') {
			$.post(admin_url + "tickets/terminal_fiel/", {
				file_csv: t
			}).done(
				function(e) {
					// res = JSON.parse(e);
				}
			)
		} else {
			alert_float('danger', 'no se a encontrado nigun archivo');
		}
	}
</script>
</body>

</html>