<?php //defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php //if(isset($client)){ ?>
	<!-- <h4 class="customer-profile-group-heading"><?php echo _l('terminal'); ?></h4> -->
	<!-- <a href="#" class="btn btn-info mbot25"  data-toggle="modal" data-target="#ticket-terminal-modal"><?php echo _l('new_terminal'); ?></a> -->
	<!-- <a href="#" class="btn btn-info mbot25" onclick="new_terminal();return false;" data-target="#ticket-terminal-modal"><?php echo _l('new_terminal'); ?></a> -->
	<?php
	// $this->load->view('admin/clients/table_terminal_html', array('class'=>'terminal-single-client'));
	// $this->load->view('admin/clients/modals/modal_terminal',array('banks' => $banks , 'model' => $model , 'sincard' => $simcard , 'operadora' => $operadora , 'userid' => $userid));
	//$this->load->view('admin/tickets/terminal/terminal',array('banks' => $banks , 'model' => $model , 'sincard' => $simcard , 'operadora' => $operadora));
	?>
<?php //} ?>

<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if(isset($client)){ ?>
<h4 class="customer-profile-group-heading"><?php echo is_empty_customer_company($client->userid) ? _l('terminal') : _l('terminal'); ?></h4>
<?php if($this->session->flashdata('gdpr_delete_warning')){ ?>
    <div class="alert alert-warning">
     [GDPR] The contact you removed has associated proposals using the email address of the contact and other personal information. You may want to re-check all proposals related to this customer and remove any personal data from proposals linked to this contact.
   </div>
<?php } ?>
<?php if((has_permission('customers','','create') || is_customer_admin($client->userid)) && $client->registration_confirmed == '1'){
   $disable_new_contacts = false;
   if(is_empty_customer_company($client->userid) && total_rows(db_prefix().'contacts',array('userid'=>$client->userid)) == 1){
      $disable_new_contacts = true;
   }
   ?>
<div class="inline-block new-contact-wrapper" data-title="<?php echo _l('customer_contact_person_only_one_allowed'); ?>"<?php if($disable_new_contacts){ ?> data-toggle="tooltip"<?php } ?>>
   <a href="#" onclick="terminals_assoc(<?php echo $client->userid; ?>); return false;" class="btn btn-info new-terminal mbot25<?php if($disable_new_contacts){echo ' disabled';} ?>"><?php echo _l('associate_terminal'); ?></a>
</div>
<?php } ?>
<?php
   
  $table_data = array_merge(array( _l('affiliate_code'),_l('serial'),_l('mac_upper'), _l('imei_upper'),_l('sim_card'),_l('terminal_number')));
   $custom_fields = get_custom_fields('terminal-single-client',array('show_on_table'=>1));
   foreach($custom_fields as $field){
      array_push($table_data,$field['name']);
   }
   echo render_datatable($table_data,'terminal-single-client'); ?> 
<?php } ?>
<div id="terminals_data"></div>
<div id="terminals_data"></div>
