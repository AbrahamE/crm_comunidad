<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php 
  $file_header = array();
$file_header[] = _l('model');
$file_header[] = _l('commodity_barcode');
$file_header[] = _l('sku_code');
$file_header[] = _l('sku_name');
$file_header[] = _l('description');
$file_header[] = _l('commodity_type');
$file_header[] = _l('unit_id');

$file_header[] = _l('commodity_group');
$file_header[] = _l('sub_group');
$file_header[] = _l('warehouse_id');
$file_header[] = _l('tax');
$file_header[] = _l('origin');
$file_header[] = _l('style_id');
$file_header[] = _l('model_id');
$file_header[] = _l('size_id');

$file_header[] = _l('date_manufacture');
$file_header[] = _l('expiry_date');
$file_header[] = _l('rate');
$file_header[] = _l('purchase_price');

 ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel_s">
                    <div class="panel-body">
                       
                        <?php if(!isset($simulate)) { ?>
            <ul>
              <li class="text-danger">1. <?php echo _l('file_xlsx_commodity'); ?></li>
              <li class="text-danger">2. <?php echo _l('file_xlsx_tax'); ?></li>
            </ul>
            <div class="table-responsive no-dt">
              <table class="table table-hover table-bordered">
                <thead>
                  <tr>
                    <?php
                      $total_fields = 0;
                      
                      for($i=0;$i<count($file_header);$i++){
                          if($i == 0 || $i == 1 ||$i == 4){
                          ?>
                          <th class="bold"><span class="text-danger">*</span> <?php echo html_entity_decode($file_header[$i]) ?> </th>
                          <?php 
                          } else {
                          ?>
                          <th class="bold"><?php echo html_entity_decode($file_header[$i]) ?> </th>
                          
                          <?php

                          } 
                          $total_fields++;
                      }

                    ?>

                    </tr>
                  </thead>
                  <tbody>
                    <?php for($i = 0; $i<1;$i++){
                      echo '<tr>';
                      for($x = 0; $x<count($file_header);$x++){
                        echo '<td>- </td>';
                      }
                      echo '</tr>';
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              <hr>

              <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'import_form')) ;?>
									<div class="col-md-6">
                                        <?php echo form_hidden('clients_import','true'); ?>
                                        <?php echo render_input('file_csv','choose_csv_file','','file'); ?>
                                    </div>
									<div class="col-md-6">
                                        <div class="form-group select-placeholder" id="ticket_contact_w">
                                            <label for="contactid"><?php echo _l('contact'); ?></label>
                                            <select name="contactid" required="true" id="contactid"
                                                class="ajax-search search-contactid" data-width="100%" data-abs-cache="false"
                                                data-live-search="true"
                                                data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                <?php if(isset($contact)) { ?>
                                                <option value="<?php echo $contact['id']; ?>" selected>
                                                    <?php echo $contact['firstname'] . ' ' .$contact['lastname']; ?></option>
                                                <?php } ?>
                                                <option value=""></option>
                                            </select>
                                            <?php echo form_hidden('userid'); ?>
                                        </div>
                                     </div>
                                     <div class="col-md-6">
                                        <?php echo render_input('name_user','ticket_settings_to','','text'); ?>
                                     </div>
                                    <div class="col-md-6">
                                        <?php echo render_input('email','ticket_settings_email','','email'); ?>    
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo render_input('rif_number_user','Rif','','text'); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo render_input('codaffiliate_user','Codigo de afiliacion','','text'); ?>    
                                    </div>
                                    <div class="col-md-6">
										<?php echo render_select('incidents',$incidents,array('id','incidents'),'incidents',(count($incidents) == 1) ? $incidents[0]['departmentid'] : '',array('required'=>'true')); ?>
									</div>
                                    <div class="col-md-6">
										<?php echo render_select('department',$departments,array('departmentid','name'),'ticket_settings_departments',(count($departments) == 1) ? $departments[0]['departmentid'] : '',array('required'=>'true'),array(),'','',false); ?>
									</div>
									<div class="col-md-6">
										<?php echo render_input('cc','CC'); ?>
                                    </div>
                                    <div class="col-md-6">
										<div class="form-group select-placeholder">
											<label for="assigned" class="control-label">
												<?php echo _l('ticket_settings_assign_to'); ?>
											</label>
											<select name="assigned" id="assigned" class="form-control selectpicker" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-width="100%">
												<option value=""><?php echo _l('ticket_settings_none_assigned'); ?></option>
												<?php foreach($staff as $member){ ?>
													<option value="<?php echo $member['staffid']; ?>" <?php if($member['staffid'] == get_staff_user_id()){echo 'selected';} ?>>
														<?php echo $member['firstname'] . ' ' . $member['lastname'] ; ?>
													</option>
												<?php } ?>
											</select>
										</div>
                                    </div>
                                    <div class="col-md-6">
										<?php $priorities['callback_translate'] = 'ticket_priority_translate'; echo render_select('priority', $priorities, array('priorityid','name'), 'ticket_settings_priority', hooks()->apply_filters('new_ticket_priority_selected', 2), array('required'=>'true')); ?>
                                    </div>
                                    <div class="col-md-6">
										<?php if(get_option('services') == 1): ?>
											<?php if(is_admin() || get_option('staff_members_create_inline_ticket_services') == '1'){
												echo render_select_with_input_group('service',$services,array('serviceid','name'),'type_request','','<a href="#" onclick="new_service();return false;"><i class="fa fa-plus"></i></a>');
											} else {
												echo render_select('service',$services,array('serviceid','name'),'ticket_settings_service');
											}
											?>
										<?php endif ?>
									</div>
                                    
                                    <div class="clearfix"></div>
                                    <hr class="hr-panel-heading" />

                                    <div class="form-group">
                                        <button type="button"
                                            class="btn btn-info import btn-import-submit"><?php echo _l('import'); ?></button>
                                        <!-- <button type="button" class="btn btn-info simulate btn-import-submit"><?php echo _l('simulate_import'); ?></button> -->
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php init_tail(); ?>
<script src="<?php echo base_url('assets/plugins/jquery-validation/additional-methods.min.js'); ?>"></script>
<!-- <script>
 $(function(){
   appValidateForm($('#import_form'),{file_csv:{required:true,extension: "csv"},source:'required',status:'required'});
 });
</script> -->
<script>
$(function() {
    var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
    init_ajax_search('contact', '#contactid.ajax-search', {
        tickets_contacts: true,
        contact_userid: function() {
            // when ticket is directly linked to project only search project client id contacts
            var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
            if (uid) {
                return uid;
            } else {
                return '';
            }
        }
    });

    $(".ajax-search .dropdown-menu").click(function() {
        var e;
        var t = $('select[name="contactid"]').val();
        var a = $('select[name="project_id"]');
        var i = a.attr("data-auto-project");
        var n = $(".projects-wrapper");

        var stock_name = [];

        $("#tableticketsresumen tbody tr").each(function(index) {
            if (index >= 0) {
                $('#terminales').html('');
                $('#table').removeClass('shadow-z-1');
                i = 0;
            }
        });

        a.attr("disabled") || (i ? (e = a.clone()).prop("disabled", !0) : (e = a.html("").clone()),
            a.selectpicker("destroy").remove(),
            (a = e),
            $("#project_ajax_search_wrapper").append(e),
            init_ajax_search("project", a, {
                customer_id: function() {
                    return $('input[name="userid"]').val();
                },
            })
        );
        if (t != '') {
            $.post(admin_url + "tickets/ticket_change_data/", {
                contact_id: t
            }).done(function(e) {
                var res = JSON.parse(e).contact_data;
                $('input[name="name_user"]').val(res.firstname + " " + res.lastname);
                $('input[name="email"]').val(res.email);
                $('input[name="userid_user"]').val(res.userid);
                $('input[name="rif_number_user"]').val(res.rif_number);
                $('input[name="codaffiliate_user"]').val(res.codaffiliate);
                $('input[name="userid"]').val(res.userid);
                //AjaxTerminal(res.userid);
                if (res.ticket_emails == "0") {
                    show_ticket_no_contact_email_warning(res.userid, res.id);
                } else {
                    //clear_ticket_no_contact_email_warning()
                }
                (i) ? n.removeClass("hide"): res.customer_has_projects ? n.removeClass("hide") :
                    n.addClass("hide");
            })
        }
    });

    (function($) {
    
    "use strict";

      appValidateForm($('#import_form'),{file_csv:{required:true,extension: "xlsx"},source:'required',status:'required'});
      // function 

      if('<?php echo html_entity_decode($active_language) ?>' == 'vietnamese')
      {
        $( "#dowload_file_sample" ).append( '<a href="'+ site_url+'modules/warehouse/uploads/file_sample/Sample_import_commodity_file_vi.xlsx" class="btn btn-primary" ><?php echo _l('download_sample') ?></a><hr>' );

      }else{
        $( "#dowload_file_sample" ).append( '<a href="'+ site_url+'modules/warehouse/uploads/file_sample/Sample_import_commodity_file_en.xlsx" class="btn btn-primary" ><?php echo _l('download_sample') ?></a><hr>' );
      }

  })(jQuery);

function uploadfilecsv(){
  "use strict";

    if(($("#file_csv").val() != '') && ($("#file_csv").val().split('.').pop() == 'xlsx')){
    var formData = new FormData();
    formData.append("file_csv", $('#file_csv')[0].files[0]);
    formData.append("csrf_token_name", $('input[name="csrf_token_name"]').val());
    formData.append("leads_import", $('input[name="leads_import"]').val());

    $.ajax({ 
      url: admin_url + 'warehouse/import_file_xlsx_commodity', 
      method: 'post', 
      data: formData, 
      contentType: false, 
      processData: false
      
    }).done(function(response) {
      response = JSON.parse(response);
      $("#file_csv").val(null);
      $("#file_csv").change();
       $(".panel-body").find("#file_upload_response").html();

      if($(".panel-body").find("#file_upload_response").html() != ''){
        $(".panel-body").find("#file_upload_response").empty();
      };

      if(response.total_rows){
        $( "#file_upload_response" ).append( "<h4><?php echo _l("_Result") ?></h4><h5><?php echo _l('import_line_number') ?> :"+response.total_rows+" </h5>" );
      }
      if(response.total_row_success){
        $( "#file_upload_response" ).append( "<h5><?php echo _l('import_line_number_success') ?> :"+response.total_row_success+" </h5>" );
      }
      if(response.total_row_false){
        $( "#file_upload_response" ).append( "<h5><?php echo _l('import_line_number_failed') ?> :"+response.total_row_false+" </h5>" );
      }
      if(response.total_row_false > 0)
      {
        $( "#file_upload_response" ).append( '<a href="'+response.site_url+'FILE_ERROR_COMMODITY'+response.staff_id+'.xlsx" class="btn btn-warning"  ><?php echo _l('download_file_error') ?></a>' );
      }
      if(response.total_rows < 1){
        alert_float('warning', response.message);
      }
    });
    return false;
    }else if($("#file_csv").val() != ''){
      alert_float('warning', "<?php echo _l('_please_select_a_file') ?>");
    }

}



});



</script>

</html>