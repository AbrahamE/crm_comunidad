<?php		
# Version 1.0.0		
$lang['commodity_type'] = 'Productos básicos ';
$lang['commodity_group'] = 'Grupo de productos básicos ';
$lang['commodity_list'] = 'Lista de productos básicos ';
$lang['bodys'] = 'Bodys ';
$lang['units'] = 'Unidades';
$lang['sizes'] = 'Tamaños ';
$lang['colors'] = 'Colores';
$lang['styles'] = 'Estilos ';
$lang['warehouse'] = 'Inventario';
$lang['warehouses'] = 'Almacen';
$lang['display'] = 'Monitor';
$lang['not_display'] = 'No mostrar';
$lang['add_warehouse_type'] = 'Agregar almacén ';
$lang['add_commodity_type'] = 'Agregue el tipo de mercancía ';
$lang['add_commodity_group_type'] = 'Agregar grupo de productos básicos ';
$lang['add_unit_type'] = 'Agregar unidad ';
$lang['add_body_type'] = 'Agregar modelo ';
$lang['add_size_type'] = 'Agregar tamaño ';
$lang['add_style_type'] = 'Agregar estilo ';
$lang['add_commodity_list'] = 'Agregar lista de productos básicos ';
$lang['commodity_code'] = 'Código de producto ';
$lang['commodity_barcode'] = 'Código de barras de productos básicos ';
$lang['commodity_name'] = 'Nombre del producto';
$lang['unit_id'] = 'Unidad';
$lang['commodity_type'] = 'Tipo de mercancía ';
$lang['warehouse_id'] = 'Almacén';
$lang['commodity_group'] = 'Grupo de productos básicos ';
$lang['tax_rate'] = 'Impuesto';
$lang['origin'] = 'Origen';
$lang['style_id'] = 'Estilo';
$lang['model_id'] = 'Modelo';
$lang['size_id'] = 'Talla';
$lang['commodity_images'] = 'Imágenes de productos básicos ';
$lang['date_manufacture'] = 'Fecha de fabricación ';
$lang['expiry_date'] = 'Fecha de caducidad';
$lang['rate'] = 'Velocidad';
$lang['type_product'] = 'Tipo de producto ';
$lang['materials'] = 'Materiales ';
$lang['foods'] = 'Alimentos ';
$lang['tools'] = 'Herramientas';
$lang['service'] = 'Servicio';
$lang['infor_detail'] = 'Detalle';
$lang['inventory_infor'] = 'Inventario Infor ';
$lang['goods_receipt'] = 'Entrada de inventario';
$lang['goods_issue'] = 'Emisión de mercancías ';
$lang['tax_money'] = 'Dinero de impuestos';
$lang['total_tax_money'] = 'Dinero total de impuestos ';
$lang['goods_money'] = 'Dinero de mercancías ';
$lang['total_goods_money'] = 'Dinero total de bienes ';
$lang['value_of_inventory'] = 'Valor del inventario ';
$lang['total_money'] = 'Pago total';
$lang['commodity'] = 'Mercancía';
$lang['supplier_code'] = 'Código de proveedor';
$lang['supplier_name'] = 'Nombre del proveedor';
$lang['deliver_name'] = 'Entregar nombre ';
$lang['Buyer'] = 'Comprador';
$lang['reference_purchase_request'] = 'Solicitud de compra de referencia ';
$lang['license_number'] = 'Número de licencia';
$lang['accounting_date'] = 'Fecha de contabilidad';
$lang['day_vouchers'] = 'Vales de día ';
$lang['purchase_order'] = 'Orden de compra';
$lang['invoice_number'] = 'Numero de factura';
$lang['address'] = 'Dirección';
$lang['search'] = 'Buscar';
$lang['stock_received_docket_number'] = 'Stock recibido número de expediente ';
$lang['stock_received_docket_detail'] = 'Stock recibido detalle de expediente ';
$lang['stock_received_docket_from_purchase_request'] = 'Stock recibido expediente de solicitud de compra ';
$lang['note'] = 'Nota';
$lang['stock_received_docket'] = 'Resbalón de importación de existencias ';
$lang['stock_received_docket_code'] = 'Código de expediente recibido de existencias ';
$lang['stock_received_manage'] = 'Gestión de entrada de inventario ';
$lang['stock_received_info'] = 'Stock recibido información ';
$lang['inventory_number'] = 'Inventario';
$lang['model_code'] = 'Código de modelo ';
$lang['model_name'] = 'Nombre del modelo';
$lang['commodity_group_code'] = 'Código de grupo de productos ';
$lang['commodity_group_name'] = 'Nombre del grupo de productos básicos ';
$lang['commodity_type_code'] = 'Código de tipo de mercancía ';
$lang['commodity_type_name'] = 'Nombre del tipo de producto ';
$lang['size_code'] = 'Código de tamaño ';
$lang['size_name'] = 'Nombre del tamaño ';
$lang['size_symbol'] = 'Símbolo de tamaño ';
$lang['style_code'] = 'Código de estilo ';
$lang['style_name'] = 'Nombre de estilo';
$lang['unit_code'] = 'Codigo de unidad';
$lang['unit_name'] = 'Nombre de la unidad';
$lang['unit_symbol'] = 'Símbolo de unidad ';
$lang['warehouse_code'] = 'Código de almacén ';
$lang['warehouse_name'] = 'Nombre del almacén ';
$lang['warehouse_address'] = 'Dirección del almacén ';
$lang['order'] = 'Orden';
$lang['added_successfully'] = 'Agregado exitosamente';
$lang['Add_commodity_type_false'] = 'Agregue el producto falso ';
$lang['updated_successfully'] = 'Actualizado correctamente ';
$lang['updated_commodity_type_false'] = 'Producto actualizado falso ';
$lang['Add_unit_type_false'] = 'Agregar unidad falsa ';
$lang['updated_unit_type_false'] = 'Tipo de unidad actualizado falso ';
$lang['Add_size_type_false'] = 'Agregar tamaño falso ';
$lang['updated_size_type_false'] = 'Tamaño actualizado falso ';
$lang['Add_style_type_false'] = 'Agregar estilo falso ';
$lang['updated_style_type_false'] = 'Estilo actualizado falso ';
$lang['Add_body_type_false'] = 'Agregar modelo falso ';
$lang['updated_body_type_false'] = 'Modelo actualizado falso ';
$lang['Add_commodity_group_type_false'] = 'Agregue el tipo de grupo de productos falso ';
$lang['updated_commodity_group_type_false'] = 'Tipo de grupo de productos actualizado falso ';
$lang['Add_warehouse_false'] = 'Agregar almacén falso ';
$lang['updated_warehouse_false'] = 'Almacén actualizado falso ';
$lang['Add_commodity_list_false'] = 'Agregar lista de productos falsos ';
$lang['updated_commodity_list_false'] = 'lista de productos actualizada falsa ';
$lang['Add_stock_received_docket_false'] = 'Agregar stock recibido expediente falso ';
$lang['stock_received_docket'] = 'Entrada de inventario ';
$lang['unit_type'] = 'Unidad';
$lang['size_type'] = 'Talla';
$lang['style_type'] = 'Estilo';
$lang['body_type'] = 'modelo';
$lang['commodity_group_type']	= '	Tipo de grupo de productos básicos';
$lang['inventory_receiving_voucher']	=  '	Recepción de inventario ';
$lang['inventory_delivery_voucher'] = 'Entrega de inventario ';
$lang['inventory'] = 'Inventario mínimo ';
$lang['inventory_config_min'] = 'Configure la lista de inventario mínimo en el inventario ';
$lang['inventory_minimum'] = 'Cantidad mínima de inventario ';
$lang['unsafe_inventory'] = 'Inventario inseguro ';
$lang['warehouse_history'] = 'Historia del almacén ';
$lang['quantity'] = 'Cantidad';
$lang['stock_import'] = 'Entrada de inventario ';
$lang['stock_export'] = 'Salida de inventario ';
$lang['increase'] = 'Incrementar';
$lang['reduction'] = 'Reducción';
$lang['sign_full_name'] = '(Firma, nombre completo) ';
$lang['stocker'] = 'Stocker ';
$lang['chief_accountant'] = 'Contador jefe ';
$lang['actually_imported'] = 'Realmente importado ';
$lang['Division'] = 'División';
$lang['deparment'] = 'Departamento ';
$lang['input_in_stock'] = 'Entrada en stock ';
$lang['debit'] = 'Débito';
$lang['credit'] = 'Crédito';
$lang['amount_of'] = 'Cantidad de';
$lang['amount_of_in_word'] = 'Cantidad de (en palabras) ';
$lang['origin_voucher_following'] = 'Comprobante de origen siguiente ';
$lang['store_input_slip'] = 'Almacenar comprobante de entrada';
$lang['no'] = 'No';
$lang['activity_log'] = 'Registro de actividades';
$lang['export_output_slip'] = 'Salida de inventario ';
$lang['export_ouput_splip'] = 'salida de inventario ';
$lang['stock_export_info'] = 'Información de salida de inventario ';
$lang['receiver'] = 'Receptor';
$lang['stock_delivery_manage'] = 'Gestión de salida de inventario ';
$lang['goods_delivery_code'] = 'Código de exportación de existencias ';
$lang['customer_code'] = 'Código de cliente';
$lang['customer_name'] = 'Nombre del cliente';
$lang['to'] = 'Receptor';
$lang['reason_for_export'] = 'Razon para exportar';
$lang['salesman'] = 'Vendedor';
$lang['reference'] = 'Referencia';
$lang['document_number'] = 'Número del Documento';
$lang['stock_export_detail'] = 'Detalle de las exportaciones de existencias ';
$lang['exporting_from_sales_slip'] = 'Exportación de comprobante de venta ';
$lang['export_type'] = 'Tipo de exportación ';
$lang['stock_summary_report'] = 'Informe resumido de existencias ';
$lang['company_name'] = 'Nombre de empresa';
$lang['opening_stock'] = 'Stock de apertura';
$lang['receipt_in_period'] = 'Importar en período ';
$lang['issue_in_period'] = 'Exportar en período ';
$lang['closing_stock'] = 'De cierre';
$lang['scheduler'] = 'Programador ';
$lang['download_pdf'] = 'Descargar pdf ';
$lang['download_xlsx'] = 'Descargar excel ';
$lang['not_yet_approve'] = 'Aún no aprobado ';
$lang['approved'] = 'Aprobado';
$lang['reject'] = 'Rechazar';
$lang['Product_does_not_exist_in_stock'] = 'El producto no existe en stock ';
$lang['in_stock'] = 'En stock: ';
$lang['product'] = 'producto';
$lang['data_invalid'] = 'Datos no válidos ';
$lang['goods_delivery'] = 'Entrega de la mercancia';
$lang['report'] = 'Reporte';
$lang['stock_take'] = 'Toma de inventario ';
$lang['approval_setting'] = 'Ajuste de aprobación ';
$lang['new_approval_setting'] = 'Nueva configuración de aprobación ';
$lang['direct_manager'] = 'Jefe directo';
$lang['department_manager'] = 'Gerente de departamento';
$lang['edit_approval_setting'] = 'Editar configuración de aprobación ';
$lang['filters_by_warehouse'] = 'Filtros por almacén ';
$lang['filters_by_commodity'] = 'Filtros por mercancía ';
$lang['_add_list'] = 'Añadir lista';
$lang['_stock_export_sales'] = '1. Ventas de exportación de existencias ';
$lang['general_infor'] = 'Información general';
$lang['staff_id'] = 'Código HR ';
$lang['date_add'] = 'Añadir fecha ';
$lang['form_code'] = 'Código de formulario ';
$lang['filters_by_status'] = 'Filtros por estado ';
$lang['inventory_ticket_code'] = 'Código de ticket de inventario ';
$lang['warehouse_inventory'] = 'Inventario de almacén';
$lang['_inventory'] = 'Inventario';
$lang['_member_inventory'] = 'Inventario de miembros ';
$lang['_inventory_results'] = 'Resultados del inventario ';
$lang['sku_code'] = 'Código Sku ';
$lang['sku_name'] = 'Sku Name ';
$lang['purchase_price'] = 'Precio de compra';
$lang['sub_group'] = 'Subgrupo ';
$lang['sub_group_code'] = 'Código de subgrupo ';
$lang['sub_group_name'] = 'Nombre del subgrupo ';
$lang['add_sub_group_type'] = 'Agregar subgrupo ';
$lang['Add_sub_group_false'] = 'Agregar subgrupo falso ';
$lang['updated_sub_group_false'] = 'Actualizar subgrupo falso ';
$lang['choose_excel_file'] = 'Elija el archivo de Excel ';
$lang['import_line_number'] = 'Importar número de línea ';
$lang['import_line_number_success'] = 'Importar número de línea correcto ';
$lang['import_line_number_failed'] = 'Importar número de línea falso ';
$lang['download_file_error'] = 'Error de archivo abajo ';
$lang['import_excel'] = 'Importar excel ';
$lang['delete_commodity_file_success'] = 'Eliminado ';
$lang['delete_commodity_file_false'] = 'Eliminar falso ';
$lang['file_xlsx_commodity'] = 'La columna con el símbolo "*" es necesaria para ingresar, la columna "Tipo de producto", "Unidad", "Grupo de producto", "Subgrupo", "Almacén de grupo", "Estilo", "Forma", "Modelo" puede introduzca "ID" o código de tipo ';
$lang['file_xlsx_tax'] = 'En la columna "Impuesto", introduzca "ID" del impuesto o "nombre del impuesto", ';
$lang['_images'] = 'Imagen';
$lang['commodity_sku_code_tooltip'] = 'Código sku creado por: el primer carácter del código de grupo y el código de subgrupo y el aumento automático de id tiene 4 caracteres ';
$lang['_color'] = 'Color';
$lang['add_color'] = 'Agregar color ';
$lang['edit_color'] = 'Editar';
$lang['color_code'] = 'Codigo de color';
$lang['color_name'] = 'Nombre del color ';
$lang['color_hex'] = 'Color hexadecimal ';
$lang['inventory_goods_materials'] = 'Inventario de bienes y materiales ';
$lang['approval_information'] = 'Información de aprobación ';
$lang['approver_list'] = 'Lista de aprobadores ';
$lang['add_item'] = 'Añadir artículo';
$lang['edit_item'] = 'Editar elemento ';
$lang['properties'] = 'Propiedades ';
$lang['warehouse_in'] = 'Almacén en ';
$lang['date_of_manufacture'] = 'Fecha de manufactura';
$lang['please_select_a_warehouse'] = 'Seleccione un almacén ';
$lang['warehouse_out'] = 'Almacén fuera ';
$lang['loss_adjustment'] = 'Pérdida y ajuste ';
$lang['loss_adjustment_detail'] = 'Detalle de pérdidas y ajustes ';
$lang['available_quantity'] = 'Cantidad disponible';
$lang['stock_quantity'] = 'Cantidad de stock';
$lang['please_select_warehouse'] = 'seleccione almacén ';
$lang['update_loss_adjustment'] = 'Actualizar el ajuste de pérdidas ';
$lang['loss'] = 'Pérdida';
$lang['datecreator'] = 'Fecha de creación ';
$lang['adjusted'] = 'Equilibrado';
$lang['warehouse_out'] = 'Almacén fuera ';
$lang['send_request_approve'] = 'Enviar solicitud aprobar ';
$lang['inventory_valuation_report'] = 'Informe de valoración de inventarios ';
$lang['amount_sold'] = 'Cantidad de venta ';
$lang['amount_purchased'] = 'Importe de la compra ';
$lang['expected_profit'] = 'Ganancia esperada';
$lang['adjust_fail'] = 'Ajustar falla ';
$lang['adjusted'] = 'Equilibrado';
$lang['sales_number'] = 'Número de venta ';
$lang['out_of_stock'] = 'Agotado';
$lang['expired'] = 'Caducado';
$lang['_warehouse'] = 'Almacén';
$lang['_time'] = 'Tiempo (perdido o ajuste) ';
$lang['acc_attach'] = 'adjuntar';
$lang['voucher_number'] = 'Número de Vales';
$lang['group_name'] = 'Nombre del grupo';
$lang['long_description'] = 'Descripción larga';
$lang['date_expire'] = 'Fecha de vencimiento ';
$lang['transaction_history'] = 'Historial de transacciones';
$lang['inventory_stock'] = 'Inventario de existencias';
$lang['old_quantity'] = 'Cantidad antigua ';
$lang['new_quantity'] = 'Nueva cantidad ';
$lang['unit_price'] = 'Precio unitario';
$lang['Amount_'] = 'Cantidad';
$lang['pending_invoice_payable'] = 'Factura pendiente por pagar';
$lang['the_invoice_number_does_not_exist'] = 'El numero de factura no existe';
$lang['The_invoice_number_already_has_an_out_of_stock'] = 'El numero de factura ya tiene una salida de existencia';
$lang['invoice_status'] = 'Estatus de la factura';
$lang['pending_payment'] = 'Por pagar';
$lang['payment'] = 'Pagada';
$lang['partially_paid'] = 'Pagada parcialmente';
$lang['pdf'] = 'PDF';
$lang['add'] = 'Agregar';
$lang['inventory_entry_report'] = 'Reporte de entrada de inventario';
$lang['inventory_release_report'] = 'Reporte de salida de inventario';
$lang['sales_report'] = 'Reporte de ventas';
$lang['status'] = 'Estatus';
$lang['invoice_number'] = 'Nª de factura';
$lang['date'] = 'Fecha';
$lang['client'] = 'Cliente';
$lang['due_date'] = 'Fecha de venciemiento';
$lang['everybody'] = 'Todos';
$lang['by_date_range'] = 'POR RANGO DE FECHA';
$lang['by_status'] = 'POR ESTATUS';
$lang['by_warehouse'] = 'POR ALMACEN';
$lang ['setting'] = 'Ajuste';
$lang ['import_profit'] = 'Importar profit';
$lang ['import_profit_item'] = 'Importar articulo profit';
$lang ['import'] = 'Importar';
$lang ['product_code'] = 'Codigo del Articulo';



