<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="ticket-terminal-modal_" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <?php echo form_open(admin_url('clients/terminal_add'),array('id'=>'ticket-terminal-formd','autocomplete'=>'off')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="add-title"><?php echo _l('new_terminal'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style='display: none'>
                        <div id="additional"></div>
                        <?php echo render_input('userid','codigó de afiliado', $userid ,'text',array('required'=>true)); ?>
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $codaffiliate=( isset($terminal) ? $terminal['codaffiliate'] : ''); ?>
                        <?php echo render_input('codaffiliate_user','Codigó de afiliado',$codaffiliate,'text',array('required'=>true)); ?>
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $serial=( isset($terminal) ? $terminal['serial'] : ''); ?>
                        <?php echo render_input('serial','Serial',$serial,'text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $mac=( isset($terminal) ? $terminal['mac'] : ''); ?>
                        <?php echo render_input('mac','MAC',$mac,'text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php $imei=( isset($terminal) ? $terminal['imei'] : ''); ?>
                        <?php echo render_input('imei','Imei',$imei,'text',array('required'=>true)); ?>
                    </div>
                     <div class="col-md-12">
                        <div id="additional"> Banco</div>
                            <?php echo render_select_terminal('banksid', 'banksid',$banks,array('id','banks'),'Banco',(isset($terminal) ? $terminal['banks'] : ''),array('required'=>'true')); ?>
                        </div> 
                    <div class="col-md-12">
                        <div id="additional"> Modelo</div>
                        <?php echo render_select_terminal('modelid', 'modelid',$model,array('modelid','model'),'Banco',(isset($terminal) ? $terminal['modelid'] : ''),array('required'=>'true')); ?>	
                    </div>
                    <div class="col-md-12">
                        <label for="serialsimcard"><?php echo _l('Serial simcard'); ?></label>
                        <select name="simcard" required="true" id="simcard" class="ajax-search simcard" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                            <!-- <?php $simcard=( isset($terminal) ? $terminal['simcard'] : ''); ?> -->
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div><br>
                        <label for="operadoraid"><?php echo _l('Operadora'); ?></label>
                        <?php echo render_select_terminal('operadoraid','operadoraid',$operadora,array('operadoraid','operadora'),'ticket_settings_departments',(isset($terminal) ? $terminal['operadoraid'] : ''),array('required'=>'true')); ?>
                    </div>

                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>

   
     
    function new_terminal(){
            $('#ticket-terminal-modal_').modal('show');
            $('.edit-title').addClass('hide');
            init_ajax_search_simcard('simcard','#simcard.ajax-search',{
                tickets_contacts: true,
                contact_userid: function(){
                        // when ticket is directly linked to project only search project client id contacts
                        var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
                        if(uid){
                            return uid;
                        } else {
                            return '';
                        }
                    }
                },
            );
            $(".simcard .dropdown-menu").click(function() {
                var t = $('#simcard').val();
                $.post(admin_url + "tickets/ticket_change_data_simcard", { contact_id: t }).done(
                    function (e) {
                        res = JSON.parse(e).contact_data;
                        if (res) {
                            console.log(res.operadora);
                            $('select[id="operadoraid"]').selectpicker('val', res.operadora);
                        }
                    }
                )
            })
        
    }
    </script>

