<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                
                <div class="panel_s">
                    <div class="panel-body">
                        <div class="clearfix"></div>
                        <div id="date-range" class="mbot15">
                            <div class="row">
                             <div class="col-md-6">
                              <label for="report-from" class="control-label"><?php echo _l('report_sales_from_date'); ?></label>
                              <div class="input-group date">
                               <input type="text" class="form-control datepicker" id="report-from" name="report-from">
                               <div class="input-group-addon">
                                <i class="fa fa-calendar calendar-icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <label for="report-to" class="control-label"><?php echo _l('report_sales_to_date'); ?></label>
                      <div class="input-group date">
                       <input type="text" class="form-control datepicker" disabled="disabled" id="report-to" name="report-to">
                       <div class="input-group-addon">
                        <i class="fa fa-calendar calendar-icon"></i>
                    </div>
                </div>
			</div>
			<div class="clearfix"></div>
			<hr class="hr-panel-heading" />
			<!-- <div class="col-md-12">
					<a href="<?php echo admin_url('visitor/create_new_visitor'); ?>" class="btn btn-info pull-left display-block mright5"><?php echo _l('new_visitor'); ?></a>
				</div>  -->
        </div>
    </div>
    <table class="table dt-table-loading table-requests">
       <thead>
           <tr>
                <th><?php echo _l('id'); ?></th>
                <th><?php echo _l('bank'); ?></th>
                <th><?php echo _l('model'); ?></th>
                <th><?php echo _l('serial'); ?></th>
                <th><?php echo _l('type_requests_razon_social'); ?></th> 
                <th><?php echo _l('requests_affiliate_code'); ?></th>
                <th><?php echo _l('requests_terminals'); ?></th>
                <th><?php echo _l('rif'); ?></th>
                <th><?php echo _l('invoice'); ?></th>
                <th><?php echo _l('phonenumber'); ?></th>
                 <!-- <th>< ?php echo _l('fountain'); ?></th> -->

                <th><?php echo _l('type_requests'); ?></th>
                <th><?php echo _l('status_requests_sale'); ?></th>
                <th><?php echo _l('status_requests_payment_confirmation'); ?></th>
                <th><?php echo _l('status_requests_bank_management'); ?></th>
                <th><?php echo _l('status_requests_account_opened'); ?></th>
                <th><?php echo _l('status_requests_affiliate_code_submission'); ?></th>
                <th><?php echo _l('status_requests_bank affiliate code'); ?></th>
                <th><?php echo _l('status_requests_assign_serial'); ?></th>
                <th><?php echo _l('status_requests_shipment_freight_parameter'); ?></th>
                <th><?php echo _l('status_requests_send_bank'); ?></th>
                <th><?php echo _l('status_requests_answer_credicard'); ?></th>
                <th><?php echo _l('status_requests_parameterization'); ?></th>
                <th><?php echo _l('status_requests_cary_transfer'); ?></th>
                <th><?php echo _l('status_requests_ready'); ?></th>
                <th><?php echo _l('status_requests_retired'); ?></th>
                <!--<th>< ?php echo _l('status_requests_ready'); ?></th>
                <th>< ?php echo _l('status_requests_transfer'); ?></th> -->



           </tr>
       </thead>
       <tbody></tbody>
       <tfoot>
           <tr>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td> 

               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td> 
               <td></td>
               <td></td>
               <!--<td></td>
               <td></td>
               <td></td> -->
               
           </tr>
       </tfoot>
   </table>
</div>
</div>
</div>
</div>
</div>
</div>
<?php init_tail(); ?>

<script>
    $(function(){

       var report_from = $('input[name="report-from"]');
       var report_to = $('input[name="report-to"]');
       var filter_selector_helper = '.expenses-filter-year,.expenses-filter-month-wrapper,.expenses-filter-month,.months-divider,.years-divider';

        var Ingenious_ServerParams = {};
           $.each($('._hidden_inputs._filters input'),function(){
            Ingenious_ServerParams[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
        });

       
       Ingenious_ServerParams['report_from'] = '[name="report-from"]';
       Ingenious_ServerParams['report_to'] = '[name="report-to"]';

       initDataTable('.table-requests', window.location.href, 'undefined', 'undefined', Ingenious_ServerParams, [0, 'desc']);

       report_from.on('change', function() {
         var val = $(this).val();
         var report_to_val = report_to.val();
         if (val != '') {
           report_to.attr('disabled', false);
           $(filter_selector_helper).removeClass('active').addClass('hide');
           $('input[name^="year_"]').val('');
           $('input[name^="expenses_by_month_"]').val('');
         } else {
            report_to.attr('disabled', true);
         }

         if ((report_to_val != '' && val != '') || (val == '' && report_to_val == '') || (val == '' && report_to_val != '')) {
             $('.table-requests').DataTable().ajax.reload();
         }

         if(val == '' && report_to_val == '' || report_to.is(':disabled') && val == ''){
                $(filter_selector_helper).removeClass('hide');
         }
        });

        report_to.on('change', function() {
             var val = $(this).val();
             if (val != '') {
               $('.table-requests').DataTable().ajax.reload();
           }
       });

       
       $('select[name="currencies"]').on('change',function(){
        $('.table-requests').DataTable().ajax.reload();
    });
   })

</script>
</body>
</html>
