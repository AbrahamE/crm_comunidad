<?php defined('BASEPATH') or exit('No direct script access allowed');

render_datatable([
    _l('name_and_surname'),
    _l('identification_card'),
    _l('business'),
    _l('rif'),
    _l('email'),
    _l('fountain'),
], (isset($class) ? $class : 'visitor'), [], [
    'data-last-order-identifier' => 'visitor',
    'data-default-order'         => get_table_last_order('visitor'),
]);
