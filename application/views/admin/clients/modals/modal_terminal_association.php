<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal fade" id="ticket-terminal-modal_" tabindex="-1" role="dialog">
    <?php if(isset($message)){ ?>
        <div class="alert alert-warning">
        <?php echo $message; ?>
        </div>
    <?php } ?>
<div class="modal-dialog">
        <?php echo form_open(admin_url('clients/associate_terminal/'.$customer_id.'/'.$contactid),array('id'=>'ticket-terminal-formd')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="add-title"><?php echo _l('associate_terminal'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style='display: none'>
                        <div id="additional"></div>
                        <?php echo render_input('userid','codigó de afiliado', $userid ,'text',array('required'=>true)); ?>
                    </div> 
                    
                    <div class="col-md-12">
                        <div id="additional"> Serial</div>
                        <?php echo render_select_terminal('serial', 'serial',$lisTerminals,array('id','serial'),'Banco',(isset($terminal) ? $terminal['serial'] : ''),array('required'=>'true')); ?>	
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"> <?php echo _l('terminal_number'); ?></div>
                        <?php echo render_input('numterminal','','' ,'number',array('min'=>1)); ?>
                    </div>   
                    <div class="col-md-12">
                        <div id="additional"> Código Afiliado</div>
                        <?php echo render_select_terminal('codeafiliado', 'codeafiliado',$acounts,array('id','nombre'),'Banco',(isset($codaffiliate) ? $$codaffiliate : ''),array('required'=>'true')); ?>	
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit"  class="btn btn-info"><?php echo _l('submit'); ?></button>
                <!-- <button href="#" onclick="manage_ticket_terminals(); return false;"  class="btn btn-info"><?php echo _l('submit'); ?></button> -->
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>       
    // cuando cierra el modal
    // window.addEventListener('load',
    (function(){
        appValidateForm($('#ticket-terminal-formd'),
        {
            name:'required',
            numterminal: {
                required:true,
                maxlength:4
            }
        },manage_ticket_terminals);
        
        $('#ticket-terminal-modal_').on('hidden.bs.modal', function(event) {
            $('#additional').html('');
            $('#ticket-terminal-modal_ input[name="serial"]').val('');           
        } );

    })();
    // );
    function manage_ticket_terminals(form) {
        
        var data = $(form).serialize();
        var url = form.action;
        $.post(url, data).done(function(response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                if($.fn.DataTable.isDataTable('.table-terminal-single-client')){
                    $('.table-terminal-single-client').DataTable().ajax.reload();
                }
                alert_float('success', response.message);
                $('#ticket-terminal-modal_').modal('hide');
            } else {
                alert_float('danger', response.message);
            }
        });
        return false;
    }


    
</script>

