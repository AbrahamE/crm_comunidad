<?php

defined('BASEPATH') or exit('No direct script access allowed');

$baseCurrency = get_base_currency();
$statuses = $this->ci->requests_model->get_status();

$aColumns = [
    db_prefix() . 'operations.id as id',
    db_prefix() . 'clients.company as company',
    db_prefix() . 'type_operations.name as type',
    db_prefix() . 'status_operations.name as status_name',
    db_prefix() . 'operations.registration_date as registration_date',
    db_prefix() . 'status_operations.day as day',
    'CASE 
    WHEN '. db_prefix(). 'operations.modified_date is null THEN ' .db_prefix(). 'operations.registration_date
    ELSE ' .db_prefix(). 'operations.modified_date
    END as modified_date',
    db_prefix() . 'clients.rif_number as rif_number',
    db_prefix() . 'invoices.id as invoices_id',
    db_prefix() . 'contacts.phonenumber as phonenumber',
    
    db_prefix() . 'operations_dates.sale as sale',
    db_prefix() . 'operations_dates.payment_confirmed as payment_confirmed',
    db_prefix() . 'operations_dates.bank_management as bank_management',
    db_prefix() . 'operations_dates.account_opened as account_opened',
    db_prefix() . 'operations_dates.send_code_af as send_code_af',
    db_prefix() . 'operations_dates.bank_code_af as bank_code_af',
    db_prefix() . 'operations_dates.serial_assignment as serial_assignment',
    db_prefix() . 'operations_dates.send_par_loading as send_par_loading',
    db_prefix() . 'operations_dates.bank_par_loading as bank_par_loading',
    db_prefix() . 'operations_dates.ccrd_answer as ccrd_answer',
    db_prefix() . 'operations_dates.parameterization as parameterization',
    db_prefix() . 'operations_dates.cari_relocated as cari_relocated',
    db_prefix() . 'operations_dates.ready as ready',
    'CASE 
    WHEN '. db_prefix(). 'operations_dates.retired is null THEN  "NO RETIRADO" 
    ELSE ' .db_prefix(). 'operations_dates.retired
    END as retired',
   
    
];

$sIndexColumn = 'id';
$sTable       = db_prefix() . 'operations';

$where  = [];
$filter = false;

if ($this->ci->input->post('company')) {
    $filter = $this->ci->input->post('company');
    array_push($where, 'AND company = "'.$filter.'"');
} if ($this->ci->input->post('status_name')) {
    $filter = $this->ci->input->post('status_name');
    array_push($where, 'AND `tblstatus_operations`.`name` = "'.$filter.'"');
} if ($this->ci->input->post('type')) {
    $filter = $this->ci->input->post('type');
    array_push($where, 'AND `tbltype_operations`.`name` = "'.$filter.'"');
}

$join = [
    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'operations.client_id',
    'LEFT JOIN ' . db_prefix() . 'status_operations ON ' . db_prefix() . 'status_operations.id_status = ' . db_prefix() . 'operations.status',
    'LEFT JOIN ' . db_prefix() . 'invoices ON ' . db_prefix() . 'invoices.id = ' . db_prefix() . 'operations.invoice_id',
    'LEFT JOIN ' . db_prefix() . 'contacts ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'contacts.userid',
    'LEFT JOIN ' . db_prefix() . 'type_operations ON ' . db_prefix() . 'type_operations.id_type = ' . db_prefix() . 'operations.type',
    'LEFT JOIN ' . db_prefix() . 'operations_dates ON ' . db_prefix() . 'operations_dates.operation_id = ' . db_prefix() . 'operations.id_operation',
   
];

$result = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where, [
    // 'hash',
    'client_id',
    'tbloperations.status',
    'color',
    'id_operation',
]);

$output  = $result['output'];
$rResult = $result['rResult'];
//var_dump($rResult); die;
foreach ($rResult as $aRow) {
  
    $row = [];
    //$numberOutput = '<a href="' . admin_url('requests/list_requests/' . $aRow['id']) . '" onclick="init_requests(' . $aRow['id'] . '); return false;">' . format_requests_number($aRow['id']) . '</a>';
    $numberOutput = '<a href="#" onclick="terminals(' . $aRow['client_id'] . ',' . $aRow['id'] . ');return false;">' . format_requests_number($aRow['id']) . '</a>';
    //$numberOutput =  format_requests_number($aRow['id']);
    $numberOutput .= '<div class="row-options">';
    $numberOutput .= '<a href="' . admin_url('requests/requests/' . $aRow['id']) . '">' . _l('edit') . '</a>';
    $numberOutput .= '| <a href="#" onclick="requests_modal_convert_document_sale('. $aRow['id'] . ');return false;">' . _l('proposal_convert_to_sales_document') . '</a>';
    $numberOutput .= '</div>';

    
    $row[] = $numberOutput;

    //$row[] = $aRow['id'];

    $row[] = '<a href="' . admin_url('clients/client/' . $aRow['client_id']) . '">' . $aRow['company'] . '</a>';
    // $row[] = $aRow['company'];

    //$row[] = format_requests_type($aRow['type']);

    $row[] = $aRow['type'];    

    $outputStatus = '<span class="inline-block lead-status-'.$aRow['status'].' label label-' . (empty($aRow['color']) ? 'default': '') . '" style="color:' . $aRow['color'] . ';border:1px solid ' . $aRow['color'] . '">' .  $aRow['status_name'];
   
    $outputStatus .= '<div class="dropdown inline-block mleft5 table-export-exclude">';
    $outputStatus .= '<a href="#" style="font-size:10px;vertical-align:middle;" class="dropdown-toggle text-dark" id="tableLeadsStatus-' . $aRow['id_operation'] . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
    if ($aRow['status'] != '1' && $aRow['status'] != '2' && $aRow['status'] != '14') {
        $outputStatus .= '<span data-toggle="tooltip" title="' . _l('ticket_single_change_status') . '"><i class="fa fa-caret-down" aria-hidden="true"></i></span>';
    }
        $outputStatus .= '</a>';

    $outputStatus .= '<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="tableLeadsStatus-' . $aRow['id_operation'] . '">';
        foreach ($statuses as $leadChangeStatus) {
                //if ($aRow['status'] != $leadChangeStatus['id_status']) {
                if ($aRow['status'] != $leadChangeStatus['id_status']) {
                    $outputStatus .= '<li>
                                        <a href="#" onclick="change_of_status('.$leadChangeStatus['id_status'].','.$aRow['id_operation'].','.$aRow['status'].'); return false;">
                                            ' . $leadChangeStatus['name'] . '
                                        </a>
                                    </li>';
                }
            
        }
    $outputStatus .= '</ul>';
    $outputStatus .= '</div>';
    
    $outputStatus .= '</span>';


    $row[] = $outputStatus;
    $row[] = $aRow['registration_date']; 
    $row[] = requests_day($aRow['id_operation'], $aRow['day'], '' ,'operations');
     
    $row[] = $aRow['modified_date'];  
    $row[] = $aRow['rif_number'];
    $row[]  = '<a href="' . admin_url('invoices/list_invoices/' . $aRow['invoices_id']) . '">' . format_invoice_number($aRow['invoices_id']) . '</a>';
    $row[] = $aRow['phonenumber'];
    
    $row[] = $aRow['sale'];
    $row[] = $aRow['payment_confirmed'];
    $row[] = $aRow['bank_management'];
    $row[] = $aRow['account_opened'];
    $row[] = $aRow['send_code_af'];
    $row[] = $aRow['bank_code_af'];
    $row[] = $aRow['serial_assignment'];
    $row[] = $aRow['send_par_loading'];
    $row[] = $aRow['bank_par_loading'];
    $row[] = $aRow['ccrd_answer'];
    $row[] = $aRow['parameterization'];
    $row[] = $aRow['cari_relocated'];
    $row[] = $aRow['ready'];
    $row[] = $aRow['retired'];
    

    //$row[] = format_requests_status($aRow['status']);
    
    //$row[] = $aRow['day'].' '. _l('Dias');
    // echo '<pre>';
    // print_r( $row); die;
    $output['aaData'][] = $row;
}