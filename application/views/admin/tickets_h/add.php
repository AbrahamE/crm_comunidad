<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">

	<div class="content">
		
		<?php echo form_open_multipart($this->uri->uri_string(),array('id'=>'new_ticket_form')); ?>

			<div class="row">
				<div class="col-md-12">
					<div class="panel_s">
						<div class="panel-body">							
							
							<div class="col-md-6">

								<div class="row">

									<div class="col-md-12">
										<?php if(!isset($project_id) && !isset($contact)): ?>
											<a href="#" id="ticket_no_contact"><span class="label label-default"><i class="fa fa-envelope"></i> <?php echo _l('ticket_create_no_contact'); ?> </span> </a>
											<a href="#" class="hide" id="ticket_to_contact"><span class="label label-default"><i class="fa fa-user-o"></i> <?php echo _l('ticket_create_to_contact'); ?></span></a>
											<div class="mbot15"></div>
										<?php endif ?>
									</div>

									<div class="col-md-12">
										<?php echo render_select('incidents',$incidents,array('id','incidents'),'incidents',(count($incidents) == 1) ? $incidents[0]['departmentid'] : '',array('required'=>'true')); ?>
									</div>

									<div class="col-md-12">
										<div class="form-group select-placeholder" id="ticket_contact_w">

											<label for="contactid"><?php echo _l('contact'); ?></label>
									
											<select name="contactid" required="true" id="contactid" class="ajax-search search-contactid" data-width="100%" data-abs-cache="false" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
												<?php if(isset($contact)) { ?>
													<option value="<?php echo $contact['id']; ?>" selected><?php echo $contact['firstname'] . ' ' .$contact['lastname']; ?></option>
												<?php } ?>
												<option value=""></option>
											</select>

											<?php echo form_hidden('userid'); ?>
										</div>
									</div>

								</div>

								<div class="row">
									<div class="col-md-6">
										<?php echo render_input('name_user','ticket_settings_to','','text',array('disabled'=>true)); ?>
									</div>
									<div class="col-md-6">
										<?php echo render_input('email_user','ticket_settings_email','','email',array('disabled'=>true)); ?>
									</div>
									<div class="col-md-6">
										<?php echo render_input('rif_number_user','Rif','','text',array('disabled'=>true)); ?>
									</div>
									<div class="col-md-6">
										<?php echo render_input('codaffiliate_user','Codigo de afiliacion','','text',array('disabled'=>true)); ?>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<?php echo render_select('department',$departments,array('departmentid','name'),'ticket_settings_departments',(count($departments) == 1) ? $departments[0]['departmentid'] : '',array('required'=>'true'),array(),'','',false); ?>
									</div>

									<div class="col-md-6">
										<?php echo render_input('cc','CC'); ?>
									</div>
													
								</div>

							</div>

							<div class="col-md-6"><br><br>
								
								<div class="row">

									<div class="col-md-12">
										<div class="form-group">
											<label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
											<input type="text" class="tagsinput" id="tags" name="tags" data-role="tagsinput">
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group form-check mtop25">
											<label class="form-check-label" for="citado">Citado</label>
											<input type="checkbox" class="form-check-input" id="citado">
										</div>
									</div>
	
									<div class="col-md-6">
										<div class="form-group select-placeholder">
											<label for="assigned" class="control-label">
												<?php echo _l('ticket_settings_assign_to'); ?>
											</label>
											<select name="assigned" id="assigned" class="form-control selectpicker" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-width="100%">
												<option value=""><?php echo _l('ticket_settings_none_assigned'); ?></option>
												<?php foreach($staff as $member){ ?>
													<option value="<?php echo $member['staffid']; ?>" <?php if($member['staffid'] == get_staff_user_id()){echo 'selected';} ?>>
														<?php echo $member['firstname'] . ' ' . $member['lastname'] ; ?>
													</option>
												<?php } ?>
											</select>
										</div>
									</div>

									<div class="col-md-6">
										<?php $priorities['callback_translate'] = 'ticket_priority_translate'; echo render_select('priority', $priorities, array('priorityid','name'), 'ticket_settings_priority', hooks()->apply_filters('new_ticket_priority_selected', 2), array('required'=>'true')); ?>
									</div>

									<div class="col-md-6">
										<?php if(get_option('services') == 1): ?>
											<?php if(is_admin() || get_option('staff_members_create_inline_ticket_services') == '1'){
												echo render_select_with_input_group('service',$services,array('serviceid','name'),'type_request','','<a href="#" onclick="new_service();return false;"><i class="fa fa-plus"></i></a>');
											} else {
												echo render_select('service',$services,array('serviceid','name'),'ticket_settings_service');
											}
											?>
										<?php endif ?>
									</div>

									<div class="col-md-6">

										<div class="terminales form-group form-group-select-input-service input-group-select">
											<label for="terminal">Terminal</label>
											<div class='input-group input-group-select select-terminal'>
												<select name="terminal" id="terminal" class="ajax-search-terminal selectpicker jqTransformSelectWrapper" data-width="100%" data-live-search="true" data-abs-cache="false" data-abs-bind-event="click" data-abs-preserve-selected="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" disabled>
													<option value=""></option>
												</select>
												<div id='addterminal' class='input-group-addon' style="opacity: 1;">
													<a href="#" onclick="new_terminal();return false;"><i class="fa fa-plus"></i></a>
												</div>
											</div>
										</div>

									</div>
								
									<!-- <div class="col-md-6">
									
										<?php echo render_input('file_csv','choose_csv_file','','file'); ?>
										<a href='#'id='excel' onclick="readExcel()" data-form="#readExcel" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info">
											<?php echo _l('Exportar desde excel'); ?>
										</a>
										
									</div> -->

								</div>	

								<div class="row">
									<div class="col-md-12">
										<?php echo render_custom_fields('tickets'); ?>
									</div>
								</div>

								<div class="row">
									<div class="form-group projects-wrapper hide">
										<label for="project_id"><?php echo _l('project'); ?></label>
										<div id="project_ajax_search_wrapper">
											<select name="project_id" id="project_id" class="projects ajax-search" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"<?php if(isset($project_id)){ ?> data-auto-project="true" data-project-userid="<?php echo $userid; ?>"<?php } ?>>
												<?php if(isset($project_id)){ ?>
													<option value="<?php echo $project_id; ?>" selected><?php echo '#'.$project_id. ' - ' . get_project_name_by_id($project_id); ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel_s">

						<div class="panel-heading">
							Terminales
						</div>
						<div class="panel-body">
							<div id='table' class='table-responsive-vertical' style='overflow: auto;'>
								<table class="table table-hover table-mc-light-blue" style="width:100%" id='tableticketsresumen'>
									<thead>
										<tr>
											<th style='display: none'>id</th>
											<th>codigo de afiliado</th>
											<th>Serial</th>
											<th>Mac</th>
											<th>Imei</th>
											<th>Banco</th>
											<th>Modelo</th>
											<th>Simcard</th>
											<th>Operadora</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody id='terminales'>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel_s">
						<div class="panel-heading">
							<?php echo _l('ticket_add_body'); ?>
						</div>
						<div class="panel-body">
							<div class="btn-bottom-toolbar text-right">
								<button type="submit" id='submit1' data-form="#new_ticket_form" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info"><?php echo _l('open_ticket'); ?></button>
							</div>
							<div class="row">
								<div class="col-md-12 mbot20 before-ticket-message">
									<div class="row">
										<div class="col-md-6">
											<select id="insert_predefined_reply" data-width="100%" data-live-search="true" class="selectpicker" data-title="<?php echo _l('ticket_single_insert_predefined_reply'); ?>">
												<?php foreach($predefined_replies as $predefined_reply){ ?>
													<option value="<?php echo $predefined_reply['id']; ?>"><?php echo $predefined_reply['name']; ?></option>
												<?php } ?>
											</select>
										</div>
										<?php if(get_option('use_knowledge_base') == 1){ ?>
											<div class="visible-xs">
												<div class="mtop15"></div>
											</div>
											<div class="col-md-6">
												<?php $groups = get_all_knowledge_base_articles_grouped(); ?>
												<select id="insert_knowledge_base_link" data-width="100%" class="selectpicker" data-live-search="true" onchange="insert_ticket_knowledgebase_link(this);" data-title="<?php echo _l('ticket_single_insert_knowledge_base_link'); ?>">
													<option value=""></option>
													<?php foreach($groups as $group){ ?>
														<?php if(count($group['articles']) > 0){ ?>
															<optgroup label="<?php echo $group['name']; ?>">
																<?php foreach($group['articles'] as $article) { ?>
																	<option value="<?php echo $article['articleid']; ?>">
																		<?php echo $article['subject']; ?>
																	</option>
																<?php } ?>
															</optgroup>
														<?php } ?>
													<?php } ?>
												</select>
											</div>
										<?php } ?>
									</div>


								</div>
							</div>
							<div class="clearfix"></div>
							<?php echo render_textarea('message','','',array(),array(),'','tinymce'); ?>
						</div>
						<div class="panel-footer attachments_area" >
							<div class="row attachments">
								<div class="attachment">
									<div class="col-md-4 col-md-offset-4 mbot15">
										<div class="form-group">
											<label for="attachment" class="control-label"><?php echo _l('ticket_add_attachments'); ?></label>
											<div class="input-group">
												<input type="file" extension="<?php echo str_replace(['.', ' '], '', get_option('ticket_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_ticket_form_accepted_mimes(); ?>">
												<span class="input-group-btn">
													<button class="btn btn-success add_more_attachments p8-half" data-max="<?php echo get_option('maximum_allowed_ticket_attachments'); ?>" type="button"><i class="fa fa-plus"></i></button>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php echo form_close(); ?>

	</div>

</div>


<?php init_tail(); ?>
<?php hooks()->do_action('new_ticket_admin_page_loaded'); ?>
<?php $this->load->view('admin/tickets/terminal/terminal', array('banks' => $banks , 'model' => $model , 'sincard' => $simcard , 'operadora' => $operadora)); ?>

<script>

	$(function(){
		var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
		init_ajax_search('contact','#contactid.ajax-search',{
			tickets_contacts: true,
			contact_userid: function(){
					// when ticket is directly linked to project only search project client id contacts
					var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
					if(uid){
						return uid;
					} else {
						return '';
					}
				}
			}
		);

		validate_new_ticket_form();

		<?php if(isset($project_id) || isset($contact)){ ?>
			$('body.ticket select[name="contactid"]').change();
		<?php } ?>

		<?php if(isset($project_id)){ ?>
			$('body').on('selected.cleared.ajax.bootstrap.select','select[data-auto-project="true"]',function(e){
				$('input[name="userid"]').val('');
				$(this).parents('.projects-wrapper').addClass('hide');
				$(this).prop('disabled',false);
				$(this).removeAttr('data-auto-project');
				$('body.ticket select[name="contactid"]').change();
			});
		<?php } ?>


		$(".ajax-search .dropdown-menu").click(function() {
			var e;
            var t = $('select[name="contactid"]').val();
            var a = $('select[name="project_id"]');
            var i = a.attr("data-auto-project");
            var n = $(".projects-wrapper");

			var stock_name = [];

			$("#tableticketsresumen tbody tr").each(function (index) {
				if (index >= 0 ) {
					$('#terminales').html('');
					$('#table').removeClass('shadow-z-1');	
					i = 0;
				}
			});

            a.attr("disabled") || ( i ? (e = a.clone()).prop("disabled", !0 ) : (e = a.html("").clone()),
                a.selectpicker("destroy").remove(),
                (a = e),
                $("#project_ajax_search_wrapper").append(e),
                init_ajax_search("project", a, {
                    customer_id: function () {
                        return $('input[name="userid"]').val();
                    },
                })
            );
			if (t != '') {
				$.post(admin_url + "tickets/ticket_change_data/", { contact_id: t }).done(function (e) {
					var res = JSON.parse(e).contact_data;
					$('input[name="name_user"]').val(res.firstname + " " + res.lastname);
					$('input[name="email_user"]').val(res.email);
					$('input[name="userid_user"]').val(res.userid);
					$('input[name="rif_number_user"]').val(res.rif_number);
					$('input[name="codaffiliate_user"]').val(res.codaffiliate);
					$('input[name="userid"]').val(res.userid);
					AjaxTerminal(res.userid);
					if (res.ticket_emails == "0") {
						show_ticket_no_contact_email_warning(res.userid, res.id);
					} else{
						clear_ticket_no_contact_email_warning()
					}
					(i) ? n.removeClass("hide") : res.customer_has_projects ? n.removeClass("hide") : n.addClass("hide");
				})
			}
		});

		function AjaxTerminal(id){
			var template =  "<label for='terminal'>Terminal</label>";
				template +=	"<div class='input-group input-group-select select-terminal'>";
				template +=	"<select name='terminal' id='terminal' class='ajax-search-terminal selectpicker jqTransformSelectWrapper' data-width='100%' data-live-search='true' data-abs-cache='false' data-abs-bind-event='click' data-abs-preserve-selected='false' data-none-selected-text='<?php echo _l('dropdown_non_selected_tex'); ?>' >";
				template +=	"<option value=''></option>";
				template +=	"</select>";
				template +=	"<div id='addterminal' class='input-group-addon' style='opacity: 1;'>";
				template +=	"<a href='#' onclick='new_terminal();return false;'><i class='fa fa-plus'></i></a>";
				template +=	"</div>";
				template +=	"</div>";

			if(id == $('#userid').val()){
				$(".terminales").html(template);
				init_ajax_search_terminal('terminal','#terminal.ajax-search-terminal',
					{
						tickets_contacts: true,
						contact_userid: id
					}
				);
				
				$(".jqTransformSelectWrapper").on("click", '.dropdown-menu.open li:not([class="divider"],[class="dropdown-header"],[class="optgroup-1"])', function (event) {
					var t = $('#terminal').val();
					$.post(admin_url + "tickets/ticket_change_data_terminal/", { contact_id: t }).done(
						function (e) {
							res = JSON.parse(e);
							if (res.contact_data) {
								console.log(res.contact_data);
								SerializeArrayTerminalExists(res.contact_data);
							}
							// else{
							// 	console.log(res.contact_data);
							// 	alert_float('danger', 'terminal no existe');
							// }
						}
					)
				});
			}
			
		}

		
	});
	
	function TerminalExists(data,id){

		var namebanks       = 'terminal['+i+'][banks]';

		var namemodel       = 'terminal['+i+'][model]';

		var nameoperadora   = 'terminal['+i+'][operadora]';

		var namesimcard     = 'terminal['+i+'][simcardid]';

		var idbanks         = 'banks_'+i+'';

		var idmodel         = 'model_'+i+'';

		var idoperadora     = 'operadora_'+i+'';

		var idsimcard       = 'simcardid_'+i+'';

		var template        = '';

		var rowCount        = $('#terminales tr').length;
    
		var error = 0;

		var controlP1 = 0;

		var controlP2 = 0;

		var controlM1 = 0;

		var controlM2 = 0;

		var stock_name = [];

		var stock_name2 = [];

		
		if(rowCount === 0){
			template = terminaltemplate(data);	
			alert_float('success', 'terminal agregada');


		} else{
			
			$("#tableticketsresumen tbody tr").each(function (index) {

				$(this).children("td:eq(0)").each(function (index2) {
					if ($(this).find('input').val() != '') {
						stock_name[controlP1++] = $(this).find('input').val();
					}
				});
				
			});
			
			if (stock_name.indexOf(data[0].value) == -1) {
				template = terminaltemplate(data);
				alert_float('success', 'terminal agregada');
			} else{
				alert_float('danger', 'terminal duplicada');
				return false;
			}
		}

		$("#terminales").append(template);
	
		// inicia selectpicker
		$('select[id="simcardid"]').selectpicker('val', sincard);	
		$('select[id="banks"]').selectpicker('val', banks);
		$('select[id="model"]').selectpicker('val', model);
		$('select[id="operadora"]').selectpicker('val', operadora);
		$('select[id="terminal"]').selectpicker('val', id);

		// cambia valores de los input 
		$('select[id="banks"]').attr("name",namebanks);
		$('select[id="banks"]').attr("id",idbanks);
		$('select[id="model"]').attr("name",namemodel);
		$('select[id="model"]').attr("id",idmodel);
		$('select[id="operadora"]').attr("name",nameoperadora);
		$('select[id="operadora"]').attr("id",idoperadora);	
		$('select[id="simcardid"]').attr("name",namesimcard);
		$('select[id="simcardid"]').attr("id",idsimcard);
		$('#table').addClass('shadow-z-1');		   
		i++;

	}

	function terminaltemplate(data){
		var template        = '';

		var InArrayTerminal = ['id',"codaffiliate_user","serial","mac","imei","modelid","banksid","simcardid","simcard","operadoraid"];	

		for (const K in data) {
			if (Object.hasOwnProperty.call(data, K)) {
				const element = data[K];	
				if(element.name == 'id'){
					template    = `<tr id='T_id_${element.value}'>`;
				}
				if (InArrayTerminal.indexOf(element.name) != -1 ) {
					switch (element.name) {
						case 'id':
							template += `<td data-title="${element.name}" style="display: none"> <div class="form-group ptop10"> <input id="${element.name}_${i}" type="text" name="terminal[${i}][${element.name}]" value="${element.value}"> </div> </td>`;
							break;
						case 'modelid':
							template += '<td data-title="'+element.name+'" > <div class="form-group ptop10"> <?php echo render_select_terminal('model', 'terminal[0][model]', $model,array('modelid','model'),'Banco',(count($model) == 1) ? $model[0]['modelid'] : '',array('required'=>'true')); ?>	</div> </td>';
							model = element.value;
							break;
						case 'simcardid':
							template += `<td data-title="sincard" > <div class="form-group ptop10"> <?php  echo render_select_terminal('simcardid', 'simcardid', $simcard,array('id','serialsimcard'),'Banco',(count($simcard) == 1) ? $simcard[0]['id'] : '',array('required'=>'true')); ?> 	</td>`;
							sincard = element.value; 
							break;
						case 'simcard':
							template += `<td data-title="${element.name}" > <div class="form-group ptop10"> <?php  echo render_select_terminal('simcardid', 'simcardid', $simcard,array('id','serialsimcard'),'Banco',(count($simcard) == 1) ? $simcard[0]['id'] : '',array('required'=>'true')); ?> 	</td>`;
							sincard = element.value; 
							break;	
						case 'banksid':
							template += '<td data-title="'+element.name+'" > <div class="form-group ptop10"> <?php echo render_select_terminal('banks', 'terminal[0][banks]', $banks,array('id','banks'),'Banco',(count($banks) == 1) ? $banks[0]['id'] : '',array('required'=>'true')); ?> </div> </td>';
							banks = element.value; 
							break;
						case "operadoraid":
							template += '<td data-title="'+element.name+'" > <div class="form-group ptop10"> <?php echo render_select_terminal('operadora','terminal[0][operadora]',$operadora,array('operadoraid','operadora'),'ticket_settings_departments',(count($operadora) == 1) ? $operadora[0]['operadoraid'] : '',array('required'=>'true')); ?> </div> </td>';
							operadora = element.value; 
							break;
						default:
							template += '<td data-title="'+element.name+'" ><div class="form-group ptop10"> <input type="text" id="'+element.name+'" name="terminal['+i+']['+element.name+']" class="form-control input_class" '+ (element.value != null ? 'value="'+element.value+'"' : "") +' required></div></td>';
							break;
					}
				}
			}
		}

		template += '<td data-title="delete"> <a href="#" onclick="DeleTeterminal(this)" class="btn btn-primary mtop10"><i class="fa fa-trash"></i></a> </td> ';
		template += '</tr>';
		return template;
	}

	function readExcel() {
		var t = $('#file_csv').val();
		$(this).hasClass("simulate") && $("#import_form").append(hidden_input("simulate", !0)), $("#import_form").submit();
		if(t != ''){
			$.post(admin_url + "tickets/terminal_fiel/", { file_csv: t}).done(
                function (e) {                   
                    // res = JSON.parse(e);
                }
            )
		}else{
			alert_float('danger', 'no se a encontrado nigun archivo');
		}
	}

</script>
</body>
</html>
