<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Rate extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payments_model');
    }

    /* In case if user go only on /payments */
    public function index()
    {
        $this->list_rate();
    }

    public function list_rate()
    {
        if (!has_permission('payments', '', 'view')
            && !has_permission('invoices', '', 'view_own')
            && get_option('allow_staff_view_invoices_assigned') == '0') {
            access_denied('payments');
        }

        $data['title'] = _l('tasa');
        $this->load->view('admin/rate/manage', $data);
    }

    public function table($clientid = '')
    {
        if (!has_permission('invoices', '', 'view')
            && !has_permission('invoices', '', 'view_own')
            && get_option('allow_staff_view_invoices_assigned') == '0') {
            ajax_access_denied();
        }

        $this->app->get_table_data('rate', [
            'clientid' => $clientid,
        ]);
    }

   
}
