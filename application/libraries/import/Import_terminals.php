<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'libraries/import/App_import.php');

class Import_terminals extends App_import
{
    protected $notImportableFields = [];

    private $countryFields = ['country', 'billing_country', 'shipping_country'];

    protected $requiredFields = ['firstname', 'lastname', 'email'];

    public function __construct()
    {
        $this->notImportableFields = hooks()->apply_filters('not_importable_clients_fields', ['userid', 'id', 'is_primary', 'password', 'datecreated', 'last_ip', 'last_login', 'last_password_change', 'active', 'new_pass_key', 'new_pass_key_requested', 'leadid', 'default_currency', 'profile_image', 'default_language', 'direction', 'show_primary_contact', 'invoice_emails', 'estimate_emails', 'project_emails', 'task_emails', 'contract_emails', 'credit_note_emails', 'ticket_emails', 'addedfrom', 'registration_confirmed', 'last_active_time', 'email_verified_at', 'email_verification_key', 'email_verification_sent_at']);

        if (get_option('company_is_required') == 1) {
            $this->requiredFields[] = 'company';
        }

        $this->addImportGuidelinesInfo('Duplicate email rows won\'t be imported.', true);

        $this->addImportGuidelinesInfo('Make sure you configure the default contact permission in <a href="' . admin_url('settings?group=clients') . '" target="_blank">Setup->Settings->Customers</a> to get the best results like auto assigning contact permissions and email notification settings based on the permission.');

        parent::__construct();
    }

    public function perform()
    {
        $this->initialize();
        $this->delimiter = ';';
        $databaseFields      = $this->getImportableDatabaseFields();
        $totalDatabaseFields = count($databaseFields);
        //var_dump($databaseFields);die;
        $t =  [];
        $id = null ;
        foreach ($this->getRows() as $rowNumber => $row) {
            $insert    = [];
            $duplicate = false;

            for ($i = 0; $i < $totalDatabaseFields; $i++) {
                if (!isset($row[$i])) {
                    continue;
                }

                $row[$i] = $this->checkNullValueAddedByUser($row[$i]);


                $insert[$databaseFields[$i]] = $row[$i];
            }

            if ($duplicate) {
                continue;
            }

            if (count($insert) > 0) {
                $this->incrementImported();
                //var_dump($insert); die;
                $id = null;

                if (!$this->isSimulation()) {

                    $insert['userid'] =  $this->ci->input->post('userid');
                    $insert['banks'] =  $this->ci->input->post('banks');
                    $insert['operadoraid'] =  6 ;
                    $insert['codaffiliate'] =  $this->ci->input->post('userid') ;
                    $insert['simcard'] = 3513;

                    $this->ci->db->where('model', $insert['modelid']);
                    $id_model = $this->ci->db->get(db_prefix().'terminal_models')->result_array();

                    if ($id_model){

                        $insert['modelid'] = $id_model[0]["modelid"];

                    }else{

                        $insert['modelid'] = 30;
                    }
                    
                    $this->ci->db->where('serial', $insert['serial']);
                    $select = $this->ci->db->get(db_prefix().'terminals')->result_array();
                    
                    if($select){ 

                        $this->ci->db->where('serial', $insert['serial']);
                        $this->ci->db->update(db_prefix() . 'terminals', [
                            'modelid' => $insert['modelid'],
                            'codaffiliate' => $insert['codaffiliate'],
                            'serial' => $insert['serial'],
                            'mac' => $insert['mac'],
                            'imei' => $insert['imei'],
                            'simcard' => $insert['simcard'],
                            'operadoraid' => $insert['operadoraid'],
                            'status' => 1,
                            'banks' => $insert['banks'],
                            'userid' => $insert['userid'],
                        ]);
                        $id = current($select)["id"];

                    }else{
                       
                        $this->ci->db->insert(db_prefix().'terminals', $insert);
                        $id = $this->ci->db->insert_id();
                        
                    }
                    $t[]  =  $this->ci->tickets_model->get_terminal($id);
                } 
            }

            if ($this->isSimulation() && $rowNumber >= $this->maxSimulationRows) {
                break;
            }
        }
            foreach($t as $key => $value){
                $insert_tickets['terminal_resumen'] = json_encode($t);
            }

            //var_dump($t); die;
            $insert_tickets['contactid'] = $this->ci->input->post('contactid');
            $insert_tickets['userid'] = $this->ci->input->post('userid');
            $insert_tickets['email_user'] = $this->ci->input->post('email');
            $insert_tickets['assigned'] = $this->ci->input->post('assigned');
            $insert_tickets['department'] = $this->ci->input->post('department');
            $insert_tickets['priority'] = $this->ci->input->post('priority');
            $insert_tickets['service'] = $this->ci->input->post('service');
            $insert_tickets['date']      = date('Y-m-d H:i:s');
            $insert_tickets['ticketkey'] = app_generate_hash();
            $insert_tickets['message'] = 'Mensaje por defecto';
           
            $Data = array(
                'ticketid'      => null,
                'adminreplying' => 1,
                'userid'        => $insert_tickets['userid'],
                'contactid'     => $insert_tickets['contactid'],
                'email'         => $insert_tickets['email_user'],
                'department'    => $insert_tickets['department'],
                'priority'      => $insert_tickets['priority'],
                'status'        => 1,
                'service'       => $insert_tickets['service'],
                'ticketkey'     => $insert_tickets['ticketkey'],
                'subject'       => 18,
                'date'          => $insert_tickets['date'],
                'sales_date'    => $insert_tickets['date'],
                'date_warranty' => $insert_tickets['date'],
                'project_id'    => 0,
                'clientread'    => 1,
                'adminread'     => 1,
                'message'       => $insert_tickets['message'],
                'assigned'      => $insert_tickets['assigned'],
                'admin'         => $insert_tickets['assigned'],
                'resumetickets' => $insert_tickets['terminal_resumen'],
                'pdf'           => ''
            );
            
            $this->ci->db->insert(db_prefix() . 'tickets', $Data);
            
    }

    protected function failureRedirectURL()
    {
        return admin_url('tickets/import');
    }

    
}
