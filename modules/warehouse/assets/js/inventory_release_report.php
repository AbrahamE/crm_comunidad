<script>
  function get_data_inventory_release_report() {
    "use strict";
    var formData = new FormData();

    formData.append("csrf_token_name", $('input[name="csrf_token_name"]').val());
    if ($("[name='expandir_rango']").prop("checked") == true) {
      formData.append("from_date", $('input[name="from_date"]').val());
      formData.append("to_date", $('input[name="to_date"]').val());
    }
    if ($("[name='expandir_almacen']").prop("checked") == true) {
      formData.append("warehouse_id", $('select[name="warehouse_id"]').val());;
    }

    $.ajax({
      url: admin_url + 'warehouse/get_data_inventory_release_report',
      method: 'post',
      data: formData,
      contentType: false,
      processData: false
    }).done(function(response) {
      var response = JSON.parse(response);
      var trs = [];
      response.value.forEach(function(value) {
        var tr = $("<tr>");
        tr.html([
          $('<td colspan="1">').html($("<a href='<?php echo admin_url("warehouse/inventory_release/") ?>" + value.id + "'>").html(value.goods_delivery_code)),
          $('<td colspan="1">').text(value.customer_code),
          $('<td colspan="1">').text(value.customer_name),
          $('<td colspan="2">').text(value.to_),
          $('<td colspan="2">').text(value.address),
          $('<td colspan="2">').text(value.firstnam)
        ]);
        trs.push(tr);
      });
      $("#inventory_entry_report table tbody").html(trs);

      var monkeyList = new List('inventaryRelease', {
        valueNames: ['name'],
        page: 5,
        pagination: true
      });

    });

  }

  function expandir_formulario(type) {

    if (type == 'rango') {
      var x = document.getElementById("rangofecha");
      if (x.style.display === "none") {
        x.style.display = "block";
      } else {
        x.style.display = "none";
      }
    }
    if (type == 'almacen') {
      var x = document.getElementById("almacen");
      if (x.style.display === "none") {
        x.style.display = "block";
      } else {
        x.style.display = "none";
      }
    }
  }


  function stock_submit(invoker) {
    "use strict";
    $('#inventory_entry_report').submit();
  }
</script>