<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="modal fade " id="terminal-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="alert alert-success" id="alert" style="display: none;">&nbsp;</div>
        <?php echo form_open(admin_url('tickets/terminal_add'),array('id'=>'add-new-terminal-form')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close " data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span class="edit-title"><?php echo _l('ticket_terminal_edit'); ?></span>
                    <span class="add-title"><?php echo _l('Nueva terminal'); ?></span>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="requiere_simcard" id="requiere_simcard"  value=""/>  

                    <div class="col-md-12 hidden" >
                        <div id="additional"></div>
                        <?php echo render_input('userid','codigó de afiliado','','text',array()); ?>
                    </div> 
                    <div class="col-md-12 hidden">
                        <div id="additional"></div>
                        <?php echo render_input('codaffiliate_user','Codigó de afiliado','','text',array()); ?>
                    </div> 
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('serial','Serial','','text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('mac','MAC','','text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"></div>
                        <?php echo render_input('imei','IMEI','','text',array('required'=>true)); ?>
                    </div>
                    <div class="col-md-12 hidden">
                        <div id="additional"> Banco</div>
                        <?php echo render_select_terminal('banksid', 'banksid', $banks,array('id','banks'),'Banco','',array()); ?>
                    </div>
                    <div class="col-md-12">
                        <div id="additional"> Modelo</div>
                        <?php echo render_select_terminal('modelid', 'modelid', $model,array('modelid','model'),'Banco',(count($model) == 1) ? $model[0]['modelid'] : '',array('required'=>'true')); ?>	
                    </div>
                    <div id ="div_simcar" class="col-md-12">
                        <div id="additional"></div><br>
                        <div class="form-group" app-field-wrapper="serialsimcard">
                            <label for="serialsimcard" class="control-label"><?php echo _l('sim_card_serial'); ?></label>
                            <select name="simcard"  id="simcard" class="ajax-search simcard" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                <option value=""></option>
                            </select>
                            <p id="serialsimcard-error" class="text-danger" style="display: none;"> </p>
                        </div>
                    </div>

                    <div class="col-md-12 hidden" >
                        <div id="additional"></div><br>
                        <label for="operadoraid"><?php echo _l('operator'); ?></label>
                        <?php echo render_select_terminal('operadoraid','operadoraid',$operadora,array('operadoraid','operadora'),'ticket_settings_departments',(count($operadora) == 1) ? $operadora[0]['operadoraid'] : '',array('required'=>'true'));?>
                    </div>

                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?> </button>
            </div>
        </div><!-- /.modal-content -->
        <?php echo form_close(); ?>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    // cuando cierra el modal
     (function(){
      
    // //     appValidateForm('#add-new-terminal-form',{name:'required'}, manage_terminals);
        $('#terminal-modal').on('hidden.bs.modal', function(event) {
            $('#additional').html('');
            //debugger
            $('#terminal-modal input[id="codaffiliate_terminal"]').val("");
            $('#terminal-modal input[id="serial"]').val("");
            $('#terminal-modal input[id="mac"]').val("");
            $('#terminal-modal input[id="imei"]').val("");
            $('#terminal-modal select[id="banksid"]').selectpicker('val', '');  
            $('#terminal-modal select[id="modelid"]').selectpicker('val', '');  
            $('#terminal-modal select[id="simcard"]').selectpicker('val', ''); 
            $('#terminal-modal select[id="operadoraid"]').selectpicker('val', '');
            
        });
     });  

    //--nueva funcion de validacion
    $('#add-new-terminal-form').validate({
            rules: {
                serial: 'required',
                mac: 'required',
                imei: 'required',
                model: 'required',
                
            },
            submitHandler: function manage_terminals(form) {   
       
            var Data = $('#add-new-terminal-form').serializeArray();
            var data = $(form).serialize();
            var url = form.action;
            var ticketArea = $('body').hasClass('ticket');
            if(ticketArea) {
                data += '&ticket_area=true';
            }
        
            $.post(url, data).done(function(e) {

                var response = JSON.parse(e);
                if(response.status == 'success'){
                    if($.fn.DataTable.isDataTable('.table-expenses')){
                        $('.table-expenses').DataTable().ajax.reload();
                    }
                    alert_float('success', response.message);
                }else{
                    alert_float('danger', response.message);
                }
                $('#terminal-modal').modal('hide');
            });
            $('#terminal-modal input[id="codaffiliate_terminal"]').val("");
            $('#terminal-modal input[id="serial"]').val("");
            $('#terminal-modal input[id="mac"]').val("");
            $('#terminal-modal input[id="imei"]').val("");
            $('#terminal-modal select[id="banksid"]').selectpicker('val', '');  
            $('#terminal-modal select[id="modelid"]').selectpicker('val', '');  
            $('#terminal-modal select[id="simcard"]').selectpicker('val', ''); 
            $('#terminal-modal select[id="operadoraid"]').selectpicker('val', '');
            //$('#erialsimcard-error').hide('');
            
            return false;
    }          

    });

  

    $("#modelid").change(function(){
        //console.log('cambio el modelo de terminal');
        let mArray = <?= $model ? json_encode($model) : []; ?>;
        //console.log(mArray);

        mArray.find( m =>  m.modelid == $('#modelid').val() );
        a = mArray.find( m =>  m.modelid == $('#modelid').val() );
        a.conexiontype;
        //console.log('CNECCION TYPE:'+a.conexiontype);  
         
            if (a.conexiontype=='Dial up/Lan'){
               $('#simcard').removeAttr('required');    
               $('#terminal-modal select[id="simcard"]').selectpicker('val', '');
               $('#simcard').val('');
               $('#operadoraid').val('');
               $('#terminal-modal select[id="operadoraid"]').selectpicker('val', '');               
               $("#requiere_simcard").val('NO');
               $("#div_simcar").hide();
            }else{
               $("#requiere_simcard").val('SI');
               $('#simcard').prop('required',true);
               $("#div_simcar").show();
            }
            //console.log( 'valor de requiere_simcard:'+ $("#requiere_simcard").val());
        //conexiontype Dial up/Lan
    });



    // function manage_terminals(form) {   
       
    //     var Data = $('#add-new-terminal-form').serializeArray();
    //     var data = $(form).serialize();
    //     var url = form.action;
    //     var ticketArea = $('body').hasClass('ticket');
    //     if(ticketArea) {
    //         data += '&ticket_area=true';
    //     }
        
    //     $.post(url, data).done(function(e) {

    //         var response = JSON.parse(e);
    //         if(response.status == 'success'){
    //             if($.fn.DataTable.isDataTable('.table-expenses')){
    //                 $('.table-expenses').DataTable().ajax.reload();
    //             }
    //             alert_float('success', response.message);
    //         }else{
    //             alert_float('danger', response.message);
    //         }
    //         $('#terminal-modal').modal('hide');
    //     });
    //     return false;
    // }

    function news_terminal(data){
        
        $('#terminal-modal').modal('show');
        $('.edit-title').addClass('hide');
        // $('#terminal-modal select[id="simcard"]').selectpicker('val', '');  
        //console.log('se llama a la funcion init ajax search del archivo main.min.js');
        init_ajax_search_simcard('simcard_available','#simcard.ajax-search',{
            tickets_contacts: true,
            contact_userid: function(){
                    // when ticket is directly linked to project only search project client id contacts
                    var uid = $('select[data-auto-project="true"]').attr('data-project-userid');
                    if(uid){
                        return uid;
                    } else {
                        return '';
                    }
                }
            },
        );
        $("body.terminals .simcard .dropdown-menu").click(function() {
            var t = $('#simcard').val();
            $.post(admin_url + "tickets/ticket_change_data_simcard", { contact_id: t }).done(
                function (e) {
                    res = JSON.parse(e).contact_data;
                    if (res) {
                        console.log(res.operadora);
                        $('select[id="operadoraid"]').selectpicker('val', res.operadora);
                    }
                }
            )
        })
    }

</script>