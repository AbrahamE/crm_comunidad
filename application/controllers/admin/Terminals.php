<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Terminals extends AdminController
{

    
    /**
     * Codeigniter Instance
     * Expenses detailed report filters use $ci
     * @var object
     */
    private $ci;

    public function __construct()
    {
        parent::__construct();

        $this->ci = &get_instance();
        $this->load->model('terminal_model');
        $this->load->model('staff_model');
        $this->load->model('tickets_model');
        $this->load->model('leads_model');
        $this->load->model('departments_model');
        $this->load->model('clients_model');
        $this->load->model('simcard_model');
        
    }

  
    public function index()
    {
        $this->list_terminals();
    }

    public function list_visitor__()
    {
        $data['title'] = _l('visitor');
        $data['sources']  = $this->leads_model->get_source();
        $this->load->view('admin/visitor/manage', $data);
    }

    public function table($clientid = '')
    {
        if (!has_permission('invoices', '', 'view')
            && !has_permission('invoices', '', 'view_own')
            && get_option('allow_staff_view_invoices_assigned') == '0') {
            ajax_access_denied();
        }

        $this->app->get_table_data('visitor', [
            'clientid' => $clientid,
        ]);
    }

    public function create_new_terminal()
    {
         if ($this->input->post()) {
            $post_data =  $this->input->post();
            //var_dump($post_data['userid']); die;
                if($post_data['userid'] != ''){
                     $data = $this->visitor_model->get_contacts($post_data['userid']);
                     //var_dump($data); die;
                     if($data == ''){
                        $data = $this->visitor_model->add_contacts($post_data);
                     }
                }
                $data = $this->visitor_model->add($post_data);
                //var_dump($data); die;
                if ($data == true) {
                    echo json_encode(['status' => 'success', 'message' => _l('added_successfully')]);die;
                }else{
                    echo json_encode(['status' => 'success', 'message' => _l('an_error_has_occurred')]);die;
                } 
                die;
         }

         
         $data['title'] = _l('visitor');
         $data['staff']     = $this->staff_model->get('', ['active' => 1]);
         $data['banks']           = $this->tickets_model->get_banks();
         $data['sources']  = $this->leads_model->get_source();
         $data['typing']  = $this->visitor_model->get_typing();
         $data['type_rif']  = $this->visitor_model->get_type_rif();
         //var_dump($data['sources']); die;
         $this->load->view('admin/visitor/new_visitor', $data);
    }


    public function list_terminals()
    {
        $data['title'] = _l('terminal');

            if ($this->input->is_ajax_request()) {
                
                $aColumns = [
                    db_prefix() . 'terminals.id as id',
                    db_prefix() . 'terminals.userid as userid',
                    db_prefix() . 'clients.company as company',
                    db_prefix() . 'terminal_models.model as model',
                    'codaffiliate',
                    'serial',
                    'mac',
                    'imei',
                    db_prefix() . 'simcard.serialsimcard as simcard',
                    db_prefix() . 'operadora.operadora as operadora',
                    db_prefix() . 'banks.banks as banks',
                    db_prefix() . 'terminal_status.name_status as status',
                ];
                $join = [
                    'LEFT JOIN ' . db_prefix() . 'banks ON ' . db_prefix() . 'banks.id = ' . db_prefix() . 'terminals.banks',
                    'LEFT JOIN ' . db_prefix() . 'operadora ON ' . db_prefix() . 'operadora.operadoraid = ' . db_prefix() . 'terminals.operadoraid',
                    'LEFT JOIN ' . db_prefix() . 'simcard ON ' . db_prefix() . 'simcard.id = ' . db_prefix() . 'terminals.simcard',
                    'LEFT JOIN ' . db_prefix() . 'terminal_models ON '. db_prefix() . 'terminal_models.modelid = ' . db_prefix() . 'terminals.modelid' ,
                    'LEFT JOIN ' . db_prefix() . 'clients ON ' . db_prefix() . 'clients.userid = ' . db_prefix() . 'terminals.userid',
                    'LEFT JOIN ' . db_prefix() .  'terminal_status  ON '  . db_prefix() . 'terminal_status.id = '  . db_prefix() . 'terminals.status'
                ];
                // $join = [];
                $where  = [
                    // ' WHERE ' . db_prefix() . 'terminals.modelid IS NOT null'
                ];
                $filter = [];
                include_once(APPPATH . 'views/admin/visitor/filter.php');
                if (count($filter) > 0) {
                    array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
                }

                $sIndexColumn = 'id';
                $sTable       = db_prefix() . 'terminals';
                $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where);
                $output  = $result['output'];
                $rResult = $result['rResult'];
            

                foreach ($rResult as $aRow) {
                   //var_dump($aRow); die;
                    $row = [];
                    for ($i = 0; $i < count($aColumns); $i++) {
                       // var_dump($aColumns); die;
                        
                        if ($aColumns[$i] == db_prefix() . 'terminals.id as id') {
                            $_data = '<a href="#" >' . $aRow['id'] . '</a>';
                            $_data .= '<div class="row-options">';
                            $_data .= '<a href="#" onclick="terminals(' . $aRow['userid'] . ',' . $aRow['id'] . ');return false;">' . _l('edit') . '</a>';
                            $_data .= '</div>';                        
                        }
                        elseif ($aColumns[$i] == db_prefix() . 'terminals.userid as userid') {
                            $_data = $aRow['userid'] != 0 ? $aRow['userid'] : 'N/A';
                        }
                        elseif ($aColumns[$i] == db_prefix() . 'clients.company as company') {
                            if($aRow['company'] != NULL){
                                $_data = '<a href="' . admin_url('clients/client/' . $aRow['userid']) . '">' . $aRow['company']. '</a>';
                            }else{
                                $_data = 'N/A'; 
                            }
                        }
                        elseif ($aColumns[$i] == db_prefix() . 'terminal_models.model as model') {
                            $_data = $aRow['model'];
                        }elseif ($aColumns[$i] == 'codaffiliate') {
                            $_data = $aRow['codaffiliate'] != 0 ? $aRow['codaffiliate'] : 'N/A';
                        }elseif ($aColumns[$i] == 'serial') {
                            $_data = $aRow['serial'];
                        }elseif ($aColumns[$i] == 'mac') {
                            $_data = $aRow['mac'];
                        }elseif ($aColumns[$i] == 'imei') {
                            $_data = $aRow['imei'];
                        }elseif ($aColumns[$i] == db_prefix() . 'simcard.serialsimcard as simcard') {
                            $_data = $aRow['simcard'];
                        }elseif ($aColumns[$i] ==  db_prefix() . 'operadora.operadora as operadora') {
                            $_data = $aRow['operadora'];
                        }elseif ($aColumns[$i] ==  db_prefix() . 'banks.banks as banks') {
                            $_data = $aRow['banks'] == null  ? 'No asociado' : $aRow['banks']; 
                        }elseif ($aColumns[$i] ==  db_prefix() . 'terminal_status.name_status as status') {
                            $_data = $aRow['status']; 
                        }
                        $row[] = $_data;
                    }
                    $output['aaData'][] = $row;
                }
                echo json_encode($output); 
                die;
            }

            $data['incidents']       = $this->departments_model->incidents();
            $data['terminal']        = $this->tickets_model->get_terminal();
            $data['banks']           = $this->tickets_model->get_banks();
            //$data['model']           = $this->tickets_model->get_model();
            $data['model']           = $this->terminal_model->get_terminal_model();

            $data['simcard']         = $this->tickets_model->get_simcard();
            $data['operadora']       = $this->tickets_model->get_operadora();
           

            $this->load->view('admin/terminals/manage', $data);
    
    }


    public function list_simcard(){
        $data['title'] = _l('simcard');

        if ($this->input->is_ajax_request()) {
            
            $aColumns = [
                db_prefix() .'simcard.id as id',
                'serialsimcard',
                db_prefix() . 'operadora.operadora',
                //db_prefix() . 'simcard.status',
                db_prefix() . 'simcard_status.name_status as status',
            ];
           
            $join = [
                'LEFT JOIN ' . db_prefix() . 'operadora ON ' . db_prefix() . 'operadora.operadoraid = ' . db_prefix() . 'simcard.operadora',
                'LEFT JOIN ' . db_prefix() .  'simcard_status  ON '  . db_prefix() . 'simcard_status.id = '  . db_prefix() . 'simcard.status',
            ];

            // echo "<pre>";
            // var_dump($join); die;
            // $join = [];
            $where  = [];
            $filter = [];
            include_once(APPPATH . 'views/admin/visitor/filter.php');
            if (count($filter) > 0) {
                array_push($where, 'AND (' . prepare_dt_filter($filter) . ')');
            }

            $sIndexColumn = 'id';
            $sTable       = db_prefix() . 'simcard';
            $result       = data_tables_init($aColumns, $sIndexColumn, $sTable, $join, $where);
            $output  = $result['output'];
            $rResult = $result['rResult'];

            //echo "<pre>";
            //var_dump($result);
            foreach ($rResult as $aRow) {

                //var_dump($aRow); die;
                $row = [];
                for ($i = 0; $i < count($aColumns); $i++) {
                    if ($aColumns[$i] == db_prefix() .'simcard.id as id') {
                        
                        $_data = '<a href="#" >' . $aRow['id'] . '</a>';
                        $_data .= '<div class="row-options">';
                        $_data .= '<a href="#" onclick="update_simcard('. $aRow['id'] . ');return false;">' . _l('edit') . '</a>';
                        $_data .= '</div>';   
                        
                    }elseif ($aColumns[$i] == 'serialsimcard') {
                        $_data = $aRow['serialsimcard'];
                    }elseif ($aColumns[$i] == 'tbloperadora.operadora') {
                        $_data = $aRow['tbloperadora.operadora'];
                    }elseif ($aColumns[$i] == db_prefix() . 'simcard_status.name_status as status') {
                       // $_data = $aRow['status'] ;
                        if($aRow['status'] =='Activa'){
                            $_data = '<span class="label s-status" style="border: 1px solid #84c529;color:#84c529;">' .$aRow['status']. '</span>';
                        }elseif($aRow['status'] =='Inactiva'){
                            $_data = '<span class="label s-status" style="border: 1px solid #FB2507;color:#FB2507;">' .$aRow['status']. '</span>';
                        }
                    }
                    $row[] = $_data;
                }
                $output['aaData'][] = $row;
            }
                
            echo json_encode($output);die;
        }


        // $data['incidents']       = $this->departments_model->incidents();
        // $data['terminal']        = $this->tickets_model->get_terminal();
        // $data['banks']           = $this->tickets_model->get_banks();
        // $data['model']           = $this->tickets_model->get_model();
        // $data['simcard']         = $this->tickets_model->get_simcard();
        $data['operadora']          = $this->simcard_model->get_sim_operator();
        // $data['client_default']  = $this->clients_model->get_contacts_email('callcenter@paytechcorp.com');

        $this->load->view('admin/sincard/list_sincard', $data);
    }

    public function simcard_add(){
        if ($this->input->is_ajax_request()) {
            $post_data = $this->input->post();
            if ($post_data) {

                $data = $this->tickets_model->add_simcard($post_data);

                if ($data) {

                    if($data === 'exist'){                        
                        echo json_encode(['status' => 'danger', 'message' => 'La simcard ya existe', 'contact_data' => $data]);die;
                    }

                    if($data === 'update'){

                        $result=$this->simcard_model->get_terminal_by_simcard($post_data['id']);
                        
                        if (isset($result)  ) {
                             $chage=$this->simcard_model->change_terminal_operator($result->id, $post_data["operadoraid"]);
                             //var_dump($chage); die;
                        }

                        echo json_encode(['status' => 'success', 'message' => 'La simcard ha sido actualizada', 'contact_data' => $data]);die;
                    }

                    echo json_encode(['status' => 'success', 'message' => 'Se añadio una nueva simcard', 'contact_data' => $data]);die;
                } else {
                    echo json_encode(['status' => 'danger', 'message' => 'Error al añadir una nueva simcard']); die;
                }
            } else {
                echo json_encode(['status' => 'danger', 'message' => 'Error interno']); die;
            }
            die;
        }
    }


    public function update_simcard($id_simcard = ''){

        //$this->load->model('tickets_model');
        $this->load->model('simcard_model');

        //$data['operadora'] = $this->tickets_model->get_operadora();
        $data['operadora'] = $this->simcard_model->get_sim_operator();

        if ($id_simcard) {
            $data['simcard']   = (array)$this->simcard_model->get_sim_operator($id_simcard);
        }

        $this->load->view('admin/sincard/simcard-model', $data);

    }



    public function rif_exists(){

        if ($this->input->post()) {
            $rif = $this->input->post('rif');
            $data = $this->visitor_model->get_rif_clients($rif);
            //var_dump($data); die;
            echo json_encode($data);
            
        }
    }
    
    

   
}
