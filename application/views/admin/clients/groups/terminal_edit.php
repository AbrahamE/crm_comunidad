<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if(isset($client)){ ?>
	<h4 class="customer-profile-group-heading"><?php echo _l('terminal'); ?></h4>
	<!-- <a href="#" class="btn btn-info mbot25"  data-toggle="modal" data-target="#ticket-terminal-modal"><?php echo _l('new_terminal'); ?></a> -->
	<a href="#" class="btn btn-info mbot25" onclick="new_terminal();return false;" data-target="#ticket-terminal-modal"><?php echo _l('new_terminal'); ?></a>
	<?php
	$this->load->view('admin/clients/table_terminal_html', array('class'=>'terminal-single-client'));
	$this->load->view('admin/clients/modals/modal_terminal_edit',array('banks' => $banks , 'model' => $model , 'sincard' => $simcard , 'operadora' => $operadora , 'userid' => $userid, 'terminal'=>$terminal));
	//$this->load->view('admin/tickets/terminal/terminal',array('banks' => $banks , 'model' => $model , 'sincard' => $simcard , 'operadora' => $operadora));
	?>
<?php } ?>
