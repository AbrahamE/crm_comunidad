<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Message_model extends App_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('projects_model');
        $this->load->model('staff_model');
    }

    public function add($data)
    {
        //echo "<pre>";print_r($data); die;
        $datecreated = date('Y-m-d');
        $this->db->insert(db_prefix() . 'message', [
            'name'       =>  $data['name'],
            'description'       =>  $data['description'],
            'date'  =>  $datecreated,
            'rel_id' => $data['rel_id'],
            'rel_type'  =>  $data['rel_type'],
        ]);
        
        return true;
    }

    public function get($id, $where = [])
    {
        $is_admin = is_admin();
        $this->db->where('id', $id);
        $this->db->where($where);
        $message = $this->db->get(db_prefix() . 'message')->row();
        if ($message) {
            $message->comments      = $this->get_task_comments($id);
            
        }

        return hooks()->apply_filters('get_task', $message);
    }

    public function get_task_comments($id)
    {
       // $task_comments_order = hooks()->apply_filters('task_comments_order', 'DESC');

        $this->db->select('id,dateadd,content,' . db_prefix() . 'staff.firstname,' . db_prefix() . 'staff.lastname,' . db_prefix() . 'message_comments.staff_id,' . db_prefix() . 'message_comments.contact_id as contact_id,file_id,CONCAT(firstname, " ", lastname) as staff_full_name');
        $this->db->from(db_prefix() . 'message_comments');
        $this->db->join(db_prefix() . 'staff', db_prefix() . 'staff.staffid = ' . db_prefix() . 'message_comments.staff_id', 'left');
        $this->db->where('message_id', $id);
        $this->db->order_by('dateadd');

        $comments = $this->db->get()->result_array();

        $ids = [];
        foreach ($comments as $key => $comment) {
            array_push($ids, $comment['id']);
            $comments[$key]['attachments'] = [];
        }

        return $comments;
    }

    public function add_message_comment($data)
    {
        //echo "<pre>"; print_r($data); die; 
        
        if (is_client_logged_in()) {
            $data['staffid']    = 0;
            $data['contact_id'] = get_contact_user_id();
        } else {
            $data['staffid']    = get_staff_user_id();
            $data['contact_id'] = 0;
        }

        $this->db->insert(db_prefix() . 'message_comments', [
            'message_id'     => $data['taskid'],
            'content'    => is_client_logged_in() ? _strip_tags($data['content']) : $data['content'],
            'staff_id'    => $data['staffid'],
            'contact_id' => $data['contact_id'],
            'dateadd'  => date('Y-m-d H:i:s'),
        ]);

        $insert_id = $this->db->insert_id();

        // if ($insert_id) {
        //     $this->db->select('rel_type,rel_id,name,visible_to_client');
        //     $this->db->where('id', $data['taskid']);
        //     $task = $this->db->get(db_prefix() . 'tasks')->row();

        //     $description     = 'not_task_new_comment';
        //     $additional_data = serialize([
        //         $task->name,
        //     ]);

        //     if ($task->rel_type == 'project') {
        //         $this->projects_model->log_activity($task->rel_id, 'project_activity_new_task_comment', $task->name, $task->visible_to_client);
        //     }

        //     $this->_send_task_responsible_users_notification($description, $data['taskid'], false, 'task_new_comment_to_staff', $additional_data, $insert_id);

        //     $this->db->where('project_id', $task->rel_id);
        //     $this->db->where('name', 'view_task_comments');
        //     $project_settings = $this->db->get(db_prefix() . 'project_settings')->row();

        //     if ($project_settings && $project_settings->value == 1) {
        //         $this->_send_customer_contacts_notification($data['taskid'], 'task_new_comment_to_customer');
        //     }

        //     hooks()->do_action('task_comment_added', ['task_id' => $data['taskid'], 'comment_id' => $insert_id]);

        //     return $insert_id;
        // }

        return $insert_id;
    }


    


}
