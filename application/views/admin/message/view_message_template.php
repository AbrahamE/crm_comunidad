<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-header task-single-header" data-task-single-id="<?php echo $task->id; ?>" >
   <?php if($this->input->get('opened_from_lead_id')){ ?>
   <a href="#" onclick="init_lead(<?php echo $this->input->get('opened_from_lead_id'); ?>); return false;" class="back-to-from-task color-white" data-placement="left" data-toggle="tooltip" data-title="<?php echo _l('back_to_lead'); ?>">
   <i class="fa fa-tty" aria-hidden="true"></i>
   </a>
   <?php } ?> 
   <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   <h4 class="modal-title"><?php echo $task->name; ?></h4>
  
</div>
<div class="modal-body">
   <div class="row">
      <div class="col-md-8 task-single-col-left">
         <?php if(total_rows(db_prefix().'taskstimers',array('end_time'=>NULL,'staff_id !='=>get_staff_user_id(),'task_id'=>$task->id)) > 0){
            $startedTimers = $this->tasks_model->get_timers($task->id,array('staff_id !='=>get_staff_user_id(),'end_time'=>NULL));

            $usersWorking = '';

            foreach($startedTimers as $t){
              $usersWorking .= '<b>' . get_staff_full_name($t['staff_id']) . '</b>, ';
            }

            $usersWorking = rtrim($usersWorking, ', ');
            ?>
         <p class="mbot20 info-block">
            <?php echo _l((count($startedTimers) == 1
               ? 'task_users_working_on_tasks_single'
               : 'task_users_working_on_tasks_multiple'),$usersWorking);
               ?>
         </p>
         <?php } ?>
         <?php if(!empty($task->rel_id)){
            echo '<div class="task-single-related-wrapper">';
            $task_rel_data = get_relation_data($task->rel_type,$task->rel_id);
            $task_rel_value = get_relation_values($task_rel_data,$task->rel_type);
            echo '<h4 class="bold font-medium mbot15">'._l('task_single_related').': <a href="'.$task_rel_value['link'].'" target="_blank">'.$task_rel_value['name'].'</a>';
            if($task->rel_type == 'project' && $task->milestone != 0){
             echo '<div class="mtop5 mbot20 font-normal">' . _l('task_milestone') . ': ';
             $milestones = get_project_milestones($task->rel_id);
             if(has_permission('tasks','','edit') && count($milestones) > 1){ ?>
         <span class="task-single-menu task-menu-milestones">
            <span class="trigger pointer manual-popover text-has-action">
            <?php echo $task->milestone_name; ?>
            </span>
            <span class="content-menu hide">
               <ul>
                  <?php
                  foreach($milestones as $milestone){ ?>
                  <?php if($task->milestone != $milestone['id']){ ?>
                  <li>
                     <a href="#" onclick="task_change_milestone(<?php echo $milestone['id']; ?>,<?php echo $task->id; ?>); return false;">
                     <?php echo $milestone['name']; ?>
                     </a>
                  </li>
                  <?php } ?>
                  <?php } ?>
               </ul>
            </span>
         </span>
         <?php } else {
            echo $task->milestone_name;
            }
            echo '</div>';
            }
            echo '</h4>';
            echo '</div>';
            } ?>
        
        
         <p class="no-margin pull-left mright5">
            <!-- <a href="#" class="btn btn-default mright5" data-toggle="tooltip" data-title="<?php echo _l('task_timesheets'); ?>"onclick="slideToggle('#task_single_timesheets'); return false;">
            <i class="fa fa-th-list"></i>
            </a> -->
        
         <div class="clearfix"></div>
         <h4 class="th font-medium mbot15 pull-left"><?php echo _l('task_view_description'); ?></h4>
         <!-- <?php if(has_permission('tasks','','edit')){ ?><a href="#" onclick="edit_task_inline_description(this,<?php echo $task->id; ?>); return false;" class="pull-left mtop10 mleft5 font-medium-xs"><i class="fa fa-pencil-square-o"></i></a>
         <?php } ?> -->
         <div class="clearfix"></div>
         <?php if(!empty($task->description)){
            echo '<div class="tc-content"><div id="task_view_description">' .check_for_links($task->description) .'</div></div>';
            } else {
            echo '<div class="no-margin tc-content task-no-description" id="task_view_description"><span class="text-muted">' . _l('task_no_description') . '</span></div>';
            } ?>
         <div class="clearfix"></div>
         <hr />
         <a href="#" onclick="add_task_checklist_item('<?php echo $task->id; ?>', undefined, this); return false" class="mbot10 inline-block">
         <span class="new-checklist-item"><i class="fa fa-plus-circle"></i>
         <?php echo _l('add_checklist_item'); ?>
         </span>
         </a>
         <div class="form-group no-mbot checklist-templates-wrapper simple-bootstrap-select task-single-checklist-templates<?php if(count($checklistTemplates) == 0){echo ' hide';}  ?>">
            <select id="checklist_items_templates" class="selectpicker checklist-items-template-select" data-none-selected-text="<?php echo _l('insert_checklist_templates') ?>" data-width="100%" data-live-search="true">
               <option value=""></option>
               <?php foreach($checklistTemplates as $chkTemplate){ ?>
               <option value="<?php echo $chkTemplate['id']; ?>">
                  <?php echo $chkTemplate['description']; ?>
               </option>
               <?php } ?>
            </select>
         </div>
         
         <a href="#" id="taskCommentSlide" onclick="slideToggle('.tasks-comments'); return false;">
            <h4 class="mbot20 font-medium"><?php echo _l('task_comments'); ?></h4>
         </a>
         <div class="tasks-comments inline-block full-width simple-editor"<?php if(count($task->comments) == 0){echo ' style="display:none"';} ?>>
            <?php echo form_open_multipart(admin_url('tasks/add_task_comment'),array('id'=>'task-comment-form','class'=>'dropzone dropzone-manual','style'=>'min-height:auto;background-color:#fff;')); ?>
            <textarea name="comment" placeholder="<?php echo _l('task_single_add_new_comment'); ?>" id="task_comment" rows="3" class="form-control ays-ignore"></textarea>
           
            <div class="dropzone-task-comment-previews dropzone-previews"></div>
            <button type="button" class="btn btn-info mtop10 pull-right hide" id="addTaskCommentBtn" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" onclick="add_message_comment('<?php echo $task->id; ?>');" data-comment-task-id="<?php echo $task->id; ?>">
            <?php echo _l('task_single_add_new_comment'); ?>
            </button>
            <?php echo form_close(); ?>
            <div class="clearfix"></div>
            <?php if(count($task->comments) > 0){echo '<hr />';} ?>
            <div id="task-comments" class="mtop10">
               <?php
                  $comments = '';
                  $len = count($task->comments);
                  $i = 0;
                  foreach ($task->comments as $comment) {
                  //echo "<pre>"; print_r($comment); die;
                    $comments .= '<div id="comment_'.$comment['id'].'" data-commentid="' . $comment['id'] . '" data-task-attachment-id="'.$comment['file_id'].'" class="tc-content task-comment'.(strtotime($comment['dateadd']) >= strtotime('-16 hours') ? ' highlight-bg' : '').'">';
                    $comments .= '<a data-task-comment-href-id="'.$comment['id'].'" href="'.admin_url('tasks/view/'.$task->id).'#comment_'.$comment['id'].'" class="task-date-as-comment-id"><small><span class="text-has-action inline-block mbot5" data-toggle="tooltip" data-title="'._dt($comment['dateadd']).'">' . time_ago($comment['dateadd']) . '</span></small></a>';
                    if($comment['staff_id'] != 0){
                     $comments .= '<a href="' . admin_url('profile/' . $comment['staff_id']) . '" target="_blank">' . staff_profile_image($comment['staff_id'], array(
                      'staff-profile-image-small',
                      'media-object img-circle pull-left mright10'
                   )) . '</a>';
                  } elseif($comment['contact_id'] != 0) {
                     $comments .= '<img src="'.contact_profile_image_url($comment['contact_id']).'" class="client-profile-image-small media-object img-circle pull-left mright10">';
                  }
                 
                  $comments .= '<div class="media-body comment-wrapper">';
                  $comments .= '<div class="mleft40">';

                  if($comment['staff_id'] != 0){
                   $comments .= '<a href="' . admin_url('profile/' . $comment['staff_id']) . '" target="_blank">' . $comment['staff_full_name'] . '</a> <br />';
                  } elseif($comment['contact_id'] != 0) {
                   $comments .= '<span class="label label-info mtop5 mbot5 inline-block">'._l('is_customer_indicator').'</span><br /><a href="' . admin_url('clients/client/'.get_user_id_by_contact_id($comment['contact_id']) .'?contactid='.$comment['contact_id'] ) . '" class="pull-left" target="_blank">' . get_contact_full_name($comment['contact_id']) . '</a> <br />';
                  }

                  $comments .= '<div data-edit-comment="'.$comment['id'].'" class="hide edit-task-comment"><textarea rows="5" id="task_comment_'.$comment['id'].'" class="ays-ignore form-control">'.$comment['content'].'</textarea>
                  <div class="clearfix mtop20"></div>
                  <button type="button" class="btn btn-info pull-right" onclick="save_edited_comment('.$comment['id'].','.$task->id.')">'._l('submit').'</button>
                  <button type="button" class="btn btn-default pull-right mright5" onclick="cancel_edit_comment('.$comment['id'].')">'._l('cancel').'</button>
                  </div>';
                 
                  $comments .=  '<span>' . $comment['content'] . '</span> <br />';
                  $comments .= '</div>';
                  if ($i >= 0 && $i != $len - 1) {
                     $comments .= '<hr class="task-info-separator" />';
                  }
                  $comments .= '</div>';
                  $comments .= '</div>';
                  $i++;
                  }
                  echo $comments;
                  ?>
            </div>
         </div>
      </div>
         <?php echo form_close(); ?>
      </div>
   </div>
</div>
<script>
   if(typeof(commonTaskPopoverMenuOptions) == 'undefined') {
      var commonTaskPopoverMenuOptions = {
         html: true,
         placement: 'bottom',
         trigger: 'click',
         template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"></div></div></div>',
      };
   }

   // Clear memory leak
   if(typeof(taskPopoverMenus) == 'undefined'){
      var taskPopoverMenus = [{
            selector: '.task-menu-options',
            title: "<?php echo _l('actions'); ?>",
         },
         {
            selector: '.task-menu-status',
            title: "<?php echo _l('ticket_single_change_status'); ?>",
         },
         {
            selector: '.task-menu-priority',
            title: "<?php echo _l('task_single_priority'); ?>",
         },
         {
            selector: '.task-menu-milestones',
            title: "<?php echo _l('task_milestone'); ?>",
         },
      ];
   }

   for (var i = 0; i < taskPopoverMenus.length; i++) {
      $(taskPopoverMenus[i].selector + ' .trigger').popover($.extend({}, commonTaskPopoverMenuOptions, {
         title: taskPopoverMenus[i].title,
         content: $('body').find(taskPopoverMenus[i].selector + ' .content-menu').html()
      }));
   }

   if (typeof (Dropbox) != 'undefined') {
      document.getElementById("dropbox-chooser-task").appendChild(Dropbox.createChooseButton({
         success: function (files) {
            taskExternalFileUpload(files,'dropbox',<?php echo $task->id; ?>);
         },
         linkType: "preview",
         extensions: app.options.allowed_files.split(','),
      }));
   }

   init_selectpicker();
   init_datepicker();
   init_lightbox();

   tinyMCE.remove('#task_view_description');

   if (typeof (taskAttachmentDropzone) != 'undefined') {
      taskAttachmentDropzone.destroy();
      taskAttachmentDropzone = null;
   }

   taskAttachmentDropzone = new Dropzone("#task-attachment", appCreateDropzoneOptions({
      uploadMultiple: true,
      parallelUploads: 20,
      maxFiles: 20,
      paramName: 'file',
      sending: function (file, xhr, formData) {
         formData.append("taskid", '<?php echo $task->id; ?>');
      },
      success: function (files, response) {
         response = JSON.parse(response);
         if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
            _task_append_html(response.taskHtml);
         }
      }
   }));

   $('#task-modal').find('.gpicker').googleDrivePicker({ onPick: function(pickData) {
      taskExternalFileUpload(pickData, 'gdrive', <?php echo $task->id; ?>);
   }});

</script>
