
<div class="col-md-12" id="report">
    <div class="panel panel-info col-md-12 panel-padding">

        <div class="panel-body" id="inventory_entry_report">
            <p>
                <h3 align="center"><?php echo mb_strtoupper(_l('inventory_entry_report')); ?></h3>
            </p>

            <div class="col-md-12">
                <table style="border: 1px solid black">
                    <thead >
                        <tr style="background-color: #afdfff">
                            <th colspan="3" style="border: 1px solid black" align="center"><?php echo _l('stock_received_docket_code') ?></th>
                            <th colspan="2" style="border: 1px solid black" align="center"><?php echo _l('salesman') ?></th>
                            <th colspan="3" style="border: 1px solid black" align="center"><?php echo _l('supplier_name') ?></th>
                            <th colspan="2" style="border: 1px solid black" align="center"><?php echo _l('total_tax_money') ?></th>
                            <th colspan="2" style="border: 1px solid black" align="center"><?php echo _l('total_goods_money') ?></th>
                            <th colspan="2" style="border: 1px solid black" align="center"><?php echo _l('value_of_inventory') ?></th>
                            <th colspan="2" style="border: 1px solid black" align="center"><?php echo _l('total_money') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($inventary as $inventarys){ ?>
                        <tr>
                            <td colspan="3" style="border: 1px solid black" id="goods_receipt_code" align="center"><?php echo $inventarys->goods_receipt_code ?></td>
                            <td colspan="2" style="border: 1px solid black" id="supplier_name" align="center"><?php echo $inventarys->supplier_name ?></td>
                            <td colspan="3" style="border: 1px solid black" id="buyer" align="center"><?php echo $inventarys->firstname?></td>
                            <td colspan="2" style="border: 1px solid black" id="total_tax_money" align="center"><?php echo $inventarys->total_tax_money ?></td>
                            <td colspan="2" style="border: 1px solid black" id="total_goods_money" align="center"><?php echo $inventarys->total_goods_money?></td>
                            <td colspan="2" style="border: 1px solid black" id="value_of_inventory" align="center"><?php echo $inventarys->value_of_inventory ?></td>
                            <td colspan="2" style="border: 1px solid black" id="total_money" align="center"><?php echo $inventarys->total_money?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>


        </div>
    </div>

</div>