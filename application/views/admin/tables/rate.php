<?php

defined('BASEPATH') or exit('No direct script access allowed');

$hasPermissionDelete = has_permission('payments', '', 'delete');

$aColumns = [
    'id',
    'tasa_c',
    'date',
    ];


$sIndexColumn = 'id';
$sTable       = db_prefix() . 'rate';

$result = data_tables_init($aColumns, $sIndexColumn, $sTable);

$output  = $result['output'];
$rResult = $result['rResult'];



foreach ($rResult as $aRow) {
    $row = [];

    $row[] = $aRow['id'];
    $row[] = $aRow['tasa_c'];
    $row[] = $aRow['date'];

    $output['aaData'][] = $row;
}
