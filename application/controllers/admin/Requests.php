<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Requests extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('requests_model');
        $this->load->model('proposals_model');
        $this->load->model('clients_model');
        $this->load->model('tickets_model');

        $this->load->model('currencies_model');
        $this->load->model('invoices_model');
        $this->load->model('simcard_model');
        $this->load->model('leads_model');     


    }

    public function index($requests_id = '')
    {
        $this->list_requests($requests_id);
    }

    public function list_requests($requests_id = '')
    {
        
      //var_dump($requests_id); die;
        close_setup_menu();

        if (!has_permission('proposals', '', 'view') && !has_permission('proposals', '', 'view_own') && get_option('allow_staff_view_estimates_assigned') == 0) {
            access_denied('proposals');
        }

        $isPipeline = $this->session->userdata('proposals_pipeline') == 'true';

        if ($isPipeline && !$this->input->get('status')) {
            $data['title']           = _l('proposals_pipeline');
            $data['bodyclass']       = 'proposals-pipeline';
            $data['switch_pipeline'] = false;
            // Direct access
            if (is_numeric($requests_id)) {
                $data['proposalid'] = $requests_id;
            } else {
                $data['proposalid'] = $this->session->flashdata('proposalid');
            }

            $this->load->view('admin/proposals/pipeline/manage', $data);
        } else {

            // Pipeline was initiated but user click from home page and need to show table only to filter
            if ($this->input->get('status') && $isPipeline) {
                $this->pipeline(0, true);
            }

            $data['requests_id']           = $requests_id;
            $data['switch_pipeline']       = true;
            $data['title']                 = _l('requests');
            $data['statuses']              = $this->proposals_model->get_statuses();

            $data['status_reques']         = $this->requests_model->get_status();
            $data['type']                  = $this->requests_model->get_type_operation();
            
            $data['proposals_sale_agents'] = $this->proposals_model->get_sale_agents();
            $data['years']                 = $this->proposals_model->get_proposals_years();
            $data['requests'] = $this->requests_model->get($requests_id);

            $this->load->model('payment_modes_model');
            $data['payment_modes'] = $this->payment_modes_model->get('', [
                'expenses_only !=' => 1,
            ]);
            $this->load->model('taxes_model');
            $data['taxes']         = $this->taxes_model->get();
            $data['currencies']    = $this->currencies_model->get();
            $data['base_currency'] = $this->currencies_model->get_base_currency();
            $this->load->model('invoice_items_model');
            $data['ajaxItems'] = false;
            if (total_rows(db_prefix() . 'items') <= ajax_on_total_items()) {
                $data['items'] = $this->invoice_items_model->get_grouped();
            } else {
                $data['items']     = [];
                $data['ajaxItems'] = true;
            }
            $data['items_groups'] = $this->invoice_items_model->get_groups();
            $data['staff']          = $this->staff_model->get('', ['active' => 1]);
            
            // echo '<pre>';
            // print_r($data); die;

            $data['billable_tasks'] = [];
            $this->load->view('admin/requests/manage', $data);
        }
    }

    public function table()
    {
        if (!has_permission('proposals', '', 'view')
            && !has_permission('proposals', '', 'view_own')
            && get_option('allow_staff_view_proposals_assigned') == 0) {
            ajax_access_denied();
        }

        $this->app->get_table_data('requests');
    }

    public function requests($id = '')
    {
        
        if ($this->input->post()) {
             
            $post_data = $this->input->post();
           
            $post_data['requests_status'] = !empty($post_data['requests_status']) 
                ? $post_data['requests_status']
                    : $post_data['requests_status_one'];
                    
            if ($id=='') {
                $data = $this->requests_model->add($post_data);
              
                if ($data) {
                    echo json_encode(['status' => 'success', 'message' => _l('added_successfully')]);
                }else{
                    echo json_encode(['status' => 'success', 'message' => _l('an_error_has_occurred')]);
                } 
                die;
            }else{
                  //var_dump('llego a la condicion de update'); die;
                $success = $this->requests_model->update($post_data);
                if ($success) {
                    //var_dump('se modificaron los datos'); die;  
                    echo json_encode(['status' => 'success', 'message' => _l('updated_successfully')]);
                }else{
                    echo json_encode(['status' => 'success', 'message' => _l('an_error_has_occurred')]);
                    //('success',_l('an_error_has_occurred'));
                    // redirect(admin_url('requests/'));
                } 
                die;
                              
            }    
        }

        $data= array();
        $data['mysimcard']='';
        if($id == '') {
            $title = _l('new_requests');
            $view='admin/requests/requests';
        }else{         
            
            
            $data_requests = $this->requests_model->get($id);
           // echo "<pre>";  print_r($data_requests); die; 
            if ($data_requests) {               
                $data['request'] = (array) $data_requests; 
                if ($data['request']['rif_reason_change']!= '' && $data['request']['rif_reason_change'] != null){
                    $data['request']['reason_change_num_rif'] = $this->requests_model->get_numbers_fom_string($data['request']['rif_reason_change']); 
                    $data['request']['reason_change_type_rif'] = $this->requests_model->get_type_from_rif($data['request']['rif_reason_change']);  
                }
                //fecha de modificacion de estatus
                $statusDate= $this->requests_model->get_status_date($id,$data['request']['status']);                
                $data['request']['status_date'] =$statusDate;

                //informacion de las posibles terminales asociadas a la operacion
                $terminal_info = (!empty($data['request']['terminal']))?$this->tickets_model->get_terminal($data['request']['terminal']):'';

                $terminal_saliente_info = (!empty($data['request']['terminal_saliente']))?$this->tickets_model->get_terminal($data['request']['terminal_saliente']):'';
                $terminal_entrante_info = (!empty($data['request']['terminal_entrante']))?$this->tickets_model->get_terminal($data['request']['terminal_entrante']):'';

                //echo "<pre>";  print_r($data['request']); die; 

                $data['terminal_info'] = $terminal_info!=''? (array) $terminal_info: '';  
                $data['terminal_saliente_info'] = $terminal_saliente_info!=''? (array) $terminal_saliente_info: '';  
                $data['terminal_entrante_info']  = $terminal_entrante_info !=''? (array) $terminal_entrante_info: '';

                if (($data['terminal_info']!='') && (!empty($data['terminal_info']['simcard']))) {
                    $data['mysimcard']=(array)$this->tickets_model->get_simcard($data['terminal_info']['simcard']);
                }
            }
              
            // var_dump($data['terminal_info']); die; 
            $title      = _l('update_requests');
            $view ='admin/requests/update_requests';
            $contact = $this->requests_model->get_contact_by_client($data['request']['client_id']);   
            
            if ($contact) {
                $data['contact'] = (array) $contact;
                }
            //echo '<pre>'; var_dump($data['contact']); die;
        }
        $data['chanels']  = $this->requests_model->get_source_requests();
        //echo '<pre>'; var_dump($data['sources']); die;
        $data['simcards'] = $this->simcard_model->search_availables_simcards();
        $data['title'] = $title;
        $data['invoice'] = $this->invoices_model->get($id);
        $data['type_operation'] = $this->requests_model->get_type_operation();
        $data['banks']          = $this->requests_model->get_banks();
        $data['status']         = $this->requests_model->get_status();
        $data['status_add']         = $this->requests_model->get_status_add();
        $contact_id = $this->input->post('contact_id');
        $contact = $this->requests_model->get_contact($contact_id);
        if ($contact) {
            $data['contact'] = (array) $contact;
        }
        $data['lisTerminals'] = $this->requests_model->get_availables_terminals();
        if (count($data['lisTerminals'])==0){
            $data['message']   = 'No hay terminales disponibles para associar'; 
        }
        
        $data['type_rif']  = $this->requests_model->get_type_rif();

        //echo '<pre>'; var_dump($data); die;
        //$this->load->view('admin/requests/requests', $data);
        $this->load->view($view, $data);
    }

    public function rif_exists(){

        if ($this->input->post()) {
            $rif = $this->input->post('rif');
            $data = $this->requests_model->get_rif_clients($rif);
            //var_dump($data); die;
            echo json_encode($data);
            
        }
    }

   
// Pipeline
    public function pipeline($set = 0, $manual = false)
    {
        if ($set == 1) {
            $set = 'true';
        } else {
            $set = 'false';
        }
        $this->session->set_userdata([
            'proposals_pipeline' => $set,
        ]);
        if ($manual == false) {
            redirect(admin_url('proposals'));
        }
    }

    // public function pipeline_open($id)
    // {
    //     if (has_permission('proposals', '', 'view') || has_permission('proposals', '', 'view_own') || get_option('allow_staff_view_proposals_assigned') == 1) {
    //         $data['proposal']      = $this->get_proposal_data_ajax($id, true);
    //         $data['proposal_data'] = $this->proposals_model->get($id);
    //         $this->load->view('admin/proposals/pipeline/proposal', $data);
    //     }
    // }

    public function update_pipeline()
    {
        if (has_permission('proposals', '', 'edit')) {
            $this->proposals_model->update_pipeline($this->input->post());
        }
    }

    public function get_pipeline()
    {
        if (has_permission('proposals', '', 'view') || has_permission('proposals', '', 'view_own') || get_option('allow_staff_view_proposals_assigned') == 1) {
            $data['statuses'] = $this->proposals_model->get_statuses();
            $this->load->view('admin/proposals/pipeline/pipeline', $data);
        }
    }

    public function pipeline_load_more()
    {
        $status = $this->input->get('status');
        $page   = $this->input->get('page');

        $proposals = $this->proposals_model->do_kanban_query($status, $this->input->get('search'), $page, [
            'sort_by' => $this->input->get('sort_by'),
            'sort'    => $this->input->get('sort'),
        ]);

        foreach ($proposals as $proposal) {
            $this->load->view('admin/proposals/pipeline/_kanban_card', [
                'proposal' => $proposal,
                'status'   => $status,
            ]);
        }
    }

    public function requests_change_data()
    {
       
        if ($this->input->is_ajax_request()) {
            $contact_id = $this->input->post('contact_id');
            echo json_encode([
                'contact_data'          => $this->clients_model->get_contact($contact_id),
                'customer_has_projects' => customer_has_projects(get_user_id_by_contact_id($contact_id)),
            ]);
        }
    }

    public function get_requests_data_ajax($id, $to_return = false)
    {
        $requests = $this->requests_model->get($id);
        //var_dump($requests); die;
        
        $data['status']           = $this->requests_model->get_status();
        $data['requests']              = $requests;
         //var_dump($data['requests']);
         //var_dump($requests->client_id); die;
        if ($to_return == false) {
            $this->load->view('admin/requests/requests_preview_template', $data);
        } else {
            return $this->load->view('admin/requests/requests_preview_template', $data, true);
        }
    }

    public function get_invoice_convert_data($id)
    {
        //var_dump($id); die;
        $this->load->model('payment_modes_model');
        $data['payment_modes'] = $this->payment_modes_model->get('', [
            'expenses_only !=' => 1,
        ]);
        $this->load->model('taxes_model');
        $data['taxes']         = $this->taxes_model->get();
        $data['currencies']    = $this->currencies_model->get();
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $this->load->model('invoice_items_model');
        $data['ajaxItems'] = false;
        if (total_rows(db_prefix() . 'items') <= ajax_on_total_items()) {
            $data['items'] = $this->invoice_items_model->get_grouped();
        } else {
            $data['items']     = [];
            $data['ajaxItems'] = true;
        }
        $data['items_groups'] = $this->invoice_items_model->get_groups();
        $data['staff']          = $this->staff_model->get('', ['active' => 1]);
        $data['requests']       = $this->requests_model->get($id);
        //var_dump($data['requests']); die;
        $data['billable_tasks'] = [];
      
        
        $this->load->view('admin/requests/invoice_convert_template', $data);
    }

    public function convert_to_invoice($id)
    {
        if (!has_permission('invoices', '', 'create')) {
            access_denied('invoices');
        }
        if ($this->input->post()) {
            $this->load->model('invoices_model');
            $invoice_id = $this->invoices_model->add($this->input->post());
            if ($invoice_id) {
                set_alert('success', _l('requests_converted_to_invoice_success'));
                $this->db->where('id', $id);
                $this->db->update(db_prefix() . 'operations', [
                    'invoice_id' => $invoice_id,
                    //'status'     => 3,
                ]);
                log_activity('Requests Converted to Invoice [InvoiceID: ' . $invoice_id . ', ProposalID: ' . $id . ']');
                hooks()->do_action('requests_converted_to_invoice', ['requests_id' => $id, 'invoice_id' => $invoice_id]);
                redirect(admin_url('invoices/invoice/' . $invoice_id));
            } else {
                set_alert('danger', _l('requests_converted_to_invoice_fail'));
            }
            // if ($this->set_proposal_pipeline_autoload($id)) {
            //     redirect(admin_url('requests'));
            // } else {
                redirect(admin_url('requests/list_requests/' . $id));
            //}
        }
    }

    public function update_requests_status()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
                
            $status = $this->requests_model->status_update($this->input->post()); 

            if ($status) {
                set_alert('success', _l('Su estatus fue actualizado correctamente'));
            }

            set_alert('error', _l('ha ocurrido un error'));
        }
    }

   
    
}
