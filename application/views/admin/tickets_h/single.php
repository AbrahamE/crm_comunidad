<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<?php set_ticket_open($ticket->adminread, $ticket->ticketid); ?>

<div id="wrapper">

   <div class="content">

      <div class="row">

         <div class="col-md-12">

            <div class="panel_s">

               <div class="panel-body">

                  <div class="horizontal-scrollable-tabs">

                     <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
                     <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>

                     <div class="horizontal-tabs">

                        <ul class="nav nav-tabs no-margin nav-tabs-horizontal" role="tablist">

                           <li role="presentation" class="<?php if ($this->session->flashdata('active_tab_settings')) {
                                                               echo 'active';
                                                            } ?>">
                              <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                                 <?php echo _l('ticket_single_settings'); ?>
                              </a>
                           </li>

                           <li role="terminals_detail">
                              <a href="#terminals_detail" aria-controls="terminals_detail" role="tab" data-toggle="tab">
                                 <?php echo _l('Terminales añadidas'); ?>
                              </a>
                           </li>

                           <li role="presentation">
                              <a href="#addreply" aria-controls="addreply" role="tab" data-toggle="tab">
                                 <?php echo _l('ticket_single_add_reply'); ?>
                              </a>
                           </li>

                           <li role="presentation">
                              <a href="#note" aria-controls="note" role="tab" data-toggle="tab">
                                 <?php echo _l('ticket_single_spare'); ?>
                              </a>
                           </li>

                           <li role="presentation">
                              <a href="#tab_reminders" onclick="initDataTable('.table-reminders', admin_url + 'misc/get_reminders/' + <?php echo $ticket->ticketid; ?> + '/' + 'ticket', undefined, undefined, undefined,[1,'asc']); return false;" aria-controls="tab_reminders" role="tab" data-toggle="tab">
                                 <?php echo _l('ticket_reminders'); ?>
                                 <?php
                                 $total_reminders = total_rows(
                                    db_prefix() . 'reminders',
                                    array(
                                       'isnotified' => 0,
                                       'staff' => get_staff_user_id(),
                                       'rel_type' => 'ticket',
                                       'rel_id' => $ticket->ticketid
                                    )
                                 );
                                 if ($total_reminders > 0) {
                                    echo '<span class="badge">' . $total_reminders . '</span>';
                                 }
                                 ?>
                              </a>
                           </li>

                           <li role="presentation">
                              <a href="#othertickets" onclick="init_table_tickets(true);" aria-controls="othertickets" role="tab" data-toggle="tab">
                                 <?php echo _l('ticket_single_other_user_tickets'); ?>
                              </a>
                           </li>

                           <li role="presentation">
                              <a href="#tasks" onclick="init_rel_tasks_table(<?php echo $ticket->ticketid; ?>,'ticket'); return false;" aria-controls="tasks" role="tab" data-toggle="tab">
                                 <?php echo _l('tasks'); ?>
                              </a>
                           </li>

                           <li role="presentation">
                              <a href="#report" aria-controls="report" role="tab" data-toggle="tab">
                                 <?php echo _l('Reporte'); ?>
                              </a>
                           </li>



                        </ul>

                     </div>

                  </div>

               </div>

            </div>

            <div class="panel_s">

               <div class="panel-body">

                  <!-- Headre -->

                  <div class="row">

                     <div class="col-md-8">

                        <h3 class="mtop4 mbot20 pull-left">
                           <span id="ticket_subject">#
                              <?php echo $ticket->ticketid; ?> - <?php echo $ticket->incidents; ?>
                           </span>
                           <?php if ($ticket->project_id != 0) {
                              echo '<br /><small>' . _l('ticket_linked_to_project', '<a href="' . admin_url('projects/view/' . $ticket->project_id) . '">' . get_project_name_by_id($ticket->project_id) . '</a>') . '</small>';
                           } ?>

                        </h3>

                        <?php echo '<div class="label mtop5 mbot15' . (is_mobile() ? ' ' : ' mleft15 ') . 'p8 pull-left single-ticket-status-label" style="background:' . $ticket->statuscolor . '">' . ticket_status_translate($ticket->ticketstatusid) . '</div>'; ?>
                        <div class="clearfix"></div>
                     </div>

                     <div class="col-md-4 text-right">
                        <div class="row">
                           <div class="col-md-6 col-md-offset-6">
                              <?php echo render_select('status_top', $statuses, array('ticketstatusid', 'name'), '', $ticket->status, array(), array(), 'no-mbot', '', false); ?>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>

                  <!-- End Header -->

                  <!-- Content -->

                  <div class="tab-content">

                     <div role="tabpanel" class="tab-pane <?php if (!$this->session->flashdata('active_tab')) {
                                                               echo 'active';
                                                            } ?>" id="addreply">
                        <hr class="no-mtop" />
                        <?php $tags = get_tags_in($ticket->ticketid, 'ticket'); ?>

                        <?php if (count($tags) > 0) : ?>
                           <div class="row">
                              <div class="col-md-12">
                                 <?php echo '<b><i class="fa fa-tag" aria-hidden="true"></i> ' . _l('tags') . ':</b><br /><br /> ' . render_tags($tags); ?>
                                 <hr />
                              </div>
                           </div>
                        <?php endif ?>

                        <?php if (sizeof($ticket->ticket_notes) > 0) : ?>
                           <div class="row">
                              <div class="col-md-12 mbot15">
                                 <h4 class="bold"><?php echo _l('ticket_single_private_staff_notes'); ?></h4>
                                 <div class="ticketstaffnotes">

                                    <div class="table-responsive">
                                       <table>

                                          <tbody>
                                             <?php foreach ($ticket->ticket_notes as $note) { ?>
                                                <tr>
                                                   <td>
                                                      <span class="bold">
                                                         <?php echo staff_profile_image($note['addedfrom'], array('staff-profile-xs-image')); ?> <a href="<?php echo admin_url('staff/profile/' . $note['addedfrom']); ?>"><?php echo _l('ticket_single_ticket_note_by', get_staff_full_name($note['addedfrom'])); ?>
                                                         </a>
                                                      </span>
                                                      <?php
                                                      //notas privadas
                                                      if ($note['addedfrom'] == get_staff_user_id() || is_admin()) { ?>
                                                         <div class="pull-right">
                                                            <a href="#" class="btn btn-default btn-icon" onclick="toggle_edit_note(<?php echo $note['id']; ?>);return false;"><i class="fa fa-pencil-square-o"></i></a>
                                                            <a href="<?php echo admin_url('misc/delete_note/' . $note["id"]); ?>" class="mright10 _delete btn btn-danger btn-icon">
                                                               <i class="fa fa-remove"></i>
                                                            </a>
                                                         </div>
                                                      <?php } ?>
                                                      <hr class="hr-10" />
                                                      <div data-note-description="<?php echo $note['id']; ?>">
                                                         <?php echo check_for_links($note['description']); ?>
                                                      </div>
                                                      <div data-note-edit-textarea="<?php echo $note['id']; ?>" class="hide inline-block full-width">
                                                         <textarea name="description" class="form-control" rows="4"><?php echo clear_textarea_breaks($note['description']); ?></textarea>
                                                         <div class="text-right mtop15">
                                                            <button type="button" class="btn btn-default" onclick="toggle_edit_note(<?php echo $note['id']; ?>);return false;"><?php echo _l('cancel'); ?></button>
                                                            <button type="button" class="btn btn-info" onclick="edit_note(<?php echo $note['id']; ?>);"><?php echo _l('update_note'); ?></button>
                                                         </div>
                                                      </div>
                                                      <small class="bold">
                                                         <?php echo _l('ticket_single_note_added', _dt($note['dateadded'])); ?>
                                                      </small>
                                                   </td>
                                                </tr>
                                             <?php } ?>
                                          </tbody>

                                       </table>
                                    </div>

                                 </div>
                              </div>
                           </div>
                        <?php endif ?>

                        <div>

                           <?php echo form_open_multipart($this->uri->uri_string(), array('id' => 'single-ticket-form', 'novalidate' => true)); ?>
                           <a href="<?php echo admin_url('tickets/delete/' . $ticket->ticketid); ?>" class="btn btn-danger _delete btn-ticket-label mright5">
                              <i class="fa fa-remove"></i>
                           </a>

                           <?php if (!empty($ticket->priority_name)) : ?>
                              <span class="ticket-label label label-default inline-block">
                                 <?php echo _l('ticket_single_priority', ticket_priority_translate($ticket->priorityid)); ?>
                              </span>
                           <?php endif ?>

                           <?php if (!empty($ticket->service_name)) : ?>
                              <span class="ticket-label label label-default inline-block">
                                 <?php echo _l('service') . ': ' . $ticket->service_name; ?>
                              </span>
                           <?php endif ?>

                           <?php echo form_hidden('ticketid', $ticket->ticketid); ?>

                           <span class="ticket-label label label-default inline-block">
                              <?php echo _l('department') . ': ' . $ticket->department_name; ?>
                           </span>

                           <?php if ($ticket->assigned != 0) : ?>
                              <span class="ticket-label label label-info inline-block">
                                 <?php echo _l('ticket_assigned'); ?>: <?php echo get_staff_full_name($ticket->assigned); ?>
                              </span>
                           <?php endif ?>

                           <?php if ($ticket->lastreply !== NULL) : ?>
                              <span class="ticket-label label label-success inline-block" data-toggle="tooltip" title="<?php echo _dt($ticket->lastreply); ?>">
                                 <span class="text-has-action">
                                    <?php echo _l('ticket_single_last_reply', time_ago($ticket->lastreply)); ?>
                                 </span>
                              </span>
                           <?php endif ?>

                           <span class="ticket-label label label-info inline-block">
                              <a href="<?php echo get_ticket_public_url($ticket); ?>" target="_blank">
                                 <?php echo _l('view_public_form'); ?>
                              </a>
                           </span>

                           <div class="mtop15">
                              <?php $use_knowledge_base = get_option('use_knowledge_base'); ?>

                              <div class="row mbot15">

                                 <div class="col-md-6">

                                    <select data-width="100%" id="insert_predefined_reply" data-live-search="true" class="selectpicker" data-title="<?php echo _l('ticket_single_insert_predefined_reply'); ?>">
                                       <?php foreach ($predefined_replies as $predefined_reply) { ?>
                                          <option value="<?php echo $predefined_reply['id']; ?>"><?php echo $predefined_reply['name']; ?></option>
                                       <?php } ?>
                                    </select>

                                 </div>

                                 <?php if ($use_knowledge_base == 1) : ?>
                                    <div class="visible-xs">
                                       <div class="mtop15"></div>
                                    </div>
                                    <div class="col-md-6">

                                       <?php $groups = get_all_knowledge_base_articles_grouped(); ?>
                                       <select data-width="100%" id="insert_knowledge_base_link" class="selectpicker" data-live-search="true" onchange="insert_ticket_knowledgebase_link(this);" data-title="<?php echo _l('ticket_single_insert_knowledge_base_link'); ?>">
                                          <option value=""></option>
                                          <?php foreach ($groups as $group) { ?>
                                             <?php if (count($group['articles']) > 0) { ?>
                                                <optgroup label="<?php echo $group['name']; ?>">
                                                   <?php foreach ($group['articles'] as $article) { ?>
                                                      <option value="<?php echo $article['articleid']; ?>">
                                                         <?php echo $article['subject']; ?>
                                                      </option>
                                                   <?php } ?>
                                                </optgroup>
                                             <?php } ?>
                                          <?php } ?>
                                       </select>

                                    </div>
                                 <?php endif ?>

                              </div>

                              <?php echo render_textarea('message', '', '', array(), array(), '', 'tinymce'); ?>
                           </div>

                           <div class="panel_s ticket-reply-tools">

                              <div class="btn-bottom-toolbar text-right">
                                 <button type="submit" class="btn btn-info" data-form="#single-ticket-form" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>">
                                    <?php echo _l('ticket_single_add_response'); ?>
                                 </button>
                              </div>

                              <div class="panel-body">

                                 <div class="row">
                                    <div class="col-md-12">
                                       <div class="col-md-6">
                                          <?php echo render_select('status', $statuses, array('ticketstatusid', 'name'), 'ticket_single_change_status', get_option('default_ticket_reply_status'), array(), array(), '', '', false); ?>
                                       </div>
                                       <div class="col-md-6">
                                          <?php echo render_input('cc', 'CC'); ?>
                                       </div>

                                       <div class="col-md-12">
                                          <?php if ($ticket->assigned !== get_staff_user_id()) { ?>
                                             <div class="checkbox">
                                                <input type="checkbox" name="assign_to_current_user" id="assign_to_current_user">
                                                <label for="assign_to_current_user"><?php echo _l('ticket_single_assign_to_me_on_update'); ?></label>
                                             </div>
                                          <?php } ?>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="checkbox">
                                             <input type="checkbox" <?php echo hooks()->apply_filters('ticket_add_response_and_back_to_list_default', 'checked'); ?> name="ticket_add_response_and_back_to_list" value="1" id="ticket_add_response_and_back_to_list">
                                             <label for="ticket_add_response_and_back_to_list"><?php echo _l('ticket_add_response_and_back_to_list'); ?></label>
                                          </div>
                                       </div>

                                    </div>
                                 </div>

                                 <hr />

                                 <div class="row attachments">

                                    <div class="attachment">

                                       <div class="col-md-5 mbot15">

                                          <div class="form-group">
                                             <label for="attachment" class="control-label">
                                                <?php echo _l('ticket_single_attachments'); ?>
                                             </label>
                                             <div class="input-group">
                                                <input type="file" extension="<?php echo str_replace(['.', ' '], '', get_option('ticket_attachments_file_extensions')); ?>" filesize="<?php echo file_upload_max_size(); ?>" class="form-control" name="attachments[0]" accept="<?php echo get_ticket_form_accepted_mimes(); ?>">
                                                <span class="input-group-btn">
                                                   <button class="btn btn-success add_more_attachments p8-half" data-max="<?php echo get_option('maximum_allowed_ticket_attachments'); ?>" type="button"><i class="fa fa-plus"></i></button>
                                                </span>
                                             </div>
                                          </div>

                                       </div>

                                       <div class="clearfix"></div>

                                    </div>

                                 </div>

                              </div>

                           </div>

                           <?php echo form_close(); ?>

                        </div>

                        <!-- End Content -->
                     </div>

                     <!-- Repuestos-->

                     <div role="tabpanel" class="tab-pane" id="note">
                        <div class="col-md-6">

                           <label for="assigned" class="control-label">
                              <?php echo _l('Selecciona una terminal'); ?>
                           </label>

                           <select name="Customer_Name_Spanisdescription" required="true" id="Customer_Name_Spanisdescription" class="selectpicker Customer_Name_Spanisdescription" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                              <option value="" selected>Nada seleccionado</option>
                              <?php foreach ($terminals as $key => $value) : ?>
                                 <option value="<?= $value->id ?>"> <?= $value->serial . ' - ' . $value->model ?> </option>
                              <?php endforeach ?>
                           </select>

                        </div>
                        <div class="col-md-6">
                           <?php $selected = array();
                           if (isset($customer_groups)) {
                              foreach ($customer_groups as $spanisdescription) {
                                 array_push($selected, $spanisdescription['spanisdescription']);
                              }
                           }
                           if (is_admin() || get_option('staff_members_create_inline_customer_groups') == '1') {
                              echo render_select_with_input_group_add_textare('spanisdescription[]', $spanisdescription, array('id', 'materialcode', 'spanisdescription'), 'Repuestos', $spanisdescription, '', array('multiple' => true, 'data-actions-box' => true), array(), '', '', false);
                           } else {
                              echo render_select('spanisdescription', $spanisdescription, array('id', 'materialcode', 'spanisdescription'), 'replacement', (count($spanisdescription) == 1) ? $spanisdescription[0]['id'] : '', array('required' => 'true'));
                           }
                           ?>
                        </div>

                        <div class="col-md-12">

                           <hr class="no-mtop" />

                           <div class="form-group">
                              <label for="note_description"><?php echo _l('ticket_single_note_heading'); ?></label>
                              <textarea class="form-control" name="note_description" rows="5"></textarea>
                              <br>
                              <a id="remover" class="btn btn-info pull-right add_note_ticket"><?php echo _l('ticket_single_spare'); ?></a>
                           </div>

                        </div>

                        <?php $rr = [] ?>

                        <?php if ($repuestos) : ?>

                           <?php foreach ($repuestos as $key => $r) : ?>

                              <?php foreach ($r["terminal"] as $k => $v) : ?>
                                 <?php array_push($rr, $r) ?>
                                 <?php if (count($terminals) == 1) : ?>
                                    <div class="col-md-12" id='rrp'>
                                    <?php elseif (count($terminals) == 2) : ?>
                                       <div class="col-md-6" id='rrp'>
                                       <?php elseif (count($terminals) >= 3) : ?>
                                          <div class="col-md-4" id='rrp'>
                                          <?php endif ?>
                                          <h3> Terminal: <?php echo $v; ?></h3>
                                          <div id='rrrp' class='table-responsive-vertical shadow-z-1'>

                                             <table class="table table-hover table-mc-light-blue" style="width:100%">
                                                <thead>
                                                   <tr>
                                                      <th>Id</th>
                                                      <th>Repuestos</th>
                                                      <th>Materialcode</th>
                                                   </tr>
                                                </thead>
                                                <tbody id='<?php echo $v; ?>'>
                                                </tbody>
                                             </table>

                                          </div>
                                          </div>
                                       <?php endforeach ?>

                                    <?php endforeach ?>

                                 <?php endif ?>

                                       </div>

                                       <!-- End Reouestos-->

                                       <!-- Recordatorio-->

                                       <div role="tabpanel" class="tab-pane" id="tab_reminders">

                                          <a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target=".reminder-modal-ticket-<?php echo $ticket->ticketid; ?>"><i class="fa fa-bell-o"></i> <?php echo _l('ticket_set_reminder_title'); ?></a>
                                          <hr />
                                          <?php render_datatable(array(_l('reminder_description'), _l('reminder_date'), _l('reminder_staff'), _l('reminder_is_notified')), 'reminders'); ?>
                                       </div>

                                       <!-- End Recordatorio-->

                                       <!-- Otros Tickets -->

                                       <div role="tabpanel" class="tab-pane" id="othertickets">
                                          <hr class="no-mtop" />
                                          <div class="_filters _hidden_inputs hidden tickets_filters">
                                             <?php echo form_hidden('filters_ticket_id', $ticket->ticketid); ?>
                                             <?php echo form_hidden('filters_email', $ticket->email); ?>
                                             <?php echo form_hidden('filters_userid', $ticket->userid); ?>
                                          </div>
                                          <?php echo AdminTicketsTableStructure(); ?>
                                       </div>

                                       <!-- Tareas -->

                                       <div role="tabpanel" class="tab-pane" id="tasks">
                                          <hr class="no-mtop" />
                                          <?php init_relation_tasks_table(array('data-new-rel-id' => $ticket->ticketid, 'data-new-rel-type' => 'ticket')); ?>
                                       </div>

                                       <!-- End Tareas -->

                                       <!-- settings -->

                                       <div role="tabpanel" class="tab-pane <?php if ($this->session->flashdata('active_tab_settings')) {
                                                                                 echo 'active';
                                                                              } ?>" id="settings">

                                          <hr class="no-mtop" />

                                          <div class="row ">


                                             <div class="col-md-6">

                                                <div class="row">
                                                   <!-- <div class="col-md-12">
                                                      <?php if (!isset($project_id) && !isset($contact)) : ?>
                                                         <a href="#" id="ticket_no_contact"><span class="label label-default"><i class="fa fa-envelope"></i> <?php echo _l('ticket_create_no_contact'); ?> </span> </a>
                                                         <a href="#" class="hide" id="ticket_to_contact"><span class="label label-default"><i class="fa fa-user-o"></i> <?php echo _l('ticket_create_to_contact'); ?></span></a>
                                                         <div class="mbot15"></div>
                                                      <?php endif ?>
                                                   </div> -->

                                                   <div class="col-md-6">
                                                      <?php echo render_select('incidents', $incidents, array('id', 'incidents'), 'incidents', $ticket->subject, array('disabled' => 'true')); ?>
                                                   </div>

                                                   <div class="col-md-6">
                                                      <?php echo render_select('department', $departments, array('departmentid', 'name'), 'ticket_settings_departments', $ticket->department, array('required' => 'true')); ?>
                                                   </div>

                                                   <!-- <div class="col-md-6">
                                                      <div class="form-group select-placeholder" id="ticket_contact_w">

                                                      <label for="contactid" class="control-label"><?php echo _l('contact'); ?></label>

                                                         <select  name="contactid" id="contactid" class="ajax-search" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>"<?php if (!empty($ticket->from_name) && !empty($ticket->ticket_email)) {
                                                                                                                                                                                                                                             echo ' data-no-contact="true"';
                                                                                                                                                                                                                                          } else {
                                                                                                                                                                                                                                             echo ' data-ticket-emails="' . $ticket->ticket_emails . '"';
                                                                                                                                                                                                                                          } ?>>
                                                            <?php $rel_data = get_relation_data('contact', $ticket->contactid);
                                                            $rel_val = get_relation_values($rel_data, 'contact'); ?>
                                                            <?php echo '<option value="' . $rel_val['id'] . '" selected data-subtext="' . $rel_val['subtext'] . '">' . $rel_val['name'] . '</option>'; ?>
                                                         </select>

                                                         <?php echo form_hidden('userid', $ticket->userid); ?>

                                                      </div>
                                                   </div> -->

                                                </div>

                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <?php echo render_input('name_user', 'ticket_settings_to', $ticket->firstname . ' ' . $ticket->lastname, 'text', array('disabled' => true)); ?>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <?php echo render_input('email_user', 'ticket_settings_email', $ticket->email, 'email', array('disabled' => true)); ?>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <?php echo render_input('rif_number_user', 'Rif', $ticket->rif_number, 'text', array('disabled' => true)); ?>
                                                   </div>
                                                   <div class="col-md-6">
                                                      <?php echo render_input('codaffiliate_user', 'Codigo de afiliacion', $ticket->codaffiliate, 'text', array('disabled' => true)); ?>
                                                   </div>
                                                </div>

                                                <div class="row">

                                                   <!-- <div class="col-md-6">
                                                      <?php echo render_input('cc', 'CC'); ?>
                                                   </div> -->

                                                </div>

                                             </div>

                                             <div class="col-md-6">

                                                <div class="row">

                                                   <div class="col-md-6">
                                                      <div class="form-group">
                                                         <label for="tags" class="control-label"><i class="fa fa-tag" aria-hidden="true"></i> <?php echo _l('tags'); ?></label>
                                                         <input type="text" class="tagsinput" id="tags" name="tags" data-role="tagsinput" value="<?php echo prep_tags_input(get_tags_in($ticket->ticketid, 'ticket')); ?>">
                                                      </div>
                                                   </div>

                                                   <!-- <div class="col-md-6">
                                                      <div class="form-group form-check mtop25">
                                                         <label class="form-check-label" for="citado">Citado  </label>
                                                         <input type="checkbox" class="form-check-input" id="citado">
                                                      </div>
                                                   </div> -->

                                                   <!-- <div class="col-md-6">
                                                      <?php //if (get_option('services') == 1) : 
                                                      ?>
                                                         <?php //echo render_select('service', $services, array('serviceid', 'name'), 'ticket_settings_service', $ticket->service); 
                                                         ?>
                                                      <?php //endif 
                                                      ?>
                                                   </div> -->

                                                   <div class="col-md-6">
                                                      <?php $priorities['callback_translate'] = 'ticket_priority_translate';
                                                      echo render_select('priority', $priorities, array('priorityid', 'name'), 'ticket_settings_priority', $ticket->priority, array('required' => 'true')); ?>
                                                   </div>



                                                </div>

                                                <div class="row">

                                                   <div class="col-md-6">
                                                      <div class="form-group select-placeholder">
                                                         <label for="assigned" class="control-label">
                                                            <?php echo _l('ticket_settings_assign_to'); ?>
                                                         </label>
                                                         <select name="assigned" id="assigned" class="form-control selectpicker" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" data-width="100%">
                                                            <option value=""><?php echo _l('ticket_settings_none_assigned'); ?></option>
                                                            <?php foreach ($staff as $member) : ?>
                                                               <?php if ($member['active'] == 0 && $ticket->assigned != $member['staffid']) {
                                                                  continue;
                                                               } ?>
                                                               <option value="<?php echo $member['staffid']; ?>" <?php if ($member['staffid'] == get_staff_user_id()) {
                                                                                                                     echo 'selected';
                                                                                                                  } ?>>
                                                                  <?php echo $member['firstname'] . ' ' . $member['lastname']; ?>
                                                               </option>
                                                            <?php endforeach ?>
                                                         </select>
                                                      </div>
                                                   </div>

                                                   <!-- <div class="col-md-6">
                                                      <?php $priorities['callback_translate'] = 'ticket_priority_translate';
                                                      echo render_select('priority', $priorities, array('priorityid', 'name'), 'ticket_settings_priority', $ticket->priority, array('required' => 'true')); ?>
                                                   </div> -->

                                                   <!-- <div class="col-md-6">

                                                      <div class="form-group form-group-select-input-service input-group-select">
                                                         <label for="terminal">Terminal</label>
                                                         <div class='input-group input-group-select select-terminal'>
                                                            <select name="terminal" required="true" id="terminal" class="ajax-search-terminal selectpicker jqTransformSelectWrapper" data-width="100%" data-live-search="true"  data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" disabled>
                                                               <option value=""></option>
                                                            </select>
                                                            <div class='input-group-addon' style="opacity: 1;">
                                                               <a href="#" onclick="new_terminal();return false;"><i class="fa fa-plus"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>

                                                   </div> -->

                                                </div>

                                                <div class="row">
                                                   <div class="col-md-12">
                                                      <?php echo render_custom_fields('tickets'); ?>
                                                   </div>
                                                </div>

                                                <!-- <div class="row">                       
                                                   <div class="form-group select-placeholder projects-wrapper<?php if ($ticket->userid == 0) {
                                                                                                                  echo ' hide';
                                                                                                               } ?>">

                                                      <label for="project_id"><?php echo _l('project'); ?></label>
                                                      <div id="project_ajax_search_wrapper">
                                                         <select name="project_id" id="project_id" class="projects ajax-search" data-live-search="true" data-width="100%" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                            <?php if ($ticket->project_id != 0) { ?>
                                                               <option value="<?php echo $ticket->project_id; ?>"><?php echo get_project_name_by_id($ticket->project_id); ?></option>
                                                            <?php } ?>
                                                         </select>
                                                      </div>

                                                   </div>
                                                </div> -->

                                             </div>

                                          </div>

                                          <div class="row">
                                             <div class="col-md-12 text-center">
                                                <a href="#" class="btn btn-info save_changes_settings_single_ticket">
                                                   <?php echo _l('Actualizar'); ?>
                                                </a>
                                             </div>
                                          </div>

                                       </div>




                                       <!-- End settings -->



                                       <div role="tabpanel" class="tab-pane <?php if ($this->session->flashdata('active_tab_settings')) {
                                                                                 echo 'active';
                                                                              } ?>" id="terminals_detail">
                                          <hr class="no-mtop" />

                                          <div class="row">
                                             <div class="col-md-12" id='table_terminal'>
                                                <div style="display: flex;flex-wrap: nowrap;flex-direction: row;justify-content: space-between;align-items: baseline;align-content: stretch;">
                                                   <h3 class="mtop4 mbot20 ">
                                                      <span>Terminales añadidas </span>
                                                   </h3>
                                                   <button id='new_proposal' class="btn btn-info">enviar propuesta</button>


                                                </div>
                                                <div class="col-md-9"></div>
                                                <div class="col-md-3">
                                                   <div class="form-group">
                                                      <input class="search form-control " placeholder="Search" />
                                                   </div>

                                                </div>
                                                <hr class="no-mtop" />

                                                <div class='table-responsive-vertical shadow-z-1'>

                                                   <table class="table table-hover table-mc-light-blue" style="width: 100%;" id='tableticketsresumen'>
                                                      <thead>
                                                         <tr>
                                                            <th><?php echo _l('Affiliate_code'); ?></th>
                                                            <th><?php echo _l('serial'); ?></th>
                                                            <th><?php echo _l('mac'); ?></th>
                                                            <th><?php echo _l('imei'); ?></th>
                                                            <th><?php echo _l('bank'); ?></th>
                                                            <th><?php echo _l('model'); ?></th>
                                                            <th><?php echo _l('repuestos'); ?></th>
                                                            <th><?php echo _l('Nivel de servicio'); ?></th>
                                                            <th><?php echo _l('Estatus de la terminal'); ?></th>
                                                         </tr>
                                                      </thead>
                                                      <tbody class="list">
                                                         <?php foreach ($terminals as $key => $value) : ?>
                                                            <tr id='<?php echo $value->id; ?>'>
                                                               <td data-title="codaffiliate"> <?php echo $value->codaffiliate; ?> </td>
                                                               <td class='serial' data-title="serial"> <?php echo $value->serial; ?> </td>
                                                               <td data-title="mac"> <?php echo $value->mac; ?> </td>
                                                               <td data-title="imei"> <?php echo $value->imei; ?> </td>
                                                               <td data-title="banks"> <?php echo $value->bank; ?> </td>
                                                               <td class='model' data-title="model"> <?php echo $value->model; ?> </td>
                                                               <td data-title="operadora">
                                                                  <p>
                                                                     <a class="btn btn-primary" data-toggle="collapse" href="<?= '#parts' . $key ?>" role="button" aria-expanded="false" aria-controls="<?= 'parts' . $key ?>">
                                                                        Ver repuestos
                                                                     </a>
                                                                  </p>
                                                                  <div class="collapse" id="<?= 'parts' . $key ?>">
                                                                     <div class="card card-body">
                                                                        <?php if (isset($repuestos['spare_parts'])) : ?>
                                                                           <?php
                                                                           $spare_parts = $repuestos['spare_parts'];
                                                                           unset($spare_parts['terminal']);
                                                                           $parts = array_filter(
                                                                              $spare_parts,
                                                                              function ($parts, $key) use ($value) {
                                                                                 return $parts[$key]['serial'] == @$value->serial;
                                                                              },
                                                                              ARRAY_FILTER_USE_BOTH
                                                                           );
                                                                           ?>
                                                                           <?php if ($parts != []) : ?>
                                                                              <ul class="list-group list-group-flush">
                                                                                 <?php foreach ($parts as $key => $s) : ?>
                                                                                    <?php foreach ($s as $key => $p) : ?>
                                                                                       <li class="list-group-item"><?= $p['spanisdescription']; ?></li>
                                                                                    <?php endforeach ?>
                                                                                 <?php endforeach ?>
                                                                              </ul>
                                                                           <?php else : ?>
                                                                              <p>No hay repuestos añadidos</p>
                                                                           <?php endif ?>
                                                                        <?php else : ?>
                                                                           <p>No hay repuestos añadidos</p>
                                                                        <?php endif ?>
                                                                     </div>
                                                                  </div>
                                                               </td>
                                                               <td data-title="servicio">
                                                                  <?php echo render_select_terminal('service-level-' . $value->id, 'service-level-' . $value->id, $services, array('serviceid', 'name'), 'ticket_settings_service', @$value->service_level ?? '', [], [], '', 'service-level'); ?>
                                                               </td>
                                                               <td data-title="servicio">
                                                                  <?php echo render_select_terminal('tickets-level-' . $value->id, 'tickets-level-' . $value->id, $tickets_level, array('id', 'level'), 'tickets_level', @$value->terminal_status ?? '', [], [], '', 'service-level'); ?>
                                                               </td>
                                                            </tr>
                                                         <?php endforeach ?>
                                                      </tbody>
                                                   </table>
                                                </div>

                                             </div>
                                          </div>
                                       </div>



                                       <!-- Reporte -->

                                       <div role="tabpanel" class="tab-pane" id="report">

                                          <div class="content">

                                             <div class="row">
                                                <div class="col-md-2">
                                                   <label for="assigned" class="control-label">
                                                      <?php echo _l('Selecciona una terminal'); ?>
                                                   </label>

                                                   <select name="Customer_terminal" required="true" id="Customer_terminal" class="selectpicker SaveSpreadsheet" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                      <option value="" selected> Nada seleccionado</option>

                                                      <?php foreach ($terminals as $key => $value) : ?>
                                                         <?php if (!empty($value->model)) : ?>
                                                            <option value="<?= $value->id ?>"> <?= $value->serial . ' - ' . $value->model; ?> </option>
                                                         <?php else : ?>
                                                            <option value="<?= $value->id ?>"> <?= $value->serial; ?> </option>
                                                         <?php endif ?>
                                                      <?php endforeach ?>
                                                   </select>
                                                </div>

                                                <div class="col-md-10">
                                                   <br>
                                                   <div class="horizontal-scrollable-tabs">

                                                      <div class="scroller arrow-left"><i class="fa fa-angle-left"></i></div>
                                                      <div class="scroller arrow-right"><i class="fa fa-angle-right"></i></div>

                                                      <div class="horizontal-tabs">

                                                         <ul class="nav nav-tabs no-margin nav-tabs-horizontal" role="tablist">


                                                            <li role="presentation" class="active">
                                                               <a href="#field_0" aria-controls="field_0" role="tab" data-toggle="tab">
                                                                  <?php echo _l('Seccion 1'); ?>
                                                               </a>
                                                            </li>

                                                            <li role="presentation">
                                                               <a href="#field_1" aria-controls="field_1" role="tab" data-toggle="tab">
                                                                  <?php echo _l('Seccion 2'); ?>
                                                               </a>
                                                            </li>

                                                            <li role="presentation">
                                                               <a href="#field_2" aria-controls="field_2" role="tab" data-toggle="tab">
                                                                  <?php echo _l('Seccion 3'); ?>
                                                               </a>
                                                            </li>

                                                            <li role="presentation">
                                                               <a href="#field_3" aria-controls="field_3" role="tab" data-toggle="tab">
                                                                  <?php echo _l('Seccion 4'); ?>
                                                               </a>
                                                            </li>

                                                            <li role="presentation">
                                                               <a href="#field_4" aria-controls="field_4" role="tab" data-toggle="tab">
                                                                  <?php echo _l('Seccion 5'); ?>
                                                               </a>
                                                            </li>

                                                         </ul>

                                                      </div>

                                                   </div>
                                                </div>
                                                <?php echo form_open(admin_url('tickets/save_spreadsheet'), array('id' => 'ticket-SaveSpreadsheet-form')); ?>
                                                <div class="tab-content">

                                                   <div role="tabpanel" class="tab-pane active" id="field_0">
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <br>
                                                            <br>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('country', 'country', 'Venezuela', 'text', array('disabled' => 'true')); ?>
                                                               <?php echo render_input('country', 'country', 'Venezuela', 'hidden', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('repair_center', 'Repair Center', 'PAYTECH VE', 'text', array('disabled' => 'true')); ?>
                                                               <?php echo render_input('repair_center', 'Repair Center', 'PAYTECH VE', 'hidden', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <!-- <label for="assigned" class="control-label">
                                                                  <?php echo _l('Customer Name'); ?>
                                                               </label>
                                                               <select name="customer_name" required="true" id="customer_name" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                                  <option value="1" selected> BANCO DEL TESORO VE </option>
                                                                  <option value="2">PAYTECH VE</option>
                                                                  <option value="3">BNC VE</option>
                                                               </select> -->
                                                               <?php echo render_select('customer_name', $customer_name, array('id', 'customer'), 'Customer name', (count($customer_name) == 1) ? $customer_name[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('in_serial_number', 'In Serial Number', ' ', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('out_serial_number', 'Out Serial Number', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3 rowModel">
                                                               <label for="terminal">Terminal Part Number</label>
                                                               <select name="terminal_part_number" required="true" id="terminal_part_number" class="ajax-search-a terminal_part_number selectpicker" data-width="100%" data-live-search="true" data-abs-cache="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                                  <option value=""></option>
                                                               </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <label for="terminal">Model</label>
                                                               <select name="model" id="model" data-width="100%" data-live-search="true" class="selectpicker model" data-title="<?php echo _l('ticket_single_insert_predefined_reply'); ?>">
                                                                  <?php foreach ($model as $models) { ?>
                                                                     <option value="<?php echo $models['modelid']; ?>"><?php echo $models['model']; ?></option>
                                                                  <?php } ?>
                                                               </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('terminal_imei_address', 'Terminal IMEI address ', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('terminal_mac_address', 'Terminal Mac address ', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_select('made_in', $made_in, array('code', 'made_in'), 'made_in', (count($made_in) == 1) ? $made_in[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('pci', 'PCI', '', 'text', array('required' => 'true')); ?>
                                                               <!-- <?php echo render_select('pci', $model, array('modelid', 'model'), 'PCI', (count($model) == 1) ? $model[0]['modelid'] : '', array('required' => 'true')); ?> -->
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('sales_date', 'Sales date', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('end_sales_warranty', 'End sales warranty', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('date_of_customization', 'Date of customization ', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('in_warranty', 'IN WARRANTY', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('out_warranty', 'OUT WARRANTY', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('in_warranty_accesories', 'IN WARRANTY  accesories', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('out_warranty_accesories', 'OUT WARRANTY  accesories', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('in_date', 'IN date', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('date_of_entry_to_repair', 'Date of entry to repair ', '', 'text', array('disabled' => 'true')); ?>
                                                               <?php echo render_input('date_of_entry_to_repair', 'Date of entry to repair ', '', 'hidden', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <label for="report-from" class="control-label"><?php echo _l('Date start stop terminal'); ?></label>
                                                               <div class="input-group date">
                                                                  <input type="text" class="form-control datepicker" id="date_start_stop_terminal" name="date_start_stop_terminal" required>
                                                                  <div class="input-group-addon">
                                                                     <i class="fa fa-calendar calendar-icon"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <label for="report-from" class="control-label"><?php echo _l('Date end stop terminal'); ?></label>
                                                               <div class="input-group date">
                                                                  <input type="text" class="form-control datepicker" id="date_end_stop_terminal" name="date_end_stop_terminal" required>
                                                                  <div class="input-group-addon">
                                                                     <i class="fa fa-calendar calendar-icon"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <label for="report-from" class="control-label"><?php echo _l('Data of repair'); ?></label>
                                                               <div class="input-group date">
                                                                  <input type="text" class="form-control datepicker" id="data_of_repair" name="data_of_repair" required>
                                                                  <div class="input-group-addon">
                                                                     <i class="fa fa-calendar calendar-icon"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <label for="report-from" class="control-label"><?php echo _l('Out date'); ?></label>
                                                               <div class="input-group date">
                                                                  <input type="text" class="form-control datepicker" id="out_date" name="out_date" required>
                                                                  <div class="input-group-addon">
                                                                     <i class="fa fa-calendar calendar-icon"></i>
                                                                  </div>
                                                               </div>
                                                            </div>

                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div role="tabpanel" class="tab-pane" id="field_1">
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <br>
                                                            <?php echo render_input('deadline', 'Deadline', 'NUll', 'hidden', array('required' => 'true')); ?>
                                                            <div class="col-md-3">
                                                               <label for="report-from" class="control-label"><?php echo _l('Report Month'); ?></label>
                                                               <div class="input-group date">
                                                                  <input type="text" class="form-control datepicker" id="report_month" name="report_month" required>
                                                                  <div class="input-group-addon">
                                                                     <i class="fa fa-calendar calendar-icon"></i>
                                                                  </div>
                                                               </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('backlog', 'backlog (Out Capacity)', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('time_of_repairclient_with_discount_supply', 'Time of Repairclient With Discount Supply', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('time_of_repairclient_with_discount_customer', 'Time of Repairclient With Discount Customer', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('sla_with_discount_cr', 'Sla % With Discount cr', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('sla_with_discount_customer', 'Sla % With Discount Customer', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('mrr', 'mrr', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('bounce', 'bounce', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <!-- <?php echo render_input('date_of_the_last_repair', 'Date of The Last Repair', 'date_of_the_last_repair', 'text', array('required' => 'true')); ?> -->
                                                               <label for="report-from" class="control-label"><?php echo _l('date of the last repair'); ?></label>
                                                               <div class="input-group date">
                                                                  <input type="text" class="form-control datepicker" id="date_of_the_last_repair" name="date_of_the_last_repair" required>
                                                                  <div class="input-group-addon">
                                                                     <i class="fa fa-calendar calendar-icon"></i>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('drr', 'drr', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('doa_repair', 'Doa Repair', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('eco_1', 'Eco 1', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('custom_doa', 'Custom Doa', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('first_pass_yield_fpytest2', 'First Pass Yield Fpytest2', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('first_pass_yield_fpyqa', 'First Pass Yield Fpyqa', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php
                                                               $cod = array_filter($incidents, function ($incident) use ($ticket) {
                                                                  return $incident["incidents"] == $ticket->incidents;
                                                               });
                                                               $code = current($cod);
                                                               echo render_input('1_customer_symptom', '1 Customer Symptom', $code["code"], 'text', array('required' => 'true'));
                                                               ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('2_customer_symptom', '2 Customer Symptom', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('3_customer_symptom', '3 Customer Symptom', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('4_customer_symptom', '4 Customer Symptom', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('5_customer_symptom', '5 Customer Symptom', '', 'text', array('required' => 'true')); ?>
                                                            </div>

                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div role="tabpanel" class="tab-pane" id="field_2">
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <br><br>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('1_technician_diagnosis', $technician, array('id', 'code', 'description'), '1. Technician Diagnosis', (count($technician) == 1) ? $technician[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('2_technician_diagnosis', $technician, array('id', 'code', 'description'), '2. Technician Diagnosis', (count($technician) == 1) ? $technician[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('3_technician_diagnosis', $technician, array('id', 'code', 'description'), '3. Technician Diagnosis', (count($technician) == 1) ? $technician[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('4_technician_diagnosis', $technician, array('id', 'code', 'description'), '4. Technician Diagnosis', (count($technician) == 1) ? $technician[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('5_technician_diagnosis', $technician, array('id', 'code', 'description'), '5. Technician Diagnosis', (count($technician) == 1) ? $technician[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_input('6_technician_diagnosis', '6 Technician Diagnosis', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_input('7_technician_diagnosis', '7 Technician Diagnosis', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_input('8_technician_diagnosis', '8 Technician Diagnosis', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_input('9_technician_diagnosis', '9 Technician Diagnosis', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_input('10_technician_diagnosis', '10 Technician Diagnosis', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('1_technician_resolution', $resolution, array('id', 'code', 'description'), '1. Technician Resolution', (count($resolution) == 1) ? $resolution[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('2_technician_resolution', $resolution, array('id', 'code', 'description'), '2. Technician Resolution', (count($resolution) == 1) ? $resolution[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('3_technician_resolution', $resolution, array('id', 'code', 'description'), '3. Technician Resolution', (count($resolution) == 1) ? $resolution[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('4_technician_resolution', $resolution, array('id', 'code', 'description'), '4. Technician Resolution', (count($resolution) == 1) ? $resolution[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-4">
                                                               <?php echo render_select('5_technician_resolution', $resolution, array('id', 'code', 'description'), '5. Technician Resolution', (count($resolution) == 1) ? $resolution[0]['code'] : '', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('6_technician_resolution', '6 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('7_technician_resolution', '7 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('8_technician_resolution', '8 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('9_technician_resolution', '9 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('10_technician_resolution', '10 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('11_technician_resolution', '11 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('12_technician_resolution', '12 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('13_technician_resolution', '13 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('14_technician_resolution', '14 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                               <?php echo render_input('15_technician_resolution', '15 Technician Resolution', 'NI', 'hidden', array('required' => 'true')); ?>
                                                            </div>

                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div role="tabpanel" class="tab-pane" id="field_3">
                                                      <div class="row">
                                                         <div class="col-md-12">
                                                            <br><br>
                                                            <?php if ($repuestos) : ?>

                                                               <div class='imputPart'>
                                                                  <div class='imputPart'>
                                                                     <h3 class="mtop4 mbot20 ">
                                                                        <span>No has selecionado la termimal</span>
                                                                     </h3>
                                                                  </div>
                                                               </div>

                                                            <?php else : ?>
                                                               <h3 class="mtop4 mbot20  imputPart">
                                                                  <span>No has añadido ningun repuesto</span>
                                                               </h3>
                                                            <?php endif ?>

                                                         </div>
                                                      </div>
                                                   </div>

                                                   <div role="tabpanel" class="tab-pane" id="field_4">
                                                      <div class="row">
                                                         <div class="col-md-12"> <br>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('in_number', 'IN Number', $ticket->ticketid, 'text', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('out_number', 'Out number', $ticket->ticketid, 'text', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('lote_placa_box_out_number', '#Lote/placa/BOX OUT Number', '', 'text', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_select('repair_status', $sepair, array('id', 'repair',), 'repair_status', (count($sepair) == 1) ? $sepair[0]['sepair'] : '', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <label for="assigned" class="control-label">
                                                                  <?php echo _l('Repair Level'); ?>
                                                               </label>
                                                               <select name="repair_level" required="true" id="repair_level" class="selectpicker" data-width="100%" data-live-search="true" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                                                                  <option value="">A</option>
                                                                  <option value="1">B</option>
                                                                  <option value="2">C</option>
                                                                  <option value="3">D</option>
                                                                  <option value="4">E</option>
                                                                  <option value="5">I</option>
                                                                  <option value="6">B.E.R</option>
                                                                  <option value="7">SWAP</option>
                                                                  <option value="8">PT</option>
                                                               </select>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('detail_repair_status', 'Detail Repair status', '', 'text', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('terminal_status', 'Terminal Status', '', 'text', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_select('accesories_status', array(
                                                                  ['id' => 1, 'accesories' => 'Terminal Satus Accesories'],
                                                                  ['id' => 2, 'accesories' => 'ACCESSORIES-GOOD CONDITION'],
                                                                  ['id' => 3, 'accesories' => 'ACCESSORIES-BATTERY CHANGED'],
                                                                  ['id' => 4, 'accesories' => 'ACCESSORIES-POWER SUPPLY CHANGED'],
                                                                  ['id' => 5, 'accesories' => 'ACCESSORIES POWER SUPPLY-BATTERY CHANGED']
                                                               ), array('id', 'accesories',), 'accesories_status', '', array('required' => 'true')); ?>
                                                            </div>

                                                            <div class="col-md-3">
                                                               <?php echo render_input('unit_cogs_USD', 'Unit Cogs (USD)', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('logistics', 'Logistics', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('other_cost', 'Other cost', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('total_cogs_unit_cogs_Logistica_otros', 'Total Cogs Unit Cogs+Logistica+otros', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('workforce_cost', 'Workforce Cost', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('app', 'App', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('workforce_cost_total_work_app', 'Workforce Cost Total Work +App', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('administration_cost', 'Administration Cost', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('external_services_cost_USD', 'External Services Cost (USD)', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('comments1', 'comments1', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('comments2', 'comments2', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('comments3', 'comments3', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                            <div class="col-md-3">
                                                               <?php echo render_input('comments4', 'comments4', '', 'text', array('required' => 'true')); ?>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>

                                                </div>

                                             </div>
                                             <div class="row">
                                                <div class="col-md-12 text-right"> <br>
                                                   <a href='#' class='btn btn-success' onclick='SaveSpreadsheet()' id='SaveSpreadsheet'>enviar</a>
                                                </div>
                                             </div>
                                             <?php echo form_close(); ?>
                                          </div>

                                       </div>

                                       <!-- End Reporte -->


                                       <!-- <div class="row">
                                          <div class="col-md-12 text-center">
                                             <hr />
                                             <a href="#" class="btn btn-info save_changes_settings_single_ticket">
                                                <?php echo _l('Actualizar'); ?>
                                             </a>
                                          </div>
                                       </div> -->

                                    </div>

                     </div>

                  </div>

               </div>

               
            </div>

            <div class="panel-body">
               <?php foreach ($ticket_replies as $reply) : ?>
                  <div class="row">
                     <div class="panel_s">
                        <div class="panel-body <?php if ($reply['admin'] == NULL) { echo 'client-reply';} ?>">
                           <div class="row">
                              <div class="col-md-3 border-right ticket-submitter-info">
                                 <p>

                                    <?php if ($reply['admin'] == NULL || $reply['admin'] == 0): ?>
                                       <?php if ($reply['userid'] != 0): ?>
                                          <a href="<?php echo admin_url('clients/client/' . $reply['userid'] . '?contactid=' . $reply['contactid']); ?>">
                                             <?php echo $reply['submitter']; ?>
                                          </a>
                                          <?php else: ?>
                                             <?php echo $reply['submitter']; ?>
                                             <br/>
                                             <a href="mailto:<?php echo $reply['reply_email']; ?>"><?php echo $reply['reply_email']; ?></a>
                                       <?php endif ?>
                                       <?php else : ?>
                                          <a href="<?php echo admin_url('profile/' . $reply['admin']); ?>"><?php echo $reply['submitter']; ?></a>
                                    <?php endif ?>

                                 </p>

                                 <p class="text-muted">

                                    <?php if ($reply['admin'] !== NULL || $reply['admin'] != 0): ?>
                                       <?php echo _l('ticket_staff_string'); ?>

                                       <?php else : ?>
                                          <?php if ($reply['userid'] != 0) : ?>
                                             <?php echo _l('ticket_client_string'); ?>
                                          <?php endif; ?>
                                    <?php endif; ?>
                                    
                                 </p>

                                 <hr />

                                 <a href="<?php echo admin_url('tickets/delete_ticket_reply/' . $ticket->ticketid . '/' . $reply['id']); ?>" class="btn btn-danger pull-left _delete mright5 btn-xs"><?php echo _l('delete_ticket_reply'); ?></a>

                                 <div class="clearfix"></div>

                                 <?php if (has_permission('tasks', '', 'create')) : ?>
                                    <a href="#" class="pull-left btn btn-default mtop5 btn-xs" onclick="convert_ticket_to_task(<?php echo $reply['id']; ?>,'reply'); return false;"><?php echo _l('convert_to_task'); ?></a>
                                    <div class="clearfix"></div>
                                 <?php endif ?>

                              </div>

                              <div class="col-md-9">

                                 <div class="row">

                                    <div class="col-md-12 text-right">
                                       <?php if (!empty($reply['message'])) : ?>
                                          <a href="#" onclick="print_ticket_message(<?php echo $reply['id']; ?>, 'reply'); return false;" class="mright5"><i class="fa fa-print"></i></a>
                                       <?php endif ?>
                                       <a href="#" onclick="edit_ticket_message(<?php echo $reply['id']; ?>,'reply'); return false;"><i class="fa fa-pencil-square-o"></i></a>
                                    </div>

                                 </div>

                                 <div class="clearfix"></div>

                                 <div data-reply-id="<?php echo $reply['id']; ?>" class="tc-content">
                                    <?php echo check_for_links($reply['message']); ?>
                                 </div>

                                 <?php if (count($reply['attachments']) > 0) : echo '<hr/>'; ?>

                                    <?php foreach ($reply['attachments'] as $attachment) :  $path = get_upload_path_by_type('ticket') . $ticket->ticketid . '/' . $attachment['file_name'];
                                       $is_image = is_image($path); ?>

                                       <?php if ($is_image) : echo '<div class="preview_image">'; ?>
                                          <a href="<?php echo site_url('download/file/ticket/' . $attachment['id']); ?>" class="display-block mbot5" <?php if ($is_image) { ?> data-lightbox="attachment-reply-<?php echo $reply['id']; ?>" <?php } ?>>
                                             <i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i> <?php echo $attachment['file_name']; ?>
                                             <?php if ($is_image) { ?>
                                                <img class="mtop5" src="<?php echo site_url('download/preview_image?path=' . protected_file_url_by_path($path) . '&type=' . $attachment['filetype']); ?>">
                                             <?php } ?>
                                          </a>
                                       <?php echo '</div>';
                                       endif ?>

                                       <?php if (is_admin() || (!is_admin() && get_option('allow_non_admin_staff_to_delete_ticket_attachments') == '1')) {
                                          echo '<a href="' . admin_url('tickets/delete_attachment/' . $attachment['id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
                                       }
                                       echo '<hr/>'; ?>

                                    <?php endforeach ?>

                                 <?php endif ?>

                              </div>

                           </div>

                        </div>

                        <div class="panel-footer">
                           <span><?php echo _l('ticket_posted', _dt($reply['date'])); ?></span>
                        </div>

                     </div>

                  </div>

               <?php endforeach ?>
            </div>

            <div class="panel_s mtop20">
               <div class="panel-body <?php if ($ticket->admin == NULL) { echo 'client-reply'; } ?>">
                  <div class="row">
                     <div class="col-md-3 border-right ticket-submitter-info ticket-submitter-info">
                        <p>
                           <?php if ($ticket->admin == NULL || $ticket->admin == 0) { ?>
                              <?php if ($ticket->userid != 0) { ?>
                                 <a href="<?php echo admin_url('clients/client/' . $ticket->userid . '?contactid=' . $ticket->contactid); ?>"><?php echo $ticket->submitter; ?> </a>
                              <?php } else {
                                 echo $ticket->submitter; ?>
                                 <br />
                                 <a href="mailto:<?php echo $ticket->ticket_email; ?>"><?php echo $ticket->ticket_email; ?></a>
                                 <hr />
                                 <?php if (total_rows(db_prefix() . 'spam_filters', array('type' => 'sender', 'value' => $ticket->ticket_email, 'rel_type' => 'tickets')) == 0) { ?>
                                    <button type="button" data-sender="<?php echo $ticket->ticket_email; ?>" class="btn btn-danger block-sender btn-xs"> <?php echo _l('block_sender'); ?> </button>
                              <?php } else {
                                    echo '<span class="label label-danger">' . _l('sender_blocked') . '</span>';
                                 }
                              }
                           } else {  ?>
                              <a href="<?php echo admin_url('profile/' . $ticket->admin); ?>"><?php echo (isset($ticket->user_firstname)) ? $ticket->staff_firstname . ' ' . $ticket->staff_lastname : ' '; ?></a>
                           <?php } ?>
                        </p>

                        <p class="text-muted">
                           <?php if ($ticket->admin !== NULL || $ticket->admin != 0) {
                              echo _l('ticket_staff_string');
                           } else {
                              if ($ticket->userid != 0) {
                                 echo _l('ticket_client_string');
                              }
                           } ?>
                        </p>

                        <?php if (has_permission('tasks', '', 'create')) { ?>
                           <a href="#" class="btn btn-default btn-xs" onclick="convert_ticket_to_task(<?php echo $ticket->ticketid; ?>,'ticket'); return false;"><?php echo _l('convert_to_task'); ?></a>
                        <?php } ?>

                     </div>

                     <div class="col-md-9">

                        <div class="row">

                           <div class="col-md-12 text-right">
                              <?php if (!empty($ticket->message)) : ?>
                                 <a href="#" onclick="print_ticket_message(<?php echo $ticket->ticketid; ?>, 'ticket'); return false;" class="mright5"><i class="fa fa-print"></i></a>
                              <?php endif ?>
                              <a href="#" onclick="edit_ticket_message(<?php echo $ticket->ticketid; ?>,'ticket'); return false;"><i class="fa fa-pencil-square-o"></i></a>
                           </div>

                        </div>

                        <div data-ticket-id="<?php echo $ticket->ticketid; ?>" class="tc-content">
                           <?php echo check_for_links($ticket->message); ?>
                        </div>

                        <?php if (isset($ticket->attachments)) : ?>
                           <?php if (count($ticket->attachments) > 0) : echo '<hr/>'; ?>

                              <?php foreach ($ticket->attachments as $attachment) : ?>

                                 <?php $path = get_upload_path_by_type('ticket') . $ticket->ticketid . '/' . $attachment['file_name'];
                                 $is_image = is_image($path); ?>

                                 <?php if ($is_image) : echo '<div class="preview_image">'; ?>

                                    <a href="<?php echo site_url('download/file/ticket/' . $attachment['id']); ?>" class="display-block mbot5" <?php if ($is_image) { ?> data-lightbox="attachment-ticket-<?php echo $ticket->ticketid; ?>" <?php } ?>>
                                       <i class="<?php echo get_mime_class($attachment['filetype']); ?>"></i> <?php echo $attachment['file_name']; ?>
                                       <?php if ($is_image) : ?>
                                          <img class="mtop5" src="<?php echo site_url('download/preview_image?path=' . protected_file_url_by_path($path) . '&type=' . $attachment['filetype']); ?>">
                                       <?php endif ?>
                                    </a>

                                 <?php echo '</div>';
                                 endif ?>

                                 <?php if (is_admin() || (!is_admin() && get_option('allow_non_admin_staff_to_delete_ticket_attachments') == '1')) {
                                    echo '<a href="' . admin_url('tickets/delete_attachment/' . $attachment['id']) . '" class="text-danger _delete">' . _l('delete') . '</a>';
                                 }
                                 echo '<hr/>'; ?>
                              <?php endforeach ?>
                           <?php endif ?>
                        <?php endif ?>
                     </div>

                  </div>

               </div>


               <div class="panel-footer">
                  <?php echo _l('ticket_posted', _dt($ticket->date)); ?>
               </div>

            </div>

         </div>

         <div class="btn-bottom-pusher"></div>

         <!-- <?php if (count($ticket_replies) > 1): ?>
            <a href="#top" id="toplink">↑</a>
            <a href="#bot" id="botlink">↓</a>
         <?php endif ?> -->

         <!-- The reminders modal -->
         <?php $this->load->view(
            'admin/includes/modals/reminder',
            array(
               'id' => $ticket->ticketid,
               'name' => 'ticket',
               'members' => $staff,
               'reminder_title' => _l('ticket_set_reminder_title')
            )
         ); ?>

         <!-- Edit Ticket Messsage Modal -->
         <div class="modal fade" id="ticket-message" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
               <?php echo form_open(admin_url('tickets/edit_message')); ?>
               <div class="modal-content">
                  <div id="edit-ticket-message-additional"></div>
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title" id="myModalLabel"><?php echo _l('ticket_message_edit'); ?></h4>
                  </div>
                  <div class="modal-body">
                     <?php echo render_textarea('data', '', '', array(), array(), '', 'tinymce-ticket-edit'); ?>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                     <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
                  </div>
               </div>
               <?php echo form_close(); ?>
            </div>
         </div>
         <!-- End Edit Ticket Messsage Modal -->

         <script>
            var _ticket_message;
         </script>

         <?php $this->load->view('admin/tickets/services/service'); ?>
         <?php init_tail(); ?>
         <?php hooks()->do_action('ticket_admin_single_page_loaded', $ticket); ?>

         <script>
            $(function() {

               $('select[id="Customer_Name"]').selectpicker('val', 1);

               var repuestos = <?php echo json_encode((isset($rr[0])) ? $rr : 'nada seleccionado'); ?>;

               $('#single-ticket-form').appFormValidator();
               init_ajax_search('contact', '#contactid.ajax-search', {
                  tickets_contacts: true
               });
               init_ajax_search('project', 'select[name="project_id"]', {
                  customer_id: function() {
                     return $('input[name="userid"]').val();
                  }
               });
               $('body').on('shown.bs.modal', '#_task_modal', function() {
                  if (typeof(_ticket_message) != 'undefined') {
                     // Init the task description editor
                     if (!is_mobile()) {
                        $(this).find('#description').click();
                     } else {
                        $(this).find('#description').focus();
                     }
                     setTimeout(function() {
                        tinymce.get('description').execCommand('mceInsertContent', false, _ticket_message);
                        $('#_task_modal input[name="name"]').val($('#ticket_subject').text().trim());
                     }, 100);
                  }
               });

               if (repuestos != '') {
                  data = rpArray(repuestos);
                  if (data) {
                     templateRp(data)
                  }

               }


               $(".model .dropdown-menu").click(function() {
                  var id = $('#model').val();
                  var template = '<label for="terminal">Terminal Part Number</label>'
                  template += '<select name="terminal_part_number" required="true" id="terminal_part_number" class="ajax-search terminal_part_number selectpicker" data-width="100%" data-live-search="true" data-abs-cache="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" >'
                  template += '<option value=""></option>'
                  template += '</select>'

                  if (id != '') {
                     $(".rowModel").html(template);
                     init_ajax_search_terminal_part_number('terminal_part_number', '#terminal_part_number.ajax-search', {
                        tickets_contacts: true,
                        contact_modelid: id
                     });
                  }
               })

               $(".SaveSpreadsheet .dropdown-menu").click(function() {
                  <?php $parts = $repuestos["spare_parts"] ?? null; ?>
                  <?php unset($parts["terminal"]); ?>
                  let terminal = <?php echo json_encode($terminals); ?>;
                  let repuestos = <?php echo json_encode($parts); ?>;
                  let imput = []
                  let ingenicolevel = [];
                  let paytechlevel = []
                  let detailrepairstatus = []
                  let terminalstatus = []
                  let ite = 1
                  let id = $('#Customer_terminal').val()
                  let template = ''
                  let _r = [];

                  let _t = terminal.find((a) => {
                     return a.id == id
                  })
                  if (repuestos != null) {
                     _r = repuestos.find((r) => {
                        [part] = r;
                        return part.serial == _t.serial
                     })
                  } else {
                     _r = []
                  }
                  $('input[name="detail_repair_status"]').val('');
                  $('input[name="terminal_status"]').val('');
                  $('select[name="repair_level"]').selectpicker('val', '');

                  if (_r.length > 0) {

                     for (const k in _r) {
                        if (Object.hasOwnProperty.call(_r, k)) {
                           const res = _r[k];
                           template = `<div class="col-md-3"> <label for="${ite}_part_status">${ite} Part Status</label> <select name="${ite}_part_status" required="true" id="${ite}_part_status" class="selectpicker" data-width="100%" data-live-search="true" data-abs-cache="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" > <option value="HR">HR</option> <option value="CN">CN</option> <option value="PR">PR</option> </select> </div>`
                           template += `<div class="col-md-3"> <div class="form-group" app-field-wrapper="${ite}_part_number"> <label for="${ite}_part_number" class="control-label">${ite}. Part Number</label> <input type="text" id="${ite}_part_number" name="${ite}_part_number" class="form-control" required="true" value="${res.materialcode}"> </div> </div>`
                           template += `<div class="col-md-3"> <div class="form-group" app-field-wrapper="${ite}_part_description"> <label for="${ite}_part_description" class="control-label">${ite}. Part Number </label> <input type="text" id="${ite}_part_description" name="${ite}_part_description" class="form-control" required="true" value="${res.spanisdescription}"> </div> </div>`
                           template += `<div class="col-md-3"> <div class="form-group" app-field-wrapper="${ite}_part_cost"> <label for="${ite}_part_cost" class="control-label">${ite}. Part Number</label> <input type="text" id="${ite}_part_cost" name="${ite}_part_cost" class="form-control" required="true" value="0"> </div> </div>`
                           ingenicolevel.push(res.ingenicolevel);
                           paytechlevel.push(res.paytechlevel);
                           detailrepairstatus.push(res.detailrepairstatus);
                           terminalstatus.push(res.terminalstatus)
                           imput.push(template)
                           ite++
                        }
                     }

                     let BER = ingenicolevel.filter(val => val == 'BER')

                     if (paytechlevel) {
                        if ($('input[name="detail_repair_status"]').val() == '') {
                           let vv = Detail_Repairstatus(detailrepairstatus);
                           if (BER) {
                              let vv = Detail_Repairstatus(detailrepairstatus, '', true);
                           }
                           $('input[name="detail_repair_status"]').val('');
                           $('input[name="detail_repair_status"]').val(vv);
                        }

                        if ($('input[name="terminal_status"]').val() == '') {
                           let t = Terminal_Status(terminalstatus);
                           if (BER) {
                              let t = Terminal_Status(terminalstatus, true);
                           }
                           $('input[name="terminal_status"]').val('')
                           $('input[name="terminal_status"]').val(t)
                        }

                        let v = paytechlevel.filter(
                           val => /[0-9]+/.test(val)
                        ).reduce((acc, val) => val > acc ? val : acc, 0);

                        if ($('select[name="repair_level"]').val() == '') {
                           if (BER) {
                              v = 6;
                           }
                           $('select[name="repair_level"]').selectpicker('val', '');
                           $('select[name="repair_level"]').selectpicker('val', v);
                        }

                     }

                  } else {
                     setTimeout(() => {
                        let resolution = [];
                        let text;
                        for (let i = 0; i <= 4; i++) {
                           template = ` <div class="col-md-3 hidden"> <label for="${ite}_part_status">${ite} Part Status</label><select name="${ite}_part_status" required="true" id="${ite}_part_status" class="selectpicker" data-width="100%" data-live-search="true" data-abs-cache="false" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" > <option value="" selected>null</option><option value="HR">HR</option><option value="CN">CN</option> <option value="PR">PR</option> </select> </div>`
                           template += `<div class="col-md-3 hidden"> <div class="form-group" app-field-wrapper="${ite}_part_number"> <label for="${ite}_part_number" class="control-label">${ite}. Part Number</label> <input type="text" id="${ite}_part_number" name="${ite}_part_number" class="form-control"  value=""> </div> </div>`
                           template += `<div class="col-md-3 hidden"> <div class="form-group" app-field-wrapper="${ite}_part_description"> <label for="${ite}_part_description" class="control-label">${ite}. Part Number </label> <input type="text" id="${ite}_part_description" name="${ite}_part_description" class="form-control"value=""> </div> </div>`
                           template += `<div class="col-md-3 hidden">  <div class="form-group" app-field-wrapper="${ite}_part_cost">  <label for="${ite}_part_cost" class="control-label">${ite}. Part Number</label>  <input type="text" id="${ite}_part_cost" name="${ite}_part_cost" class="form-control" value="">  </div> </div>`
                           text = `#${ite}_technician_resolution`;
                           resolution.push($(text).val());
                           imput.push(template);
                           ite++
                        }

                        let nivel_a = resolution.filter(val => val == '51' || val == '54' || val == '55').length
                        let nivel_b = resolution.filter(val => val == '10').length

                        if (nivel_a != 0 && nivel_b != 0) {
                           nivel_a = false;
                        }

                        if (nivel_b != 0) {
                           let b = Detail_Repairstatus('', 'b')
                           if ($('input[name="detail_repair_status"]').val() == '') {
                              $('input[name="detail_repair_status"]').val('');
                              $('input[name="detail_repair_status"]').val(b);
                           }
                           if ($('input[name="terminal_status"]').val() == '') {
                              $('input[name="terminal_status"]').val('')
                              $('input[name="terminal_status"]').val(b)
                           }
                           if ($('select[name="repair_level"]').val() == '') {
                              $('select[name="repair_level"]').selectpicker('val', '');
                              $('select[name="repair_level"]').selectpicker('val', 1);
                           }
                           imput.push(`<h3 class="mtop4 mbot20"><span>${b}</span></h3>`);
                        }

                        if (nivel_a != 0) {
                           let a = Detail_Repairstatus('', 'a')
                           if ($('input[name="detail_repair_status"]').val() == '') {
                              $('input[name="detail_repair_status"]').val('');
                              $('input[name="detail_repair_status"]').val(a);
                           }
                           if ($('input[name="terminal_status"]').val() == '') {
                              $('input[name="terminal_status"]').val('')
                              $('input[name="terminal_status"]').val(a)
                           }
                           imput.push(`<h3 class="mtop4 mbot20"><span>${a}</span></h3>`);
                        }

                        $(".imputPart").html(imput);

                        return false
                     }, 1000);

                  }

                  $(".imputPart").html(imput);

               })

               function Detail_Repairstatus(data, nivel, BER) {
                  if (BER) {
                     return 'B.E.R ( PCBA)'
                  }

                  if (data) {

                     let parts = data.filter(val => val == 'Parts').length
                     let cosmetic = data.filter(val => val == 'Cosmetic Parts').length
                     let ber = data.filter(val => val == 'B.E.R ( PCBA)').length

                     if (ber) {
                        return 'B.E.R ( PCBA)'
                     }

                     if (cosmetic > 0 && parts > 0) {
                        return 'Parts + Cosmetic Parts'
                     }

                     if (parts == 5 || parts > 0) {
                        return 'Parts'
                     }
                     if (cosmetic == 5 || cosmetic > 0) {
                        return 'Cosmetic Parts'
                     }

                  } else {
                     if (nivel == 'b') {
                        return 'MAINTENANCE'
                     }
                     if (nivel == 'a') {
                        return 'SOFTWARE'
                     }

                  }
               }

               function Terminal_Status(data, ber) {
                  if (ber) {
                     return 'CUSTOMER ABUSE'
                  }

                  let FUNTIONALFAILURE = data.filter(a => a == "FUNTIONAL FAILURE").length
                  let CUSTOMERABUSE = data.filter(a => a == "CUSTOMER ABUSE").length

                  if (CUSTOMERABUSE > 0 && FUNTIONALFAILURE > 0) {
                     return 'FUNTIONAL FAILURE + CUSTOMER ABUSE'
                  }

                  if (FUNTIONALFAILURE == 5 || FUNTIONALFAILURE > 0) {
                     return 'FUNTIONAL FAILURE'
                  }
                  if (CUSTOMERABUSE == 5 || CUSTOMERABUSE > 0) {
                     return 'CUSTOMER ABUSE'
                  }

               }

               function ticket_detail({
                  id,
                  terminal,
                  nivel,
                  url,
                  status_terminal
               }) {

                  $.post(url, {
                     id,
                     nivel,
                     terminal,
                     status_terminal
                  }).done(function(e) {
                     var response = JSON.parse(e);
                     if (response.contact_data.status == 'success') {
                        alert_float(
                           response.contact_data.status,
                           response.contact_data.message
                        );
                        return false;
                     }

                     alert_float(
                        response.contact_data.status,
                        response.contact_data.message
                     );
                     return false;
                  });

                  return false;
               }


               $('.service-level').change(function() {

                  let tr = $(this).parent().parent().parent().parent();
                  let terminal = $(tr).attr('id');
                  let $this = $('#' + terminal + ' td');

                  let [, , , , , , ,
                     nivel,
                     status_terminal
                  ] = $this

                  status_terminal = $(status_terminal).find('select').val();
                  nivel = $(nivel).find('select').val();

                  let id = $('input[name="ticketid"]').val();
                  let url = '<?= base_url('admin/tickets/create_ticket_detail') ?>';

                  ticket_detail({
                     id,
                     url,
                     terminal,
                     nivel,
                     status_terminal
                  });
                  return false;
               })

               $('#new_proposal').on('click', function() {
                  var url = '<?= base_url('admin/tickets/create_ticket_proposal') ?>';
                  var id = $('input[name="ticketid"]').val();
                  $.post(url, {
                     id: id
                  }).done(function(e) {
                     var response = JSON.parse(e);
                     if (response.contact_data.status == 'success') {
                        alert_float(response.contact_data.status, response.contact_data.message);
                     } else {
                        alert_float(response.contact_data.status, response.contact_data.message);
                     }
                  });
               });

            });


            var options = {
               valueNames: ['serial', 'model']
            };

            var userList = new List('table_terminal', options);


            var Ticket_message_editor;
            var edit_ticket_message_additional = $('#edit-ticket-message-additional');

            function edit_ticket_message(id, type) {
               edit_ticket_message_additional.empty();
               // type is either ticket or reply
               _ticket_message = $('[data-' + type + '-id="' + id + '"]').html();
               init_ticket_edit_editor();
               tinyMCE.activeEditor.setContent(_ticket_message);
               $('#ticket-message').modal('show');
               edit_ticket_message_additional.append(hidden_input('type', type));
               edit_ticket_message_additional.append(hidden_input('id', id));
               edit_ticket_message_additional.append(hidden_input('main_ticket', $('input[name="ticketid"]').val()));
            }

            function init_ticket_edit_editor() {
               if (typeof(Ticket_message_editor) !== 'undefined') {
                  return true;
               }
               Ticket_message_editor = init_editor('.tinymce-ticket-edit');
            }
            <?php if (has_permission('tasks', '', 'create')) { ?>

               function convert_ticket_to_task(id, type) {
                  if (type == 'ticket') {
                     _ticket_message = $('[data-ticket-id="' + id + '"]').html();
                  } else {
                     _ticket_message = $('[data-reply-id="' + id + '"]').html();
                  }
                  var new_task_url = admin_url + 'tasks/task?rel_id=<?php echo $ticket->ticketid; ?>&rel_type=ticket&ticket_to_task=true';
                  new_task(new_task_url);
               }
            <?php } ?>



            function SaveSpreadsheet() {
               var url = $("#ticket-SaveSpreadsheet-form").attr('action');
               var Data = $('#ticket-SaveSpreadsheet-form').serializeArray();
               var v = $('input[name="ticketid"]').val();
               var t = $('select[name="Customer_terminal"]').val();
               $.post(url, {
                  data: Data,
                  id: v,
                  terminal: t
               }).done(function(e) {
                  var response = JSON.parse(e);
                  if (response.status == 'success') {
                     alert_float(response.status, response.message);
                     window.location.reload();
                  } else {
                     alert_float(response.status, response.message);
                  }
               });
               return false;
            }
         </script>

         </body>

         </html>